/**
 * Created by PIV on 24.06.2016.
 */
module.exports = {
  application: {
    // daysHidden: 15,
    // daysReminder: 90,
    appName: 'example.com',
    appDeveloper: 'PIV.',
    appIdIOS: 123,
    appIdAndroid: 123
  },
};

module.exports.default = module.exports;

