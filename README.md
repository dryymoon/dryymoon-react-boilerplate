### Начало

Перед деплоем и разработкой нужно выполнить в корне проекта

```npm install```

#####For 32 bit system.

http://sharp.dimens.io/en/stable/install/

For Linux 32Bit: ```curl -s https://raw.githubusercontent.com/lovell/sharp/master/preinstall.sh | sudo bash -```

#####For Mac 
brew install graphicsmagick imagemagick --with-webp


Для ручной работы с переводами добавить строку в ```~/.bach_profile```     
```echo 'export PATH=${PATH}:/usr/local/opt/gettext/bin'```

##### For Linux

```sudo apt-get install libcairo2-dev libjpeg8-dev libpango1.0-dev libgif-dev build-essential g++ nasm```

##### For Windows
Unsupported, sorry. 
If you succesfully run development environment on window OS. Please write me igor@pilipenko.cc, how you can do this.

### Разработка

#### Быстрая верстка
```npm run dev-cli```
Порт 3000.
Умеет в рантайме апдейтить верстку и подули патчить, стили менять
иногда глючит, редко надо чистить кеш, папки: .tmp, .happypack

#### Собрать конкретный проект "YOURPROJECT" для прод
```npm run build --project=YOURPROJECT```

#### Серверный рендеринг в Прод, клиент в Дев, отлов ошибок несхождения рендера сервера и клиента
```npm run build-dev-ssr && npm run dev-prod```
В Браузере надо смотреть в консоли на парметр Server rendering is Equal to Client
Или другое сообщение при ошибке и кусок кода где она приозошла, дальше магия поиска и шаманства

#### Серверный рендеринг в Дев и клиент в Дев
```npm run build-dev-ssr && npm run dev-ssr```
Удобно дебажить ноду и рендер в ней.

#### Прод с сервингом статики
```npm run build && npm run standalone```
Порт 8080.

#### Прод без сервинга статики (c Nginx используется)
```npm run build && npm run start```
Порт 8000.

#### Другое
```npm run build-watch``` - сбилдить в прод режиме и следит за изменениями

```npm run build-front-only``` - сбилдить только статический фронт в прод режиме

```npm run dev-front-only``` - сбилдить только статический фронт в дев режиме

```npm run build-by-env``` - сбилдить по переменным env (используется при автоматических сборках как правило)

```npm run build-app``` - сбилдить в проде мобильное приложение или обновить его автоматом у всех

```npm run build-tv``` - сбилдить в проде ТВ приложение


### Как создать файлы переводов

```bash
cd ./translations/
msginit -i translations.pot -o ru.po -l ru
```
### Как ручками мержить файлы переводов

```bash
cd ./translations/
msgmerge ru.po translations.pot -U
```


### Стандарты

data-test={htmlElement}_{TypeOrAction}_{id}


### Проблема с Source Map-ом стилей


###### Суть
При сборке проэкта в сгенерированом файле (файлах) ```.css.map```,
встречаются ссылки на несуществующие сорсы.
Выглядит это примерно так:
```
webpack:///./src-sites/__propject__/body/<no source>
```
###### Причина
Наблюдается при включенных в сборщике плагинах __postcss-font-magician__ и/или __postcss-cssnext__.
Как выяснилось,```<no source>``` появляется тогда, когда в файлах стилей присутствует директива ```@import```.
То есть проблема в каскадности стилей (кто бы мог подумать).

###### Предпринималось
К вышеуказанным плагинам и __postcss/sass-loader__ в частности применялись различные комбинации опций,
что в итоге не дало никаких результатов. Махинации с ```resolve-url-loader``` также не помогли.

###### Возможные варианты решения
1. Подключить главный файл стилей (в нашем случае __theme.scss__) один раз.
https://github.com/webpack-contrib/sass-loader/issues/145 Например с помощью пакетов 
```sass-import-once``` или ```node-sass-importer```.
2. Создать алиас для главного файла стилей как ниже (ибо sass-loader магьот).
https://stackoverflow.com/questions/36926521/webpack-sass-loader-cannot-dedupe-common-scss-imports
https://github.com/webpack-contrib/sass-loader#usage
```
// webpack.config.js
   resolve: {
     alias: {
       styles: 'pathTo/theme.scss'
     }
   }
```
```
// some.scss
@import '~theme.scss'
```
3. Прописать в sass-loader includePaths
https://github.com/webpack-contrib/sass-loader#examples
```
{
    loader: "sass-loader",
    options: {
        includePaths: ["absolute/path/a", "absolute/path/b"]
    }
}
```
4. Отключить плагины __postcss-font-magician__/__postcss-cssnext__.
