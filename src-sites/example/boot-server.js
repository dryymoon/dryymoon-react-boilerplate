/**
 * Created by dryymoon on 18.04.2018.
 */
import ClientBearerToken from '../../src-core/api/middlewares/client-bearer-token';

const BearerTokenService = new ClientBearerToken();

export const apiConfig = {
  url: 'https://example.com',
  middlewares: [
    BearerTokenService.middleware(),
  ],
};

export const defaultClientMedia = { type: 'screen', width: '1280px' };

export default {};
