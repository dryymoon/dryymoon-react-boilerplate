/**
 * Created by dryymoon on 17.02.17.
 */

/* eslint-disable function-paren-newline, no-unused-vars, max-len */
import Default from './page-default';

// const pageLazy = () => import(/* webpackChunkName: "page-lazy" */'./page-lazy');

const routes = {
  path: '/',
  component: Default,
};

export default routes;

/*
{
          name: 'lazy',
          path: '/lazy/',
          getComponent(l, cb) {
            pageLazy().then(module => cb(null, module.default));
          }
 */
