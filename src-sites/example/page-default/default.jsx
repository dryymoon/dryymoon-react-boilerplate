/**
 * Created by PIV on 04.04.2017.
 */
import React, { Component } from 'react';
import List from 'components/list';
import { linkDecorator } from 'components/link';
import Helmet from 'react-helmet';
import ccn from 'ccn';
import bem from 'bem';

import styles from './default.scss';

@ccn
@bem
@linkDecorator
export default class Default extends Component {
  render() {
    return (
      <div data-block="defaultBlock" data-bemstyles={styles}>
        <Helmet>
          <html lang="ru" />
          <title>TEST PAGE</title>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
          <meta name="format-detection" content="telephone=no" />
        </Helmet>
        <List>
          <a>1</a>
          <a>2</a>
          <a>3</a>
          <a>4</a>
          <a>5</a>
          <a>6</a>
          <a>7</a>
        </List>
      </div>
    );
  }
}
