/**
 * Created by dryymoon on 18.04.2018.
 */
import ClientBearerToken from '../../src-core/api/middlewares/client-bearer-token';
import ravenApiMiddleware from '../../src-core/api/middlewares/raven-middleware';
import media from '!!sass-vars-to-js-loader!./media.scss'; // eslint-disable-line
import './theme.scss';
import './default.scss';
import routes from './routes';

const apiToken = __CLIENT__ && window.globalAgAccessToken;
const BearerTokenService = new ClientBearerToken(apiToken);

export const sentryConfig = {
  point: '',
  tags: {},
};

export const apiConfig = {
  url: '/api',
  middlewares: [
    ravenApiMiddleware,
    BearerTokenService.middleware(),
  ],
};

export { media, routes };
