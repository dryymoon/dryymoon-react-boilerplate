const fs = require('fs-extra');
const path = require('path');

module.exports = {};
module.exports.beforeConfigure = (config => ({ ...config, inlineSources: true, hashLength: 0 }));
module.exports.done = ({ rootDir, dstPath }) => {
  const copyPath = path.join(rootDir, '/webpack/dev-cli-www');
  fs.mkdirpSync(copyPath);
  fs.emptyDirSync(copyPath);
  fs.copySync(path.join(dstPath, '/www/index.html'), path.join(copyPath, './index.html'));
};
