/**
 * Created by dryymoon on 17.02.17.
 */

/* eslint-disable function-paren-newline, no-unused-vars, max-len */

import Default from './page-default';
import Settings from './page-settings';

// const pageDefault = () => import(/* webpackChunkName: "page-default" */'./page-default');

const routes = [
  {
    path: __DEV_CLI__ ? '/settings' : '/devcli',
    component: Settings,
  },
  {
    path: '*',
    component: Default,
  },
];

export default routes;

/* getComponent(l, cb) {
  pageDefault().then(module => cb(null, module.default));
}, */
