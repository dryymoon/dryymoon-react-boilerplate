/**
 * Created by PIV on 04.04.2017.
 */
import React, { Component } from 'react';
import Loader from 'react-loader-spinner';
import { linkDecorator } from 'components/link';
import FlexBox from 'components/flexbox';
import Button from 'components/button';
import popup from 'components/pop-up';
import Tooltip from 'components/tooltip';
import Helmet from 'react-helmet';
import bem from 'bem';
import io from 'socket.io-client';
import PageSettings from '../page-settings/page-settings';
import styles from './default.scss';
// import '../../unisender/routes';

const socket = io.connect(window.location.origin, { path: '/__devcli-runtime' });
socket.on('connect', () => console.log('CONNECTED'));

@bem
@linkDecorator
export default class Default extends Component {
  constructor() {
    super();
    this.setBuildProgress = this.setBuildProgress.bind(this);
  }

  state = {};

  componentDidMount() {
    socket.on('build-progress', this.setBuildProgress);
    this.progressDomNode = document.getElementById('progress');
    // socket.on('build-progress', ({ p§ercentage }) =>
    // console.log('message', (percentage * 100).toFixed(0)));
  }

  componentWillUnmount() {
    socket.off('build-progress', this.setBuildProgress);
  }

  setBuildProgress({ percentage = 0 }) {
    const percents = percentage * 100;
    let buildProgress;
    if (percents < 100) buildProgress = percents.toFixed(2);
    if (percents >= 100) buildProgress = percents.toFixed(1);
    this.progressDomNode.innerHTML = buildProgress;
    // this.setState({ buildProgress });
  }

  showPopup() {
    popup(PageSettings);
  }

  render() {
    const { buildProgress } = this.state;
    return (
      <FlexBox data-block="defaultBlock" data-bemstyles={styles} data-mods="fullScreen">
        <Helmet>
          <html lang="en" />
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
          <meta name="format-detection" content="telephone=no" />
        </Helmet>
        <Tooltip title="Click to configure">
          <Button onClick={this.showPopup}>
            <FlexBox data-elem="spinnerWrapper">
              <Loader type="TailSpin" color="#00BFFF" height="60" width="60" />
              <FlexBox data-elem="textInSpinnerWrapper">
                <div data-elem="textInSpinner" id="progress">{buildProgress}</div>
              </FlexBox>
            </FlexBox>
          </Button>
        </Tooltip>
      </FlexBox>
    );
  }
}
