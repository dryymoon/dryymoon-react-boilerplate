import React, { Component } from 'react';
import ccn from 'ccn';
import bem from 'bem';
import Tabs from 'components/react-nojs-tabs';
import styles from './tab-switcher.scss';

@ccn
@bem
export default class TabsSwitcher extends Component {
  render() {
    return (
      <Tabs {...this.props} data-block="tabsSwitcher" data-bemstyles={styles} />
    );
  }
}
