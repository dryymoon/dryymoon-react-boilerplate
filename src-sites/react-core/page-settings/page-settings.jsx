/* eslint-disable no-unused-vars */
import React, { Component } from 'react';

import FlexBox from 'components/flexbox';
import cookies from 'core/helpers/react/ReactCookiesProvider';
import Link from 'components/link';
import api from 'core/api/api-decorator';
import Button from 'components/button';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import isNaN from 'lodash/isNaN';
import bem from 'bem';
import { Input, CheckBox } from '../../spa/part-form/index';
import './page-settings.scss';

import io from 'socket.io-client';
import notification from '../../spa/part-notification';
import { t } from 'c-3po';

@api
@cookies
@bem
export default class PageSettings extends Component {
  static propTypes = {};

  constructor(props, ctx) {
    super(props, ctx);
    this.state = {
      availableProjects: {},
    };

    this.socket = io({ path: '/__devcli-runtime' });

    // broadcast
    this.socket.on('broadcast', ({ config }) => {
      this.setAvailableProjects(config);
    });
  }

  componentWillMount() {
    this.socket.emit('getConfig');
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !isEqual(nextProps, this.props) || !isEqual(nextState, this.state);
  }

  setAvailableProjects(availableProjects) {
    this.setState({ availableProjects });
  }

  setSitePort(projectName, port) {

    const config = {
      ...this.state.availableProjects,
      [projectName]: { ...this.state.availableProjects[projectName], port }
    };
    this.socket.emit('saveAndCheckPort', { projectName, port });
    // this.socket.emit('saveConfig', config);
    this.setState({
      availableProjects: config
    });
  }

  toggleBuildSite(projectName) {
    const { availableProjects } = this.state;
    const { enabled } = availableProjects[projectName];

    this.setState({
      availableProjects: {...availableProjects, [projectName]: {...availableProjects[projectName], $blocked: true}}
    });
    if (!enabled) {
      this.api.post(`/project/${projectName}/start`).then(() => {
        this.setState({
          availableProjects: {...availableProjects, [projectName]: {...availableProjects[projectName], $blocked: false}}
        });
      }).catch(err=> {
        this.setState({
          availableProjects: {...availableProjects, [projectName]: {...availableProjects[projectName], $blocked: false}}
    });
        });
    }
    if (enabled) {
      this.api.post(`/project/${projectName}/stop`).then(() => {
        this.setState({
          availableProjects: {...availableProjects, [projectName]: {...availableProjects[projectName], $blocked: false}}
        });
      }).catch(err=> {
        this.setState({
          availableProjects: {...availableProjects, [projectName]: {...availableProjects[projectName], $blocked: false}}
        });
        });
    }
  }

  handleRuntimeConstNameChange = (idx, projectName, keyOrValue, value) => {
    const {availableProjects} = this.state;
    const project = availableProjects[projectName];
    const {runtimeConstants} = project;
    const newRuntimeConstArray = [];
    runtimeConstants.map((runtimeConst, sidx) => {
      if (idx !== sidx) {
        newRuntimeConstArray.push(runtimeConst);
      } else if (idx === sidx) {
        newRuntimeConstArray.push({ ...runtimeConst, [keyOrValue]: value });
      }
    });
    const config = {
      ...availableProjects,
      [projectName]: { ...availableProjects[projectName], runtimeConstants: newRuntimeConstArray }
    };
    this.setState({
      availableProjects: config
    });
    this.socket.emit('saveRuntimeConstants', { projectName, newRuntimeConstArray });
  };

  handleAddRuntimeConst = projectName => () => {
    const project = this.state.availableProjects[projectName];
    const { runtimeConstants = [] } = project;
    runtimeConstants.push({ key: '', value: '' });
    const config = {
      ...this.state.availableProjects,
      [projectName]: {
        ...this.state.availableProjects[projectName],
        runtimeConstants
      }
    };
    this.setState({ availableProjects: config });
    this.socket.emit('saveRuntimeConstants', { projectName, newRuntimeConstArray: runtimeConstants });
    this.forceUpdate();
  };

  handleRemoveRuntimeConst = (idx, projectName) => () => {
    const {availableProjects} = this.state;
    const project = availableProjects[projectName];
    const newRuntimeConstArray = [];
    const {runtimeConstants} = project;
    runtimeConstants.map((value, constIdx) => {
      if (idx !== constIdx) {
        newRuntimeConstArray.push(value);
      }
    });
    const config = {
      ...this.state.availableProjects,
      [projectName]: {
        ...this.state.availableProjects[projectName],
        runtimeConstants: newRuntimeConstArray
      }
    };
    this.socket.emit('saveRuntimeConstants', { projectName, newRuntimeConstArray });
    this.setState({
      availableProjects: config
    });
  };

  render() {
    const { availableProjects } = this.state;
    const hostName = window.location.hostname;
    const protocol = window.location.protocol;
    return (
      <FlexBox data-block="pageSettings">
        <table data-elem="table" cellPadding="0" cellSpacing="0">
          <thead>
            <tr>
              <td>#</td>
              <td width="10%">Name</td>
              <td width="5%">Build</td>
              <td width="10%">Build Status</td>
              <td width="10%">Last ERR</td>
              <td width="15%">Port</td>
              <td width="15%">Port Status</td>
              <td width="5%">Pid</td>
              <td width="30%">Runtime Constants</td>
            </tr>
          </thead>
          <tbody>
            {!isEmpty(availableProjects) && Object.keys(availableProjects).map((projectName, index) => {
              let buildStatus = '';
              const project = availableProjects[projectName];
              const {
                $starting: starting,
                $started: started,
                port = '',
                enabled,
                $blocked,
                $buildStatus = {},
                $lastErr = '',
                $pid: pid = '',
                runtimeConstants = []
              } = project;
              let { $portStatus: portStatus = '' } = project;
              if ($buildStatus && $buildStatus.percentage && !isNaN(parseInt($buildStatus.percentage * 100))) {
                buildStatus = `${parseInt($buildStatus.percentage * 100)}%`;
              }
              if ($buildStatus && $buildStatus.error) buildStatus = $buildStatus.error;
              if (portStatus === 'open') portStatus = '👎 port is in use';
              if (portStatus === 'closed') portStatus = '👌 port is free';
              if (portStatus === 'openedByCurrentProject') portStatus = `🚀 port is used by ${projectName} project`;
              return (
                <tr key={projectName}>
                  <td>{index}</td>
                  <td>
                    {!starting && !started && projectName}
                    {(starting || started) &&
                    <Link
                      newWindow
                      blank
                      to={`${protocol}//${hostName}:${port}`}
                      data-elem="link">
                      {projectName}
                    </Link>
                    }
                  </td>
                  <td>
                    <div className="runningProjectsCheckBox">
                      <CheckBox
                        disabled={$blocked}
                        checked={enabled}
                        onChange={() => this.toggleBuildSite(projectName)}
                      />
                    </div>
                  </td>
                  <td>
                    <span>{buildStatus}</span>
                  </td>
                  <td>
                    <span>{$lastErr}</span>
                  </td>
                  <td>
                    <div className="runningProjectsCheckBox">
                      <Input
                        data-elem="portInput"
                        disabled={enabled}
                        value={port}
                        onChange={({ value }) => this.setSitePort(projectName, value)}
                      />
                    </div>
                  </td>
                  <td>
                    <span data-elem="portStatus">{portStatus}</span>
                  </td>
                  <td>
                    <span className="runningProjectPid">{pid}</span>
                  </td>
                  <td>
                    {runtimeConstants.map((runtimeConstant, idx) => (
                      <div className="runtimeConstString">
                        <Input
                          disabled={enabled}
                          placeholder="key"
                          value={runtimeConstant.key}
                          onChange={({ value }) => this.handleRuntimeConstNameChange(idx, projectName, 'key', value)}
                        />

                        <Input
                          disabled={enabled}
                          placeholder="value"
                          value={runtimeConstant.value}
                          onChange={({ value }) => this.handleRuntimeConstNameChange(idx, projectName, 'value', value)}
                        />
                        <Button
                          disabled={enabled}
                          type="button"
                          onClick={this.handleRemoveRuntimeConst(idx, projectName)}
                          className="small"
                        >-</Button>
                      </div>
                    ))}

                    <Button
                      disabled={enabled}
                      type="button"
                      onClick={this.handleAddRuntimeConst(projectName)}
                      className="small"
                    >+</Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </FlexBox>
    );
  }
}
