/**
 * Created by dryymoon on 18.04.2018.
 */
import media from '!!sass-vars-to-js-loader!./media.scss'; // eslint-disable-line
import './theme.scss';
import routes from './routes';


export const apiConfig = {
  url: '/api',
  middlewares: [],
};

export { media, routes };

export default {};
