/**
 * Created by PIV on 11.08.2016.
 */
// TODO  IDEA https://github.com/Browsersync/recipes/tree/master/recipes/webpack.react-hot-loader

/* eslint-disable */
const portscanner = require('portscanner');
const cluster = require('cluster');
const fs = require('fs');
const path = require('path');
const app = require('express')();
const proxy = require('express-http-proxy');
const lodash = require('lodash');
const appRootDir = require('app-root-dir').get();
const cachePath = path.join(appRootDir, '.tmp');
require('mkdirp').sync(cachePath);

const { rootDir, sitesRootDir, sitesNames } = require('./helpers/available-sites');
const clientBuildConfig = require('./main.client.config');
const socketStore = {};
let socketCounter = 0;

if (cluster.isMaster) {
  let config = {};
  let activePorts = [];
  const server = require('http').createServer(app);
  const masterPort = process.env.PORT || 3000;
  server.listen(masterPort);

  const io = require('socket.io')(server, { path: '/__devcli-runtime' });

  function exitProcess(reason) {
    if (reason) console.error(reason);
    lodash.mapValues(cluster.workers, worker => worker.kill());
    process.exit();
  }

  process.on('uncaughtException', exitProcess);
  // Prevents Zombie Processes
  require('on-exit')(exitProcess);

  app.use("/favicon.ico", require('express').static(__dirname + '/assets/react.ico'));

  app.post('/api/project/:projectName/start', (req, res) => {
    const { projectName } = req.params;
    //  Promise.resolve().then(() => {
    if (!config[projectName]) return; // Promise.reject(`config doesn't know ${projectName}
    // project`);
    if (config[projectName].$starting) return; // Promise.resolve();
    if (config[projectName].$started) return; // Promise.resolve();
    //  }).catch(error => {});
    config[projectName].enabled = true;
    saveConfig(config);
    startProject(projectName)
      .then(() => res.json({ result: { success: true }, success: true }))
      .catch(() => res.json({ result: { success: false }, success: false }));
  });

  app.post('/api/project/:projectName/stop', (req, res) => {
    const { projectName } = req.params;
    if (!config[projectName]) return Promise.reject(`config doesn't know ${projectName} project`);
    config[projectName].enabled = false;
    config[projectName].$buildStatus = { percentage: undefined };
    io.sockets.emit('broadcast', { config });
    saveConfig(config);
    Promise.resolve()
      .then(()=>killProject(projectName))
      .then(() => res.json({ result: { success: true }, success: true }))
      .catch(() => res.json({ result: { success: false }, success: false }))
  });

  app.post('/api/project/:projectName/restart', (req, res) => {
    const { projectName } = req.params;
    if (!config[projectName]) return Promise.reject(`config doesn't know ${projectName} project`);
    Promise.resolve()
      .then(() => killProject(projectName))
      .then(() => startProject(projectName))
      .then(() => res.json({ result: { success: true }, success: true }))
      .catch(() => res.json({ result: { success: false }, success: false }))
  });

  app.get('*', (req, res) => {
    const filePath = path.join(__dirname, './dev-cli-www/index.html');
    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.set('content-type', 'text/html');
      res.send(data);
      res.end();
    });
  });

  sitesNames.forEach(projectName => config[projectName] = { projectName });
  const configFilePath = path.join(__dirname, './.dev-cli-sites-config.json');

  if (fs.existsSync(configFilePath)) {
    let configFronJson;
    try {
      configFronJson = JSON.parse(fs.readFileSync(configFilePath));
      configFronJson = lodash.pick(configFronJson, sitesNames); // filter, maybe old config
      lodash.assign(config, configFronJson);
      config = validateConfig(config);
    } catch (e) {
      const jsonPath = path.dirname(configFilePath);
      const backupJsonFileName = './.backup-dev-cli-sites-config.json';
      const jsonBackupFile = path.join(jsonPath, backupJsonFileName);
      console.log('🔥 Error JSON not valid, path: ', configFilePath, `backup ${backupJsonFileName} created`);
      fs.createReadStream(configFilePath).pipe(fs.createWriteStream(jsonBackupFile));
    }
  }

  lodash.values(config)
    .filter(({ enabled }) => !!enabled)
    .forEach(({ projectName }) => {
      startProject(projectName).catch(error => {
      });
    });

  const broadcast = lodash.throttle(function () {
    io.sockets.emit('broadcast', { config });
  }, 500);

  function validateConfig(config) {
    let validatedConfig = config;
    lodash.keys(config).forEach(key => {
      if (!validatedConfig[key].projectName) validatedConfig[key].projectName = key;
    });
    return validatedConfig;
  }

  function checkPort(rawPort) {
    const port = +rawPort;
    if (!lodash.isInteger(port)) return Promise.reject(`checkPort Error: port '${rawPort}' is not integer`);
    if (port <= 0 || port >= 65536) return Promise.reject(`checkPort Error: port '${rawPort}'
    is out of renge 1-65536`);
    return portscanner.checkPortStatus(port)
      .then((status) => status === 'closed').catch(error => {
        return Promise.reject(`some other check port Error: ${error}`);
      });
  }

  function startProject(projectName) {
    let port = lodash.get(config, [projectName, 'port']);
    if (!port) {
      const usedPorts = lodash.values(lodash.mapValues(config, ({ port }) => port)).filter(it => it);
      if (usedPorts.length === 0) usedPorts.push(masterPort);
      port = Math.max(...usedPorts) + 1;
      config[projectName].port = port;
    }
    if (activePorts.indexOf(port) > -1) {
      config[projectName].$portStatus = 'open';
      config[projectName].enabled = false;
      return Promise.reject('port is not free - cant start');
    }
    activePorts.push(port);

    console.log(`🚀 project starting: ${projectName}`);
    config[projectName].$starting = true;
    config[projectName].$buildStatus = { percentage: 0 };

    return checkPort(port).then(portStatus => {
      if (!portStatus) {
        config[projectName].$starting = false;
        config[projectName].$buildStatus = undefined;
        config[projectName].enabled = false;
        config[projectName].$portStatus = 'open';
        io.sockets.emit('broadcast', { config });
        return Promise.reject('port is not free - cant start');
      }
      config[projectName].$portStatus = 'openedByCurrentProject';

      const { runtimeConstants = [] } = config[projectName];
      const worker = cluster.fork({
        port,
        projectName,
        runtimeConstants: JSON.stringify(runtimeConstants)
      });
      config[projectName].$pid = worker.process.pid;
      config[projectName].$starting = false;
      config[projectName].$started = true;

      io.sockets.emit('broadcast', { config });

      worker.on('exit', (code, signal) => {
        config[projectName].$pid = undefined;
        config[projectName].$started = false;
        console.log(`💀 project stopped: ${projectName}`);
        if (!config[projectName].enabled) {
          checkPort(config[projectName].port).then(portStatus => {
            if (portStatus) config[projectName].$portStatus = 'closed';
            if (!portStatus) config[projectName].$portStatus = 'open';
            io.sockets.emit('broadcast', { config });
          });
        }
        if (config[projectName].enabled) startProject(projectName);
      });

      worker.on('message', function (msg) {
        if (!msg || !lodash.isObject(msg)) return;
        const { percentage, error } = msg;
        if (error) {
          killProject(projectName);
          config[projectName].enabled = false;
          config[projectName].$buildStatus = { error: '🚧 Stopped after error' };
          config[projectName].$lastErr = JSON.stringify(error);
          saveConfig(config);
        }
        if (percentage) {
          config[projectName].$buildStatus = { percentage: percentage };
        }
        broadcast();
      });
    }).catch(err => {
      console.log('🖕 checkPort catch error', err);
    });
  }

  function filterConfig(configAllFields) {
    if (!lodash.isObject(configAllFields)) return {};
    let filteredConfig = {};
    lodash.keys(configAllFields).forEach(key => {
      lodash.assign(filteredConfig, { [key]: lodash.omitBy(configAllFields[key], (value, key) => key[0] === '$') });
    });
    return filteredConfig;
  }

  function saveConfig(configAllFields) {
    const filteredConfig = filterConfig(lodash.pick(configAllFields, sitesNames));
    fs.writeFileSync(configFilePath, JSON.stringify(filteredConfig, null, 4));
  }

  function killProject(projectName) {
    if (!config[projectName].$starting && !config[projectName].$started) return;
    const pid = lodash.get(config, [projectName, '$pid']);
    const port = lodash.get(config, [projectName, 'port']);
    if (pid) {
      const indexOfPort = activePorts.indexOf(port);
      if (indexOfPort !== -1) activePorts.splice(indexOfPort, 1);
      process.kill(pid, 'SIGKILL');
      config[projectName].$starting = false;
      config[projectName].$started = false;
      config[projectName].$pid = undefined;
      config[projectName].$buildStatus = { percentage: undefined };
    }
  }

  io.on('connection', (socket) => {
    const currSocketId = socketCounter++;
    socketStore[currSocketId] = socket;

    socket.on('saveRuntimeConstants', ({ projectName, newRuntimeConstArray }) => {
      if (!config[projectName]) return;
      config[projectName].runtimeConstants = newRuntimeConstArray;
      saveConfig(config);
    });

    socket.on('saveAndCheckPort', ({ projectName, port }) => {
      if (!config[projectName]) return;
      if (!lodash.isNaN(+port)) {
        config[projectName].port = +port
      }
      else config[projectName].port = port;
      saveConfig(config);
      checkPort(port).then(portStatus => {
          if (portStatus) config[projectName].$portStatus = 'closed';
          if (!portStatus) config[projectName].$portStatus = 'open';
          io.sockets.emit('broadcast', { config });
        }
      ).catch((err) => {
        config[projectName].$portStatus = err;
        io.sockets.emit('broadcast', { config });
      });
    });

    socket.on('getConfig', () => {
      io.sockets.emit('broadcast', { config });
    });
  });

}

let runtime = {
  emitter: new (require('events'))(),
  siteName: null,
  wpConfig: null,
  wpCompiler: null,
  wpDevMware: null,
  wpHotMware: null,
  wpBuildProgress: 0,
};

const getWpDevServerOptions = (webpackConfig) => ({
  quiet: true, // Due to friendly-errors-webpack-plugin
  noInfo: true,
  hot: true,
  inline: true,
  lazy: false,
  publicPath: webpackConfig.output.publicPath,
  headers: { 'Access-Control-Allow-Origin': '*' },
  stats: "errors-only",
});

if (cluster.isWorker) {
  const projectPort = cluster.worker.process.env.port || null;
  const projectName = cluster.worker.process.env.projectName || null;
  const runtimeConstants = JSON.parse(cluster.worker.process.env.runtimeConstants);
  global.__OBFUSCATE_STYLES__ = false;

  process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = "0"; // allow HTTPS request with invalid certificates

  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  /* PROXY */
  const origConsole = console.info;
  console.info = () => null;

  const { devCliHttpProxyMiddleware } = getBuildCustomHooks(projectName);

  if (devCliHttpProxyMiddleware) devCliHttpProxyMiddleware(app);

  console.info = origConsole;
  /* END PROXY */

  app.use("/favicon.ico", require('express').static(__dirname + '/assets/react.ico'));

  buildRuntime(projectName, runtimeConstants, projectPort);
  const filePath = path.join(__dirname, './dev-cli-www/index.html');

  app.use('/assets/', require('express').static(path.join(__dirname, './dev-cli-www/assets/')));

  app.get('*', (req, res, next) => {
    if (runtime.wpBuildProgress >= 1) return next();
    if (!req.accepts('html')) return next();
    const filePath = path.join(__dirname, './helpers/pre-load2.html');
    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.set('content-type', 'text/html');
      res.send(data);
      res.end();
    });
  });

  // app.use('/api', require('http-proxy-middleware')(
  //   (pathname, req) => {
  //     if (!req.activeSite) return;
  //     // const clientApiUrl = apiClientConfig.url; // get(req, 'vhost.client.api.url');
  //     // const serverApiUrl = apiServerConfig.url; // get(req, 'vhost.server.api.url');
  //     // return (clientApiUrl && serverApiUrl && req.path.startsWith(clientApiUrl));
  //   },
  //   {
  //     target: 'http://www.example.org',
  //     router: (req) => {
  //     },
  //     changeOrigin: true,
  //     secure: false,
  //     pathRewrite: (path, req) => path.replace('^/api', '')
  //   }
  // ));

// Подпорка для api Proxy что при неправильном запросе оно не вылетало.
  app.use('/', (req, res, next) => res.finished ? res.end() : next());

  app.use((req, res, next) => runtime.wpDevMware ? runtime.wpDevMware(req, res, next) : next());
  app.use((req, res, next) => runtime.wpHotMware ? runtime.wpHotMware(req, res, next) : next());

  app.use('/', (req, res, next) => {
    console.log('GET COMPILED HTML');
    const { siteName, wpConfig, wpCompiler } = runtime;
    if (!siteName) return next();
    const filename = path.join(wpConfig.output.path, '../index.html');
    wpCompiler.outputFileSystem.readFile(filename, (err, result) => {
      if (err) return next();
      res.set('content-type', 'text/html');
      res.send(result);
      res.end();
    });
  });

  const server = require('http').createServer(app);
  const io = require('socket.io')(server, { path: '/__devcli-runtime' });

  io.on('connection', (socket) => {
    const currSocketId = socketCounter++;
    socketStore[currSocketId] = socket;
    socket.on('build-runtime', buildRuntime);
    socket.on('disconnect', () => delete socketStore[currSocketId]);
    socket.emit('build-progress', { percentage: runtime.wpBuildProgress });
  });

  server.listen(projectPort, () => console.log('app listening on port', projectPort));

}

function runHook(hookName) {
  return config => {
    const hook = config.hooks[hookName];
    return Promise.resolve(hook ? hook(config) : config).then(newConfig => ({ ...config, ...newConfig }));
  }
}

function emit(channel, data) {
  Object.keys(socketStore).forEach(key => socketStore[key].emit(channel, data));
}

function getBuildCustomHooks(siteName) {
  const buildCustomHooksFilePath = getBuildCustomHooksFilePath(siteName);
  let buildCustomHooks;
  if (fs.existsSync(buildCustomHooksFilePath)) {
    buildCustomHooks = require(buildCustomHooksFilePath);
  }
  return buildCustomHooks || {};
}

function getBuildCustomHooksFilePath(siteName) {
  return path.join(sitesRootDir, siteName, "./build.js");
}

function buildRuntime(siteName, runtimeConstants, projectPort) {
  if (!sitesNames || sitesNames.indexOf(siteName) === -1) return;
  if (!siteName) return;
  if (siteName === runtime.siteName) return;
  if (runtime.wpDevMware) runtime.wpDevMware.close();
  if (runtime.buildCustomHooksFileWatcher) {
    runtime.buildCustomHooksFileWatcher.close();
    runtime.buildCustomHooksFileWatcher = undefined;
  }
  const buildCustomHooksFilePath = getBuildCustomHooksFilePath(siteName);

  if (fs.existsSync(buildCustomHooksFilePath)) {
    runtime.buildCustomHooksFileWatcher = fs.watch(buildCustomHooksFilePath);
    runtime.buildCustomHooksFileWatcher.on('change', (eventName, file) => {
      console.log(`PROCESS RESTART DIE TO ${eventName} of file ${file}`);
      delete require.cache[require.resolve(buildCustomHooksFilePath)];
      // process.send({ siteName, action: 'restart', port: projectPort });
      process.exit();
    });
  }
  const eslintGlobals = [];
  lodash.mapValues(runtimeConstants, (v, k) => v !== undefined && eslintGlobals.push(k));

  return Promise
    .resolve({
      app,
      projectPort,
      siteName,
      rootDomPoint: require('lodash/camelCase')(`react-root-${siteName}`),
      hooks: getBuildCustomHooks(siteName),
      rootDir,
      sitesRootDir,
      // srcHash: sitesHashs[siteName],
      // srcPath: path.join(sitesRootDir, siteName),
      // dstPath: path.join(rootDir, '/build', siteName),
      devcli: true,
      eslintGlobals
    })
    .then(runHook('beforeConfigure'))
    .then(config => ({
      ...config,
      clientConfig: clientBuildConfig(config),
      projectPort: projectPort
    }))
    .then(runHook('afterClientConfigure'))
    .then(runHook('beforeClientBuild'))
    .then((config) => {
      const wpConfig = config.clientConfig;
      runtimeConstants.map((it) => {
        if (it.key && it.value) {
          wpConfig.plugins.push(new (require('webpack').DefinePlugin)({
            [it.key]: JSON.stringify(it.value)
          }));
        }
      });
      // TODO runtimeConstants must be an {}  ???
      // wpConfig.plugins.push(new (require('webpack').DefinePlugin)(
      //   lodash.mapValues(runtimeConstants, v => JSON.stringify(v))
      // ));

      const wpCompiler = require('webpack')(wpConfig);
      wpCompiler.apply(new (require('webpack/lib/ProgressPlugin'))((percentage, msg) => {
        runtime.wpBuildProgress = percentage;
        process.send({ siteName, percentage, msg });
        emit('build-progress', { percentage, msg });
      }));
      const wpDevMware = require('webpack-dev-middleware')(wpCompiler, getWpDevServerOptions(wpConfig));
      wpDevMware.waitUntilValid(() => runtime.emitter.emit('build-done'));
      const wpHotMware = require('webpack-hot-middleware')(wpCompiler);
      Object.assign(runtime, { siteName, wpConfig, wpCompiler, wpDevMware, wpHotMware });
    }).catch(error => {
      process.send({ siteName, error });
    });
}