/**
 * Created by PIV on 11.08.2016.
 */
// TODO  IDEA https://github.com/Browsersync/recipes/tree/master/recipes/webpack.react-hot-loader

/* eslint-disable */
// "use strict";

const portscanner = require('portscanner');
const cluster = require('cluster');
const fs = require('fs');
const path = require('path');
const app = require('express')();
const proxy = require('express-http-proxy');
const lodash = require('lodash');
const appRootDir = require('app-root-dir').get();
const cachePath = path.join(appRootDir, '.tmp');
require('mkdirp').sync(cachePath);
const { rootDir, sitesRootDir, sitesNames } = require('./helpers/available-sites');
const clientBuildConfig = require('./main.client.config');
const sockets = {};
let isProjectRestarting = false;
let runtime = {
  emitter: new (require('events'))(),
  siteName: null,
  wpConfig: null,
  wpCompiler: null,
  wpDevMware: null,
  wpHotMware: null,
  wpBuildProgress: 0,
};

const getWpDevServerOptions = (webpackConfig) => ({
  quiet: true, // Due to friendly-errors-webpack-plugin
  // quiet: false,
  noInfo: true,
  hot: true,
  inline: true,
  lazy: false,
  publicPath: webpackConfig.output.publicPath,
  headers: { 'Access-Control-Allow-Origin': '*' },
  stats: "errors-only",
  /* watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  } */
});

if (cluster.isMaster) {

  function exitProcess(reason) {
    if (reason) console.error(reason);
    lodash.mapValues(cluster.workers, worker => worker.kill());
    process.exit();
  }

  process.on('uncaughtException', exitProcess);
  // Prevents Zombie Processes
  require('on-exit')(exitProcess);

  // WEB SERVER

  app.use("/favicon.ico", require('express').static(__dirname + '/assets/react.ico'));

  app.get('*', (req, res) => {
    const filePath = path.join(__dirname, './dev-cli-www/index.html');
    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.set('content-type', 'text/html');
      res.send(data);
      res.end();
    });
  });

  // app.use('/assets/', require('express').static(path.join(__dirname, './dev-cli-www/assets/')));

  const server = require('http').createServer(app);
  server.listen(3000);
  const io = require('socket.io')(server, { path: '/__devcli-runtime' });

  // END WEB SERVER

  let config = {};

  sitesNames.forEach(projectName => config[projectName] = { projectName });

  let activeProjects = {};
  let projectsPid = {};
  let projectsCheckedPorts = {};

  const configFilePath = path.join(__dirname, './.dev-cli-sites-config.json');

  if (fs.existsSync(configFilePath)) {
    let configFronJson = JSON.parse(fs.readFileSync(configFilePath));
    configFronJson = lodash.pick(configFronJson, sitesNames); // filter, maybe old config
    lodash.assign(config, configFronJson);
  }

  lodash.values(config)
    .filter(({ enabled }) => !!enabled)
    .forEach(({ projectName }) => startProject(projectName));

  async function startProject(projectName) {
    if (config[projectName].$starting) return;
    if (config[projectName].$started) return;

    console.log(`🦄 project starting: ${projectName}`);

    config[projectName].$starting = true;

    let port = lodash.get(config, [projectName, 'port']);
    if (!port) {
      const usedPorts = values(lodash.mapValues(config, ({ port }) => port)).filter(it => it);
      port = Math.max(...usedPorts) + 1;
    }

    const portStatus = await portscanner.checkPortStatus(port);
    if (portStatus !== 'closed') {
      config[projectName].$starting = false;
      return Promise.reject();
    }

    config[projectName].port = port;

    const worker = cluster.fork({ port, projectName, });
    // TODO add runtimeConstants: JSON.stringify(runtimeConstants || [])

    config[projectName].$pid = worker.process.pid;
    config[projectName].$starting = false;
    config[projectName].$started = true;

    worker.on('exit', (code, signal) => {
      config[projectName].$started = false;
      console.log(`🦄 project stopped: ${projectName}`);
      if (config[projectName].enabled) startProject(projectName);
    });

    worker.on('message', function (msg) {
      if (!msg || !lodash.isObject(msg)) return;
      const { type, payload } = msg;
      if (type === 'progress') config[projectName].$progress = payload;
    });

    // TODO Change this broadcast
    io.sockets.emit('broadcast', { config });
  }

  let socketId = 0;

  function startProject1(config, projectName, io, availableProjects) {

    console.log('🦄 start building ', projectName);

    let socketConnected;
    io.on('connection', function (socket) {
      socketConnected = socket;
    });

    const { port, runtimeConstants } = config.runningProjects[projectName];

    activeProjects = {
      ...activeProjects, [projectName]: {
        port: port,
        progress: `checking port ${port}, 0`,
        pid: ''
      }
    };

    socketConnected && socketConnected.emit('returnRunningProjects', activeProjects);

    portscanner.checkPortStatus(port)
      .then(status => {
        if (status === 'closed') {

          const projects = availableProjects || sitesConfigFromJson;
          projects[projectName].isRunning = true;
          fs.writeFileSync(configFilePath, JSON.stringify(projects)); // save config

          projectsCheckedPorts[projectName] = {
            port: port,
            canUsePort: false,
            portStatus: `${projectName} is running on ${port} port`
          };
          socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);

          const worker = cluster.fork({
            port: port, projectName: projectName,
            runtimeConstants: JSON.stringify(runtimeConstants || [])
          });

          const pid = worker.process.pid;
          projectsPid[pid] = projectName;

          activeProjects = {
            ...activeProjects, [projectName]: {
              port: port,
              progress: 'starting, 0',
              pid: worker.process.pid
            }
          };

          worker.on('message', function (msg) {
            if (msg && msg.action === 'restart') {
              isProjectRestarting = true;
              const projectName = projectsPid[pid];
              const port = activeProjects[projectName].port;
              projectsCheckedPorts[projectName] = { portStatus: 'you can use this port' };
              delete activeProjects[projectName];
              socketConnected && socketConnected.emit('returnRunningProjects', activeProjects);
              socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
              console.log('need to restart');
              worker.disconnect();
              worker.process.kill();

              config.runningProjects = {
                ...activeProjects, [projectName]: {
                  port: port, progress: 'starting, 0', pid: pid
                }
              };
              startProject(config, projectName, socketConnected);
            }

            if (msg && msg.action === 'showProgress') {
              const percentage = parseInt(msg.percentage * 100);
              if (!activeProjects[msg.siteName]) {
                activeProjects[msg.siteName] = { progress: 'starting, 0', pid: '' }
              }
              if (activeProjects[msg.siteName].progress !== percentage) {
                activeProjects[msg.siteName].progress = percentage;
                socketConnected && socketConnected.emit('returnRunningProjects', activeProjects);
              }
            }
          });

          worker.on('exit', function () {
              if (isProjectRestarting) {
                isProjectRestarting = false;
                return;
              }

              portscanner.checkPortStatus(port)
                .then(status => {
                  if (status === 'closed') {
                    projectsCheckedPorts[projectName] = { portStatus: 'you can use this port' };
                    socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
                  }
                  if (status === 'open') {
                    projectsCheckedPorts[projectName] = { portStatus: 'port is in use' };
                    socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
                  }
                })
                .catch(err => {
                  console.error('portScanner cant check port', err.message);
                  projectsCheckedPorts[projectName] = {
                    port: port,
                    canUsePort: false,
                    portStatus: err.message
                  };
                  socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
                });
            }
          );

        }
        if (status === 'open') {
          projectsCheckedPorts[projectName] = {
            port: port,
            canUsePort: false,
            portStatus: `${port} port is in use`
          };
          socketConnected && socketConnected.emit('returnProjectsCheckedPorts', projectsCheckedPorts);

          delete activeProjects[projectName];
          socketConnected && socketConnected.emit('returnRunningProjects', activeProjects);
        }
      })
      .catch(err => {
        console.error(err)
      });
  }

  function killProject(config, projectName, socket, availableProjects) {
    const { pid } = activeProjects[projectName];
    const projectToKill = activeProjects[projectName];
    if (projectToKill && pid) process.kill(pid, 'SIGKILL');
    delete activeProjects[projectName];
    socket.emit('returnRunningProjects', activeProjects);

    if (availableProjects) {
      availableProjects[projectName].isRunning = false;
      fs.writeFileSync(configFilePath, JSON.stringify(availableProjects));
    }
  }

  // start projects at start (if it was specified in config by IsRunning)
  const arrayOfSiteNames = Object.keys(sitesConfigFromJson);
  for (let l = 0; l < arrayOfSiteNames.length; l += 1) {
    const projectName = arrayOfSiteNames[l];
    if (sitesConfigFromJson[projectName].isRunning) {
      activeProjects = {
        ...activeProjects,
        [projectName]: sitesConfigFromJson[projectName]
      };
      startProject({ runningProjects: activeProjects }, projectName, io);
    }
  }

  io.on('connection', (socket) => {

    const currSocketId = socketId++;
    sockets[currSocketId] = socket;

    socket.on('buildProject', ({ config, projectName, availableProjects }) => {
      startProject(config, projectName, socket, availableProjects);
    });

    socket.on('killProject', ({ config, projectName, availableProjects }) => {
      killProject(config, projectName, socket, availableProjects);
    });

    socket.on('checkPort', ({ projectName, port }) => {
      portscanner.checkPortStatus(port)
        .then(status => {
          if (status === 'closed') {
            projectsCheckedPorts[projectName] = {
              port: port, canUsePort: true, portStatus: 'you' +
                ' can use this port'
            };
          }
          if (status === 'open') {
            projectsCheckedPorts[projectName] = {
              port: port,
              canUsePort: false,
              portStatus: 'port is in use'
            };
          }
          socket.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
        })
        .catch(err => {
          console.error('portScanner cant check port', err.message);
          projectsCheckedPorts[projectName] = {
            port: port,
            canUsePort: false,
            portStatus: err.message
          };
          socket.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
        });
    });

    socket.on('getProjectsCheckedPorts', () => {
      socket.emit('returnProjectsCheckedPorts', projectsCheckedPorts);
    });

    socket.on('getAvailableProjects', () => {
      socket.emit('returnAvailableProjects', JSON.parse(fs.readFileSync(configFilePath)));
    });
    socket.on('saveConfig', (config) => {
      fs.writeFileSync(configFilePath, JSON.stringify(config));
    });
    socket.on('getRunningProjects', () => {
      socket.emit('returnRunningProjects', activeProjects);
    });
  });

}

if (cluster.isWorker) {
  const projectPort = cluster.worker.process.env.port || null;
  const projectName = cluster.worker.process.env.projectName || null;
  const runtimeConstants = cluster.worker.process.env.runtimeConstants;
  global.__OBFUSCATE_STYLES__ = false;

  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // allow HTTPS request with invalid certificates

  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

  app.use(require('cookie-parser')());

  app.use("/favicon.ico", require('express').static(__dirname + '/assets/react.ico'));

  // const activeSite = projectName;
  const buildCustomHooksFilePath = path.join(sitesRootDir, projectName, "./build.js");
  buildRuntime(projectName, runtimeConstants, buildCustomHooksFilePath, projectPort);
  const filePath = path.join(__dirname, './dev-cli-www/index.html');

  if (fs.existsSync(buildCustomHooksFilePath)) {
    const buildCustomHooksFile = require(buildCustomHooksFilePath);
    const proxyConfig = buildCustomHooksFile.proxyConfig || undefined;
    proxyConfig && proxyConfig(app, filePath);
  }

  app.use('/assets/', require('express').static(path.join(__dirname, './dev-cli-www/assets/')));

  app.get('*', (req, res, next) => {
    if (runtime.wpBuildProgress >= 1) return next();
    if (!req.accepts('html')) return next();
    const filePath = path.join(__dirname, './helpers/pre-load2.html');
    fs.readFile(filePath, (err, data) => {
      if (err) throw err;
      res.set('content-type', 'text/html');
      res.send(data);
      res.end();
    });
  });

  const origConsole = console.info;
  console.info = () => null;
  // app.use('/api', require('http-proxy-middleware')(
  //   (pathname, req) => {
  //     if (!req.activeSite) return;
  //     // const clientApiUrl = apiClientConfig.url; // get(req, 'vhost.client.api.url');
  //     // const serverApiUrl = apiServerConfig.url; // get(req, 'vhost.server.api.url');
  //     // return (clientApiUrl && serverApiUrl && req.path.startsWith(clientApiUrl));
  //   },
  //   {
  //     target: 'http://www.example.org',
  //     router: (req) => {
  //     },
  //     changeOrigin: true,
  //     secure: false,
  //     pathRewrite: (path, req) => path.replace('^/api', '')
  //   }
  // ));
  console.info = origConsole;

// Подпорка для api Proxy что при неправильном запросе оно не вылетало.
  app.use('/', (req, res, next) => res.finished ? res.end() : next());

  app.use((req, res, next) => runtime.wpDevMware ? runtime.wpDevMware(req, res, next) : next());
  app.use((req, res, next) => runtime.wpHotMware ? runtime.wpHotMware(req, res, next) : next());

  app.use('/', (req, res, next) => {
    console.log('GET COMPILED HTML');
    const { siteName, wpConfig, wpCompiler } = runtime;
    if (!siteName) return next();
    const filename = path.join(wpConfig.output.path, '../index.html');
    wpCompiler.outputFileSystem.readFile(filename, (err, result) => {
      if (err) return next();
      res.set('content-type', 'text/html');
      res.send(result);
      res.end();
    });
  });

  const server = require('http').createServer(app);
  const io = require('socket.io')(server, { path: '/__devcli-runtime' });

  let socketId = 0;

  io.on('connection', (socket) => {
    const currSocketId = socketId++;
    sockets[currSocketId] = socket;
    socket.on('build-runtime', buildRuntime);
    socket.on('disconnect', () => delete sockets[currSocketId]);
    socket.emit('build-progress', { percentage: runtime.wpBuildProgress });
  });

  server.listen(projectPort, () => console.log('app listening on port', projectPort));

}

function runHook(hookName) {
  return config => {
    const hook = config.hooks[hookName];
    return Promise.resolve(hook ? hook(config) : config).then(newConfig => ({ ...config, ...newConfig }));
  }
}

function emit(channel, data) {
  Object.keys(sockets).forEach(key => sockets[key].emit(channel, data));
}

function buildRuntime(siteName, runtimeConstants, buildCustomHooksFilePath, projectPort) {
  if (!sitesNames || sitesNames.indexOf(siteName) === -1) return;
  if (!siteName) return;
  if (siteName === runtime.siteName) return;
  if (runtime.wpDevMware) runtime.wpDevMware.close();
  if (runtime.buildCustomHooksFileWatcher) {
    runtime.buildCustomHooksFileWatcher.close();
    runtime.buildCustomHooksFileWatcher = undefined;
  }

  let buildCustomHooks;
  if (fs.existsSync(buildCustomHooksFilePath)) {
    buildCustomHooks = require(buildCustomHooksFilePath);
    runtime.buildCustomHooksFileWatcher = fs.watch(buildCustomHooksFilePath);
    runtime.buildCustomHooksFileWatcher.on('change', (eventName, file) => {
      console.log(`PROCESS RESTART DIE TO ${eventName} of file ${file}`);
      process.send({ siteName, action: 'restart', port: projectPort });
    });
  }
  const eslintGlobals = [];
  const constantsFromRuntime = JSON.parse(runtimeConstants);
  constantsFromRuntime.map((it) => {
    if (it.key && it.value) {
      eslintGlobals.push(it.key);
    }
  });

  return Promise
    .resolve({
      app,
      projectPort,
      siteName,
      rootDomPoint: require('lodash/camelCase')(`react-root-${siteName}`),
      hooks: buildCustomHooks || {},
      rootDir,
      sitesRootDir,
      // srcHash: sitesHashs[siteName],
      // srcPath: path.join(sitesRootDir, siteName),
      // dstPath: path.join(rootDir, '/build', siteName),
      devcli: true,
      eslintGlobals: eslintGlobals
    })
    .then(runHook('beforeConfigure'))
    .then(config => ({
      ...config,
      clientConfig: clientBuildConfig(config),
      projectPort: projectPort
    }))
    .then(runHook('afterClientConfigure'))
    .then(runHook('beforeClientBuild'))
    .then((config) => {
      const wpConfig = config.clientConfig;
      constantsFromRuntime.map((it) => {
        if (it.key && it.value) {
          wpConfig.plugins.push(new (require('webpack').DefinePlugin)({
            [it.key]: JSON.stringify(it.value)
          }));
        }
      });
      const wpCompiler = require('webpack')(wpConfig);
      wpCompiler.apply(new (require('webpack/lib/ProgressPlugin'))((percentage, msg) => {
        runtime.wpBuildProgress = percentage;
        process.send({ siteName, action: 'showProgress', percentage, msg });
        emit('build-progress', { percentage, msg });
      }));
      const wpDevMware = require('webpack-dev-middleware')(wpCompiler, getWpDevServerOptions(wpConfig));
      wpDevMware.waitUntilValid(() => runtime.emitter.emit('build-done'));
      const wpHotMware = require('webpack-hot-middleware')(wpCompiler);
      Object.assign(runtime, { siteName, wpConfig, wpCompiler, wpDevMware, wpHotMware });
    })
}