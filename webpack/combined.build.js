/**
 * Created by dryymoon on 15.04.2018.
 */
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const isFunction = require('lodash/isFunction');
const { hashElement } = require('folder-hash');
const md5 = require('md5');
const colors = require('colors');
const promiseSequence = require('promise-sequence');

const clientBuildConfig = require('./main.client.config');
const serverBuildConfig = require('./main.server.config');

let { rootDir, sitesRootDir, sitesNames } = require('./helpers/available-sites');

const errors = [];

colors.setTheme({
  noProjectToBuild: ['yellow', 'bold', 'bgBlack'],
  projectToBuild: ['green', 'bold'],
  projectNotToBuild: ['yellow', 'bold']
});

const projectToBuildFromTerminalArg = process.env['npm_config_project'];

Promise.all([
  hashElement(sitesRootDir,
    {
      folders: {
        exclude: ['**/.*', '**/node_modules', '**/_*'],
        matchPath: true,
        matchBasename: false,
      }
    }),
  hashElement(path.join(rootDir, 'src-core'),
    { folders: { exclude: ['**/.*', '**/_*'], matchPath: true, matchBasename: false, } }),
  hashElement(path.join(rootDir, 'webpack'),
    {
      folders: {
        exclude: ['**/.*', 'dev-cli-www/*', '**/_*'],
        matchPath: true,
        matchBasename: false,
      },
      files: { // TODO need exclude only pfolders
        exclude: ['index.html'],
      },
    })
])
  .then(([{ children }, { hash: coreHash }, { hash: webpackHash }]) => {
    const sitesHashs = {};
    const sitesCoreHash = {};
    const sitesWebpackHash = {};
    // children.forEach(({ name, hash }) => console.log(name, hash));
    children.forEach(({ name, hash }) => {
      sitesHashs[name] = md5(hash);
      sitesCoreHash[name] = md5(coreHash);
      sitesWebpackHash[name] = webpackHash;
    }
    );
    if (projectToBuildFromTerminalArg && sitesNames.indexOf(projectToBuildFromTerminalArg) === -1 ) {
      console.log(`where is no project with name: ${projectToBuildFromTerminalArg}`.noProjectToBuild);
    }
    const sitesNamesForBuild = sitesNames.filter(siteName => {
      if (projectToBuildFromTerminalArg && siteName !== projectToBuildFromTerminalArg) return false;
      if (!fs.existsSync(path.join(sitesRootDir, siteName, './package.json'))) return false;
      const packageJsonPath = path.join(rootDir, 'build', siteName, './package.json');
      let reasonsToRebuild = [];
      let needToRebuild = false;
      if (!fs.existsSync(packageJsonPath)) {
        reasonsToRebuild.push('packageJsonPath does not exist');
        needToRebuild = true;
      } else {
        if (projectToBuildFromTerminalArg && siteName === projectToBuildFromTerminalArg){
          needToRebuild = true;
          reasonsToRebuild.push('project was specified in terminal command');
        } else {
          const siteJson = require(packageJsonPath);
          if (siteJson.srcHash !== sitesHashs[siteName]) {
            needToRebuild = true;
            reasonsToRebuild.push('srcHash changed');
          }
          if (siteJson.coreHash !== sitesCoreHash[siteName]) {
            needToRebuild = true;
            reasonsToRebuild.push('coreHash changed');
          }
          if (siteJson.webpackHash !== sitesWebpackHash[siteName]) {
            needToRebuild = true;
            reasonsToRebuild.push('webpackHash changed');
          }
        }
      }

      if (needToRebuild) {
        console.log(`site name: ${siteName} - need to rebuild, REASONS: ${reasonsToRebuild.join(", ")}`.projectToBuild);
      } else {
        console.log(`site name: ${siteName} - don't need to rebuild`.projectNotToBuild);
      }
      return needToRebuild
    });

    const buildCustomHooks = {};

    sitesNamesForBuild.map(siteName => {
      const buildCustomHooksFilePath = path.join(sitesRootDir, siteName, "./build.js");
      buildCustomHooks[siteName] = {};
      if (!fs.existsSync(buildCustomHooksFilePath)) return;
      buildCustomHooks[siteName] = require(buildCustomHooksFilePath);
    });

    const buildTasks = sitesNamesForBuild.map(siteName => Promise
      .resolve({
        siteName,
        rootDomPoint: require('lodash/camelCase')(`react-root-${siteName}`),
        hooks: buildCustomHooks[siteName] || {},
        rootDir,
        sitesRootDir,
        srcHash: sitesHashs[siteName],
        coreHash: sitesCoreHash[siteName],
        webpackHash: sitesWebpackHash[siteName],
        // srcHash: siteName,

        srcPath: path.join(sitesRootDir, siteName),
        dstPath: path.join(rootDir, '/build', siteName),
      })
      .then(runHook('beforeConfigure'))
      .then(config => ({
        ...config,
        clientConfig: clientBuildConfig(config)
      }))
      .then(runHook('afterClientConfigure'))
      .then(runHook('beforeClientBuild'))
      .then(config => new Promise((resolve, reject) => {
        const { clientConfig } = config;
        webpack(clientConfig).run((err, stats) => {
          if (checkIfError(err, stats)) return reject(err, stats);
          resolve(config);
        });
      }))
      .then(runHook('afterClientBuild'))
      .then(config => {
        if (!fs.existsSync(path.join(sitesRootDir, siteName, './boot-server.js'))) return config;
        return Promise
          .resolve({
            ...config,
            serverConfig: serverBuildConfig(config)
          })
          .then(runHook('afterServerConfigure'))
          .then(runHook('beforeServerBuild'))
          .then(config => new Promise((resolve, reject) => {
            const { serverConfig } = config;
            webpack(serverConfig).run((err, stats) => {
              if (checkIfError(err, stats)) return reject(err, stats);
              resolve(config);
            });
          }))
          .then(runHook('afterServerBuild'));
      })
      .then(runHook('done'))
    );

    return promiseSequence(buildTasks.map(task => task.catch((err) => errors.push(err))));
  })
  .catch(err => errors.push(err))
  .then(() => errors.forEach(console.error));

function checkIfError(err, stats) {
  if (err) return true;
  if (stats.compilation.errors && stats.compilation.errors.length && process.argv.indexOf('--watch') == -1) {
    return true;
  }
  return false;
}

function runHook(hookName) {
  return config => {
    const hook = config.hooks[hookName];
    return Promise.resolve(hook ? hook(config) : config).then(newConfig => ({ ...config, ...newConfig }));
  }
}