/**
 * Created by dryymoon on 14.04.2018.
 */
const webpack = require('webpack');
const path = require('path');
const env = require('./helpers/cascaded-env');
const fs = require('fs');
const resolve = require('resolve');
const appRootDir = require('app-root-dir')
  .get();
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function (preConfig) {

  let {
    sitesRootDir,
    siteName,
    srcHash,
    coreHash,
    webpackHash,
    devcli,
    rootDomPoint,
    publicPath,
    singleJsFile,
    inlineSources,
    hashLength,
    buildTarget = 'web',
    filename,
    hmr = true,
    postcssPxToEm = true,
    postcssPrependSelectors = true,
  } = preConfig;

  rootDomPoint = rootDomPoint || 'react-root-default'; // eslint-disable-line
  srcHash = srcHash || ''; // eslint-disable-line
  publicPath = require('lodash/isString')(publicPath) ? publicPath : '/assets/';
  hashLength = require('lodash/isSafeInteger')(hashLength) ? hashLength : 5;

  if (['web', 'app', 'appTv'].indexOf(buildTarget) === -1) {
    console.error('Build target not contains allowed value');
    buildTarget = 'web';
  }

  if (devcli) buildTarget = 'web';

  const tmpDir = path.join(appRootDir, '.tmp');

  let dev = env.NODE_ENV === 'development';
  if (devcli) dev = true;

  let app = false;
  let appTv = false;

  if (buildTarget === 'app') app = true;
  if (buildTarget === 'appTv') {
    app = true;
    appTv = true;
  }

  if (app) inlineSources = true;
  if (devcli) inlineSources = false;

  const inlineJs = !!inlineSources;
  const inlineCss = !!inlineSources;
  const inlineAssets = !!inlineSources;

  const prod = !dev;
  // TODO Исправить Переводы и унести обратно
  const config = {};

  config.output = {};

  config.output.path = path.join(appRootDir, `/build/${siteName}/www/assets/`);

  const babelConfig = {};

  // babelConfig.cacheDirectory = true;

  /* if (dev) babelConfig.cacheDirectory =
   path.join(tmpDir, (devcli && 'devcli' || (devssr || dev) && 'dev') + "-client-babel-cache");
   if (babelConfig.cacheDirectory && fs.existsSync(babelConfig.cacheDirectory)) {
   fs.readdirSync(babelConfig.cacheDirectory).forEach(function (file) {
   fs.unlinkSync(path.join(babelConfig.cacheDirectory, file));
   });
   }
   */
  babelConfig.sourceMaps = true;
// babelConfig.cacheIdentifier = (prod && 'production' || dev && 'development' || devcli && 'fastdevelopment');
  babelConfig.presets = [];
  if (prod) babelConfig.presets.push('react-optimize');
  babelConfig.presets.push('react');

  babelConfig.presets.push(['env', {
    modules: false,
    shippedProposals: true, // to support spread operators
    forceAllTransforms: true
  }]);
  babelConfig.presets.push('stage-0');
  babelConfig.plugins = [];

  if (!devcli) {
    babelConfig.plugins.push(['c-3po',
      {
        extract: {
          output: path.join(config.output.path, '../../translations.pot'),
          location: 'file'
        },
        addComments: 'translator:', // Named comment 'translator:',
        sortByMsgid: true
      }
    ]);
  }

  // if (dev) babelConfig.plugins.push("transform-runtime");
  babelConfig.plugins.push('transform-decorators-legacy');
  babelConfig.plugins.push('transform-react-display-name');
  if (!devcli) babelConfig.plugins.push('transform-inline-environment-variables');

  if (prod) babelConfig.plugins.push('lodash');
  // if (devcli) babelConfig.plugins.push("react-hot-loader/babel");

  const reactTransforms = [];
  if (devcli) {
    reactTransforms.push({
      'transform': 'react-transform-catch-errors',
      'imports': ['react', 'redbox-react']
    });
  }
  if (devcli && hmr) {
    reactTransforms.push({
      'transform': 'react-transform-hmr',
      'imports': ['react'],
      'locals': ['module']
    });
  }

  // TODO Add https://www.npmjs.com/package/@doochik/babel-plugin-transform-react-componentdidcatch

  babelConfig.plugins.push(['react-transform', { 'transforms': reactTransforms }]);

  babelConfig.plugins.push(path.join(__dirname, './plugins/babel-plugin-import-glob.js'));

  // babelConfig.plugins.push('add-module-exports');

  // babelConfig.plugins.push("system-import-transformer");
  if (inlineJs || singleJsFile) {
    babelConfig.plugins.push('dynamic-import-node');
    babelConfig.plugins.push('remove-webpack');
  }

  babelConfig.plugins.push('transform-modern-regexp');

  const babelLoader = 'babel-loader?' + JSON.stringify(babelConfig);

  const eslintConfig = { fix: true, quiet: false, failOnError: false };

  const eslintLoader = `eslint-loader?${JSON.stringify(eslintConfig)}`;

  /*
   *  ***********************************
   *        Перелопачивание стилей
   *  ***********************************
   */

  const postcssPlugins = [];

// Заворачивает :hover в mediaquery, нужно для мобилок, чтоб клик не тормозил.
  postcssPlugins.push(require('./helpers/postcss-wrap-selectors-by-mediaquery')({
    match: ':hover',
    mediaquery: '(min-width:1000px), (hover) '
  }));

// Шаманит с изображениями, чтоб они вписывались в контенер
  postcssPlugins.push(require('postcss-object-fit-images'));

// Подставляет градиент в изображение до момента загрузки
// postcssPlugins.push(require('postcss-resemble-image').default());

// Нормализирует символы для _баного Эксплорера.
  postcssPlugins.push(require('postcss-normalize-charset')());

// Шаманит с фонтами, дописывает правила. # Sourcemaps Не отрабатывает
// postcssPlugins.push(require('postcss-font-magician')());

// Фильтрует и оптимизирует CSS. # Sourcemaps Не отрабатывает
  postcssPlugins.push(require('postcss-cssnext')());

// Дикая хрень, все px перебивает в em по всему css, причем перещитывает по указаному значению, получается сайт резинка.
  if (postcssPxToEm) postcssPlugins.push(require('./helpers/postcss-pix-to-em')({ base: /*projectConfig.baseFontSizePx*/ 14 })); // Base font size; 16px by default // devcli && 1 ||

// Хз зачем оно, раньше использовали, сейчас вроде нет, ни на что не влияет
// postcssPlugins.push(require('postcss-calc')({ mediaQueries: true, selectors: true }));

// Полифил для осла по Флексу
  postcssPlugins.push(require('postcss-flexbox')()); //https://github.com/archana-s/postcss-flexbox

// Добавляет селекто для апликухи
  if (postcssPrependSelectors) postcssPlugins.push(require('./helpers/postcss-prepend-selectors')({ selector: `#${rootDomPoint} ` }));

// Фильтрация CSS по заданному медиаправилу, для телеков нада
  app && appTv && postcssPlugins.push(require('postcss-unmq')({ type: 'screen', width: 700 })); //  height: 768, resolution: '1dppx', color: 3

  /**
   * CONSTRUCT WEBPACK CONFIG
   * */

  if (!filename) filename = hashLength ? `[name]-[hash:${hashLength}].[ext]` : '[name].[ext]';

  config.mode = prod ? 'production' : 'development';

  config.context = appRootDir;
  config.target = 'web';
  config.devtool = 'source-map';// || 'nosources';
  // if(devcli) config.devtool = 'cheap-module-source-map';
  config.stats = 'errors-only';
  config.entry = {};
  config.entry.main = [];

  // config.entry.main.push('normalize.css');

// devcli && config.entry.main.push('babel-polyfill'); // IE 11 Fackup Fixes
// devcli && config.entry.main.push('eventsource-polyfill'); // IE 11 Fackup Fixes
  devcli && hmr && config.entry.main.push('webpack-hot-middleware/client?path=/__webpack_hmr&reload=true'); //&timeout=1000
  devcli && config.entry.main.push('webpack/hot/only-dev-server');
  devcli && config.entry.main.push('react-hot-loader/patch');

  config.entry.main.push('./src-core/client.jsx');
  config.output = config.output || {};

  // config.output.rootPath = path.join(appRootDir, `/build/${siteName}/`);
  config.output.path = path.join(appRootDir, `/build/${siteName}/www/assets/`);

  // БЫЛО
  /* config.output.filename = `${assetsFsPath}[name]-[chunkhash].js`;
   if (devcli) config.output.filename = `${assetsFsPath}[name]-[hash].js`;
   config.output.chunkFilename = `${assetsFsPath}[name]-[chunkhash].js`;
   if (devcli) config.output.chunkFilename = `${assetsFsPath}[name]-[hash].js`;
   */
  // СТАЛО

  config.output.filename = filename.replace('[ext]', 'js');
  config.output.chunkFilename = filename.replace('[ext]', 'js');

  // config.output.sourceMapFilename = `${assetsFsPath}map/[filebase].map`;
  config.output.publicPath = publicPath;
// can be blank
// __webpack_public_path__ = myRuntimePublicPath
// rest of your application entry

  if (hashLength > 0) config.output.hashDigestLength = hashLength;
  config.output.pathinfo = !prod;
// config.output.path = path.join(appRootDir, env.SPA_BUILD_DST_PATH);
// if (!config.output.path) throw new Error('env.SPA_BUILD_DST_PATH must be setted');
// if (!devcli) require('mkdirp').sync(config.output.path);

// Ныжно для Hot module replacement
  if (devcli) config.recordsPath = path.join(tmpDir, 'dev-cli-record-path.json');

  config.resolve = { modules: [], extensions: [], alias: {} };
  config.resolve.modules.push('node_modules');
  config.resolve.extensions.push('.js');
  config.resolve.extensions.push('.jsx');
  config.resolve.extensions.push('.json');
  config.resolveLoader = { alias: {}, modules: [] };
  config.resolveLoader.alias['joint-loader'] = path.join(__dirname, './loaders/joint-loader');
  config.resolveLoader.alias['json-schema-loader'] = path.join(__dirname, './loaders/json-schema-loader');
  config.resolveLoader.modules.push(path.join(appRootDir, './node_modules/'));
  config.resolveLoader.modules.push(path.join(sitesRootDir, siteName, './node_modules/'));
  // config.resolveLoader.alias['babel-loader'] = babelLoader;

  config.module = { rules: [] };

  const acorn = require('acorn');

  config.module.rules.push({
    resource: {
      test: /\.js$|\.jsx$/,
      and: [
        {
          test: /\/node_modules\//,
        },
        {
          test: (scriptPath) => {
            const code = fs.readFileSync(scriptPath, 'utf8');
            try {
              acorn.parse(code, { ecmaVersion: 5 });
              return false;
            } catch (err) {
              // console.log(`❌ ${scriptPath} is not ES5`);
              return true;
            }
          }
        }
      ]
      /* test: function(fileName) {
       console.log('\n\n', args);
       }, */
      /* or: [
       { include: /node_modules\/regex-css-media-query/ },
       { include: /node_modules\/unique-string/ },
       { include: /node_modules\/crypto-random-string/ },
       { include: /node_modules\/css-media-features/ },
       // { include: /node_modules\/@ckeditor\/.+\/src\/.+/ },
       // { resourceQuery: /babel/ }
       ] */
    },
    use: ['babel-loader?' + JSON.stringify({ ...babelConfig, babelrc: false })],
  });

  config.module.rules.push({
    test: /\.js$|\.jsx$/,
    // exclude: /(node_modules|bower_components)\/(?!(unique-string|crypto-random-string|css-media-features)\/)/,
    exclude: /(node_modules|bower_components)/,
    use: ['babel-loader?' + JSON.stringify(babelConfig), dev && eslintLoader], // 'import-glob', // 'glob-loader',
    // sideEffects: false // maybe need remove
  });

  const joint = !devcli && `joint-loader?source=true&instance=${siteName}`;

  config.module.rules.push({
    test: /\.schema\.json$/,
    use: [joint, 'json-schema-loader?ajvTest=true'] // &name=schema/[name].[ext]
  });

  config.module.rules.push({
    test: /\.json$/,
    use: [joint, 'json-loader'],
    type: 'javascript/auto'
  });

  config.module.rules.push({ test: /\.ya?ml$/, use: [joint, 'yml-loader'], });
  config.module.rules.push({
    test: /\.dot$/, use: [
      joint,
      'dot-tpl-loader',
      'extract-loader',
      {
        loader: 'html-loader',
        options: { interpolate: true, minimize: false, conservativeCollapse: false }
      }
    ]
  });

  const ExtractLoader = function (customLoader) {
    return [
      dev && `style-loader?sourceMap=true&convertToAbsoluteUrls=true&hmr=${!!devcli}`, // &singleton=true
      prod && MiniCssExtractPlugin.loader,
      joint,
      'css-loader?importLoaders=1&localIdentName=[local]&sourceMap=true',
      {
        loader: `postcss-loader`,
        options: {
          // ident: 'postcss-loader-options',
          ident: 'postcss',
          sourceMap: true,
          plugins: postcssPlugins
        }
      },
      'resolve-url-loader?keepQuery=true&sourceMap=true',
      customLoader,
      {
        loader: 'string-replace-loader',
        options: {
          multiple: [
            { search: '__ROOT_DOM_POINT__', replace: rootDomPoint },
          ]
        }
      }
    ].filter(it => it);
  };

  config.module.rules.push({
    test: /\.css$/, use: ExtractLoader()
  });
  config.module.rules.push({
    test: /\.less$/,
    use: ExtractLoader('less-loader?outputStyle=expanded&sourceMap=true&sourceMapContents=true&javascriptEnabled=true')
  });
  config.module.rules.push({
    test: /\.scss$/,
    use: ExtractLoader('sass-loader?outputStyle=expanded&sourceMap=true&sourceMapContents=true')
  });

  config.module.rules.push({
    test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
    use: [joint,
      'url-loader?' + (!inlineAssets && 'limit=10000&') + `mimetype=application/font-woff&outputPath=fonts&name=${filename}`]
  });
  config.module.rules.push({
    test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
    use: [joint,
      'url-loader?' + (!inlineAssets && 'limit=10000&') + `mimetype=application/font-woff2&outputPath=fonts&name=${filename}`]
  });
  config.module.rules.push({
    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    use: [joint,
      'url-loader?' + (!inlineAssets && 'limit=10000&') + `mimetype=application/octet-stream&outputPath=fonts&name=${filename}`]
  });
  config.module.rules.push({
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    use: [joint,
      'url-loader?' + (!inlineAssets && 'limit=10000&') + `mimetype=application/vnd.ms-fontobject&outputPath=fonts&name=${filename}`]
  });
  config.module.rules.push({
    test: /\.svg$/,
    issuer: /\.js$|\.jsx$/,
    use: [joint,
      'svg-react-loader', {
        loader: 'svgo-loader',
        options: {
          plugins: [
            { removeAttrs: { 'attrs': ['data-name', 'svg:height', 'svg:width'] } },
            { removeTitle: true },
            { removeViewBox: false },
            { removeXMLNS: true }, // removes xmlns attribute (for inline svg, disabled by default)
            { convertPathData: true },
            /* CHANGED */
            { collapseGroups: false },
            /* END CHANGED */
            { convertStyleToAttrs: true },
            {
              cleanupIDs: {
                prefix: {
                  toString() {
                    this.counter = this.counter || 0;
                    return `id-${this.counter++}`;
                  }
                }
              }
            }]
        }
      }]
  });
  config.module.rules.push({
    test: /\.svg$/,
    issuer: /\.(css|less|scss)$/,
    use: [
      joint,
      'svg-url-loader?' + (!inlineAssets && 'limit=10000&') + `&outputPath=svg&name=${filename}`,
      {
        loader: 'svgo-loader',
        options: {
          plugins: [
            { removeAttrs: { 'attrs': ['data-name', 'svg:height', 'svg:width'] } },
            { removeTitle: true },
            { removeViewBox: false },
            // { removeXMLNS: true }, // removes xmlns attribute (for inline svg, disabled by default)
            { convertPathData: true },
            { cleanupIds: true }
          ]
        }
      }
    ]
  });

  config.module.rules.push({
    test: /\.(jpe?g|png|gif)$/,
    // loader: "joint!url?" + (!app && 'limit=1024') + "&name=images/[name]' + hashFSName + '.[ext]"
    use: [joint,
      'url-loader?' + (!inlineAssets && 'limit=4096') + `&outputPath=images&name=${filename}`]
    // '!image-maxsize-webpack-loader'
    // '!image-webpack-loader?{optimizationLevel: 7, interlaced: true, quality: 77, pngquant: {quality: "65-90", speed: 4}}' +
    // + require('postcss-srcset').loader,
    //'file?hash=sha512&digest=hex&name=' + hashFSName + '.[ext]',
    // 'image-webpack?{optimizationLevel: 7, interlaced: true, quality: 77, pngquant: {quality: "65-90", speed: 4}}'
  });

  config.module.rules.push({ test: /\.po$/, use: [joint, 'json-loader', 'po-gettext-loader'] });

  config.module.rules.forEach(rule => rule.use = rule.use.filter(it => it));

  config.optimization = {};

  config.optimization.minimizer = [];
  /* TODO Remove it after tests, after 01-01-2020
  false && config.optimization.minimizer.push(
    new (require('uglify-es-webpack-plugin'))({
      cache: false,
      test: /\.js($|\?)/i,
      parallel: true,
      sourceMap: true,
      uglifyOptions: {
        // safari10: true,
        // ecma: 8,
        mangle: false,
        compress: {
          dead_code: true,
          // drop_console: false,
          inline: 1,
        },
        output: {
          ascii_only: true
        }
      },
    })
  ); */

  config.optimization.minimizer.push(
    new (require('terser-webpack-plugin'))({
      parallel: true,
      sourceMap: true,
      terserOptions: {
        ecma: 5,
      },
    })
  );


// Оптимизирует Медиаправила в CSS
  config.optimization.minimizer.push(
    new (require('postcss-assets-webpack-plugin'))({
      test: /\.css$/,
      log: false,
      plugins: [
        // Pack same CSS media query rules into one media query rule
        require('css-mqpacker')
      ]
    }));

// Оптимизирует CSS
  config.optimization.minimizer.push(
    new (require('optimize-css-assets-webpack-plugin'))({
      assetNameRegExp: /\.css$/,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: {
        discardDuplicates: { removeAll: true },
        safe: true,
        svgo: false,
        map: { inline: false, annotation: false },
      },
      canPrint: false
    }));

  /*  config.optimization = { // need to test - something new
   splitChunks: {
   cacheGroups: {
   projectLib: {
   test: (module, chunks) => module.context == path.join(sitesRootDir, siteName),
   name: "projectLib",
   chunks: "all",
   minChunks: 1,
   minSize: 0,
   priority: 20,
   enforce: true
   },
   vendors: {
   test: (module, chunks) => module.depth > 0,
   name: "vendors",
   chunks: "all",
   minChunks: 1,
   minSize: 0,
   priority: 10,
   enforce: true
   }
   }
   },
   runtimeChunk: { name: "manifest" },
   };*/

  // config.optimization.runtimeChunk = { name: 'boot' };

  config.optimization.splitChunks = {
    cacheGroups: {},
  };

  config.optimization.splitChunks.cacheGroups.commons = {
    test: /\.(css|less|scss)$/,
    name: 'commons',
    chunks: 'all',
    minChunks: 2,
    reuseExistingChunk: true,
    minSize: 0,
    // enforce: true
  };

  /* config.optimization.splitChunks.cacheGroups.project = {
   chunks: "initial",
   minChunks: 1,
   minSize: 0,
   enforce: true
   }; */

  config.optimization.splitChunks.cacheGroups.vendors = {
    test: /node_modules/,
    chunks: 'initial',
    name: 'vendor',
    // priority: 100,
    enforce: true
  };

  if (inlineJs || singleJsFile) {
    config.optimization.runtimeChunk = undefined;
    config.optimization.splitChunks = {};
  }

  config.plugins = [];
  config.plugins.push(new (require('webpack-runtime-public-path-plugin'))({ runtimePublicPath: 'window.__WEBPACK_RUNTIME_PUBLIC_PATH__' }));
  // config.plugins.push(new (require('simple-progress-webpack-plugin'))({ format: 'minimal' }));
  config.plugins.push(new (require('friendly-errors-webpack-plugin'))({
    compilationSuccessInfo: {
      // messages: [`You application is running here http://localhost:${env.DEV_CLI_PORT}\n`],
      // notes: ['Some additionnal notes to be displayed unpon successful compilation']
    },
  }));

  config.plugins.push(new (require('html-webpack-plugin'))({
    filename: '../index.html',
    title: '',
    baseFontSize: 14, // appTv && 32 || projectConfig.baseFontSizePx || 14,
    appMountId: rootDomPoint,
    cache: true,
    template: './src-core/assets/index.html.dot',
    inlineSource: // embed all javascript and css inline
      (inlineJs && inlineCss && '.(js|css)$')
      || (inlineJs && '.(js)$')
      || (inlineCss && '.(css)$')
      || false,
    minify: false,
    inject: true,
    chunksSortMode: 'dependency'
  }));

  config.plugins.push(new (require('html-webpack-inline-source-plugin'))());
  if (inlineJs) config.plugins.push(new (require('ignore-emit-webpack-plugin'))(/\.js$/));
  if (inlineCss) config.plugins.push(new (require('ignore-emit-webpack-plugin'))(/\.css$/));
  config.plugins.push(new (require('html-beautify-webpack-plugin'))());

  prod && config.plugins.push(new MiniCssExtractPlugin({
    publicPath: '',
    filename: filename.replace('[ext]', 'css'),
    chunkFilename: filename.replace('[ext]', 'css'),
    allChunks: true,
  }));

// Оптимизирует Медиаправила в CSS
  if (!devcli) {
    config.plugins.push(new (require('postcss-assets-webpack-plugin'))({
      test: /\.css$/, log: false, plugins: [require('css-mqpacker')]
    }));
  }

// Оптимизирует CSS
  if (false && !devcli) {
    config.plugins.push(new (require('optimize-css-assets-webpack-plugin'))({
      assetNameRegExp: /\.css$/,
      cssProcessor: require('cssnano'),
      cssProcessorOptions: {
        discardDuplicates: { removeAll: true },
        safe: true,
        svgo: false,
        map: { inline: false }
      },
      canPrint: false
    }));
  }

// Указывает всему вебпаку минимизировать и оптимизировать сборку
  if (prod) config.plugins.push(new webpack.LoaderOptionsPlugin({ minimize: true }));
  if (prod) config.plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
  if (dev) config.plugins.push(new webpack.NamedModulesPlugin());
  if (prod) config.plugins.push(new webpack.HashedModuleIdsPlugin());
  if (devcli) config.plugins.push(new webpack.HotModuleReplacementPlugin());

  const releaseName = (new Date().toISOString());
  config.plugins.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(prod ? 'production' : 'development')
    },
    __CLIENT__: true,
    __SERVER__: false,
    __SRC_SITE_FOLDER__: JSON.stringify(siteName),
    __ROOT_DOM_POINT__: JSON.stringify(rootDomPoint),
    __DEV_CLI__: !!devcli,
    __DEVELOPMENT__: !prod,
    __APP__: app,
    __AMP__: false,
    __PUBLIC_PATH__: `(window.__WEBPACK_RUNTIME_PUBLIC_PATH__ || "${config.output.publicPath}")`,
    __RELEASE__: JSON.stringify(releaseName),
  }));

  config.plugins.push(
    new webpack.NormalModuleReplacementPlugin(/(.*)__SRC_SITE_FOLDER__(\.*)/, (resource) => {
      resource.request = resource.request.replace(/__SRC_SITE_FOLDER__/, siteName);
    })
  );

  config.plugins.push(
    new webpack.NormalModuleReplacementPlugin(/react-dnd\/lib\//, (resource) => {
      // hack for fix old and new react-dnd

      const { context, request } = resource;

      try {
        // If we found file with DND
        resolve.sync(request, { basedir: context });
        return resource;
      } catch (e) {
      }

      try {
        // If we not found file with old DND, try replace to new file location
        const newRequest = request.replace('react-dnd/lib/', 'react-dnd/lib/esm/');
        resolve.sync(newRequest, { basedir: context });
        resource.request = newRequest;
        return resource;
      } catch (e) {
      }

      // normal webpack behaviour
      return resource;
    })
  );

  config.plugins.push(new (require('circular-dependency-plugin'))({
    exclude: /node_modules/,
    failOnError: true
  }));

  config.plugins.push(new (require('case-sensitive-paths-webpack-plugin'))());

  !devcli && config.plugins.push(
    new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)({
      analyzerMode: 'static', // server
      reportFilename: path.resolve(config.output.path, '../../report.html'),
      openAnalyzer: false,
      // defaultSizes: 'gzip'
    })
  );

  const provide = {
    Raven: 'raven-js'
  };
  if (app) {
    provide.sessionStorage = 'localstorage-memory';
    provide['window.sessionStorage'] = 'localstorage-memory';
    provide.localStorage = 'localstorage-memory';
    provide['window.localStorage'] = 'localstorage-memory';
  }

  config.plugins.push(new webpack.ProvidePlugin(provide));
  !devcli && config.plugins.push(new (require('./loaders/joint-loader').master)({
    instance: siteName
  }));

  !devcli && config.plugins.push(new (require('clean-webpack-plugin'))(
    [path.resolve(appRootDir, config.output.path, '../../')],
    { root: path.join(appRootDir, 'build'), beforeEmit: false, dry: false, verbose: false, }
  ));

  const sitePackageJson = require('lodash/omit')(
    require(path.join(appRootDir, 'src-sites', siteName, './package.json')),
    ['dependencies', 'devDependencies', 'scripts']
  );

  config.plugins.push(new (require('generate-json-webpack-plugin'))(
    '../../package.json',
    require('sort-package-json')({
      ...sitePackageJson,
      srcHash,
      coreHash,
      webpackHash
    }),
    null, '\t'
  ));

  return config;
};
