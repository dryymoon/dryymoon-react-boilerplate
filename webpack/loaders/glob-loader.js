/**
 * Created by dryymoon on 17.04.2018.
 */
var loaderUtils = require("loader-utils");
var glob = require("glob");
var isGlob = require("is-glob");
var path = require("path");

module.exports = function (source) {
  if (this.cacheable) this.cacheable();
  var self = this;
  var regex = /.?import + ?((\w+) +from )?([\'\"])(.*?)\3/gm;
  var importModules = /import +(\w+) +from +([\'\"])(.*?)\2/gm;
  var importFiles = /import +([\'\"])(.*?)\1/gm;
  var importSass = /@import +([\'\"])(.*?)\1/gm;
  var resourceDir = path.dirname(this.resourcePath);

  function replacer(match, fromStatement, obj, quote, filename) {
    var modules = {};
    var withModules = false;
    if (!glob.hasMagic(filename)) return match;
    var result = glob
      .sync(filename, {
        cwd: resourceDir
      })
      .map(function (file, index) {
        var fileName = quote + file + quote;
        self.addDependency(fileName);

        if (match.match(importSass)) {
          return '@import ' + fileName;
        }

        if (match.match(importModules)) {
          var moduleName = obj + index;
          modules[file] = moduleName;
          withModules = true;
          return 'import * as ' + moduleName + ' from ' + fileName;
        }

        if (match.match(importFiles)) {
          return 'import ' + fileName;
        }

      })
      .join('; \n');
    if (result && withModules) {
      result += `; \nconst ${obj} = { \n`; // [' + modules.join(', ') + ']';
      result += Object.keys(modules).map(key => `'${key}' : ${modules[key]}`).join(', \n');
      result += '\n }; \n';
    }

    if (!result) result = `const ${obj} = []`;

    return result;
  }

  return source.replace(regex, replacer);

  // var res = source.replace(regex, replacer);
  // return res;
};

/* module.exports = function (content, sourceMap) {
 this.cacheable();

 var resourceDir = path.dirname(this.resourcePath);
 var files = glob.sync(content.trim(), {
 cwd: resourceDir
 });

 console.log('files', files);

 return "module.exports = {\n" + files.map(function (file) {
 var name = path.basename(file, path.extname(file));
 this.addDependency(file);

 return "  '" + name + "': require(" + JSON.stringify(file) + ")"
 }, this).join(",\n") + "\n};";
 }; */

/* module.exports.pitch = function (remainingRequest, precedingRequest, data) {
 const file = remainingRequest.split('!').pop();
 console.log('PITCH FILE', file);

 console.log('PITCH', {remainingRequest, precedingRequest, data});
 // if (!isGlob(file)) return;
 return `module.exports = 'a';`;
 return;
 if (this.cacheable) this.cacheable();

 const options = loaderUtils.getOptions(this) || {};
 var resourceDir = path.dirname(this.resourcePath);
 throw new Error('asdasd');
 var files = glob.sync(content.trim(), {
 cwd: resourceDir
 });

 console.log('files', files);

 return "module.exports = {\n" + files.map(function (file) {
 var name = path.basename(file, path.extname(file));
 this.addDependency(file);

 return "  '" + name + "': require(" + JSON.stringify(file) + ")"
 }, this).join(",\n") + "\n};";
 };

 module.exports.raw = true; */