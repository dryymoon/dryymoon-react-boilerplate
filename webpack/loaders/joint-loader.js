/**
 * Created by DryyMoon on 12.02.2015.
 * License GPL v3.
 * E-Mail: igor@pilipenko.cc
 */

const EventEmitter = require('events');
const colors = require('colors');
const loaderUtils = require('loader-utils');
const path = require('path');
const Q = require('q');
const md5 = require('md5');
// const fs = require('fs');
const fs = require('virtualfs').default;

/**
 * Initiate module global state
 */

const instances = {};
let masterCounter = 0;

function InstanceCreator({ instanceId }) {
  const tmpDir = `/joint_cache/${instanceId}`;
  const tmpDirArr = (tmpDir.split('/')).filter(it => it);
  let testedPath = '/';
  while (tmpDirArr.length) {
    testedPath = path.join(testedPath, tmpDirArr.shift());
    if (!fs.existsSync(testedPath)) fs.mkdirSync(testedPath);
  }

  this.instanceId = instanceId;
  this.emitter = new EventEmitter();
  this.emitter.setMaxListeners(100000);
  this.state = 'new';

  this.pendingResources = {};
  this.masters = {};

  this.registerMaster = (masterId) => {
    this.masters[masterId] = true;
  };

  this.dropMaster = (masterId) => {
    delete this.masters[masterId];
    if (Object.keys(this.masters).length === 0) {
      this.emitter.emit(`masters-finish-build`, {});
    }
  };

  this.emitResource = (resourcePath, source, map) => {
    this.store(resourcePath, source, map);
    this.emitter.emit(`resource-awailable:${resourcePath}`, {
      resourcePath,
      source,
      map
    });
    delete this.pendingResources[resourcePath];
    return Promise.resolve();
  };

  this.awaitResource = (resourcePath) => {
    const resourceAlreadyExists = this.exists(resourcePath);
    let resolved = false;

    if (resourceAlreadyExists) {
      resolved = true;
      return Promise.resolve(this.read(resourcePath));
    }

    const masterAvailable = Object.keys(this.masters).length;
    if (!resourceAlreadyExists && !masterAvailable) {
      this.pendingResources[resourcePath] = true;
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      this.emitter.on(`resource-awailable:${resourcePath}`, (data) => {
        resolved = true;
        resolve(data);
      });

      this.emitter.on(`masters-finish-build`, () => {
        if (resolved) return;
        reject();
      });
      this.pendingResources[resourcePath] = true;
    });
  };

  this.makeCachePath = (resourcePath) => {
    const ext = path.extname(resourcePath);
    const hash = md5(resourcePath);
    return path.join(tmpDir, hash + ext);
  };

  this.exists = (resourcePath) => {
    return !!fs.existsSync(this.makeCachePath(resourcePath));
  };

  this.read = (resourcePath) => {
    const cachePath = this.makeCachePath(resourcePath);
    return {
      resourcePath,
      source: fs.readFileSync(cachePath, 'utf8'),
      map: fs.existsSync(cachePath + '.map') && fs.readFileSync(cachePath + '.map', 'utf8')
    }
  };

  this.store = (resourcePath, source, map) => {
    const cachePath = this.makeCachePath(resourcePath);
    fs.writeFileSync(cachePath, source, 'utf8');
    if (map) fs.writeFileSync(cachePath + '.map', map, 'utf8');
  };

  return this;
}

const getInstance = (instanceId = 'default') => {
  instances[instanceId] = instances[instanceId] || new InstanceCreator({ instanceId });
  return instances[instanceId];
};

/*
 * Joint Loader
 */

module.exports = function (source, map) {
  if (this.cacheable) this.cacheable();
  const callback = this.async();
  const options = loaderUtils.getOptions(this) || {};
  const isSource = options.emit || options.source;
  if (isSource && !options.disabled) {
    getInstance(options.instance)
      .emitResource(this.resourcePath, source, map);
  }
  callback(null, source, map);
};

module.exports.pitch = function (remainingRequest, precedingRequest, data) {
  if (this.cacheable) this.cacheable();
  const options = loaderUtils.getOptions(this) || {};
  const isSlave = options.await || options.slave;
  if (!isSlave) return;
  if (options.disabled) return;
  const resourcePath = this.resourcePath;
  this.addDependency(resourcePath);
  const callback = this.async();
  getInstance(options.instance)
    .awaitResource(resourcePath)
    .then(({ source, map }) => callback(null, source, map))
    .catch(() => {
      var src = 'module.exports = require(' + JSON.stringify('-!' + remainingRequest) + ')';
      callback(null, src, '');
    });
};

/*
 * External raw access to loader
 */

// module.exports.emit = emitResource;
// module.exports.await = awaitResource;

/*
 * Joint Plugin for use only in master class
 */

var MasterPlugin = function ({ instance } = {}) {
  this.masterId = masterCounter++;
  this.instance = getInstance(instance);
};

MasterPlugin.prototype.apply = function (compiler) {
  this.instance.registerMaster(this.masterId);
  const that = this;
  compiler.plugin("compile", function (params) {
    compiler.options.joint = {
      instance: that.instance,
      master: that.masterId
    };
  });

  compiler.plugin("done", function (compilation) {
    that.instance.dropMaster(that.masterId);
  });

};

// storage.emitter.on('master-finished', () => inspectUnresolvedResources('master-finished'));
// process.on('exit', () => inspectUnresolvedResources('exit'));

process.on('exit', () => {
  Object.keys(instances).forEach(key => {
    const inst = instances[key];
    const pendingResources = Object.keys(inst.pendingResources);
    if (pendingResources.length === 0) {
      console.log('\nJoint: all resources resolved\n'.green);
      return;
    }
    console.error('\nJoint-loader don`t awaited files:'.underline.red);
    pendingResources.forEach(res => console.log(res.toString().red));
    console.log('\n');
  })
});

module.exports.master = MasterPlugin;