/**
 * Created by dryymoon on 30.05.17.
 */
'use strict';
const Ajv = require('ajv');
const ajvMergePatch = require('ajv-merge-patch');
const cloneDeepWith = require('lodash/cloneDeepWith');
const loaderUtils = require('loader-utils');
const metaSchema = require('ajv/lib/refs/json-schema-draft-06.json');
const fs = require('fs');
const path = require('path');
// const pack = require('ajv-pack');
const $RefParser = require('json-schema-ref-parser');

module.exports = function (s, sourceMap) {
  this.cacheable && this.cacheable();
  const callback = this.async();
  const config = loaderUtils.getOptions(this);
  /* const query = Object.assign(
   { useDefaults: true, coerceTypes: true, allErrors: false },
   config,
   { sourceCode: true, missingRefs: true, extendRefs: "fail" });
   */

  const parser = new $RefParser();
  const context = config.context; // || this.options.context;
  const that = this;

  parser.bundle(this.resourcePath)
    .then((schema) => {
      parser.$refs.paths().map(that.addDependency);
      const json = JSON.stringify(schema, null, 2);
      if (config.ajvTest) {
        const ajv = new Ajv({ missingRefs: true, extendRefs: 'fail'}); // schemaId: 'auto'
        ajv.addMetaSchema(metaSchema);
        ajvMergePatch(ajv);
        // const validator =
        ajv.compile(schema);
        // const code = pack(ajv, validator);
      }

      if (config.name && that.emitFile) {
        const fsName = loaderUtils.interpolateName(that,
          config.name || '[name].[ext]',
          { context, content: json });
        that.emitFile(fsName, json);
      }
      callback(null, `module.exports = ${json};`, sourceMap);
    })
    .catch(err => callback(err, '', sourceMap));
};
