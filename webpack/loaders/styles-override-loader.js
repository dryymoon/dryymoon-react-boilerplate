'use strict';
var Joint = require('./joint-loader');

module.exports = function (source, _map) {
  this.cacheable && this.cacheable();
  var opt = this.options.styleOverrider || {};
  var readableSource = source.toString('UTF-8');
  if(opt.exclude && opt.exclude.test(this.resourcePath)){
    return readableSource;
  }

  var callback = this.async();

  Joint.await(this.resourcePath, function(err, sourceJs){
    if(err) return callback(err);
    var newSourceJs = sourceJs && sourceJs.replace('module.exports','exports.locals');
    readableSource = `${readableSource}\n\n${newSourceJs};`;
    callback(null, readableSource);
  });
};

/*
 ##############################################################################################
 resourcePath /SHARA/project/src/components/react-nojs-tabs/react-nojs-tabs.scss
 source----------------- exports = module.exports = require("./../../../node_modules/css-loader/lib/css-base.js")();
 // imports


 // module
 exports.push([module.id, ".reactNoJsTabs__tabs{margin:0 0 -1px;padding:0;list-style:none;display:table}.reactNoJsTabs__tabs_fullWidth{width:100%}.reactNoJsTabs__tabs_equalWidth{table-layout:fixed}.reactNoJsTabs__tab{display:table-cell;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;vertical-align:bottom}.reactNoJsTabs__tabElement{position:relative;z-index:1}.reactNoJsTabs__tabElement_active{cursor:default;display:none}.reactNoJsTabs__tabElement_passive{cursor:pointer;display:block}.reactNoJsTabs__control{display:none}.reactNoJsTabs__content{position:relative;clear:both;display:none}", ""]);

 // exports
 exports.locals = {
 "reactNoJsTabs": "reactNoJsTabs",
 "reactNoJsTabs__tabs": "reactNoJsTabs__tabs",
 "reactNoJsTabs__tabs_fullWidth": "reactNoJsTabs__tabs_fullWidth",
 "reactNoJsTabs__tabs_equalWidth": "reactNoJsTabs__tabs_equalWidth",
 "reactNoJsTabs__tab": "reactNoJsTabs__tab",
 "reactNoJsTabs__tabElement": "reactNoJsTabs__tabElement",
 "reactNoJsTabs__tabElement_active": "reactNoJsTabs__tabElement_active",
 "reactNoJsTabs__tabElement_passive": "reactNoJsTabs__tabElement_passive",
 "reactNoJsTabs__control": "reactNoJsTabs__control",
 "reactNoJsTabs__content": "reactNoJsTabs__content"
 }; { data: undefined,
 inputValue: null,
 query: '',
 async: [Function: async],
 callback: [Function] }
 */
