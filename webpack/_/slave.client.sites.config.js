/**
 * Created by dryymoon on 14.04.2018.
 */
const webpack = require('webpack');
const path = require('path');
const env = require('./helpers/cascaded-env');
const appRootDir = require('app-root-dir').get();
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const tmpDir = path.join(appRootDir, '.tmp');

let dev = env.NODE_ENV === 'development';
let devcli = dev && !!env.WEBPACK_BUILD_TYPE_DEVCLI;
const app = false;
const appTv = false;
const prod = !dev;

const babelConfig = {};

/* if (dev) babelConfig.cacheDirectory =
 path.join(tmpDir, (devcli && 'devcli' || (devssr || dev) && 'dev') + "-client-babel-cache");
 if (babelConfig.cacheDirectory && fs.existsSync(babelConfig.cacheDirectory)) {
 fs.readdirSync(babelConfig.cacheDirectory).forEach(function (file) {
 fs.unlinkSync(path.join(babelConfig.cacheDirectory, file));
 });
 }
 */
babelConfig.sourceMaps = true;
// babelConfig.cacheDirectory = true;
// babelConfig.cacheIdentifier = (prod && 'production' || dev && 'development' || devcli && 'fastdevelopment');
babelConfig.presets = [];
if (prod) babelConfig.presets.push("react-optimize");
babelConfig.presets.push("react");

babelConfig.presets.push(["env", { "modules": false }]);
babelConfig.presets.push("stage-0");
babelConfig.plugins = [];

if (!devcli) babelConfig.plugins.push(['c-3po',
  {
    extract: { output: './translations/translations.pot', location: 'file' },
    // resolve: { translations: "default", unresolved: "warn" }, // prod ? "fail" : "warn"
    addComments: 'translator:', // Named comment 'translator:',
    sortByMsgid: true
  }
]);

if (dev) babelConfig.plugins.push("transform-runtime");
babelConfig.plugins.push("transform-decorators-legacy");
babelConfig.plugins.push("transform-react-display-name");
if (!devcli) babelConfig.plugins.push("transform-inline-environment-variables");

if (prod) babelConfig.plugins.push("lodash");

const reactTransforms = [];
if (devcli) reactTransforms.push({ "transform": "react-transform-catch-errors", "imports": ["react", "redbox-react"] });
if (devcli) reactTransforms.push({ "transform": "react-transform-hmr", "imports": ["react"], "locals": ["module"] });
if (!devcli) reactTransforms.push({ "transform": "react-transform-sentry", "imports": ["react"] });

babelConfig.plugins.push(["react-transform", { "transforms": reactTransforms }]);
babelConfig.plugins.push("import-glob");
// if (app) babelConfig.plugins.push('system-import-transformer');

const babelLoader = 'babel-loader?' + JSON.stringify(babelConfig);

const eslintConfig = { fix: true, quiet: false, failOnError: false };

const eslintLoader = `eslint-loader?${JSON.stringify(eslintConfig)}`;

/*
 *  ***********************************
 *        Перелопачивание стилей
 *  ***********************************
 */

// Нужная хрень для переписывалки названия классов, сначала они туда собираются потом вытягиваются оттуда.
// config.styleOverrider = {};
// config.styleOverrider.exclude = (/(node_modules|bower_components)/);

const postcssPlugins = [];

// Подмена имен стилей, конфигурируется в глобальном конфиге в корне проекта.

/* (prod || devssr) && postcssPlugins.push(
 require('postcss-modules')({
 scopeBehaviour: 'local',
 generateScopedName: function (className, filename, cssFileContents) {
 if ((/(node_modules|bower_components)/).test(filename)) return className;
 if (overrideStyleClasses[className]) return overrideStyleClasses[className];
 if (projectConfig.joke) {
 stopWords = arrUnique(stopWords.map(function (it) {
 return it.replace(/ /g, "");
 }));
 var newName = stopWords.splice(Math.floor(Math.random() * stopWords.length), 1);
 return overrideStyleClasses[className] = newName[0];
 }
 if (obfuscateStyles) {
 return overrideStyleClasses[className] = hashids.encode(Object.keys(overrideStyleClasses).length)
 }
 return overrideStyleClasses[className] = className;
 },
 getJSON: function (cssFileName, json) {
 Joint.emit(cssFileName, `module.exports=${require('tosource')(require('sort-object')(json))};`);
 }
 })); */

// Заворачивает :hover в mediaquery, нужно для мобилок, чтоб клик не тормозил.
postcssPlugins.push(require('./helpers/postcss-wrap-selectors-by-mediaquery')({
  match: ':hover',
  mediaquery: '(min-width:1000px), (hover) '
}));

// Normalize css
// postcssPlugins.push(require('postcss-normalize')({ forceImport: true }));

// Удаляет нах коментарии
// postcssPlugins.push(require('postcss-discard-comments')({ removeAll: true }));

// Шаманит с изображениями, чтоб они вписывались в контенер
postcssPlugins.push(require('postcss-object-fit-images'));

// Подставляет градиент в изображение до момента загрузки
// postcssPlugins.push(require('postcss-resemble-image').default());

// Нормализирует символы для _баного Эксплорера.
postcssPlugins.push(require('postcss-normalize-charset')());

// Шаманит с фонтами, дописывает правила. # Sourcemaps Не отрабатывает
// postcssPlugins.push(require('postcss-font-magician')());

// Фильтрует и оптимизирует CSS. # Sourcemaps Не отрабатывает
// postcssPlugins.push(require('postcss-cssnext')());

// Дикая хрень, все px перебивает в em по всему css, причем перещитывает по указаному значению, получается сайт резинка.
postcssPlugins.push(require('postcss-pix-to-em')({ base: /*projectConfig.baseFontSizePx*/ 14 })); // Base font size; 16px by default // devcli && 1 ||

// Хз зачем оно, раньше использовали, сейчас вроде нет, ни на что не влияет
// postcssPlugins.push(require('postcss-calc')({ mediaQueries: true, selectors: true }));

// Полифил для осла по Флексу
postcssPlugins.push(require('postcss-flexbox')()); //https://github.com/archana-s/postcss-flexbox

// Добавляет селекто для апликухи
postcssPlugins.push(require('postcss-prepend-selector')({ selector: '#react-mount-node ' }));

// Фильтрация CSS по заданному медиаправилу, для телеков нада
app && appTv && postcssPlugins.push(require('postcss-unmq')({ type: 'screen', width: 700 })); //  height: 768, resolution: '1dppx', color: 3

/**
 * CONSTRUCT WEBPACK CONFIG
 * */


const hashLen = 5;
const fsFileName = hashLen ? `[name]-[hash]` : '[name]';
const assetsFsPath = 'assets/';

const config = {};
config.mode = prod ? 'production' : 'development';

config.context = appRootDir;
config.target = 'web';
config.devtool = 'source-map';// || 'nosources';
config.stats = "errors-only";
config.entry = {};
config.entry.main = [];

config.entry.main.push('normalize.css');

// devcli && config.entry.main.push('babel-polyfill'); // IE 11 Fackup Fixes
// devcli && config.entry.main.push('eventsource-polyfill'); // IE 11 Fackup Fixes
devcli && config.entry.main.push('webpack-hot-middleware/client?path=/__webpack_hmr&reload=true'); //&timeout=1000
// devcli && config.entry.main.push('webpack/hot/only-dev-server');
// devcli && config.entry.main.push('react-hot-loader/patch');

config.entry.main.push('./src-core/client.jsx');
config.output = {};

config.output = {};
config.output.hashDigestLength = hashLen;
config.output.filename = `${assetsFsPath}${fsFileName}.js`;
config.output.chunkFilename = `${assetsFsPath}${fsFileName}.js`;
config.output.publicPath = '/';
// config.output.path = path.join(appRootDir, env.SPA_BUILD_DST_PATH);
config.output.path = path.join(appRootDir, '/build/');
// if (!config.output.path) throw new Error('env.SPA_BUILD_DST_PATH must be setted');
// if (!devcli) require('mkdirp').sync(config.output.path);

// Ныжно для Hot module replacement
if (devcli) config.recordsPath = path.join(tmpDir, 'dev-cli-record-path.json');

config.resolve = { modules: [], extensions: [], alias: {} };
// config.resolve.modules.push('src');
// config.resolve.modules.push('src-core');
// config.resolve.modules.push('src-sites');
config.resolve.modules.push('node_modules');
config.resolve.extensions.push('.js');
config.resolve.extensions.push('.jsx');
config.resolve.extensions.push('.json');
config.resolveLoader = { alias: {} };
// config.resolveLoader.alias.overrider = path.join(__dirname, "./loaders/styles-override-loader");
config.resolveLoader.alias['joint-loader'] = path.join(__dirname, "./loaders/joint-loader");
config.resolveLoader.alias['json-schema-loader'] = path.join(__dirname, "./loaders/json-schema-loader");
// config.resolveLoader.alias['glob-loader'] = path.join(__dirname, "./loaders/glob-loader");
// config.resolveLoader.alias['sass-to-js-var-loader'] = 'sass-vars-to-js-loader';

config.module = { rules: [] };
/* false && config.module.rules.push({
 test: /\.js$|\.jsx$/,
 exclude: /(node_modules|bower_components)/,
 enforce: 'pre',
 use: ['glob-loader']
 }); */
config.module.rules.push({
  test: /\.js$|\.jsx$/,
  exclude: /(node_modules|bower_components)\/(?!(unique-string|crypto-random-string)\/)/,
  use: [babelLoader, dev && eslintLoader], // 'import-glob', // 'glob-loader',
  // sideEffects: false // maybe need remove
});

const joint = !devcli && 'joint-loader?source=true';

config.module.rules.push({
  test: /\.schema.json$/,
  use: [joint, 'jsonSchema?ajvTest=true'] // &name=schema/[name].[ext]
});

// config.module.rules.push({ test: /\.json$/, use: [joint, 'json-loader'] });
config.module.rules.push({ test: /\.ya?ml$/, use: [joint, 'yml-loader'], });
config.module.rules.push({
  test: /\.dot$/, use: [
    "dot-tpl-loader",
    "extract-loader",
    {
      loader: "html-loader",
      options: { interpolate: true, minimize: false, conservativeCollapse: false }
    }
  ]
});
/* config.module.rules.push({
 test: /\.html$/, loaders: [
 'file?name=st-html/[name].[ext]', // ' + hashFSName + '
 "extract-loader",
 "html-loader?attrs=script:src&interpolate&minimize=false&conservativeCollapse=false"
 // "html-loader?attrs=script:src"
 ]
 }); */

const ExtractLoader = function (customLoader) {
  return ExtractTextPlugin.extract({
    publicPath: '',
    fallback: 'style-loader?sourceMap=true&convertToAbsoluteUrls=true',
    use: [joint,
      // (prod || devssr) && 'overrider',
      'css-loader?importLoaders=1&localIdentName=[local]&sourceMap=true',
      {
        loader: 'postcss-loader',
        options: {
          // sourceMap: !noCssSourceMaps,
          sourceMap: true,
          plugins: postcssPlugins
        }
      },
      'resolve-url-loader?keepQuery=true&sourceMap=true',
      customLoader
    ].filter(it => it)
  });
};

config.module.rules.push({
  test: /\.css$/, use: ExtractLoader()
});
config.module.rules.push({
  test: /\.less$/,
  use: ExtractLoader('less-loader?outputStyle=expanded&sourceMap=true&sourceMapContents=true')
});
config.module.rules.push({
  test: /\.scss$/,
  use: ExtractLoader('sass-loader?outputStyle=expanded&sourceMap=true&sourceMapContents=true')
});

config.module.rules.push({
  test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
  use: [joint,
    "url-loader?" + (!app && 'limit=10000&') + `mimetype=application/font-woff&name=${assetsFsPath}fonts/${fsFileName}.[ext]`]
});
config.module.rules.push({
  test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
  use: [joint,
    "url-loader?" + (!app && 'limit=10000&') + `mimetype=application/font-woff2&name=${assetsFsPath}fonts/${fsFileName}.[ext]`]
});
config.module.rules.push({
  test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
  use: [joint,
    "url-loader?" + (!app && 'limit=10000&') + `mimetype=application/octet-stream&name=${assetsFsPath}fonts/${fsFileName}.[ext]`]
});
config.module.rules.push({
  test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
  use: [joint,
    "url-loader?" + (!app && 'limit=10000&') + `mimetype=application/vnd.ms-fontobject&name=${assetsFsPath}fonts/${fsFileName}.[ext]`]
});
config.module.rules.push({
  test: /\.svg$/,
  issuer: /\.js$|\.jsx$/,
  use: [joint,
    'svg-react-loader', {
      loader: 'svgo-loader',
      options: {
        plugins: [
          { removeAttrs: { "attrs": ["data-name", "svg:height", "svg:width"] } },
          { removeTitle: true },
          { removeXMLNS: true }, // removes xmlns attribute (for inline svg, disabled by default)
          { convertPathData: true },
          {
            cleanupIDs: {
              prefix: {
                toString() {
                  this.counter = this.counter || 0;
                  return `id-${this.counter++}`;
                }
              }
            }
          }]
      }
    }]
});
config.module.rules.push({
  test: /\.svg$/,
  issuer: /\.(css|less|scss)$/,
  use: [
    joint,
    "svg-url-loader?" + (!app && 'limit=10000&') + `&name=${assetsFsPath}svg/${fsFileName}.[ext]`,
    {
      loader: 'svgo-loader',
      options: {
        plugins: [
          { removeAttrs: { "attrs": ["data-name", "svg:height", "svg:width"] } },
          { removeTitle: true },
          // { removeXMLNS: true }, // removes xmlns attribute (for inline svg, disabled by default)
          { convertPathData: true },
          { cleanupIds: true }
        ]
      }
    }
  ]
});

config.module.rules.push({
  test: /\.(jpe?g|png|gif)$/,
  // loader: "joint!url?" + (!app && 'limit=1024') + "&name=images/[name]' + hashFSName + '.[ext]"
  use: [joint, // 'srcset-loader',
    'url-loader?' + (!app && 'limit=4096') + `&name=${assetsFsPath}images/${fsFileName}.[ext]`]
  // '!image-maxsize-webpack-loader'
  // '!image-webpack-loader?{optimizationLevel: 7, interlaced: true, quality: 77, pngquant: {quality: "65-90", speed: 4}}' +
  // + require('postcss-srcset').loader,
  //'file?hash=sha512&digest=hex&name=' + hashFSName + '.[ext]',
  // 'image-webpack?{optimizationLevel: 7, interlaced: true, quality: 77, pngquant: {quality: "65-90", speed: 4}}'
});

config.module.rules.push({ test: /\.po$/, use: [joint, 'json-loader', 'po-gettext-loader'] });

config.module.rules.forEach(rule => rule.use = rule.use.filter(it => it));

config.plugins = [];
// config.plugins.push(new (require('webpack-runtime-public-path-plugin'))({ runtimePublicPath: 'window.__WEBPACK_RUNTIME_PUBLIC_PATH__' }));
config.plugins.push(new (require('simple-progress-webpack-plugin'))());
config.plugins.push(new (require('friendly-errors-webpack-plugin'))({
  compilationSuccessInfo: {
    messages: [`You application is running here http://localhost:${env.DEV_CLI_PORT}\n`],
    // notes: ['Some additionnal notes to be displayed unpon successful compilation']
  },
}));
config.plugins.push(new (require('html-webpack-plugin'))({
  filename: './index.html',
  title: '',
  baseFontSize: 14, // appTv && 32 || projectConfig.baseFontSizePx || 14,
  appMountId: "react-mount-node",
  cache: true,
  template: './src-core/assets/index.html.dot',
  inlineSource: false && '.(js|css)$', // embed all javascript and css inline
  minify: false,
  inject: true,
  chunksSortMode: 'dependency'
}));

config.plugins.push(new (require("webpack-stats-plugin").StatsWriterPlugin)({
  fields: null,
  filename: "node_modules.json",
  transform(stats) {
    const main = require('lodash/find')(stats.chunks, { entry: true, initial: true });
    // const regexpModulesOnly = new RegExp(/.+\/node_modules\/(.+?)\/.+/);
    // const regexpModulesPath = new RegExp(/.+\/node_modules\/(.+)/); //?)\/.+/);
    const regexpModulesPath = new RegExp(/.+\/node_modules\/([\w+|\-|\/|.]+)/);

    let modules = main.modules.map(({ identifier }) => {
      const some = regexpModulesPath.exec(identifier);
      if (some) return some[1];
    }).filter(it => it);
    modules = require('uniq')(modules);
    const packageJsonDeps = require(path.join(appRootDir, './package.json')).dependencies;
    const packageJsonDepsArr = Object.keys(packageJsonDeps);
    const startsWith = require('lodash/startsWith');
    modules = modules.filter(path => packageJsonDepsArr.some(dep => startsWith(path, dep)));
    // modules = require('lodash/intersection')(modules, packageJsonDepsArr);
    return JSON.stringify(modules, null, 2);
  }
}));

/* config.plugins.push(new (require('autodll-webpack-plugin'))({
 inject: true, // will inject the DLL bundles to index.html
 filename: config.output.filename,
 context: config.context,
 entry: {
 vendor: [
 'react',
 'react-dom'
 ]
 },
 plugins: [
 new (require('uglifyjs-webpack-plugin'))({
 cache: false,
 test: /\.js($|\?)/i,
 parallel: true,
 sourceMap: true,
 uglifyOptions: {
 safari10: true,
 ecma: 8,
 // compress: { dead_code: true, }
 }
 })
 ],
 inherit: true
 })); */
config.plugins.push(new (require('html-webpack-inline-source-plugin')));
config.plugins.push(new (require('html-beautify-webpack-plugin'))());
config.plugins.push(new ExtractTextPlugin({
  filename: `${assetsFsPath}${fsFileName}.css`, allChunks: true, disable: devcli
}));

/* config.plugins.push(new (require('directory-tree-webpack-plugin'))({
 dir: './src-sites/',
 path: './src-sites/struct.json',
 // extensions: /\.js/
 })); */

// Оптимизирует Медиаправила в CSS
if (!devcli) config.plugins.push(new (require('postcss-assets-webpack-plugin'))({
  test: /\.css$/,
  log: false,
  plugins: [
    // Pack same CSS media query rules into one media query rule
    require('css-mqpacker')
  ]
}));

// Оптимизирует CSS
if (!devcli) config.plugins.push(new (require('optimize-css-assets-webpack-plugin'))({
  assetNameRegExp: /\.css$/,
  cssProcessor: require('cssnano'),
  cssProcessorOptions: {
    discardDuplicates: { removeAll: true },
    safe: true,
    svgo: false,
    map: { inline: false }
  },
  canPrint: false
}));

// Указывает всему вебпаку минимизировать и оптимизировать сборку
if (prod) config.plugins.push(new webpack.LoaderOptionsPlugin({ minimize: true }));
if (prod) config.plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
if (dev) config.plugins.push(new webpack.NamedModulesPlugin());
if (prod) config.plugins.push(new (require('uglifyjs-webpack-plugin'))({
  cache: false,
  test: /\.js($|\?)/i,
  parallel: true,
  sourceMap: true,
  uglifyOptions: {
    safari10: true,
    ecma: 8,
    // compress: { dead_code: true, }
  }
}));

const release = new Date().toISOString();

if (devcli) config.plugins.push(new webpack.HotModuleReplacementPlugin());
const maybeDynamicVariables = {
  __RELEASE__: JSON.stringify(release),
  /* __GIT_COMMIT__: JSON.stringify(gitCommit),
   __GIT_BRANCH__: JSON.stringify(gitBranch), */
  __API_URL__: JSON.stringify(''),
  /* __SENTRY_POINT__: JSON.stringify(sentryPoint), */
};

if (env.SPA_DYNAMIC_VARIABLES) {
  const dynVarsArr = env.SPA_DYNAMIC_VARIABLES.split(',').filter(it => it);
  const usedDynVars = require('lodash/intersection')(dynVarsArr, Object.keys(maybeDynamicVariables));
  usedDynVars.forEach(key => maybeDynamicVariables[key] = `"__DYNAMIC__${key}"`);
}

config.plugins.push(new webpack.DefinePlugin(require('lodash/assign')({
  'process.env': {
    NODE_ENV: JSON.stringify(prod ? 'production' : 'development')
  },
  __CLIENT__: true,
  __SERVER__: false,
  __DEV_CLI__: !!devcli,
  __DEVELOPMENT__: !prod,
  __APP__: app,
  __AMP__: false,
  // __PUBLIC_PATH__: `(${runtimePublicPath} || "${config.output.publicPath}")`,
  __PUBLIC_PATH__: config.output.publicPath,
  __DEFAULT_LOGIN__: JSON.stringify(env.SPA_DEV_DEFAULT_LOGIN),
  __DEFAULT_PASSWORD__: JSON.stringify(env.SPA_DEV_DEFAULT_PASSWORD),
  // __DEV_VERIFY_SSR_RERENDER__: (global.VERIFY_SSR || false), // && !app && (devssr || devprod),

}, maybeDynamicVariables)));

config.plugins.push(new (require('circular-dependency-plugin'))({
  exclude: /node_modules/,
  failOnError: true
}));

// ####### Safe Delete unneded hashed files
!devcli && hashLen && config.plugins.push(new (require('./plugins/safe-clean.plugin'))(hashLen));

!devcli && config.plugins.push(new (require('assets-webpack-plugin'))({
  path: config.output.path,
  filename: 'assets.json',
  prettyPrint: true,
  update: true,
  // fullPath: true,
}));

// !devcli && config.plugins.push(new (require('./loaders/joint-loader')).master());

// if(devcli) config.plugins.push(new (require('npm-install-webpack-plugin'))());

module.exports = config;