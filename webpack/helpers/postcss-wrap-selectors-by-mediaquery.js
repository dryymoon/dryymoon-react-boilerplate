/**
 * Created by dryymoon on 09.12.16.
 */
const postcss = require('postcss');

// plugin
module.exports = postcss.plugin('postcss-dryymoon-wrap-selectors-by-mediaquery', function (options) {
  return (css) => {
    options = options || {};

    const matchSelector = options.match; // = :hover
    const mediaQuery = options.mediaquery; // = '(min-width:1000px), (hover) '
    if (!matchSelector || !mediaQuery) return;

    // walk each rule in the stylesheet
    css.walkRules((rule) => {
      if (rule.selector.indexOf(matchSelector) === -1) return;

      const selectors = rule.selector.replace(/\r?\n|\r/g, ' ').split(',');

      // Build new selector with mediaquery

      const selectorsWithHover = selectors
        .filter(
          function (selector) {
            return (selector.indexOf(matchSelector) !== -1);
          });

      const newRule = rule.cloneBefore();

      newRule.selector = selectorsWithHover.join(',');

      const newMediaRule = postcss.atRule({
        name: 'media',
        params: mediaQuery,
      });

      wrap(newRule, newMediaRule);

      // Delete or filter existed rule

      const selectorsWithOutHover = selectors
        .filter(function (sel) {
          return sel.indexOf(':hover') === -1;
        }).map(function (sel) {
          return sel.trim();
        });

      if (selectorsWithOutHover.length > 0) {
        rule.selector = selectorsWithOutHover.join(', ');
      } else {
        rule.remove();
      }

    });
  };
});

/*
@media (min-width:1000px), hover{
.item__link:hover{
  text-decoration:none;
  box-shadow:inset 0 0 0 0.07142857142857142em #e6e6e6, 0 0.14285714285714285em 0.42857142857142855em 0 rgba(0, 0, 0, .15);
} ==>
.item__link:hover{
    text-decoration:none;
    box-shadow:inset 0 0 0 0.07142857142857142em #e6e6e6, 0 0.14285714285714285em 0.42857142857142855em 0 rgba(0, 0, 0, .15);
  }
}
================================================
.item__link:hover .item__developerLogoWrap,
.item__link:hover .item__topicContainer{
  opacity:1;
} ==>

@media (min-width:1000px), hover{

.item__link:hover .item__developerLogoWrap, .item__link:hover .item__topicContainer{
    opacity:1;
  }
}

*/

function wrap(rule, wrappingWith) {
  rule.replaceWith(wrappingWith);
  wrappingWith.append(rule);
  wrappingWith.source = rule.source;
}
