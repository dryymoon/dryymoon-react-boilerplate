const fs = require('fs');
const path = require('path');

const rootDir = path.join(__dirname, '../../');

const isDirectory = source => fs.lstatSync(source).isDirectory() || fs.lstatSync(source).isSymbolicLink();
const getDirectories = source => fs.readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

const sitesRootDir = path.join(rootDir, './src-sites/');
const sitesNames = getDirectories(sitesRootDir)
  .map(absPath => path.basename(absPath)) //.split('/').pop())
  .sort()
  .filter(name => name && (name[0] !== '_'))
  .filter(name => name && (name[0] !== '.'));

module.exports = {
  rootDir, sitesRootDir, sitesNames
};
