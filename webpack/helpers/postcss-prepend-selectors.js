/**
 * Created by dryymoon on 22.04.2018.
 */
var postcss = require('postcss');

module.exports = postcss.plugin('postcss-prepend-selector', function (opts) {
  opts = opts || {};
  return function (css) {
    let pluginEnabled = true;
    css.walk(some => {
      if (!some) return;
      if (some.type === 'comment' && some.text.includes('postcss-prepend-selectors-disable')) {
        pluginEnabled = false;
        return some.remove();
      }
      if (some.type === 'comment' && some.text.includes('postcss-prepend-selectors-enable')) {
        pluginEnabled = true;
        return some.remove();
      }
      if (pluginEnabled && some.type === 'rule') {
        some.selectors = some.selectors.map(function (selector) {
          if (/^([0-9]*[.])?[0-9]+\%$|^from$|^to$/.test(selector)) {
            // This is part of a keyframe
            return selector;
          }

          if (selector.startsWith('html')) return selector;
          if (selector.startsWith('body')) return selector;

          if (selector.startsWith(opts.selector.trim())) {
            return selector;
          }

          return opts.selector + selector;
        });
      }
    })
  };
});