/**
 * Created by dryymoon on 25.05.17.
 */

const fs = require('fs');
const appRootDir = require('app-root-dir').get();
const env = require('node-env-file');

const dirArr = appRootDir.split('/');

do {
  const path = dirArr.join('/');
  const file = path + '/.env';
  if (fs.existsSync(file)) {
    env(file, { verbose: false, overwrite: false, raise: false, logger: console });
  }
} while (dirArr.pop());

const pathDefault = appRootDir + '/.env.default';

if (fs.existsSync(pathDefault)) {
  env(pathDefault, { verbose: false, overwrite: false, raise: false, logger: console });
}

module.exports = process.env;

/* console.log('########');
Object.keys(process.env).forEach(function(key){
  if(key[0] !== 'n') {
    console.log(key, process.env[key]);
  }
});
console.log('########');
*/
