#!/usr/bin/env bash
# TODO: Update Sentry domain
HOST=domain.com
ORG=organization
PROJECT=project
RELEASE=$1
TOKEN=7826772932bb4be9b798406723da6d4a538ed560946a4786a222886b6fde692b

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../../" && pwd )"
cd $DIR

if [ -z $RELEASE ]; then
   echo "You must specify a version"
   exit 1;
   fi

curl http://$HOST/api/0/projects/$ORG/$PROJECT/releases/ \
  -X POST \
  -H "Authorization: Bearer $TOKEN" \
  -H 'Content-Type: application/json' \
  -d "{\"version\": \"$RELEASE\"}"

FILES='*.js.map *.js'
cd ./build-app/assets/
for f in $FILES
do
  echo "Processing $f file..."
  curl http://$HOST/api/0/projects/$ORG/$PROJECT/releases/$RELEASE/files/ \
    -X POST \
    -H "Authorization: Bearer $TOKEN" \
    -F file=@$f \
    -F name="sentry://./$f"
  done

cd ./../
for f in *.html
do
curl http://$HOST/api/0/projects/$ORG/$PROJECT/releases/$RELEASE/files/ \
  -X POST \
  -H "Authorization: Bearer $TOKEN" \
  -F file=@$f \
  -F name="$f"
  done

cd ./../
