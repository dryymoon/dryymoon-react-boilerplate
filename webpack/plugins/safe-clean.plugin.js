/**
 * Created by dryymoon on 16.04.2018.
 */
const fs = require('fs');
const path = require('path');
const escapeRegExp = require('lodash/escapeRegExp');

function WebpackSafeDeletePlugin(hashLen) {
  this.hashLen = hashLen;
};

WebpackSafeDeletePlugin.prototype.apply = function (compiler) {
  compiler.plugin('done', safeDelete.bind(this));
};

module.exports = WebpackSafeDeletePlugin;

function safeDelete(stats) {
  const context = stats.compilation.options.context;
  const hashLen = this.hashLen;
  const fileHash = stats.hash.substring(0, hashLen);
  const assets = stats.compilation.assets;
  const hashedAssets = Object
    .keys(assets)
    .map((key) => {
      const asset = assets[key];
      if (!asset.emitted) return;
      if (!asset.existsAt) return;
      const filePath = path.resolve(context, asset.existsAt);
      if (path.basename(filePath).indexOf(fileHash) > -1) return filePath;
    })
    .filter(it => it);
  hashedAssets.forEach((assetPath) => {
    const dir = path.dirname(assetPath);
    const file = path.basename(assetPath);
    const re = new RegExp(escapeRegExp(file).replace(fileHash, '.'.repeat(hashLen)) + '$');
    fs.readdirSync(dir).map((existedFile) => {
      if (existedFile.match(re) && (existedFile !== file)) {
        const deletePath = path.join(dir, existedFile);
        console.log('Deleted Old File: \x1b[31m%s\x1b[0m', deletePath);
        fs.unlinkSync(deletePath);
      }
    });
  });
}