/**
 * Created by dryymoon on 16.04.2018.
 */

// Плагин ловит ассеты и пишет их в файл
!app && (prod || devssr) && config.plugins.push(new (require('assets-webpack-plugin'))({
  filename: 'assets.json',
  prettyPrint: true,
  path: path.join(config.output.path, '../'), //projectConfig.buildDir,
  assetsRegex: /\.(jpe?g|png|gif|svg|css|js)$/,
  fullPath: true,
  processOutput: (assets) => {
    var obj = {
      webPath: config.output.publicPath, // assetsUrl,
      fsPath: `./${path.relative(path.join(config.output.path, '../'), config.output.path)}/`,
      css: {},
      js: {}
    };
    Object.keys(assets)
      .reverse()
      .filter(it => ['main', 'common', 'boot'].indexOf(it) > -1)
      .forEach((key) => {
        if (assets[key].css) {
          obj.css[key] = {
            url: assets[key].css,
            path: obj.fsPath + assets[key].css.slice(obj.webPath.length)
          };
        }
        if (assets[key].js) {
          obj.js[key] = {
            url: assets[key].js,
            path: obj.fsPath + assets[key].js.slice(obj.webPath.length)
          };
        }
      });
    // TODO Тупая заглушка
    var jobj = JSON.stringify(obj, null, 2);
    var pathFile = path.join(__dirname, '../src-core/assets.json');
    Joint.emit(pathFile, `module.exports=${jobj}`);
    // fs.writeFileSync(pathFile, jobj);
    return jobj;
  }
}));