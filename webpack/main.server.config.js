/**
 * Created by dryymoon on 14.04.2018.
 */
// Webpack config for creating the server production code.
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const appRootDir = require('app-root-dir').get();
const tmpDir = path.join(appRootDir, '.tmp');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const projectConfig = require(path.join(appRootDir, 'config'));
const nodeExternals = require('webpack-node-externals');
// const WebpackNotifierPlugin = require('webpack-notifier');
const env = require('./helpers/cascaded-env');

// const clientConfig = require('./main.client.config');

/*
 * ******************************
 * Конфигурим сборку
 * ******************************
 */

module.exports = function (preConfig) {

  let {
    clientConfig,
    sitesRootDir,
    siteName,
    srcHash,
    coreHash,
    webpackHash,
    rootDomPoint,
  } = preConfig;

  rootDomPoint = rootDomPoint || 'react-root-default'; // eslint-disable-line

  let dev = env.NODE_ENV === 'development';
  let devcli = dev && !!env.WEBPACK_BUILD_TYPE_DEVCLI;
  const app = false;
  const appTv = false;
  const prod = !dev;

// console.log('Is server for PROD? ', prod);

  const babelConfig = {
    // cacheDirectory: prod && path.join(tmpDir, "prod-server-babel-cache")
    // || dev && path.join(tmpDir, "dev-ssr-server-babel-cache"),
    sourceMaps: true,
    presets: ["react-optimize", "react", ["env", {
      shippedProposals: true, // to support spread operators
      forceAllTransforms: true,
      targets: { node: "current", modules: false }
    }]],
    plugins: [
      "transform-export-default",
      // "transform-export-extensions", // test for srcset-loader
      // "transform-runtime",
      "transform-decorators-legacy",
      // "transform-react-display-name",
      // "transform-react-inline-elements",
      // "transform-inline-environment-variables",
      "syntax-object-rest-spread",
      // "transform-es2015-destructuring",
      // "transform-es2015-parameters",
      // "transform-es2015-sticky-regex",
      // "transform-es2015-unicode-regex",
      // "transform-strict-mode",
      // "transform-object-rest-spread",
      // "array-includes",
      // "transform-exponentiation-operator",
      // "transform-async-to-generator",
      // "syntax-trailing-function-commas",
      "transform-class-properties",
      "system-import-transformer",
    ]
  };

  // babelConfig.plugins.push("import-glob");
  babelConfig.plugins.push(path.join(__dirname, "./plugins/babel-plugin-import-glob.js"));

  const babelLoader = `babel-loader?${JSON.stringify(babelConfig)}`;

  const config = {};
  config.mode = clientConfig.mode;
  config.context = clientConfig.context;
  config.target = 'node';
  config.devtool = clientConfig.devtool;
  config.stats = clientConfig.stats;
  config.entry = [];
// config.entry.push('./src/helpers/react/ReactCachePatch.js');
// config.entry.push('normalize.css');
  config.entry.push(path.join(__dirname, './helpers/cascaded-env'));
  config.entry.push('./src-core/server.js');
  config.node = {
    global: true,
    __dirname: false,
    __filename: false,
    process: true,
    Buffer: true
  };

// Нужная штука, позволяет не все фигачить в сервак, нужно для ЕКСПРЕСС, иначе не летит...
  config.externals = [nodeExternals({
    whitelist: [/\.(css|less|scss)$/]
  })];

  config.output = {};
  config.output.path = clientConfig.output.path;

  config.output.filename = '../../backend.js';
  config.output.publicPath = clientConfig.output.publicPath; // '/assets/'; // projectConfig.assetsUrl;
  config.resolve = { modules: [], extensions: [], alias: {} };
// config.resolve.alias.leaflet = 'leaflet-headless';
// config.resolve.alias['leaflet-headless/dist/leaflet.css'] = 'leaflet/dist/leaflet.css';

// config.resolve.modules.push('src');
// config.resolve.modules.push('src-core');
// config.resolve.modules.push('src-sites');
  config.resolve.modules.push('node_modules');
  config.resolve.extensions.push('.js');
  config.resolve.extensions.push('.jsx');
  config.resolve.extensions.push('.json');
  config.resolveLoader = { alias: {}, modules: [] };
  // config.resolveLoader.alias['file-loader'] = 'file-loader-not-emit-file';
  // config.resolveLoader.alias['url-loader'] = 'url-loader-not-emit-file';
  config.resolveLoader.alias['joint-loader'] = path.join(__dirname, "./loaders/joint-loader");
  config.resolveLoader.alias['json-schema-loader'] = path.join(__dirname, "./loaders/json-schema-loader");
  config.resolveLoader.modules.push(path.join(appRootDir, './node_modules/'));
  config.resolveLoader.modules.push(path.join(sitesRootDir, siteName, './node_modules/'));

  config.module = { rules: [] };
  config.module.rules.push({ test: /\.js$|\.jsx$/, exclude: /(node_modules|bower_components)/, use: [babelLoader] });
  const joint = `joint-loader?slave=true&instance=${siteName}`;
  config.module.rules.push({ test: /\.schema\.json$/, use: [joint] });
  // config.module.rules.push({ test: /\.json$/, use: [joint, 'json-loader'], type: "javascript/auto" });
  config.module.rules.push({ test: /\.json$/, use: [joint], type: "javascript/auto" });
  config.module.rules.push({ test: /\.dot$/, use: [joint] });
  // config.module.rules.push({ test: /\.(css|less|scss)$/, use: ExtractTextPlugin.extract({ use: [joint] }) });
  config.module.rules.push({
    test: /\.(css|less|scss)$/, use: [
      // dev && 'style-loader?sourceMap=true&convertToAbsoluteUrls=true',
      // prod &&
      MiniCssExtractPlugin.loader,
      joint
    ]
  });
  config.module.rules.push({ test: /\.po$/, use: [joint] });
  config.module.rules.push({ test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, use: [joint] });
  config.module.rules.push({ test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, use: [joint] });
  config.module.rules.push({ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [joint] });
  config.module.rules.push({ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [joint] });
  config.module.rules.push({ test: /\.svg$/, issuer: /\.js$|\.jsx$/, use: [joint] });
  config.module.rules.push({ test: /\.svg$/, issuer: /\.(css|less|scss)$/, use: [joint] });
  config.module.rules.push({ test: /\.(jpe?g|png|gif)$/, use: [joint] });
//config.module.rules.push({ test: /\.(ico)$/, loader: "joint!file" });

  config.module.rules.forEach(rule => rule.use = rule.use.filter(it => it));

// config.stats = { children: true, errorDetails: true };
  config.plugins = [];

  config.plugins.push(new (require('simple-progress-webpack-plugin'))({ format: 'minimal' }));
  config.plugins.push(new (require('friendly-errors-webpack-plugin'))({
    compilationSuccessInfo: {
      // messages: [`You application is running here http://localhost:${env.DEV_CLI_PORT}\n`],
      // notes: ['Some additionnal notes to be displayed unpon successful compilation']
    },
  }));

// Чтоб сервак был одним файлом
// config.plugins.push(new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }));

// Плагин покажет когда собрался БЕКЕНД
  /* config.plugins.push(new WebpackNotifierPlugin({
   title: 'BackEnd',
   contentImage: path.join(__dirname, './helpers/back.png')
   })); */

  const sentryPoint = env.SPA_SENTRY_PRIVATE_POINT || projectConfig.sentryPrivatePoint || null;

// Плагин заменяет одно на другое
  config.plugins.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(prod ? 'production' : 'development')
    },
    __CLIENT__: false,
    __SERVER__: true,
    __SRC_SITE_FOLDER__: JSON.stringify(siteName),
    __ROOT_DOM_POINT__: JSON.stringify(rootDomPoint),
    __DEV_CLI__: false,
    __DEVELOPMENT__: !prod,
    __APP__: false,
    __AMP__: false,
    __SENTRY_PRIVATE_POINT__: JSON.stringify(sentryPoint)
  }));

  config.plugins.push(
    new webpack.NormalModuleReplacementPlugin(/(.*)__SRC_SITE_FOLDER__(\.*)/, (resource) => {
      resource.request = resource.request.replace(/__SRC_SITE_FOLDER__/, siteName);
    })
  );

  if (dev) config.plugins.push(new webpack.NamedModulesPlugin());
  if (prod) config.plugins.push(new webpack.HashedModuleIdsPlugin());

  // prod &&
  config.plugins.push(new MiniCssExtractPlugin({
    filename: '../../backend.css', allChunks: true,
  }));

  // Оптимизирует Медиаправила в CSS
  if (!devcli) config.plugins.push(new (require('postcss-assets-webpack-plugin'))({
    test: /\.css$/, log: false, plugins: [require('css-mqpacker')]
  }));

  config.plugins.push(
    new webpack.ProvidePlugin({
      Raven: "raven"
    })
  );

  config.plugins.push(new (require('circular-dependency-plugin'))({
    exclude: /node_modules/,
    failOnError: true
  }));

  const corePackageJson = require(path.join(appRootDir, './package.json'));
  const sitePackageJson = require(path.join(appRootDir, 'src-sites', siteName, './package.json'));
  const mergedPackageJson = JSON.parse(require('merge-package-json')({
    dependencies: corePackageJson.dependencies
  }, sitePackageJson));

  config.plugins.push(new (require('generate-json-webpack-plugin'))(
    '../../package.json',
    require('sort-package-json')({
      ...mergedPackageJson,
      main: "./backend.js",
      srcHash,
      coreHash,
      webpackHash
    }),
    null, '\t'
  ));

  return config;
};
//module.exports = config;
