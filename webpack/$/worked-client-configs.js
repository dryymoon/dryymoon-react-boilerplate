/**
 * Created by dryymoon on 22.04.2018.
 */

// Подмена имен стилей, конфигурируется в глобальном конфиге в корне проекта.

// Нужная хрень для переписывалки названия классов, сначала они туда собираются потом вытягиваются оттуда.
// config.styleOverrider = {};
// config.styleOverrider.exclude = (/(node_modules|bower_components)/);


/* (prod || devssr) && postcssPlugins.push(
 require('postcss-modules')({
 scopeBehaviour: 'local',
 generateScopedName: function (className, filename, cssFileContents) {
 if ((/(node_modules|bower_components)/).test(filename)) return className;
 if (overrideStyleClasses[className]) return overrideStyleClasses[className];
 if (projectConfig.joke) {
 stopWords = arrUnique(stopWords.map(function (it) {
 return it.replace(/ /g, "");
 }));
 var newName = stopWords.splice(Math.floor(Math.random() * stopWords.length), 1);
 return overrideStyleClasses[className] = newName[0];
 }
 if (obfuscateStyles) {
 return overrideStyleClasses[className] = hashids.encode(Object.keys(overrideStyleClasses).length)
 }
 return overrideStyleClasses[className] = className;
 },
 getJSON: function (cssFileName, json) {
 Joint.emit(cssFileName, `module.exports=${require('tosource')(require('sort-object')(json))};`);
 }
 })); */

// Normalize css
// postcssPlugins.push(require('postcss-normalize')({ forceImport: true }));

// Удаляет нах коментарии
// postcssPlugins.push(require('postcss-discard-comments')({ removeAll: true }));




false && config.plugins.push(new (require("webpack-stats-plugin").StatsWriterPlugin)({
  fields: null,
  filename: "node_modules.json",
  transform(stats) {
    const main = require('lodash/find')(stats.chunks, { entry: true, initial: true });
    // const regexpModulesOnly = new RegExp(/.+\/node_modules\/(.+?)\/.+/);
    // const regexpModulesPath = new RegExp(/.+\/node_modules\/(.+)/); //?)\/.+/);
    const regexpModulesPath = new RegExp(/.+\/node_modules\/([\w+|\-|\/|.]+)/);

    let modules = main.modules.map(({ identifier }) => {
      const some = regexpModulesPath.exec(identifier);
      if (some) return some[1];
    }).filter(it => it);
    modules = require('uniq')(modules);
    const packageJsonDeps = require(path.join(appRootDir, './package.json')).dependencies;
    const packageJsonDepsArr = Object.keys(packageJsonDeps);
    const startsWith = require('lodash/startsWith');
    modules = modules.filter(path => packageJsonDepsArr.some(dep => startsWith(path, dep)));
    // modules = require('lodash/intersection')(modules, packageJsonDepsArr);
    return JSON.stringify(modules, null, 2);
  }
}));


config.plugins.push(new (require("webpack-stats-plugin").StatsWriterPlugin)({
  filename: "stats.json",
}));

!devcli && config.plugins.push(new (require('assets-webpack-plugin'))({
  path: config.output.path,
  filename: '../assets.json',
  prettyPrint: true,
  update: true,
  // fullPath: true,
}));

// if(devcli) config.plugins.push(new (require('npm-install-webpack-plugin'))());

// !devcli && config.plugins.push(new (require('./loaders/joint-loader')).master());

// if(devcli) config.plugins.push(new (require('npm-install-webpack-plugin'))());

// ####### Safe Delete unneded hashed files
// !devcli && hashLen && config.plugins.push(new (require('./plugins/safe-clean.plugin'))(hashLen));

/* config.module.rules.push({
 test: /\.html$/, loaders: [
 'file?name=st-html/[name].[ext]', // ' + hashFSName + '
 "extract-loader",
 "html-loader?attrs=script:src&interpolate&minimize=false&conservativeCollapse=false"
 // "html-loader?attrs=script:src"
 ]
 }); */
