/**
 * Created by dryymoon on 26.01.17.
 */

import PouchDB from 'pouchdb-core';
import IndexedDB from 'pouchdb-adapter-idb';
import WebSQL from 'pouchdb-adapter-websql';
import UpSert from 'pouchdb-upsert';

PouchDB.plugin(IndexedDB);
PouchDB.plugin(WebSQL);
PouchDB.plugin(UpSert); // https://github.com/pouchdb/upsert

export default PouchDB;
