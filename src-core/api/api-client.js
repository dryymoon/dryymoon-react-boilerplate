/* eslint-disable max-len */
import axios, { Cancel, CancelToken } from 'axios'; // axios,
import urlLib from 'url';
import forOwn from 'lodash/forOwn';
import isObject from 'lodash/isObject';
import flattenDeep from 'lodash/flattenDeep';
import mapValues from 'lodash/mapValues';
import uniqueString from 'unique-string'; // eslint-disable-line
import Promise from '../helpers/cancelable-promise';

const httpMethods = ['get', 'post', 'put', 'delete', 'patch'];

const $channels = Symbol('apiChannels');
const $queues = Symbol('apiQueues');

export default class ApiClient {
  constructor(url, ...middlewares) {
    this.url = url;

    this.axios = axios;

    this.middlewares = flattenDeep(middlewares).filter(it => it);

    this.cache = new ApiCache();

    this[$queues] = {};

    this[$channels] = {};

    this._pendingRequests = {};

    this.methods = [];

    this.applyMiddlewares = this.applyMiddlewares.bind(this);
    // const apiUrl = urlLib.parse(rawApiUrl);

    // const prependUrlPath = apiUrl.pathname;
    /* const maybePrependUrlHost = urlLib.format({
     protocol: apiUrl.protocol,
     host: apiUrl.host,
     // pathname: apiUrl.pathname
     }); */

    // const { pathname: prependUrlPath, } = urlLib.parse(apiUrl);
    // const maybeUrlHostPort =

    httpMethods.forEach(method => this.createApiMethod(method));

    this.prependMiddleware = this.prependMiddleware.bind(this);
    this.appendMiddleware = this.appendMiddleware.bind(this);
    this.setUrl = this.setUrl.bind(this);
    this.cancelAllRequests = this.cancelAllRequests.bind(this);
    this.createApiMethod = this.createApiMethod.bind(this);
  }

  createApiMethod(method) {
    if (this[method]) return;

    this.methods.push(method);

    const self = this;

    this[method] = (requestPath, options = {}) => {
      const {
        postData,
        sync = false,
        queryString = {},
        config: extendedConfig = {},
        force = false,
        syncFromCache = false,
        // forceFromCache = false,
        cancelable,
      } = options;

      let { channel } = options;

      /* let {
       channel: { name: channelName, ...channelOpts } = {},
       } = options; */
      // queryString

      const requestId = uniqueString(32);

      const config = {
        requestId,
        baseURL: self.url, // maybePrependUrlHost,
        // url,
        // path: urlJoin(config.baseURL, config.url),
        method,
        timeout: 60000,
        ...extendedConfig,
      };

      const { pathname, query = {} } = urlLib.parse(requestPath, true);
      // parsedUrl.
      const formattedRequestUrl = urlLib.format({
        pathname, query: { ...query, ...queryString }
      });

      config.url = formattedRequestUrl;
      const cacheKey = formattedRequestUrl;

      // Maybe in cache
      if (method === 'get' && (self.cache.exists(cacheKey)) && !force) {
        if (__DEVELOPMENT__) {
          // eslint-disable-next-line no-console
          console.info(`Api #${requestId} cached answer ${method.toUpperCase()}::${cacheKey}`);
        }
        const answer = self.cache.get(cacheKey);

        if (sync) return answer;

        if (cancelable) return { promise: Promise.resolve(answer) };

        return Promise.resolve(answer);
      }

      if (sync) return undefined;

      if (method === 'get' && syncFromCache) return self.cache.get(cacheKey);

      if (__DEVELOPMENT__) {
        // eslint-disable-next-line no-console
        console.info(`Api #${requestId} REQUESTED ${method.toUpperCase()}::${config.url}`);
      }

      // `params` are the URL parameters to be sent with the request
      // Must be a plain object or a URLSearchParams object
      // if (queryString) config.params = queryString;

      if (postData) config.data = postData;

      let cancelAxiosRequestFn = null;
      config.cancelToken = new CancelToken(c => cancelAxiosRequestFn = c);

      if (channel && !__CLIENT__) channel = undefined;

      if (isObject(channel)) {
        channel = channel.name;

        if (__DEVELOPMENT__) {
          // eslint-disable-next-line
          console.warn(`Api #${requestId} request channel object is deprecated, name is ${channel}, use channel=string`);
        }
      }

      if (channel && self[$channels][channel]) {
        // self[$channels][channel] = self[$channels][channel] || [];
        // self[$channels][channel].forEach(it => it.cancel());
        // self[$channels][channel] = [];
        self[$channels][channel].cancel();
      }

      const onCancel = () => {
        if (__DEVELOPMENT__) {
          // eslint-disable-next-line no-console
          console.info(`Api #${requestId} CANCELED  ${method.toUpperCase()}::${config.url}`);
        }
        cancelAxiosRequestFn();
      };

      const apiCall = Promise.resolve(self.applyMiddlewares(self.middlewares, 'req', config, options)
        .then(modifiedConfig => axios.request(modifiedConfig))
        .then((apiResult) => {
          self._pendingRequests[requestId] = undefined;
          delete self._pendingRequests[requestId];
          return self.applyMiddlewares(self.middlewares, 'res', apiResult, options);
        })
        .then(({ data, status }) => {
          if (channel && self[$channels][channel]) {
            // const reqIndexInChannel = findIndex(self[$channels][channel], { requestId });
            // self[$channels][channel].splice(reqIndexInChannel, 1);
            self[$channels][channel] = undefined;
          }

          if (status === 204) return;

          const { success, result: payload, $$expires } = data;

          if (!success) return Promise.reject(data);
          // if (status === 'error') return Promise.reject(data);

          if (method === 'get') {
            // let urlToCache = cacheKey;
            // if (cacheKey !== resUrl) urlToCache = urlLib.parse(resUrl).path;
            self.cache.set(cacheKey, payload, { expires: $$expires });
          }

          return payload;
        })
        .catch((apiErr) => {
          self._pendingRequests[requestId] = undefined;
          delete self._pendingRequests[requestId];

          if (channel && self[$channels][channel]) {
            // const reqIndexInChannel = findIndex(self[$channels][channel], { requestId });
            self[$channels][channel] = undefined; // .splice(reqIndexInChannel, 1);
          }
          if (apiErr instanceof Cancel) return Promise.reject(apiErr);
          return self
            .applyMiddlewares(self.middlewares, 'err', apiErr, options)
            .then(newApiErr => Promise.reject(newApiErr || apiErr));
        }), onCancel);

      if (channel) {
        self[$channels][channel] = { /* channelName: channel, requestId, config, apiCall, */ cancel: apiCall.cancel };
      }

      // self._pendingRequests[requestId] = apiCall;

      if (cancelable) {
        // eslint-disable-next-line
        if (__DEVELOPMENT__) console.warn('Api request with cancelable options is deprecated, use instead cancel function');
        return { cancel: apiCall.cancel, promise: apiCall };
      }

      return apiCall;
    };
  }

  cancelAllRequests() {
    mapValues(this._pendingRequests, reqPromise => reqPromise.cancel());
  }

  setUrl(url) {
    if (typeof (url) !== 'string') throw new Error('ApiClient url param must be string');
    this.url = url;
  }

  prependMiddleware(middleware) {
    this.middlewares.unshift(middleware);
  }

  appendMiddleware(middleware) {
    this.middlewares.push(middleware);
  }

  fillCache(Obj) {
    forOwn(Obj, (it, key) => {
      this.cache.set(key, it);
    });
  }

  getAllCache() {
    return this.cache.getAll();
  }

  enableExpireCheck() {
    this.cache.enableExpireCheck();
  }

  // eslint-disable-next-line class-methods-use-this
  awaitInit() {
    return Promise.resolve(this.applyMiddlewares(this.middlewares, 'init', this));
  }

  applyMiddlewares(middlewares = [], type, configurable, params) {
    if (middlewares.length === 0) return Promise.resolve(configurable);

    // return Promise.resolve();

    const [{ [type]: fn } = {}, ...rest] = middlewares;
    // if (!fn) return Promise.resolve(this.applyMiddlewares(rest, type, configurable, params));

    return Promise
      .resolve()
      .then(() => {
        if (fn) return fn.call(this, configurable, params);
      })
      .then(result => this.applyMiddlewares(rest, type, result || configurable, params));
    /*
     if (fn) return Promise.resolve()

     let configured = configurable;
     if (fn) configured = fn.call(this, configurable, params);
     if (configured === undefined) configured = configurable;

     return this.applyMiddlewares(rest, type, configured, params); */
  }
}

export class ApiCache {
  constructor() {
    this.expireCheck = false;
    this.cacheData = {};
    this.cacheExpires = {};
  }

  enableExpireCheck() {
    this.expireCheck = true;
  }

  set(key, it, { expires } = {}) {
    // const isNeedForCache = !this.expireCheck || isFresh(expires);

    // if (!isNeedForCache) return;

    this.cacheData[key] = it;
    this.cacheExpires[key] = expires;
  }

  get(key) {
    return this.cacheData[key];
  }

  exists(key) {
    const expires = this.cacheExpires[key];
    if (this.expireCheck && !isFresh(expires)) return;
    return this.cacheData[key];
  }

  getAll() {
    return this.cacheData;
  }
}

function isFresh(expire) {
  if (!expire) return false;
  return expire > new Date().getTime();
}

/* const handleCallback = (resolve, reject, callback, data) => {
 try {
 resolve(callback(data));
 } catch (e) {
 reject(e);
 }
 }; */

// console.log('Config', config);
// request.end((apiError, apiResult = {}) => {
/*
 * Apply Middlewares err function
 * and return rejected request
 */
/* if (apiError) {
 middlewares.forEach(({ err }) => err && err(apiError, apiResult));
 const { body, header: { expires } = {} } = apiResult;
 // TODO: Rewrite this
 return reject({
 payload: body || apiError,
 response: apiResult,
 expires: new Date(expires).getTime()
 });
 } */

/*
 * Apply Middlewares res function
 * and return resolved request
 */
/* middlewares.forEach(({ res }) => res && res(apiResult));

 const { body, header: { expires } = {} } = apiResult;
 Object.defineProperty(body, 'expires', { value: new Date(expires).getTime() });
 // request.expires
 return resolve(body); */

//   awaitAllRequests() {
//     return Promise.all(this.pendingPromises);
// return Promise.resolve();
//  }

/*
 Currently the retrying logic checks for:

 ECONNRESET
 ETIMEDOUT
 EADDRINFO
 ESOCKETTIMEDOUT
 superagent client timeouts
 bad gateway errors (502, 503, 504 statuses)
 Internal Server Error (500 status)
 */
