/**
 * AJAX Service Module.
 * Implements classes for Asynchronous requests: JSON data, pages etc.
 *
 * @exports Json
 */

function _getCookie(name) {
  const regexpStr = name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1');
  const matches = document.cookie.match(new RegExp(`(?:^|; )${regexpStr}=([^;]*)`));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

/**
 * @typedef {Object} JsonRequestParams
 * @property {string}  url - Request URL
 * @property {Object}  params - params, that will be send to server
 * @property {string}  method=POST - Request method
 * @property {function} onUploadProgress - Calls on onProgress XHR event
 */


/**
 * Calls JSON request and waits for JSON data =)
 * All AJAX Requests must implements with Promises
 * @class Json
 */
// class Ajax {
/**
 * Call AJAX Request, and returns the promise
 * @param {JsonRequestParams} props
 * @return Promise
 */
export default function request(props) {
  const propsModified = Object.assign({}, {
    url: '',
    params: {},
    method: 'POST',
    onUploadProgress: () => { },
    _sendMethod: jsonSend,
    _receiveMethod: jsonReceive

  }, props);

  /* Call XHR and return promise */
  return new Promise((accept, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        // *** If HTTP status != 200 - throw error
        if (xhr.status !== 200) {
          const msg = `Request to ${propsModified.url} Failed: ${xhr.status} ${xhr.statusText}`;
          reject({ message: msg, data: {} });
        } else {
          propsModified._receiveMethod(xhr, propsModified, accept, reject);
        }
      }
    };

    xhr.upload.onprogress = (evt) => {
      propsModified.onUploadProgress(evt.loaded, evt.total);
    };

    xhr.open(propsModified.method, propsModified.url, true);
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('X-XSRF-TOKEN', _getCookie('XSRF-TOKEN'));
    propsModified._sendMethod(xhr, propsModified);
  });
}

/**
 * Send JSON Data
 * @param xhr
 * @param props
 * @private
 */
function jsonSend(xhr, props) {
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify(props.params));
}

/**
 * JSON Receiver
 * @param xhr
 * @param props
 * @param accept
 * @param reject
 * @private
 */
function jsonReceive(xhr, props, accept, reject) {
  // Trying to parse JSON, and throw error, if it's failed
  try {
    const data = JSON.parse(xhr.response);

    // *** If response contains error - throws this error
    if (typeof data.status !== 'undefined' && data.status === 'error') {
      // *** Server error info and trace information
      reject({
        message: data.error,
        data
      });
      return;
    }
    //      if (props.showSuccess) {
    //        Notify.success('', props.showSuccessMessage);
    //      }
    accept(data);
    // Handle JSON Parse Error
  } catch (e) {
    // console.log('Error in JSON Parser: ', e);
    const msg = 'Server response not in JSON format';
    // error(msg, props);
    reject({ message: msg, data: {} });
  }
}

/*
function httpReceive(xhr, props, accept) { // , reject
  const data = xhr.response;
  accept(data);
}

function fileSend(xhr, props) {
  xhr.setRequestHeader('Content-Type', props.file.type);
  xhr.send(props.file);
}


 function upload(file, props = {}) {
 const newProps = Object.assign({}, {
 uploader: 'image',
 file,
 url: `/service/form/upload-${props.uploader}`,
 _sendMethod: fileSend
 }, props);
 return this.request(newProps);
 }


 function html(props = {}) {
 const newProps = Object.assign({}, {
 _receiveMethod: httpReceive
 }, props);
 return this.request(newProps);
 }

 function status(response) {
 console.log(response);
 }

 function error(message, props) {
 if (!props.showError) return;
 // Notify.error(props.showErrorHeader, message);
 }
 */
