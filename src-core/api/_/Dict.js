/**
 * Created by sem on 23.12.15.
 */
import Ajax from './Ajax';
// import Notify from 'Common/Helpers/Notify';

let _instance = null;

export default class Dict {

  /**
   * Creating a singleton instance
   * @returns {*}
   */
  constructor() {
    // Singleton
    if (_instance != null) {
      return _instance;
    }
    _instance = this;

    this._dictUrl = '/service/dict';
    this._dicts = {};
  }

  /**
   * Returns the singleton instance
   * @return {Dict}
   */
  static Instance() {
    return _instance || new this();
  }

  /**
   * Return promise (XHR Promise), that contain a corresponding dictionary
   * @param dict
   * @return {Promise}
   */
  list(dict) {
    if (!this._dicts[dict]) {
      this._dicts[dict] = Ajax
        .request({
          method: 'GET',
          url: `${this._dictUrl}/json-list/${dict}`,
          showError: false,
          showSuccess: false
        });
      //        .catch(( error ) => {
      // Notify.error('Ошибка получения справочника', 'Не удалось получить
      // справочник ' + dict + ': ' + error.message);
      //        });
    }
    return this._dicts[dict];
  }
}
