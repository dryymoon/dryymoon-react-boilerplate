/**
 * Created by dryymoon on 26.12.16.
 */
/* eslint-disable */
import isFunction from 'lodash/isFunction';
import isString from 'lodash/isString';
import isObject from 'lodash/isObject';
// import { fetchByUrl } from './data-redux';
import { buildUrl } from './utils';
export default function resolveOnServer({
  store: { dispatch },
  renderProps: { routes = [], components = [], location, params = {}, ...rest }
}) {
  console.log('\n\nROUTES:\n');
  console.dir(routes);
  console.log('\n\nREST:\n');
  console.dir(rest);
  // const isNeedSetProps = false;
  const promisesArr =
    components
      .filter(({ dataPreload } = {}) => Array.isArray(dataPreload))
      .reduce((prev, { dataPreload }) => prev.concat(dataPreload), [])
      .map(req => mapper({ req, dispatch, location, params }));
  return Promise.all(promisesArr).catch((err) => console.log('ERR in PROMISE ALL', err));
}
function mapper({ req, dispatch, location, params = {} }) {
  const url = buildUrl(req);
  return dispatch(fetchByUrl(url, { forceOnServer: true }));
}
/*
 location:
 [0]    { pathname: '/cabinet/mini-site',
 [0]      search: '?gala=2',
 [0]      hash: '',
 [0]      state: null,
 [0]      action: 'POP',
 [0]      key: '70hhmn',
 [0]      query: { gala: '2' },
 [0]      '$searchBase': { search: '?gala=2', searchBase: '' } },
 */
/*
 params: { owner: 'vitaliy', splat: undefined },
 */