/**
 * Created by DryyMoon on 15.01.2017.
 */

import React, { Component } from 'react';
import getDisplayName from 'react-display-name';
import isEmpty from 'lodash/isEmpty';
// import isArray from 'lodash/isArray';
import forOwn from 'lodash/forOwn';
// import assign from 'lodash/assign';
import isObject from 'lodash/isObject';
import isFunction from 'lodash/isFunction';

import flatten from 'lodash/flatten';
// import MultiData from './multidata';

function factory(...args) {
  return (BaseComponent) => {
    /* Maybe is Empty */
    if (isEmpty(args)) return BaseComponent;

    /* Maybe contains arrays */
    const arr = flatten(args);

    const inject = {};
    // const postModifiers = {};
    /* const preload = [];
     arr.forEach((it) => {
     if (isObject(it)) {
     const { $post, $pre, $map, ...rest } = it;
     // const $post = it.$post;
     forOwn(rest, (value, key) => {
     inject[key] = {
     $prop: key,
     $path: value,
     ...{ $post, $pre, $map }
     };
     preload.push(inject[key]);
     });
     // return assign(injectProps, it);
     } else {
     preload.push(it);
     }
     }); */

    class ComponentWithDataPreload extends Component {
      static displayName = `dataPreload(${getDisplayName(BaseComponent)})`;
      static dataInject = inject;
      render() {
        return (
          <MultiData {...{ inject, preload }}>
            <BaseComponent {...this.props} />
          </MultiData>
        );
      }
    }
    return ComponentWithDataPreload;
  };
}
export default factory;
