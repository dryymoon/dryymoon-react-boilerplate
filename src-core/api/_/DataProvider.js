/**
 * Created by sem on 29.03.16.
 */

import Ajax from './Ajax';

export default class DataProvider {

  /**
   * Creating a singleton instance
   * @returns {*}
   */
  constructor(params) {
    this._providerUrl = '/service/data';
    this._provider = params.provider;
    this._params = {};
  }

  /**
   * Returns the singleton instance
   * @return {DataProvider}
   */
  static Instance(provider) {
    return new this({ provider });
  }

  /**
   * Add data provider param (addSomething in data provider)
   * @param param
   * @param value
   * @return {DataProvider}
   */
  add(param, value) {
    this._params[param] = value;
    return this;
  }

  /**
   * Query data provider
   * @return {Promise}
   */
  query() {
    return Ajax
      .request({
        method: 'POST',
        url: `${this._providerUrl}/query/${this._provider}`,
        // showError: false,
        // showSuccess: false,
        params: this._params
      });
    //      .catch(() => { // error
    // Notify.error('Ошибка получения данных', 'Не удалось получить данные: : ' + error.message);
    //      });
  }

}
