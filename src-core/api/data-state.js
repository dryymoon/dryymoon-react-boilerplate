/**
 * Created by DryyMoon on 15.01.2017.
 */
/* eslint-disable */
import { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import bem from 'bem';
import apiDecorator from './api-decorator';

@apiDecorator
export default class DataState extends Component {
  static propTypes = {
    url: PropTypes.string,
    getParams: PropTypes.oneOfType([PropTypes.string, PropTypes.object]), // eslint-disable-line
    postParams: PropTypes.object, // eslint-disable-line
    // method: PropTypes.oneOf(['get', 'post']),
    children: PropTypes.node,
    callback: PropTypes.func,
    onError: PropTypes.func,
    onLoadStart: PropTypes.func,
    onLoadEnd: PropTypes.func,

    $post: PropTypes.func,
    // default: PropTypes.any, // eslint-disable-line
    // name: PropTypes.string,
    // bind: PropTypes.func, // This is a component
    disabled: PropTypes.bool,
    // loader: PropTypes.node,
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.onSuccessRequest = this.onSuccessRequest.bind(this);
    this.onRequestError = this.onRequestError.bind(this);
    this.onLoadEnd = this.onLoadEnd.bind(this);
  }

  componentDidMount() {
    this.run();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!isEqual(this.props, nextProps)) {
      this.run(nextProps);
    }
  }

  componentWillUnmount() {
    this.cancelRequest();
  }

  /* shouldComponentUpdate(nextProps, nextState) {

   } */

  onSuccessRequest(payload) {
    let pl = payload;
    if (this.props.$post) pl = this.props.$post(pl);
    if (this.props.callback) this.props.callback(pl);
  }

  onRequestError(err) {
    if (this.props.onError) this.props.onError(err);
  }

  onLoadStart() {
    if (this.loading) return;
    this.loading = true;
    if (this.props.onLoadStart) this.props.onLoadStart();
  }

  onLoadEnd() {
    if (!this.loading) return;
    if (this.props.onLoadEnd) this.props.onLoadEnd();
  }

  run(maybeNextProps) {
    // this.setState({ loading: true });
    this.cancelRequest();
    const { get: apiGet } = this.api;
    const { url, getParams, postParams, disabled } = maybeNextProps || this.props;
    if (disabled) return;
    const { promise, cancel } = apiGet(url, {
      queryString: getParams,
      postData: postParams,
      cancelable: true
    });
    this.$cancelRef = cancel;
    this.running = true;
    promise
      .then((result) => {
        this.running = false;
        this.$cancelRef = undefined;
        this.onSuccessRequest(result);
      })
      .catch((err) => {
        this.running = false;
        this.$cancelRef = undefined;
        this.onRequestError(err);
      })
      .then(() => this.onLoadEnd());
    if (this.running) this.onLoadStart();
  }

  cancelRequest() {
    if (this.$cancelRef) this.$cancelRef();
  }

  render() {
    // if (this.props.loader && this.state.loading) return this.props.loader;
    if (this.props.children) return Children.only(this.props.children);
    return null;
  }
}
