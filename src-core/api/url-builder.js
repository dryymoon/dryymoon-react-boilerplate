/**
 * Created by DryyMoon on 17.01.2017.
 */
// import isObject from 'lodash/isObject';
import isString from 'lodash/isString';
import urlLib from 'url';

function urlBuilder(...args) {
  // Missing args
  if (args.length === 0) return null;

  let path = '';
  let query = null;

  if (isString(args[0])) {
    path = args[0];
    query = args[1];
  }

  const url = urlLib({ pathname: path, query });
  return url;
}


export default urlBuilder;
