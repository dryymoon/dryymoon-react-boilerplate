/**
 * Created by DryyMoon on 15.01.2017.
 */
/* eslint-disable react/sort-comp, react/jsx-filename-extension, max-len, camelcase */
import React, { Component } from 'react'; // PropTypes
import PropTypes from 'prop-types';
import getDisplayName from 'react-display-name';
import castArray from 'lodash/castArray';
import isString from 'lodash/isString';
import startsWith from 'lodash/startsWith';
import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';
import isObject from 'lodash/isObject';
import isEmpty from 'lodash/isEmpty';
import has from 'lodash/has';
// import get from 'lodash/get';
import omitBy from 'lodash/omitBy';
import pickBy from 'lodash/pickBy';
import values from 'lodash/values';
import flatMap from 'lodash/flatMap';
import mapValues from 'lodash/mapValues';
import assign from 'lodash/assign';
import isEqual from 'lodash/isEqual';
import uniq from 'lodash/uniq';
import { navigateHelperShape } from '../helpers/router/nav-helper';
import apiDecorator from './api-decorator';
import Promise from '../helpers/cancelable-promise';

let db;
// let PouchDB;
if (__CLIENT__) {
  if (window.localStorage && (window.localStorage.getItem('__RELEASE__') !== __RELEASE__)) {
    Object.keys(window.localStorage).forEach((key) => {
      if (startsWith(key, '$__cache')) window.localStorage.removeItem(key);
    });
    // window.localStorage.clear();
    window.localStorage.setItem('__RELEASE__', __RELEASE__);
  }

  if (window.localStorage) {
    db = {
      setItem: (key, value) => {
        try {
          window.localStorage.setItem(`$__cache:${key}`, JSON.stringify(value));
        } catch (e) { // eslint-disable-line
        }
      },
      getItem: (key) => {
        try {
          const maybeData = window.localStorage.getItem(`$__cache:${key}`);
          if (!maybeData) return;
          return JSON.parse(maybeData);
        } catch (e) { // eslint-disable-line
        }
      }
    };
  }
  // import(
  /* webpackChunkName: "pouchdb" */
  /* webpackMode: "lazy" */
  // 'pouchdb-browser')
  // .then(module => PouchDB = module.default);
  // .then(() => createAndCheckDbOnClient());
}

/* function createAndCheckDbOnClient() {
 const tempDb = new PouchDB('apiCache', { auto_compaction: true, revs_limit: 1 });
 tempDb.get('$_DB_PARAMS')
 .then(({ buildRev }) => {
 if (buildRev !== __RELEASE__) {
 return tempDb.destroy().then(() => createAndCheckDbOnClient());
 }
 db = tempDb;
 window.pouchDb = db;
 })
 .catch(({ status }) => {
 if (status === 404) {
 return tempDb
 .put({ _id: '$_DB_PARAMS', buildRev: __RELEASE__ })
 .then(() => createAndCheckDbOnClient());
 }
 });
 } */

export default function factory(...args) {
  return (BaseComponent) => {
    // Check BaseComponent
    if (!isFunction(BaseComponent)) {
      throw new Error('data-preload factory must decorate only React Component');
    }

    const existedPreload = BaseComponent.dataPreload || [];

    // TODO: Починить прелоад на роутах которые динамически грузяться :)

    @apiDecorator
    class PreloadComponent extends Component {
      static dataPreload = [...existedPreload, ...args];
      static displayName = `dataPreload(${getDisplayName(BaseComponent)})`;

      static propTypes = {
        $firstTimeLoaded: PropTypes.bool,
        $dataReady: PropTypes.bool,
        $loading: PropTypes.bool,
        $forceDataReload: PropTypes.func,
      };

      static contextTypes = {
        ...BaseComponent.contextTypes,
        ...navigateHelperShape,
        notification: PropTypes.object // eslint-disable-line
      };

      constructor(props, context) {
        super(props, context);
        this.mounted = false;
        const { location } = context;
        this.fnArgs = buildFnArgs({ ...props, location });
        const { routeProps } = this.fnArgs;
        this.state = {
          $routeProps: routeProps,
          $firstTimeLoaded: false,
          $dataReady: false,
          $loading: true
        };
        this.forceDataReload = this.forceDataReload.bind(this);
        this.dataIncubator = this.dataIncubator.bind(this);
      }

      UNSAFE_componentWillMount() {
        const { location } = this.context;
        this.fnArgs = buildFnArgs({ ...this.props, location });
        const { routeProps } = this.fnArgs;
        this.dataIncubatorResults = this.dataIncubator(this.fnArgs, { initial: true, });

        const {
          resolvedInjects,
          unresolvedInjectsPromise,
          featuredInjectsPromise
        } = this.dataIncubatorResults;

        this.allowSetState = true;

        this.setState({
          ...this.state,
          ...resolvedInjects,
          $routeProps: routeProps,
          $dataReady: !unresolvedInjectsPromise,
          $firstTimeLoaded: !unresolvedInjectsPromise && !featuredInjectsPromise,
          $loading: !!(unresolvedInjectsPromise || featuredInjectsPromise)
        });

        this.updateProcessor();
      }

      componentDidMount() {
        this.mounted = true;
      }

      setState(newState, cb) {
        if (!this.allowSetState) return;
        // console.log('DATA_PRELOAD set state', { ...this.state, ...newState });
        super.setState(newState, cb);
      }

      UNSAFE_componentWillReceiveProps(nextProps, nextContext) {
        const nextFnArgs = buildFnArgs({ ...nextProps, location: nextContext.location });

        if (!isEqual(this.fnArgs, nextFnArgs)) {
          this.dataIncubatorResults = this.dataIncubator(nextFnArgs);
          const { resolvedInjects } = this.dataIncubatorResults;
          this.fnArgs = nextFnArgs;

          if (this.pendingPromise) this.pendingPromise.cancel();

          const newState = {};

          // if (!this.state.$loading) assign(newState, { $loading: true });

          assign(newState, resolvedInjects);

          if (!isEmpty(newState)) this.setState(newState);

          this.updateProcessor();
        }
      }

      forceDataReload() {
        const { $forceDataReload } = this.props;
        if ($forceDataReload) $forceDataReload();
        const nextProps = this.props;
        const nextContext = this.context;
        const nextFnArgs = buildFnArgs({ ...nextProps, location: nextContext.location });
        this.dataIncubatorResults = this.dataIncubator(nextFnArgs);
        const { resolvedInjects } = this.dataIncubatorResults;
        this.fnArgs = nextFnArgs;

        if (this.pendingPromise) this.pendingPromise.cancel();

        const newState = {};

        // if (!this.state.$loading) assign(newState, { $loading: true });

        assign(newState, resolvedInjects);

        if (!isEmpty(newState)) this.setState(newState);

        this.updateProcessor();
      }

      updateProcessor() {
        const {
          cachedInjectsPromise, unresolvedInjectsPromise,
          featuredInjectsPromise, // cacheSetters
        } = this.dataIncubatorResults;

        if (cachedInjectsPromise) {
          cachedInjectsPromise
            .then(results => assign({}, ...results))
            .then(results => this.setState(results));
        }

        if (unresolvedInjectsPromise || featuredInjectsPromise) {
          if (!this.state.$loading) this.setState({ $loading: true });

          // const pendingUnresolvedInjectsPromise =
          if (unresolvedInjectsPromise) {
            unresolvedInjectsPromise
            // Todo тут может буть нужно тоже отменять этот запрос cancel
            // Todo тут дулаеется два раза setState и два раза перерендер... исправить нужно
            // .then(results => assign({}, ...results))
              .then(results => this.setState({
                ...results,
                $routeProps: this.fnArgs.routeProps,
                $dataReady: true,
                $firstTimeLoaded: true,
              })).catch(() => null);
          }

          this.pendingPromise = Promise
            .all([unresolvedInjectsPromise, featuredInjectsPromise]); // , this.loadingPromise

          this.pendingPromise
            .then(results => assign({}, ...results))
            .then((results) => {
              // if (cachedInjectsPromise) cachedInjectsPromise.cancel();
              this.pendingPromise = undefined;
              this.setState({
                ...results,
                $routeProps: this.fnArgs.routeProps,
                $dataReady: true,
                $firstTimeLoaded: true,
                $loading: false
              });

              /* mapValues(cacheSetters, (setter, propName) => {
               if (!has(results, propName)) return;
               const some = setter(results[propName], this.fnArgs, propName);
               if (!some) return;
               castArray(some).filter(isObject).forEach(setOrUpdateCacheDb);
               }); */
            })
            .catch((err) => {
              console.error('Data Preload fail: ', JSON.parse(JSON.stringify(err))); // eslint-disable-line

              /* if (this.notificationErrorUid) this.context.notification.hide(this.notificationErrorUid);

               const apiReqUrl = get(err, 'config.url');
               let message = `Api request failed, detail: \n${JSON.stringify(err)}`;
               if (apiReqUrl) message = `Api request failed to: \n${apiReqUrl}`;

               const { uid: newNotificationErrorUid } = this.context.notification.error({
               autoDismiss: 0,
               dismissible: 'button',
               position: 'tc',
               title: 'Api preload failed',
               message,
               });

               this.notificationErrorUid = newNotificationErrorUid; */

              Raven.captureException('Data Preload fail', {
                level: 'error',
                extra: { err }
              });

              return Promise.reject(err);
              // setTimeout(() => this.forceDataReload(), 5000);
              // TODO дописать релоад при ошибке
            });
          /* .catch((err) => {
           if (err === 'normalCancel') return;
           throw new Error(`Error in preload update data, ${JSON.stringify(err)}`);
           }); */
          // .then(() => this.loadingPromise = null);
        } else if (!isEqual(this.state.$routeProps, this.fnArgs.routeProps)) {
          this.setState({ $routeProps: this.fnArgs.routeProps });
        }
      }

      componentWillUnmount() {
        this.allowSetState = false;
        this.mounted = false;
      }

      render() {
        const { $routeProps } = this.state;
        if (!this.state.$dataReady) return null;

        let { $firstTimeLoaded, $dataReady, $loading } = this.state;

        if (has(this.props, '$forceDataReload')) { // has upper preload
          $firstTimeLoaded = this.props.$firstTimeLoaded && $firstTimeLoaded;
          $dataReady = this.props.$dataReady && $dataReady;
          $loading = this.props.$loading || $loading;
        }

        return (
          <BaseComponent
            {...this.props}
            {...this.state}
            {...{ $firstTimeLoaded, $dataReady, $loading }}
            {...$routeProps}
            $forceDataReload={this.forceDataReload}
            $cacheGetter={getFromCacheDb}
            $cacheSetter={setOrUpdateCacheDb}
          />);
      }

      dataIncubator(fnArgs, { initial = false } = {}) {
        return dataIncubator(fnArgs, {
          apiGet: this.api.get,
          dataPreloadConfig: PreloadComponent.dataPreload,
          initial,
        });
      }
    }

    return PreloadComponent;
  };
}

function setOrUpdateCacheDb({ key, value } = {}) {
  if (!db) return;
  if (!key) return;
  db.setItem(key, value);
}

function getFromCacheDb(key) {
  if (!db) return;
  if (!key) return;
  return db.getItem(key);
}

export function dataIncubator(fnArgs, { apiGet, initial = false, dataPreloadConfig }) {
  // const preloadParams = PreloadComponent.dataPreload;

  // const { get: apiGet } = this.api;

  /* Build Needle preload and ijects */
  const injectRequests = values(injectProcessor(dataPreloadConfig, {}, fnArgs));
  let preloadRequests = uniq(reqProcessor(dataPreloadConfig, [], fnArgs));

  // Remove urls that is in injects.
  const injectUrls = injectRequests.map(({ url }) => url).filter(url => url);
  preloadRequests = preloadRequests
    .filter(it => it) // Remove undefined urls
    // Remove injected requests
    .filter(url => injectUrls.indexOf(url) === -1)
    // Remove cached results
    .filter(url => apiGet(url, { sync: true }) === undefined);

  const unresolved = preloadRequests;

  const preloadResolvers = preloadRequests.map(url => () => apiGet(url).then(() => null));
  const cachedInjectsResolvers = [];
  const unresolvedInjectResolvers = [];
  const featuredInjectResolvers = [];
  const resolvedInjects = {};
  const cacheSetters = {};
  // const cacheGettersPropToId = {};

  injectRequests.forEach((req) => {
    const {
      url, value, propName, $injectRaw, $default, $injectImmediately, $resolveOnce,
      $cacheGetter, $cacheSetter, $catch, // $forceFromCache,
    } = req;

    if (url) {
      const cachedResult = apiGet(url, { sync: true });
      if (cachedResult !== undefined) {
        resolvedInjects[propName] = postProcessor(cachedResult, req, fnArgs);
        return;
      }
      unresolved.push(url);
    }

    let getter;

    if (url && !$catch) {
      getter = () => apiGet(url)
        .then(resp => postProcessor(resp, req, fnArgs))
        .then(modifiedRes => ({ [propName]: modifiedRes }));
    }

    if (url && $catch) {
      getter = () => apiGet(url)
        .catch(err => $catch(err))
        .then(resp => postProcessor(resp, req, fnArgs))
        .then(modifiedRes => ({ [propName]: modifiedRes }));
    }

    if ((initial && $injectRaw) || ($injectRaw && $injectImmediately)) {
      resolvedInjects[propName] = postProcessor(value, req, fnArgs);
      return;
    }

    if (!initial && $injectRaw) {
      getter = () => Promise.resolve({ [propName]: postProcessor(value, req, fnArgs) });
      featuredInjectResolvers.push(getter);
      return;
    }

    /* if (getter && initial && $forceFromCache) {
     const maybeInCache = apiGet(url, { sync: true, forceFromCache: true });
     if (maybeInCache !== undefined) {
     resolvedInjects[propName] = postProcessor(maybeInCache, req, fnArgs);
     featuredInjectResolvers.push(getter);
     return;
     }
     } */

    if (getter && !initial && $resolveOnce) return;

    if (__CLIENT__ && db && getter && $cacheSetter) {
      cacheSetters[propName] = $cacheSetter;
    }

    if (__CLIENT__ && db && getter && $cacheGetter) { // db &&
      let meybeId;
      if (isFunction($cacheGetter)) meybeId = $cacheGetter(propName, fnArgs);
      if (isString($cacheGetter)) meybeId = $cacheGetter;
      if (meybeId) {
        // cacheGettersPropToId[propName] = meybeId;
        const maybeItem = getFromCacheDb(meybeId);
        if (maybeItem !== undefined) {
          resolvedInjects[propName] = maybeItem;
          featuredInjectResolvers.push(getter);
          return;
        }
      }
    }

    // TODO temporary disabled

    /* if (false && url && getter) {
     const maybeCachedAnswer = apiGet(url, { syncFromCache: true });

     if (maybeCachedAnswer) {
     resolvedInjects[propName] = postProcessor(maybeCachedAnswer, req, fnArgs);
     featuredInjectResolvers.push(getter);
     return;
     }
     } */

    if (getter && initial && has(req, '$default')) {
      resolvedInjects[propName] = postProcessor($default, req, fnArgs);
      featuredInjectResolvers.push(getter);
      return;
    }

    if (getter) {
      unresolvedInjectResolvers.push(getter);
      // unresolvedInjectReqs.push(req);
    }
  });

  let cachedInjectsPromise;
  let unresolvedInjectsPromise;
  let featuredInjectsPromise;
  let unresolvedPreloadsPromise;

  if (__CLIENT__ && cachedInjectsResolvers.length) {
    cachedInjectsPromise = Promise.all(cachedInjectsResolvers);
  }

  if (__CLIENT__ && preloadResolvers.length) {
    unresolvedPreloadsPromise = Promise.all(preloadResolvers.map(fn => fn()));
  }

  if (__CLIENT__ && unresolvedInjectResolvers.length) {
    unresolvedInjectsPromise = Promise
      .all(unresolvedInjectResolvers.map(fn => fn()))
      .then(results => assign({}, ...results));
  }

  if (__CLIENT__ && featuredInjectResolvers.length) {
    featuredInjectsPromise = Promise
      .all(featuredInjectResolvers.map(fn => fn()))
      .then(results => assign({}, ...results));
  }

  if (__SERVER__ && (unresolved.length > 0)) {
    if (!__DEVELOPMENT__) {
      Raven.captureException('Preload Component catch some not preloaded data:', {
        level: 'error',
        extra: { unresolved }
      });
    } else {
      // eslint-disable-next-line
      console.error('Preload Component catch some not preloaded data:', unresolved);
    }
  }

  const pendingPromise = Promise
    .all([unresolvedInjectsPromise, featuredInjectsPromise]);
  // const brokenCacheableResolversPromise =
  pendingPromise
    .then(results => assign({}, ...results))
    .then((results) => {
      if (cachedInjectsPromise) cachedInjectsPromise.cancel();

      mapValues(cacheSetters, (setter, propName) => {
        if (!has(results, propName)) return;
        let some;
        if (isFunction(setter)) {
          some = setter(results[propName], fnArgs, propName);
        }
        if (isString(setter)) {
          some = { key: setter, value: results[propName] };
        }
        if (!some) return;
        castArray(some).filter(isObject).forEach(setOrUpdateCacheDb);
      });
    })
    .catch(() => null);

  const ret = {
    resolvedInjects,
    cachedInjectsPromise,
    featuredInjectsPromise,
    unresolvedInjectsPromise,
    unresolvedPreloadsPromise
  };

  /* Object.defineProperty(ret, 'brokenCacheableResolversPromise', {
   configurable: true,
   value: brokenCacheableResolversPromise,
   }); */

  return ret;
}

export function buildFnArgs(props) {
  const {
    location: { pathname, search, query, params: paramsRunning } = {}, // action,
    params: paramsPrerender,
    children, route, routeParams, router, components, matchContext, routes = [], // eslint-disable-line
    ...rest
  } = props;
  const params = paramsPrerender || paramsRunning;
  // const method = action ? action.toLowerCase() : undefined;

  const customRoutePropsArr = routes
    .map(({ props: routeProps } = {}) => routeProps)
    .filter(it => it);
  const customRoutePropsObj = assign({}, ...customRoutePropsArr);

  return {
    pathname,
    search,
    query,
    params,
    props: { ...customRoutePropsObj, ...rest },
    routeProps: customRoutePropsObj,
  }; // method,
}

/*
 * reqProcessor
 */

export function preloadExecutor({ renderProps, apiInstance: { get: apiGet } }) {
  // Use this without withRouter:
  const preloadArr = flatMap(renderProps.components, ({ dataPreload = [] } = {}) => dataPreload);

  // Use this with withRouter:
  // const preloadArr = flatMap(renderProps.components,
  //  ({ WrappedComponent: { dataPreload = [] } = {} } = {}) => dataPreload);
  const newPreloadArr = reqProcessor(preloadArr, [], buildFnArgs(renderProps));
  const requestArr = newPreloadArr.map(uri => apiGet(uri));

  return Promise.all(requestArr, () => requestArr.forEach(it => it.cancel()));
}

/*
 * reqProcessor - строит массив ссылок для предзагрузки.
 */
function reqProcessor(unsafeArr = [], acc = [], ...args) {
  const arr = castArray(unsafeArr);

  if (arr.length === 0) return acc;

  const [some, ...rest] = arr;

  if (isString(some)) return reqProcessor(rest, [...acc, some], ...args);

  if (isArray(some)) {
    const subArray = reqProcessor(some, [], ...args);
    return reqProcessor(rest, [...acc, ...subArray], ...args);
  }

  if (isFunction(some)) {
    const resultArr = reqProcessor([some(...args)], [], ...args);
    return reqProcessor(rest, [...acc, ...resultArr], ...args);
  }

  if (isObject(some)) {
    const filteredObj = omitBy(some, (value, key) => key[0] === '$');

    // Don`t Load IT
    if (some.$injectRaw) return reqProcessor(rest, acc, ...args);

    // Don`t Load IT
    // if (__CLIENT__ && has(some, '$default')) return reqProcessor(rest, acc, ...args);

    let additArr = values(filteredObj);
    if (isFunction(some.$pre)) {
      additArr = additArr.map(it => some.$pre(it, ...args));
    }
    return reqProcessor(rest, [...acc, ...additArr], ...args);
  }

  return reqProcessor(rest, acc, ...args);
}

/*
 * injectProcessor - строит обьект ссылок для предзагрузки и постобработки.
 */

function injectProcessor(some, { acc = {}, propName, additional = {} } = {}, ...args) {
  /* eslint-disable no-param-reassign */

  if (propName && additional.$injectRaw) {
    return { ...acc, [propName]: { propName, value: some, ...additional } };
  }

  if (propName && isString(some)) {
    return { ...acc, [propName]: { propName, url: some, ...additional } };
  }

  if (isArray(some)) {
    const resultsArr = some.map(it => injectProcessor(it, {}, ...args));
    return assign({}, acc, ...resultsArr);
  }

  if (isFunction(some)) {
    return injectProcessor(some(...args), {}, ...args);
  }

  if (isObject(some)) {
    const { $pre, ...rest } = some;

    let obj = omitBy(rest, (value, key) => key[0] === '$');

    if (isFunction($pre)) obj = mapValues(obj, value => $pre(value, ...args));

    const modifiers = pickBy(rest, (value, key) => key[0] === '$');
    // const $groupId = uuid.v4();
    obj = mapValues(obj, (value, key) =>
      injectProcessor(value, { propName: key, additional: { ...modifiers } })); // $groupId

    const resultsArr = values(obj);

    return assign({}, acc, ...resultsArr);
  }

  return acc;
}

function postProcessor(resp, req = {}, fnArgs) {
  const { $post } = req;
  if (!isFunction($post)) return resp;
  return $post(resp, fnArgs);
}
