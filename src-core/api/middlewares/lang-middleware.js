/**
 * Created by dryymoon on 26.04.17.
 */
/* eslint-disable no-param-reassign */
export default class LangMiddleware {
  constructor(lang) {
    if (lang) this.setLang(lang);
  }

  setLang(lang) {
    this.lang = lang;
  }

  getLang() {
    return this.lang;
  }

  middleware() {
    const that = this;
    return {
      req: (reqConfig) => {
        const lang = that.getLang() || 'en';
        reqConfig.headers = {
          ...reqConfig.headers,
          'Accept-Language': lang
        };
        return reqConfig;
      }
    };
  }
}
