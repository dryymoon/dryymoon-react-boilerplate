/**
 * Created by dryymoon on 13.04.2018.
 */
import isBrowser from 'isBrowser';
import startsWith from 'lodash/startsWith';
import template from './template.dot';

const mware = {
  err: (error) => {
    if (__DEVELOPMENT__
      && isBrowser
      && error instanceof Error
      && error.toString().indexOf('Error: Network Error') > -1
      && __ROOT_DOM_POINT__
    ) {
      const { config: { baseURL } = {} } = error;
      if (!startsWith(baseURL, 'https')) return;
      window.openApiCertFacker = () => {
        const strWindowFeatures = 'menubar=no,toolbar=no,location=no,resizable=yes,' +
          'scrollbars=yes,status=no,dependent=yes';
        const windowObjectReference = window.open(baseURL, 'Api Certificate Facker', strWindowFeatures);
        setInterval(() => {
          if (windowObjectReference.closed) window.location.reload();
        }, 500);
        return false;
      };

      document.getElementById(__ROOT_DOM_POINT__).innerHTML = template({
        url: baseURL,
        onClickFn: 'openApiCertFacker'
      });
    }
  }
};

export default mware;

// setTimeout(() => mware.fail(new Error('Error: Network Error'), { baseURL: 'http://example.com' }), 10000);
