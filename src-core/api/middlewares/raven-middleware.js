/**
 * Created by PIV on 04.08.2016.
 */
/* eslint-disable no-param-reassign */

export default {
  err: (apiError) => {
    if (__DEVELOPMENT__) return;
    Raven.captureException('Preload Component catch some not preloaded data:', {
      level: 'error',
      extra: { ...apiError }
    });
  }
};
