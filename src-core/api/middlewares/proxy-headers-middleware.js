/**
 * Created by PIV on 04.08.2016.
 */
/* eslint-disable no-param-reassign */
import normalizeHeaderCase from 'header-case-normalizer';
import mapKeys from 'lodash/mapKeys';

export default function (serverRequestHeaders) {
  return {
    req: (req) => {
      const normalized = mapKeys(serverRequestHeaders,
        (value, key) => normalizeHeaderCase(key));
      req.headers = { ...req.headers, ...normalized };
      return req;
    }
  };
}
