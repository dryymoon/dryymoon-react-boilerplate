/**
 * Created by dryymoon on 26.04.17.
 */
/* eslint-disable no-param-reassign */
export default class BearerToken {
  bearerPrefix = 'bearer ';

  constructor(existingToken) {
    const token = existingToken;
    if (token) this.setToken(token);
  }

  setToken(token) {
    this.token = token;
  }

  getToken() {
    return this.token;
  }

  middleware() {
    const that = this;
    return {
      req: (reqConfig) => {
        const token = that.getToken();
        if (token) {
          reqConfig.headers = {
            ...reqConfig.headers,
            Authorization: `${this.bearerPrefix}${token}`
          };
        }
        return reqConfig;
      }
    };
  }
}
