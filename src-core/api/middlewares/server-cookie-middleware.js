/**
 * Created by PIV on 04.08.2016.
 */
import setCookieParser from 'set-cookie-parser';
import mapValues from 'lodash/mapValues';
import forOwn from 'lodash/forOwn';
import normalizeHeaderCase from 'header-case-normalizer';
// import cookie from 'cookie';

class SetCookieCollector {
  constructor(clientCookies = {}) {
    this.buildCookieHeader = this.buildCookieHeader.bind(this);
    /*
     cookie-parser может вернуть в качестве значения - строку или объект,
     но в дальнейшем мы не парсим установки кукисов с АПИ в обект, только в строку,
     Поэтому только сдесь мы преобразовываем строку в обект.
     */
    this.requestCookies = mapValues(clientCookies, value =>
      typeof value === 'object'
        ? `j:${JSON.stringify(value)}`
        : String(value)
    );
    this.receivedSetCookies = {};
    this.alreadyGetted = false;
  }

  middleware() {
    const self = this;
    return {
      req: ({ headers, ...rest }) => ({
        headers: { ...headers, ...self.buildCookieHeader() },
        ...rest
      }),
      res: (apiResult) => {
        if (self.alreadyGetted) return;
        if (!apiResult.headers['set-cookie']) return;
        const parsedCookiesArr = setCookieParser(apiResult.headers['set-cookie']);
        parsedCookiesArr.forEach((it) => {
          self.receivedSetCookies[it.name] = it;
          self.requestCookies[it.name] = it.value;
        });
      }
    };
  }

  buildCookieHeader() {
    this.cookieHeaderName = this.cookieHeaderName || normalizeHeaderCase('cookie');
    const cookie = Object
      .keys(this.requestCookies)
      .map((key) => {
        // TODO: maybe NEED encodeURIComponent
        const value = encodeURIComponent(this.requestCookies[key]);
        return `${key}=${value}`;
      })
      .join('; ');
    if (!cookie) return {};
    return { [this.cookieHeaderName]: cookie };
  }

  injectCookies(serverResp) {
    if (this.alreadyGetted) throw new Error('Flush can be called only once');
    this.alreadyGetted = true;
    forOwn(this.receivedSetCookies, ({ name, value, maxAge: maxAgeInSeconds, ...opts }) => {
      const maxAge = maxAgeInSeconds * 1000;
      serverResp.cookie(name, value, { ...opts, maxAge });
    });

    delete this.receivedSetCookies;
  }
}

function createInstance(...args) {
  return new SetCookieCollector(...args);
}

export default createInstance;

export { createInstance };
