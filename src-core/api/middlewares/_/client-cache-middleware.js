/**
 * Created by dryymoon on 26.01.17.
 */

class ApiCache {
  constructor({ preloaded }) {
    this.cache = preloaded || {};
    this.res = this.res.bind(this);
  }

  req(config) {
    const { config: { method, url } } = config;
    if (method === 'get') {
      return config.resolve;
      // this.cache[url] = data;
    }
  }

  getCache() {
    return this.cache;
  }

}

function createInstance(...args) {
  return new ApiCache(...args);
}

export default createInstance;

export { createInstance };
