/**
 * Created by dryymoon on 23.01.17.
 */
import urlLib from 'url';

class PreloadCollector {
  constructor() {
    this.cache = {};
    this.alreadyGetted = false;
  }

  serverMiddleware() {
    const self = this;
    return {
      res: ({ config: { method, url }, data }) => {
        if (!self.alreadyGetted && method === 'get') {
          const relativeUrl = urlLib.parse(url).path;
          self.cache[relativeUrl] = data;
        }
      }
    };
  }

  getCache() {
    return this.cache;
  }

}

function createInstance(...args) {
  return new PreloadCollector(...args);
}

export default createInstance;

export { createInstance };
