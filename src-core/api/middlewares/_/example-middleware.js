/**
 * Created by PIV on 04.08.2016.
 */
/*
 * We use axios as requester
 * https://github.com/mzabriskie/axios#instance-methods
 * And you can modify request and responce of this lib.
 * See documentation for any expirience.
 */



export default function () {
  return {
    methods: {
      // methodName: (methodArgs) => null,
    },
    req: requestConfig => requestConfig,
    res: apiResult => apiResult,
    err: apiError => apiError
  };
}
