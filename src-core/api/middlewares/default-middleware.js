/**
 * Created by PIV on 04.08.2016.
 */
/* eslint-disable no-param-reassign */

export default {
  req: (req) => {
    req.headers = req.headers || {};
    req.headers['X-Requested-With'] = 'XMLHttpRequest';
  },
  /* res: (apiResult) => {
    const { data, headers: { expires } = {} } = apiResult;
    // eslint-disable-next-line no-param-reassign
    if (data && expires) apiResult.data.$$expires = new Date(expires).getTime();
  } */
};
