/**
 * Created by dryymoon on 20.01.18.
 */
/* eslint-disable */
import { Component } from 'react';
import PropTypes from 'prop-types';
import { match } from 'react-router';
import Observer from '@researchgate/react-intersection-observer';
// import flatten from 'lodash/flatten';
// import CancelablePromise from '../helpers/cancelable-promise';
// import api from 'api';
// import urlLib from 'url';
// import sameOrigin from 'same-origin';
// import isBrowser from '../isBrowser';

// @api
export default class CachePreload extends Component {
  static propTypes = {
    // api: PropTypes.object, // eslint-disable-line
    routes: PropTypes.object, // eslint-disable-line
    children: PropTypes.node
  };

  constructor() {
    super();
    this.preload = this.preload.bind(this);
    this.preloadAsync = (...args) => setTimeout(() => this.preload(...args), 0);
    this.handleIntersectionChange = this.handleIntersectionChange.bind(this);
  }

  runPreload() {

  }

  handleIntersectionChange({ isIntersecting }) {
  }

  render() {
    const { ...restProps } = this.props;
    return (
      <Observer
        {...restProps}
        onChange={this.handleIntersectionChange}
      />
    );
  }
}

/* getChildContext() {
 return { preload: this.preloadAsync };
 } */

/* preload(relativeUrl) {
 const { routes } = this.props;
 match({ routes, location: relativeUrl }, (err, redirect, renderProps) => {
 if (!renderProps) return;
 const { components: currComponents = [], routes: currRoutes = [] } = renderProps;

 // Preload async components
 const getComponents = currRoutes
 .map(({ getComponent } = {}) => getComponent)
 .filter(it => it);
 getComponents.map(getComponent => getComponent(null, () => null));

 // Preload data
 });
 } */

/* const location = { ...renderProps.location, params: renderProps.params };

 const datasDeepArrs = currComponents
 .map(({ dataPreload } = {}) => dataPreload)
 .filter(it => it);
 const datasArr = flatten(datasDeepArrs);

 console.log('renderProps in preload', renderProps, datasArr); */
