/**
 * Created by sem on 26.10.16.
 */

import PropTypes from 'prop-types';
import getDisplayName from 'react-display-name';
import extend from 'lodash/extend';
import values from 'lodash/values';
import ApiClient from './api-client';

function factory() {
  return function wrapComponent(Component) {
    return class ComponentWithApi extends Component {
      static displayName = `api(${getDisplayName(Component)})`;

      static contextTypes = extend(
        Component.contextTypes || {},
        { api: PropTypes.instanceOf(ApiClient) }
      );

      constructor(props, context) {
        super(props, context);
        const { api } = context;
        this.__apiIterator = 0;
        this.__apiChannel = {};
        this.api = {};// { __iterator: 0, __channel: {} };
        api.methods.forEach(method => this.api[method] = (...args) => {
          const apiCall = api[method](...args);

          if (apiCall && apiCall.cancel) {
            // Может не быть вообще запроса, поскольку запрос может быть синхронным
            this.__apiIterator += 1;
            const iterator = this.__apiIterator;
            this.__apiChannel[iterator] = apiCall.cancel;
            Promise.resolve(apiCall)
              .then(() => delete this.__apiChannel[iterator])
              .catch(() => delete this.__apiChannel[iterator]);
          }
          return apiCall;
        });
      }

      componentWillUnmount() {
        values(this.__apiChannel).forEach(cancel => cancel && cancel());
        if (super.componentWillUnmount) super.componentWillUnmount();
      }
    };
  };
}

export default factory();
export { factory };
