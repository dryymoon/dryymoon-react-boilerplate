/**
 * Created by dryymoon on 19.04.2018.
 */
import React from 'react';
import { SensorProvider } from 'components/sensor';
import { Provider as DataPrefetchProvider } from 'component/prefetch';
import { Provider as ReduxProvider } from 'react-redux';
import { applyRouterMiddleware, Router } from 'react-router'; // Router
import { useScroll } from 'react-router-scroll';
import { Provider as ReactUniqueIdProvider } from '../helpers/react/ReactUniqueIds';
import { Provider as ReactNotifyProvider } from '../helpers/react/ReactNotifyProvider';
import ReactApiProvider from '../helpers/react/ReactApiProvider';
import { ReactCookiesProvider } from '../helpers/react/ReactCookiesProvider';
import OriginProvider from '../helpers/router/origin-provider';
import shouldUpdateScroll from '../helpers/router/should-update-scroll';

if (__DEVELOPMENT__) window.React = React;

export default function ({ renderProps, api, routes, store, media, history, cookies }) { // eslint-disable-line
  const routerMiddleware = applyRouterMiddleware(useScroll(shouldUpdateScroll));
  return (
    <ReactCookiesProvider cookies={cookies}>
      <SensorProvider provideMedia={media}>
        <OriginProvider>
          <ReactUniqueIdProvider>
            <ReactApiProvider api={api}>
              <DataPrefetchProvider routes={routes} history={history}>
                <ReduxProvider store={store}>
                  <ReactNotifyProvider>
                    <Router {...renderProps} render={routerMiddleware} />
                  </ReactNotifyProvider>
                </ReduxProvider>
              </DataPrefetchProvider>
            </ReactApiProvider>
          </ReactUniqueIdProvider>
        </OriginProvider>
      </SensorProvider>
    </ReactCookiesProvider>
  );
}
