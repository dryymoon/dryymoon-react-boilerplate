/**
 * Created by dryymoon on 19.04.2018.
 */
import consoleRavenPlugin from 'raven-js/plugins/console';
import Raven from 'raven-js';

export default function ({ point, tags = {}, release } = {}) {
  if (!point) return;
  // if (!__DEVELOPMENT__) return;
  if (window.__RAVEN__EXISTED__) return;

  consoleRavenPlugin(Raven, null, { levels: ['warn', 'error'] });

  const config = {};
  config.environment = __DEVELOPMENT__ ? 'development' : 'production';
  if (release) config.release = release;
  config.tags = { side: 'browser', ...tags };

  Raven.config(point, config).install();

  window.onunhandledrejection = evt => Raven.captureException(evt.reason);
  window.__RAVEN__EXISTED__ = true;
}
