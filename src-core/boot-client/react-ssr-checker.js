/**
 * Created by dryymoon on 19.04.2018.
 */
/* eslint-disable no-console */
import get from 'lodash/get';

export default function (reactMountNode) {
  // eslint-disable-next-line no-undef
  if (!__DEVELOPMENT__ || __DEV_CLI__) return;

  const hasCheckSum = get(reactMountNode, ['firstChild', 'attributes', 'data-react-checksum']);

  if (hasCheckSum) {
    // eslint-disable-next-line no-console
    console.info('Server-side and Front-end React render is equal.');
    return true;
  }

  // eslint-disable-next-line no-console
  console.error(`Server-side React render was discarded.
    Make sure that your initial render
    does not contain any client-side code.`);
}
