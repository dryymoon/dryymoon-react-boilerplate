/* eslint-disable object-property-newline */
import Express from 'express';
import fs from 'fs';
import os from 'os';
import cluster from 'cluster';
import 'log-timestamp';
import 'source-map-support/register';
// import 'pretty-error/start';
// import PrettyError from 'pretty-error';
import ErrorHtmlRenderer from 'error-html';
import ReactDOM from 'react-dom/server';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import CookiesLib from 'cookies';
import proxyMiddleware from 'http-proxy-middleware';
import pathLib from 'path';
import Helmet from 'react-helmet';
import http from 'http';
import urlLib from 'url';
import { match, createMemoryHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import ravenLib from 'raven';
import cheerio from 'cheerio';
import { minify as minifyHtml } from 'html-minifier';
// import { html as htmlBeautify } from 'js-beautify';
// import htmlBeautify from 'pretty';
import { preloadExecutor } from './api/data-preload';
import reactRoot from './boot-server/server-react-root';
import env from '../webpack/helpers/cascaded-env';
import configureStore from './helpers/redux/configure-store';
import ApiClient from './api/api-client';
import apiCookieMiddleware from './api/middlewares/server-cookie-middleware';
import { setMultipleCss, getInlineStyles } from './helpers/styles/elimitateCss';
import CssSelectorStore from './helpers/styles/cssSelectorStore';
import getRoutes from './routes';

import {
  apiConfig as apiClientConfig,
  media as clientMedia,
  routes as siteRoutes
} from '../src-sites/__SRC_SITE_FOLDER__/boot-client';

import {
  apiConfig as apiServerConfig,
  defaultClientMedia
} from '../src-sites/__SRC_SITE_FOLDER__/boot-server';

const wwwPort = env.SERVER_PORT || 8080;

const errorHtmlRenderer = new ErrorHtmlRenderer({
  appPath: process.cwd(),
});

if (!__DEVELOPMENT__ && __SENTRY_PRIVATE_POINT__) {
  global.Raven = new ravenLib.Client(__SENTRY_PRIVATE_POINT__, {
    environment: __DEVELOPMENT__ ? 'development' : 'production',
    tags: {
      side: 'ssr'
    }
  });

  global.Raven.patchGlobal((e) => {
    console.log('Server got an Error', e);
  });

  global.Raven.on('error', (e) => {
    // The event contains information about the failure:
    //   e.reason -- raw response body
    //   e.statusCode -- response status code
    //   e.response -- raw http response object

    console.log('Event sentry logging fail, die to: ', e.statusCode, e.reason);
  });
  global.RavenInited = true;
}
// https://docs.sentry.io/clients/node/

if (cluster.isMaster && !__DEVELOPMENT__) {
  // Count the machine's CPUs
  const cpuCount = os.cpus().length;
  const countOfWorkers = (cpuCount > 1) ? cpuCount - 1 : 1;
  // Create a worker for each CPU
  for (let i = 0; i < countOfWorkers; i += 1) cluster.fork();
  // Listen for dying workers
  cluster.on('exit', (worker) => {
    // Replace the dead worker, we're not sentimental
    console.log('Worker %d died :(', worker.id); // eslint-disable-line no-console
    cluster.fork();
  });

  console.info('==> 💻  Open http://%s:%s in a browser to view the app.', '0.0.0.0', wwwPort);
  // console.info('==> 👽  Current backend API URL is %s', APIURL);
} else {
  /*
   *  Server Code for workers
   */

  // const cryptr = new CryptrLib(' ');
  // const iconPath = pathLib.join(__dirname, './icons.json');
  // const iconsHtmlHeaderString = '';
  /* if (fs.existsSync(iconPath)) {
   const icons = readJsonFileSync();
   iconsHtmlHeaderString = icons && icons.html && icons.html.join(' ');
   } */

  const indexHtml = fs.readFileSync(pathLib.join(__dirname, './www/index.html')).toString();

  // TODO : Fix this for Production
  // process.on('unhandledRejection', err => console.error(err.stack));
  // process.on('uncaughtException', err => console.error(err.stack));

  // PREPARE CSS
  /* const cssPaths = values(readJsonFileSync(pathLib.join(__dirname, './assets.json')))
    .map(({ css: cssPath }) => cssPath && pathLib.join(__dirname, cssPath))
    .filter(it => it); */
  setMultipleCss([pathLib.join(__dirname, './backend.css')]);

  const app = new Express();

  app.use((req, res, next) => {
    // TODO Remove this in prod version When use NGINX
    req.connection.setNoDelay(true);
    next();
  });

  /* app.use((req, res, next) => {
    const vhostConfig = vhostMatch({ req });
    if (!vhostConfig) return res.status(404).send('Vhost Not found').end();
    const { folder } = vhostConfig;
    Promise.all([sitesBootClient(folder), sitesBootServer(folder)])
      .then(([client, server]) => {
        // eslint-disable-next-line no-param-reassign
        req.vhost = { client, server };
        next();
      })
      .catch((err) => {
        if (__DEVELOPMENT__) return res.status(500).send(errorHtmlRenderer.render(err)).end();
        return res.status(500).send(indexHtml).end();
      })
      .catch(err => res.status(520).send(err.toString()).end());
  }); */

  app.use(compression());

  app.use(proxyMiddleware(
    (pathname, req) => {
      const clientApiUrl = apiClientConfig.url; // get(req, 'vhost.client.api.url');
      const serverApiUrl = apiServerConfig.url; // get(req, 'vhost.server.api.url');
      return (clientApiUrl && serverApiUrl && req.path.startsWith(clientApiUrl));
    },
    {
      target: 'http://www.example.org',
      router: () => apiServerConfig.url, // req => , //req.vhost.server.api.url,
      // changeOrigin: true,
      secure: false,
      pathRewrite: path => path.replace(apiClientConfig.url, '')
      // (path, req) => path.replace(req.vhost.client.api.url, '')
    }
  ));

  // TODO Change this path to get from project config
  app.use('/assets', Express.static(`${__dirname}/www/assets`, {
    dotfiles: 'ignore',
    maxage: '30d'
  }));

  app.use((req, res, next) => {
    // eslint-disable-next-line no-param-reassign
    req.forwardedSecure = (req.headers['x-forwarded-proto'] === 'https');
    next();
  });

  app.use(cookieParser());

  app.use((req, res) => {
    const apiCookie = apiCookieMiddleware(req.cookies); // create instance

    const api = new ApiClient(
      apiServerConfig.url,
      // get(req, 'vhost.server.api.url'),
      apiCookie.middleware(),
      apiServerConfig.middlewares || [],
      // ...get(req, 'vhost.server.api.middlewares', []),
    );

    const memoryHistory = createMemoryHistory(req.originalUrl);

    const store = configureStore(memoryHistory, api);

    const history = syncHistoryWithStore(memoryHistory, store);

    history.listen(({ action, ...restUrlParams }) => {
      if (action === 'REPLACE') res.redirect(urlLib.format(restUrlParams));
      if (action === 'PUSH') res.location(urlLib.format(restUrlParams));
    });

    // const routes = getRoutes(req.vhost.client.routes);

    const cookies = new CookiesLib(req, res);

    const routes = getRoutes(siteRoutes, { api, store, history, cookies, serverSideRequest: req, serverSideResponse: res, });

    match({ history, routes, location: req.originalUrl }, (error, redirect, renderProps) => {
      if (redirect) {
        return res.redirect(urlLib.format(redirect));
      }

      if (error) {
        if (__DEVELOPMENT__) return res.status(500).send(errorHtmlRenderer.render(error)).end();
        return res.status(500).send(indexHtml).end();
      }

      if (renderProps) {
        // TODO Add diffrent codes if case api responce or reject on preload
        console.time('preloadExecutor');
        return api.awaitInit()
          .then(() => preloadExecutor({ renderProps, apiInstance: api }))
          .then(() => {
            console.timeEnd('preloadExecutor');
            res.status(200);
            // TODO Life Hach For AMP Type render
            global.__AMP__ = false; // !!req.amp;

            const selectorsStore = new CssSelectorStore();

            const component = reactRoot({
              renderProps, api, store, serverSideRequest: req, serverSideResponse: res, cookies,
              media: clientMedia, defaultMedia: defaultClientMedia,
              // media: req.vhost.client.media, defaultMedia: req.vhost.server.defaultClientMedia,
              pushSelector: selectorsStore.push
            });

            console.time('renderToString');
            const content = ReactDOM.renderToString(component);
            console.timeEnd('renderToString');
            // console.log('ReactDOM.renderToString Finished');
            const helmet = Helmet.renderStatic();
            // console.log('headData',headData.style.toComponent());
            // console.log('headData',headData.style.toString());
            // console.log('Helmet',Helmet);
            console.time('getInlineStyles');
            const fastStyles = getInlineStyles(selectorsStore.get());
            console.timeEnd('getInlineStyles');

            console.time('reduxStoreString');
            const reduxStoreString = Buffer.from(JSON.stringify(store.getState()), 'utf8').toString('base64');
            console.timeEnd('reduxStoreString');
            // cryptr.encrypt(JSON.stringify(store.getState()));
            console.time('apiPreloadString');
            // const __cache = apiPreload.getCache();
            const __cache = api.getAllCache();
            const apiPreloadString = Buffer.from(JSON.stringify(__cache), 'utf8').toString('base64');
            console.timeEnd('apiPreloadString');
            console.time('cheerio');
            const $ = cheerio.load(minifyHtml(indexHtml, { collapseWhitespace: true }));

            helmet.htmlAttributes.toString().split(' ').filter(it => it).forEach((attr) => {
              const [name, val] = attr.split('=');
              if (name && val) $('html').attr(name, val.replace(/^"|"$/g, ''));
            });

            helmet.bodyAttributes.toString().split(' ').filter(it => it).forEach((attr) => {
              const [name, val] = attr.split('=');
              if (name && val) $('body').attr(name, val.replace(/^"|"$/g, ''));
            });

            if (fastStyles) {
              $('#react-mount-node').after($('html head link[rel="stylesheet"]'));
              $('html head link[rel="stylesheet"]').remove();
              $('html head').append(`<style type="text/css" id="fastStyles">${fastStyles}</style>`);
            }

            $('html head').prepend(
              helmet.title.toString(),
              helmet.meta.toString(),
              helmet.link.toString()
            );

            $(`#${__ROOT_DOM_POINT__}`).after(
              `<script id="__reduxStore" type="text/javascript">window.__reduxStore='${reduxStoreString}';</script>`,
              `<script id="__apiPreload" type="text/javascript">window.__apiPreload='${apiPreloadString}';</script>`,
            );
            // $.html(htmlBeautify($.html(), { ocd: true }));
            $(`#${__ROOT_DOM_POINT__}`).html(content);
            const html = $.html();
            console.timeEnd('cheerio');
            console.time('beatifier');
            // html = htmlBeautify($.html(), { ocd: true, unformatted: ['#react-mount-node'] });
            console.timeEnd('beatifier');
            apiCookie.injectCookies(res);

            return res.send(html);
          }).catch((renderError) => {
            res.send(indexHtml);
            if (!__DEVELOPMENT__ && global.RavenInited) {
              global.Raven.captureException('Server normal render in stage of DataPreload got an error', {
                level: 'error',
                extra: { detail: renderError }
              });
            } else {
              console.error('Server normal render in stage of DataPreload got an error: ',
                renderError); //eslint-disable-line
            }
          });
      }

      res.status(404).send('Page Not found').end();
    });
  });

  if (!(+wwwPort)) throw new Error('web server port not defined');

  const webServer = new http.Server(app);

  webServer.listen({
    host: '0.0.0.0',
    port: wwwPort
  }, (err) => {
    if (err) throw new Error(err);
  });
}
