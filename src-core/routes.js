/* eslint-disable object-property-newline */
// import getComponent from './helpers/router/get-component';

// import unisenderV5Routes from '../src-sites/unisender_v5/routes';
// eslint-disable-next-line import/no-webpack-loader-syntax
// import isObject from 'lodash/isObject';
// import isArray from 'lodash/isArray';
import isFunction from 'lodash/isFunction';
import castArray from 'lodash/castArray';
import NavigateHelper from './helpers/router/nav-helper'; // , { outsideDefaultRoute }

export default function (siteRoutes, {
  api, store, history, cookies, serverSideRequest, serverSideResponse
}) {
  let childRoutes = siteRoutes;

  if (isFunction(siteRoutes)) {
    childRoutes = siteRoutes({
      api, store, history, cookies,
      serverSideRequest, serverSideResponse
    });
  }

  // if (isArray(childRoutes)) childRoutes = siteRoutes;
  // if (isObject(childRoutes)) childRoutes = [siteRoutes];
  return {
    component: NavigateHelper,
    /* eslint-disable no-unused-vars */
    onChange: (prevState, nextState, replace, callback) => {
      // console.log('onChange', prevState, nextState);
      callback();
      //
      // setTimeout(()=>callback(), 10000);
    },
    childRoutes: castArray(childRoutes)
  };
}
