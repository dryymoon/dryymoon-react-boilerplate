/**
 * Created by dryymoon on 23.08.17.
 */
import React, { Component } from 'react';
import MomentLib, {} from 'react-moment';
// import 'moment/locale/ru';
// import 'moment/locale/en-gb';
import 'moment-timezone';
import { connect } from 'react-redux';
import omit from 'lodash/omit';

const mapStateToProps = ({ MomentDefaults = {} }, props) => ({ ...MomentDefaults, ...props });

@connect(mapStateToProps)
export default class Moment extends Component {
  render() {
    const filteredProps = omit(this.props, ['dispatch']);
    return (
      <MomentLib {...filteredProps} />
    );
  }
}
