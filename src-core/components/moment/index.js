/**
 * Created by dryymoon on 23.08.17.
 */

import Module from './moment';
import { sets } from './redux';

export default Module;

export { sets };
