/* eslint-disable max-len, no-console */
/**
 * Created by dryymoon on 24.08.17.
 */
import find from 'lodash/find';
import startsWith from 'lodash/startsWith';
import ReducersRegistry from '../../helpers/redux/reducers-registry';

const momentLocales = require.context('moment/locale', false, /\.js$/, 'lazy');

// `!file-loader?context=${__dirname}/../../../../build/block-editor/www&outputPath=block-editor/&name=[path][name].[ext]!../../../../build/block-editor/www`,
// '!file-loader?context=moment/locale&outputPath=moment-locales/&name=[path][name].[ext]!moment/locale',


// console.warn('momentLocales.keys()', momentLocales.keys(), momentLocales('./uk.js'));

const SETS_DEFAULT = 'MomentDefaults/SETS';

export const sets = (obj = {}) => (dispatch, getState) => {
  const { locale: currLocale } = getState().MomentDefaults;
  const { locale: nextLocale } = obj;

  if (currLocale !== nextLocale) {
    const localeLoaderKey = find(momentLocales.keys(), v => startsWith(v, nextLocale, 2));
    if (localeLoaderKey) {
      momentLocales(localeLoaderKey);
    } else {
      console.warn(`Cant find localization ${nextLocale} for moment`);
    }
  }

  dispatch({ type: SETS_DEFAULT, ...obj });
};

export const initialState = {
  tz: 'Etc/UCT',
  interval: 0,
  locale: 'en',
};

export default function reducer(state = initialState, action = {}) {
  const { type, ...restAction } = action;
  switch (type) {
    case SETS_DEFAULT:
      return { ...state, ...restAction };

    default:
      return state;
  }
}

ReducersRegistry.register({ MomentDefaults: reducer });
