import React, { Component } from 'react';
import ccn from 'ccn';
import './tooltip-cont.scss';

@ccn
export default class TooltipContainer extends Component {
  render() {
    return <div id="spaAppTooltipContainer" />;
  }
}
