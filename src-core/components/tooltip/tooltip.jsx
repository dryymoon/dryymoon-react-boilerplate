import { Component } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom'; // eslint-disable-line
import ccn from 'ccn';
import EventEmitter from 'eventemitter3';
import styles from './tooltip.scss';

// TODO BemStyle Override

const isBrowser =
  !!(typeof window !== 'undefined' && window.document && window.document.createElement);

function override(name) {
  return styles[name] || name;
}

let emitter = null;
if (isBrowser) emitter = new EventEmitter();

@ccn
class Tooltip extends Component {
  static selectors = Object.keys(styles);

  static propTypes = {
    container: PropTypes.any, // eslint-disable-line
    containerId: PropTypes.string,
    children: PropTypes.node.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool]),
    position: PropTypes.oneOf(['left', 'top', 'right', 'bottom']),
    fixed: PropTypes.bool,
    space: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    manualBr: PropTypes.bool,
    breakAll: PropTypes.bool,
    // forceShow:
  };

  static defaultProps = {
    position: 'top',
    fixed: true,
    space: 5
  };

  constructor(...args) {
    super(...args);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.handleMouseOut = this.handleMouseOut.bind(this);
    this.getTooltipPosition = this.getTooltipPosition.bind(this);
    this.getTooltipOffset = this.getTooltipOffset.bind(this);
    this.hideTooltip = this.hideTooltip.bind(this);
    this.handleShowToolTip = this.handleShowToolTip.bind(this);
    this.rand = Math.random();
    this.showed = false;
  }

  componentDidMount() {
    const { manualBr, breakAll } = this.props;
    if (this.props.container) this.container = this.props.container;
    if (this.props.containerId) this.container = document.getElementById(this.props.containerId);

    if (!this.container) {
      this.container = document.body;
      const appMountPoint = document.getElementById(__ROOT_DOM_POINT__);
      if (appMountPoint) this.container = appMountPoint;

      const maybeToolTipContainer = document.getElementById('spaAppTooltipContainer');
      if (maybeToolTipContainer) this.container = maybeToolTipContainer;
    }

    this.componentEl = findDOMNode(this); // eslint-disable-line
    this.tooltipEl = document.createElement('div');

    const tooltipArrowEl = document.createElement('div');
    // tooltipArrowEl.id = `id-${this.rand}`;
    tooltipArrowEl.className = override('tooltip-arrow');

    const tooltipContentEl = document.createElement('div');
    tooltipContentEl.className = override('tooltip-inner') +
      (manualBr ? ` ${override('tooltip-inner-manualBr')}` : '') +
      (breakAll ? ` ${override('tooltip-inner-breakAll')}` : '');
    tooltipContentEl.textContent = this.getTooltipTitle();

    this.tooltipEl.appendChild(tooltipArrowEl);
    this.tooltipEl.appendChild(tooltipContentEl);
    this.tooltipEl.className = `${override('tooltip')} ${override(this.props.position)}`;
    this.container.appendChild(this.tooltipEl);
    this.hideTooltip();
    emitter.on('tooltip-showed', this.handleShowToolTip);
    this.componentEl.addEventListener(this.props.fixed ? 'mouseenter' : 'mousemove', this.handleMouseMove);
    this.componentEl.addEventListener('mouseleave', this.handleMouseOut);
  }

  componentDidUpdate({ title: prevTitle = '', position: prevPosition }) {
    const currTitle = this.getTooltipTitle();
    const { position: currPosition } = this.props;

    if (prevTitle !== currTitle) {
      this.tooltipEl.childNodes[1].textContent = currTitle;
      if (!currTitle) this.hideTooltip();
    }

    if (prevPosition !== currPosition) {
      this.tooltipEl.className = `${override('tooltip')} ${override(currPosition)}`;
    }

    if (this.showed) this.updatePosition();
  }

  componentWillUnmount() {
    emitter.off('tooltip-showed', this.handleShowToolTip);
    this.componentEl.removeEventListener(this.props.fixed ? 'mouseenter' : 'mousemove', this.handleMouseMove);
    this.componentEl.removeEventListener('mouseleave', this.handleMouseOut);
    this.container.removeChild(this.tooltipEl);
  }

  getTooltipTitle() {
    const { title } = this.props;
    return title || '';
  }

  getTooltipPosition(e) {
    let pointX;
    let pointY;
    const bodyRect = document.body.getBoundingClientRect();
    const containerRect = this.container.getBoundingClientRect();
    const containerOffsetX = containerRect.left - bodyRect.left;
    const containerOffsetY = containerRect.top - bodyRect.top;
    if (this.props.fixed) {
      const componentRect = this.componentEl.getBoundingClientRect();
      const componentOffsetX = componentRect.left - containerOffsetX;
      const componentOffsetY = componentRect.top - containerOffsetY;
      const componentWidth = this.componentEl.offsetWidth;
      const componentHeight = this.componentEl.offsetHeight;
      let cOffsetX = 0;
      let cOffsetY = 0;
      switch (this.props.position) {
        case 'top':
          cOffsetX = componentWidth / 2;
          cOffsetY = 0;
          break;
        case 'right':
          cOffsetX = componentWidth;
          cOffsetY = componentHeight / 2;
          break;
        case 'bottom':
          cOffsetX = componentWidth / 2;
          cOffsetY = componentHeight;
          break;
        case 'left':
          cOffsetX = 0;
          cOffsetY = componentHeight / 2;
          break;
        default:
          break;
      }
      pointX = componentOffsetX + cOffsetX + (window.scrollX || window.pageXOffset);
      pointY = componentOffsetY + cOffsetY + (window.scrollY || window.pageYOffset);
    } else {
      const { clientX, clientY } = e;
      pointX = (clientX - containerOffsetX) + (window.scrollX || window.pageXOffset);
      pointY = (clientY - containerOffsetY) + (window.scrollY || window.pageYOffset);
    }
    return {
      x: pointX,
      y: pointY
    };
  }

  getTooltipOffset() {
    const tooltipW = this.tooltipEl.offsetWidth;
    const tooltipH = this.tooltipEl.offsetHeight;
    let offsetX = 0;
    let offsetY = 0;
    switch (this.props.position) {
      case 'top':
        offsetX = -(tooltipW / 2);
        offsetY = -(tooltipH + Number(this.props.space));
        break;
      case 'right':
        offsetX = Number(this.props.space);
        offsetY = -(tooltipH / 2);
        break;
      case 'bottom':
        offsetX = -(tooltipW / 2);
        offsetY = Number(this.props.space);
        break;
      case 'left':
        offsetX = -(tooltipW + Number(this.props.space));
        offsetY = -(tooltipH / 2);
        break;
      default:
        break;
    }
    return {
      x: offsetX,
      y: offsetY
    };
  }

  handleShowToolTip(id) {
    if ((id !== this.rand) && this.showed) this.hideTooltip();
  }

  handleMouseMove(e) {
    if (!this.getTooltipTitle()) return;
    if (e) this.lastEv = e;
    if (!this.showed) {
      this.showed = true;
      emitter.emit('tooltip-showed', this.rand);
    }
    this.updatePosition(e);
  }

  updatePosition(e) {
    const tooltipPosition = this.getTooltipPosition(e || this.lastEv);
    const tooltipOffset = this.getTooltipOffset();

    this.tooltipEl.style.left = `${tooltipPosition.x + tooltipOffset.x}px`;
    this.tooltipEl.style.top = `${tooltipPosition.y + tooltipOffset.y}px`;
    this.tooltipEl.style.opacity = 1;
  }

  handleMouseOut() {
    this.hideTooltip();
  }

  hideTooltip() {
    this.showed = false;
    // this.tooltipEl.style.transition = 'opacity 0.4s';
    this.tooltipEl.style.left = '-5000px';
    this.tooltipEl.style.top = '-5000px';
    this.tooltipEl.style.opacity = 0;
  }

  render() {
    return this.props.children;
  }
}

export default Tooltip;
