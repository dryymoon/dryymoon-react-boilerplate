/* eslint-disable no-unused-vars, no-mixed-operators */
import assign from 'lodash/assign';
import keyBy from 'lodash/keyBy';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import isString from 'lodash/isString';
import values from 'lodash/values';
import keys from 'lodash/keys';
import xor from 'lodash/xor';
import get from 'lodash/get';
import omit from 'lodash/omit';
import mapValues from 'lodash/mapValues';
import cloneDeep from 'lodash/cloneDeep';
import intersection from 'lodash/intersection';
import isEmpty from 'lodash/isEmpty';
import uniqueString from 'unique-string'; // eslint-disable-line
// import Invalidator from 'components/invalidator';
// import configTree from './config-tree';
// import nodeSchema from '../schema/automation/chain/node.schema.json';

// const nodeFastValidator = new Invalidator(nodeSchema);

const modelExample = {
  $$acyclic: null,
  nodes: {
    id1: {
      id: 'id1',
      $$hasParent: false,
      $$inEdges: [],
      $$outEdges: [],
    },
    id2: {}
  },
  edges: {
    id3: {
      id: 'id3',
      from: 'id1',
      to: 'id2',
      // name: 'gala'
    },
    id4: {}
  }
};

export default class NamedTree {
  constructor(model = {}) {
    this.$$model = cloneDeep(model);
    const { nodes = [], edges = [] } = model;
    this.$$model.nodes = keyBy(nodes, 'id');
    this.$$model.edges = keyBy(edges, 'id');
    this.$$model.nodes = mapValues((node) => {
      const newNode = { $$inEdges: [], $$outEdges: [], ...node };
      // newNode
    });
    this.$$checkACyclic();
    // this.$$foundRoots();
  }

  transaction() {

  }

  commit() {

  }

  parse() {

  }

  walkNodes() {

  }

  stringify() {

  }

  createCursor() {

  }

  cloneCursor(existedCursor) {

  }

  $$checkACyclic() {
    const visitedNodes = [];
    this.$$model.$$acyclic = true;
    this.walkNodes(({ id }) => {
      if (visitedNodes.indexOf(id) > -1) this.$$model.$$acyclic = false;
      visitedNodes.push(id);
    });
  }
}

export class Model {
  constructor(model, beforeTransactionModel) {
    this._ = { beforeTransactionModel };
    return this.refresh(model);
  }

  transaction() {
    return new Model(this.getModel(), this);
  }

  commit(steals) {
    if (!this._.beforeTransactionModel) throw new Error('Commit without transaction');
    if (!isEmpty(this._.transactionErrors)) {
      if (steals) return this._.beforeTransactionModel;
      throw new Error('TransAction has Errors');
    }
    return this._.beforeTransactionModel.refresh(this.refresh().getModel());
  }

  refresh(model) {
    this._.model = model && cloneDeep(model) || this._.model;
    // this._.nodeConfig = mapValues(this._.model, ({ type }) => config[type]);
    // this._.nodeChildKeys = mapValues(this._.nodeConfig,
    // ({ treeChilds = {} }) => keys(treeChilds));
    this._.nodeChildKeys = mapValues(this._.model, node => Model.getNodePorts(node));
    this._.nodeParentId = {};
    this._.nodeParentPort = {};
    values(this._.model).forEach(({ id: parentId, ...parentNode }) => {
      this._.nodeChildKeys[parentId].forEach((chKey) => {
        const maybeChildId = parentNode[chKey];
        if (!maybeChildId) return;
        this._.nodeParentId[maybeChildId] = parentId;
        this._.nodeParentPort[maybeChildId] = chKey;
      });
    });
    this._.rootIds = xor(keys(this._.model), keys(this._.nodeParentId));
    return this;
  }

  getModel() {
    return this._.model;
  }

  getRootNodes() {
    return this._.rootIds.map(id => this.getNode(id));
  }

  cloneModel(...args) {
    const [{ nextId, nodeOrId } = {}] = args;
    if (!nodeOrId) {
      const newModels = this.getRootNodes().map(root =>
        this.cloneModel({ nodeOrId: root }));
      return this.refresh(assign({}, ...newModels));
    }
    const node = this.getNode(nodeOrId);

    const copyNode = Model.cloneNode(node);
    const newId = nextId || uniqueString(32);
    copyNode.id = newId;

    const accChildArr = this.getNodePorts(node).map((port) => {
      const oldCh = this.getChildNode(node, port);
      if (oldCh) {
        const newChId = uniqueString(32);
        copyNode[port] = newChId;
        return this.cloneModel({ nextId: newChId, nodeOrId: oldCh });
      }
      return null;
    });
    return assign({}, ...accChildArr, { [newId]: copyNode });
  }

  static fromBackedModel(backendModel) {
    if (!isArray(backendModel)) {
      throw new Error(`This isn\`t backend model: ${JSON.stringify(backendModel)}`);
    }
    return new Model(keyBy(backendModel, 'id'));
  }

  getBackendModel() {
    this.refresh();
    return values(this._.model);
  }

  getParentNode(nodeOrId) {
    const nodeId = getNodeId(nodeOrId);
    const parentNodeId = this._.nodeParentId[nodeId];
    if (!parentNodeId) return;
    return this.getNode(parentNodeId);
  }

  getParentNodePort(nodeOrId) {
    const nodeId = getNodeId(nodeOrId);
    return this._.nodeParentPort[nodeId];
  }

  getParentNodes(nodeOrId) {
    const parentNode = this.getParentNode(nodeOrId);
    if (parentNode) return [parentNode, ...this.getParentNodes(parentNode)];
    return [];
  }

  static isNewNode({ model = {}, node: { id } = {} }) {
    if (!id) return true;
    return !model[id];
  }

  static getNodePorts(node) {
    if (!node || !node.type) return [];
    return keys(get(configTree, [node.type], {}));
  }

  getNodePorts(nodeOrId) {
    const nodeId = getNodeId(nodeOrId);
    return this._.nodeChildKeys[nodeId] || [];
  }

  static getNodePortConfig({ type }, port) {
    return get(configTree, [type, port], {});
  }

  static hasNodeChilds(node, port) {
    const ports = Model.getNodePorts(node);

    if (ports.length === 0) return false;

    if (port) {
      if (ports.indexOf(port) === -1) return false;
      return !!node[port];
    }

    return ports.some(p => !!node[p]);
  }

  static hasNodeUnconnectedPorts(node) {
    const ports = Model.getNodePorts(node);

    if (ports.length === 0) return false;

    return ports.some(p => !node[p]);
  }

  getChildNode(nodeOrId, portOrNull) {
    const nodeId = getNodeId(nodeOrId);
    const { model } = this._;
    const ports = this.getNodePorts(nodeId);
    if (ports.length === 1) {
      const childId = model[nodeId][ports[0]];
      return this.getNode(childId);
    }
    if (portOrNull && (ports.length > 1)
      && (ports.indexOf(portOrNull) > -1)) {
      const childId = model[nodeId][portOrNull];
      return this.getNode(childId);
    }
    // return false;
  }

  getNodeChilds(nodeOrId) {
    const ports = this.getNodePorts(nodeOrId);
    const node = this.getNode(nodeOrId);
    if (!ports || !node) return [];
    return ports.map(port => this.getNode(node[port])).filter(it => it);
  }

  getNode(nodeOrId) {
    if (!nodeOrId) return;
    return this._.model[getNodeId(nodeOrId)];
  }

  getAllNodes() {
    return values(this._.model);
  }

  removeNode(nodeOrId) {
    const node = this.getNode(nodeOrId);
    const parentNode = this.getParentNode(nodeOrId);
    if (!node) return this;
    if (parentNode) this.unlinkNodes(node, parentNode);
    this.getNodeChilds(nodeOrId).map(child => this.removeNode(child));
    this._.model = omit(this._.model, getNodeId(nodeOrId));
    this.refresh();
    return this;
  }

  static cloneNode(node) {
    const id = uniqueString(32);
    const newNode = { ...cloneDeep(node), id };
    return omit(newNode, Model.getNodePorts(newNode));
  }

  cloneNode(nodeOrId) {
    return Model.cloneNode(this.getNode(nodeOrId));
  }

  addNode(node) {
    const clonedNode = cloneDeep(node);
    const { id, type } = clonedNode;
    if (this._.model[id]) throw new Error('Model already has node with this id');
    if (!id) throw new Error('Node hasn`t id');
    if (!type) throw new Error('Node hasn`t type');
    this.refresh({ ...this._.model, [id]: clonedNode });
    const hasUnknownChilds = this._.nodeChildKeys[id].some(key => clonedNode[key]);
    if (hasUnknownChilds) throw new Error('Node has Childs, you must clone node before add');
    return this;
  }

  updateNode(unsafeNode) {
    let node = cloneDeep(unsafeNode);
    const { id, type } = node;
    if (!this._.model[id]) throw new Error('Model hasn`t node with this id');
    if (!type) throw new Error('Node hasn`t type');
    const ports = Model.getNodePorts(node);
    node = omit(node, ports);
    ports.map((port) => {
      const chId = getNodeId(this.getChildNode(node, port));
      if (chId) node[port] = chId;
      return null;
    });
    this.refresh({ ...this._.model, [id]: node });
    return this;
  }

  addPartModel(unsafeModel) {
    if (!unsafeModel) return this;
    const model = cloneDeep(unsafeModel);
    const [hasIntersections] = intersection(keys(this._.model), keys(model));
    if (hasIntersections) {
      throw new Error('Cant add addPartModel, ' +
        'because Models has intersection, ' +
        'cloneModel before addPartModel');
    }
    return this.refresh({ ...this._.model, ...model });
  }

  unlinkNodes(node1, node2) {
    if (!this.getNode(node1) || !this.getNode(node2)) return this;
    let parent;
    let child;
    if (getNodeId(this.getParentNode(node2)) === getNodeId(node1)) {
      parent = node1;
      child = node2;
    }
    if (getNodeId(this.getParentNode(node1)) === getNodeId(node2)) {
      parent = node2;
      child = node1;
    }
    if (parent && child) {
      const parentId = getNodeId(parent);
      const childId = getNodeId(child);
      this._.model[parentId][this.getParentNodePort(childId)] = undefined;
      this.refresh();
    }
    return this;
  }

  linkNodes(parent, child, portOrNull) {
    if (!this.getNode(parent) || !this.getNode(child)) return this;
    const ports = this.getNodePorts(parent);
    let port = null;
    if (ports.length === 1) port = ports[0];
    if (ports.length > 1 && ports.indexOf(portOrNull) > -1) port = portOrNull;

    if (!this.getParentNode(child)
      && !this.getChildNode(parent, portOrNull)
      && port) {
      this._.model[getNodeId(parent)][port] = getNodeId(child);
      this.refresh();
    }
    return this;
  }

  isValidTree() {
    // Only one root in Tree
    const rootIds = this.getRootNodes();
    if (rootIds.length !== 1) return false;

    // Root node is Start type
    const rootNode = this.getNode(rootIds[0]);
    if (rootNode.type !== 'start') return false;

    // Tree hasn`t empty ports in any node
    if (this.getAllNodes().some(node =>
      this.getNodePorts(node).length !== this.getNodeChilds(node).length)) return false;

    // Trigger is not follow after Start
    if (this.getChildNode(rootNode).type === 'trigger') return false;

    // Finish is not follow after Start
    if (this.getChildNode(rootNode).type === 'finish') return false;
  }

  isValidAllNodes() {
    return this.getAllNodes().each(Model.isNodeValid);
  }

  static isNodeValid(node) {
    // return nodeFastValidator.validate(node);
  }
}

export function getNodeId(nodeOrId) {
  if (isString(nodeOrId)) return nodeOrId;
  if (isObject(nodeOrId) && nodeOrId.id) return nodeOrId.id;
  // throw new Error('It is not a node');
}
export const isNewNode = Model.isNewNode;
export const isNodeValid = Model.isNodeValid;
export const cloneNode = Model.cloneNode;
export const hasNodeChilds = Model.hasNodeChilds;
export const getNodePorts = Model.getNodePorts;
export const getNodePortConfig = Model.getNodePortConfig;
export const fromBackedModel = Model.fromBackedModel;
export const hasNodeUnconnectedPorts = Model.hasNodeUnconnectedPorts;
