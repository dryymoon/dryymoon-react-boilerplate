import { sessionStorage, localStorage } from './state-persistance';

export default sessionStorage;
export { sessionStorage, localStorage };
