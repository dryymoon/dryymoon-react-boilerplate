/**
 * Created by dryymoon on 11.09.17.
 */
import React, { Component } from 'react';
import isBrowser from 'isBrowser';

function configurator(key, storage) {
  return function decorator(DecoratedComponent) {
    return class StateSaverToStorage extends Component {
      /* constructor(props, ...args) {
       let newProps = props;
       if (storage) {
       try {
       const savedState = JSON.parse(storage.getItem(key));
       newProps = { savedState, ...props };
       } catch (e) {
       }
       }

       super(newProps, ...args);

       if (this.getStateToSave) this.getStateToSave = this.getStateToSave.bind(this);
       }

       forceSaveState() {
       if (!storage) return;
       const { getStateToSave } = this;
       let { state } = this;
       if (state) {
       if (getStateToSave) state = getStateToSave(state);
       storage.setItem(key, JSON.stringify(state));
       }
       }

       componentWillUnmount() {
       this.forceSaveState();
       if (super.componentWillUnmount) super.componentWillUnmount();
       } */

      $load() {
        if (!storage) return;
        try {
          return JSON.parse(storage.getItem(key));
        } catch (e) { // eslint-disable-line no-empty
        }
      }

      $save(data) {
        if (!storage) return;
        storage.setItem(key, JSON.stringify(data));
      }

      render() {
        const { $load, $save } = this;
        return (<DecoratedComponent {...this.props} {...{ $load, $save }} />);
      }
    };
  };
}

export function sessionStorage(key) {
  if (isBrowser && window.sessionStorage) return configurator(key, window.sessionStorage);
  return configurator(key);
}

export function localStorage(key) {
  if (isBrowser && window.localStorage) return configurator(key, window.localStorage);
  return configurator(key);
}
