/**
 * Created by Vit on 25.07.2016.
 */
import React, { Component } from 'react';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const shadeCss = require('./shade.scss');

@ccn
@bem
class Shade extends Component {
  static propTypes = {};

  render() {
    return (
      <div data-block="shade" data-bemstyles={shadeCss} />
    );
  }
}
export default Shade;
