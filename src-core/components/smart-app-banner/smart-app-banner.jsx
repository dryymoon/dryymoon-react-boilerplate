/**
 * Created by dryymoon on 09.12.16.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ua from 'ua-parser-js';
import Rating from 'components/star-rating';
import sensor from 'components/sensor';
// import appIcon from '../../../src-native/assets/app-icon.png';
import isBrowser from '../../helpers/isBrowser';
import cookies from '../../helpers/react/ReactCookiesProvider';
import './smart-app-banner.scss';
import googlePlayImg from './google_play_200.png';
/* let GooglePlayScrapper = () => Promise.reject({});
 let AppStoreScrapper = () => Promise.reject({});

 if (__SERVER__) {
 GooglePlayScrapper = require('google-play-scraper');
 AppStoreScrapper = require('app-store-scraper');
 } */

const allSettings = {
  ios: {
    appMeta: 'apple-itunes-app',
    iconRels: ['apple-touch-icon-precomposed', 'apple-touch-icon'],
    getStoreLink: ({ language = 'us', appId } = {}) =>
      `https://itunes.apple.com/${language}/app/id${appId}`,
  },
  android: {
    appMeta: 'google-play-app',
    iconRels: ['android-touch-icon', 'apple-touch-icon-precomposed', 'apple-touch-icon'],
    getStoreLink: ({ appId } = {}) =>
      `http://play.google.com/store/apps/details?id=${appId}`,
  }
};
@sensor
@cookies
class SmartAppBanner extends Component {
  static propTypes = {
    fixedBottom: PropTypes.bool,
    userAgent: PropTypes.string,
    defaultLanguage: PropTypes.string,
    daysHidden: PropTypes.number,
    // daysReminder: PropTypes.number,
    appName: PropTypes.string.isRequired,
    appDeveloper: PropTypes.string.isRequired,

    appIdIOS: PropTypes.number, //eslint-disable-line
    appIdAndroid: PropTypes.string, //eslint-disable-line

    priceText: PropTypes.string,
    price: PropTypes.string,
    installButtonText: PropTypes.string,
    installButtonTextIOS: PropTypes.string,
    installButtonTextAndroid: PropTypes.string,
    force: PropTypes.string,
    children: PropTypes.node,
    disabled: PropTypes.bool,
    icon: PropTypes.any // eslint-disable-line
  };

  static defaultProps = {
    userAgent: isBrowser ? window.navigator.userAgent : '',
    defaultLanguage: 'ru',
    daysHidden: 15,
    // daysReminder: 90,
    priceText: 'Цена',
    price: 'Бесплатно',
    installButtonText: 'Установить',
    installButtonTextIOS: 'Смотреть',
    installButtonTextAndroid: 'Установить',
    force: '',
  };

  constructor(props, context) {
    super(props, context);
    this.close = this.close.bind(this);
    this.install = this.install.bind(this);
    this.state = {}; // this.combineState(props, context);
  }

  componentDidMount() {
    this.language = getBrowserPreferLang(this.props.defaultLanguage);
    this.setState(this.combineState()); // eslint-disable-line
    if (!this.domNode) return;
    this.height = this.domNode.offsetHeight;
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.force !== this.props.force) {
      this.setState(this.combineState(nextProps));
    }
  }

  componentDidUpdate() {
    if (!this.isEnabled()) return;
    if (!this.domNode) return;
    this.height = this.domNode.offsetHeight;
  }

  onScrollSync({ scrollTop } = {}) {
    if (!this.props.fixedBottom) return;
    if (!this.isEnabled()) return;
    if (!this.domNode) return;
    let bottom = 0;
    if (scrollTop > 0) bottom = `${-scrollTop}px`;
    if (scrollTop > this.height) bottom = `${-this.height}px`;
    if (this.lastBottom === bottom) return;
    this.lastBottom = bottom;
    this.domNode.style.bottom = bottom;
  }

  getDeviceType() {
    const agent = ua(this.props.userAgent);

    // iOS >= 6 has native support for Smart Banner
    if (agent.browser.name === 'Safari' && agent.os.name === 'iOS' && parseInt(agent.os.version, 10) >= 6) return null;
    if (agent.os.name === 'iOS') return 'ios';
    if (agent.os.name === 'Android') return 'android';
  }

  combineState(maybeProps, maybeContext) {
    const props = maybeProps || this.props;
    const context = maybeContext || this.context;

    const isCookiesExist = context.cookies.get('smartappbanner-closed') || context.cookies.get('smartappbanner-installed');
    if (!props.force && isCookiesExist) return { disabled: true };

    const deviceType = props.force || this.getDeviceType();
    const settings = allSettings[deviceType];
    let appId = null;
    if (deviceType === 'android') appId = props.appIdAndroid;
    if (deviceType === 'ios') appId = props.appIdIOS;

    return { deviceType, settings, appId };
  }

  close() {
    this.setState({ disabled: true });
    this.cookies.set('smartappbanner-closed', 'true', {
      path: '/',
      expires: +new Date() + (this.props.daysHidden * 1000 * 60 * 60 * 24)
    });
  }

  install() {
    /* this.cookies.set('smartappbanner-installed', 'true', {
     path: '/',
     expires: +new Date() + (this.props.daysReminder * 1000 * 60 * 60 * 24)
     }); */
    // const that = this;
    // setTimeout(() => that.setState({ disabled: true }), 0);
  }

  retrieveInfo() {
    // const { language } = this.props;
    const { settings, appId } = this.state;
    const link = settings.getStoreLink({ language: this.language, appId });

    return { link };
  }

  isEnabled() {
    const { deviceType, appId, disabled } = this.state;
    if (deviceType && appId && !disabled) return true;
  }

  renderBanner() {
    if (__APP__) return null;
    if (__SERVER__) return null;
    const { deviceType } = this.state;
    if (!this.isEnabled()) return null;

    const wrapperClassName = `smartbanner smartbanner_${deviceType}`;
    const wrapperStyles = {};

    if (this.props.fixedBottom) {
      wrapperStyles.position = 'fixed';
      wrapperStyles.bottom = this.lastBottom || 0;
      wrapperStyles.zIndex = 9998;
    }

    let inner;
    switch (this.state.deviceType) {
      case 'android':
        inner = this.renderAndroidBanner();
        break;
      case 'ios':
        inner = this.renderIOSBanner();
        break;
      default:
        inner = null;
    }
    return (
      <div
        className={wrapperClassName}
        style={wrapperStyles}
        ref={ref => this.domNode = ref}>
        {inner}
      </div>
    );
  }

  renderAndroidBanner() {
    const { link } = this.retrieveInfo();
    const { icon } = this.props;
    const appImageStyle = {};
    if (icon) appImageStyle.backgroundImage = `url(${icon})`;

    return (
      <div className="smartbanner__container">
        <span
          className="smartbanner__appImage"
          style={appImageStyle} />
        <div
          className="smartbanner__appDescription">
          <div className="smartbanner__appName">{this.props.appName}</div>
          <Rating interactive={false} rating={3.6} />
        </div>
        <div
          className="smartbanner__installContainer"
          style={{ backgroundImage: `url(${googlePlayImg})` }}>
          <a href={link} onClick={this.install} className="smartbanner__installButton">
            {this.props.installButtonTextAndroid || this.props.installButtonText}
          </a>
        </div>
        <button className="smartbanner__closeButton" onClick={this.close}>&times;</button>
      </div>
    );
  }

  renderIOSBanner() {
    const { link } = this.retrieveInfo();
    const { icon } = this.props;
    const appImageStyle = {};
    if (icon) appImageStyle.backgroundImage = `url(${icon})`;

    return (
      <div className="smartbanner__container">
        <button className="smartbanner__closeButton" onClick={this.close}>&times;</button>
        <span
          className="smartbanner__appImage"
          style={appImageStyle} />
        <div
          className="smartbanner__appDescription">
          <div className="smartbanner__appName">{this.props.appName}</div>
          <div className="smartbanner__appDeveloper">{this.props.appDeveloper}</div>
          <Rating interactive={false} rating={3.6} />
          <div className="smartbanner__appPrice">{this.props.priceText}: {this.props.price}</div>
        </div>
        <a href={link} onClick={this.install} className="smartbanner__installButton">
          {this.props.installButtonTextIOS || this.props.installButtonText}
        </a>
      </div>
    );
  }

  render() {
    const { disabled, children } = this.props;
    if (disabled) return null;

    const banner = this.renderBanner();

    if (children) {
      return (
        <div>
          {children}
          {banner}
        </div>
      );
    }

    return banner;
  }
}

export default SmartAppBanner;

function getBrowserPreferLang(defLang) {
  const lang =
    (navigator.languages && navigator.languages[0])
    || navigator.language
    || navigator.userLanguage;

  if (lang) return lang.slice(0, 2);

  return defLang || 'us';
}
