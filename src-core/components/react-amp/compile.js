import fs from 'fs';
import path from 'path';

const appRootDir = require('app-root-dir')
  .get();

const builtinsDir = fs.readdirSync(path.join(appRootDir, './node_modules/amp-html/builtins/'));
const extensionsDir = fs.readdirSync(path.join(appRootDir, './node_modules/amp-html/extensions/'));

const getAmpComponents = (dirItems, regex) => {
  const ampComponents = dirItems.reduce(
    (components, dirItem) => {
      const component = dirItem.match(regex);
      if (component !== null && component[1]) components.push(component[1]);
      return components;
    }, []
  );

  return ampComponents;
};

const builtins = getAmpComponents(builtinsDir, /^amp-(.*?)\.js$/);
const extensions = getAmpComponents(extensionsDir, /^amp-(.*?)$/);

const combined = {};

builtins.forEach(it => combined[it] = { name: it, tag: `amp-${it}` });
extensions.forEach(it =>
  combined[it] = {
    name: it,
    tag: `${it}`,
    script: {
      tag: `amp-${it}`,
      url: `https://cdn.ampproject.org/v0/amp-${it}-0.1.js`
    }
  }
);


fs.writeFile(path.join(__dirname, './tags.json'), JSON.stringify(combined), (err) => {
  if (err) throw err;
  // eslint-disable-next-line
  console.log('Compilation complete.');
});

/*
 function capitalize(string) {
 return string.charAt(0).toUpperCase() + string.slice(1);
 }

 const code = [];

 builtins.map((it)=>{
 code.push(`export const Amp${capitalize(it)}`)
 });

 fs.writeFile(path.join(__dirname,'./index.js'), code, err => {
 if (err) throw err;
 // eslint-disable-next-line
 console.log('Compilation complete.');
 });
 */
