import React from 'react';
import Helmet from 'react-helmet';
import tags from './tags.json';

const reactAmpHtml = (directive) => { // , addScript = () => {}
  const tag = tags[directive];
  const AmpComponent = tag.tag;
  if (!tag) {
    throw new Error(`"${directive}" is not a valid amp-html directive.`);
  }

  const ret = {};
  ret.script = () => null;


  if (tag.script) {
    ret.script = () => (<Helmet
      script={[
        { async: undefined, 'custom-element': tag.script.tag, src: tag.script.url }
      ]} />);
  }
  ret[`Amp${capitalizeFirstLetter(directive)}Script`] = ret.script;

  ret.component = props => <AmpComponent {...props} />;

  ret[`Amp${capitalizeFirstLetter(directive)}`] = ret.component;
  ret.tag = ret.component;

  return ret;
};

export default reactAmpHtml;

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
