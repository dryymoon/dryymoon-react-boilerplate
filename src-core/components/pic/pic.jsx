/**
 * Created by Vit on 21.07.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import amphtml from '../react-amp';
import ccn from '../../helpers/styles/CollectClasses';

import styles from './pic.scss';
import picAmpCss from './pic-amp.scss';

import srcDefault from './data/unnamed.png';

// import Helmet from 'react-helmet';

const ampScripts = [];
const addScript = script => ampScripts.push(script);

const AmpImg = amphtml('img', addScript).tag;

@ccn
@bem
class Pic extends Component {
  static propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,

    src: PropTypes.string.isRequired,
    /*
    source: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        s: PropTypes.arrayOf(PropTypes.shape({
          width: PropTypes.number.isRequired,
          height: PropTypes.number.isRequired,
          url: PropTypes.string.isRequired
        })),
        t: PropTypes.oneOf(['image/jpeg']).isRequired,
        b64: PropTypes.string.isRequired, // Может нах не надо.
        ratio: PropTypes.number.isRequired
      })
    ]),
    */
    layout: PropTypes.string,
    alt: PropTypes.string,
    inlineBlock: PropTypes.string
  };

  render() {
    // const { source } = this.props;
    const { inlineBlock } = this.props;
    const { src = srcDefault, alt = '' } = this.props;
    const { width, height } = this.props;
    const { layout } = this.props;

    const mods = { inlineBlock };

    if (__AMP__) {
      //      const stylesArr = [];
      //      stylesArr.push(/*`.picAmp amp-img#${this.state.id} {max-width: ${width}px;}`*/);
      //      const combinedStyle = stylesArr.join(' ');
      // <Helmet style={[{ cssText: combinedStyle }]} />

      return (
        <div
          data-block="picAmp"
          data-bemstyles={picAmpCss}
          // style={{ maxWidth: `${width}px` }}
        >

          <AmpImg
            // ref={(ref) => { this.picRef = ref; }}
            src={src}
            width={width}
            height={height}
            // style={{ maxWidth: `${width}px` }}
            layout={layout || 'responsive'}
            placeholder
            alt={alt}
          />
        </div>
      );
    }

    return (
      <div data-block="pic" data-mods={mods} data-bemstyles={styles}>
        <div data-elem="wrapper">
          <img
            data-elem="img"
            src={src || `${srcDefault}`}
            alt={alt}
          />
        </div>
      </div>
    );
  }
}
export default Pic;
