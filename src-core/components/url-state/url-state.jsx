import { Component } from 'react';
import { withRouter } from 'react-router';
import getDisplayName from 'react-display-name';
import extend from 'extend';

function factory(tagOrTags) {
  return function wrapComponent(BaseComponent) {
    @withRouter
    class ComponentWithUrlState extends BaseComponent {
      static displayName = `urlState(${getDisplayName(BaseComponent)})`;

      static contextTypes = extend(BaseComponent.contextTypes || {}, {
        //   getUid: PropTypes.func
      });

      constructor(props, context) {
        super(props, context);
        // this.getUid = context.getUid;
        // this.uid = context.getUid();
      }

      setUrlState(tag, value) {
        if (typeof tag === 'string' && typeof value !== 'undefined') {
          const obj = {};
          obj[tag] = value;
          this.setUrlState(obj);
        }
      }

      getUrlState(tag = null) {

      }
    }

    return ComponentWithUrlState;
  };
}
export default factory();
export { factory };
