/**
 * Created by Vit on 03.08.2016.
 */
/* eslint-disable */
import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import bezierEasing from 'bezier-easing';
import isEqual from 'lodash/isEqual';
import isMatch from 'lodash/isMatch';
import sensor from 'components/sensor';
import parse from 'parse-unit';
import uniqueId from 'uniqueId';
import ccn from 'ccn';
import bem from 'bem';

const initialStyles = require('./slider.scss');

@ccn
@bem
@sensor
@uniqueId
class Slider extends Component {
  static defaultProps = {
    itemWidth: '94%',
    itemStickX: 'center'
  };

  static propTypes = {
    itemWidth: (props, propName, componentName) =>
      validateItemWidth(props[propName]),
    itemStickX: (props, propName, componentName) =>
      validateItemStickX(props[propName]),
    children: PropTypes.node
  };

  static scrollAnimation = {
    overRegress: {
      duration: 400,
      easing: bezierEasing(0.455, 0.03, 0.515, 0.955)
    },
    prolongation: {
      duration: 5000,
      easing: bezierEasing(0.165, 0.84, 0.44, 1) // easeOutQuart
    },
    onBtn: {
      duration: 100,
      easing: bezierEasing(0.39, 0.575, 0.565, 1)
    }
  };

  constructor(props, context) {
    super(props, context);
    /* this.handleDragging = this.handleDragging.bind(this);
    this.handleDragStart = this.handleDragStart.bind(this);
    this.handleDragEnd = this.handleDragEnd.bind(this);
    this.startScrollProlongation = this.startScrollProlongation.bind(this);
    this.runScrollProlongation = this.runScrollProlongation.bind(this); */
    this.state = {
      // styles: {}
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (isEqual(this.props, nextProps) && isMatch(this.state, nextState)) return false;
    return true;
  }

  componentDidMount() {
    const { scrollBarWidth } = this.sensor.getRuntime();
    this.rootSelector = document.getElementsByTagName('body')[0];
    // eslint-disable-next-line
    this.setState({
      containerStyle: {
        overflowX: 'scroll',
        marginBottom: `-${scrollBarWidth}px`,
        paddingBottom: `${scrollBarWidth}px`
      }
    });
  }

  getSnapDestX() {
    const [widthInt, widthType] = parse(this.props.itemWidth);
    const [snapDestVal, snapDestType] = parse(this.props.itemStickX);
    if (snapDestType === 'center' && widthType === '%') return `${100 % widthInt / 2}%`;
    if (snapDestType === 'left' && widthType === '%') return '0%';
    if (snapDestType === 'right' && widthType === '%') return `${100 % widthInt}%`;
    if (snapDestType === '%' && widthType === '%') return `${snapDestType}%`;
  }

  getStyles() {
    const width = this.props.itemWidth;
    return `
      .slider-container-${this.uid} {
        -webkit-scroll-snap-points-x: repeat(${width});
        -moz-scroll-snap-points-x: repeat(${width});
        -ms-scroll-snap-points-x: repeat(${width});
        scroll-snap-points-x: repeat(${width});
        -webkit-scroll-snap-destination: ${this.getSnapDestX()} 0%;
        -moz-scroll-snap-destination: ${this.getSnapDestX()} 0%;
        -ms-scroll-snap-destination: ${this.getSnapDestX()} 0%;
        scroll-snap-destination: ${this.getSnapDestX()} 0%;
      }
      .slider-item-${this.uid} {
        width: ${width}  
      }
    `;
  }

  getContainerStyle() {
    return this.state.containerStyle;
  }

  render() {
    const containerStyle = this.getContainerStyle();
    const children = Children.toArray(this.props.children);
    // if (children.length < 3) return null;
    // const arLeft = cloneElement(children.shift(), { 'data-elem': "arrow", 'data-mods': "left" });
    // const arRight = cloneElement(children.pop(), { 'data-elem': "arrow", 'data-mods': "right" });
    return (
      <div
        data-block="slider"
        data-bemstyles={initialStyles}
        className={`slider-${this.uid}`}
      >
        <Helmet style={[{ cssText: this.getStyles() }]}/>
        <div
          data-elem="scrollContainer"
          style={containerStyle}
          ref={el => this.scrollContainerRef = el}
          className={`slider-container-${this.uid}`}
        >
          { children.map((it, index) => {
            // if ((index === 0) || (index === (children.length - 1))) return null;
            if (!it) return null;
            const key = it.props && it.props.key ? it.props.key : index;
            return (
              <div
                data-elem="item"
                key={key}
                className={`slider-item-${this.uid}`}>
                {it}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default Slider;

function validateItemWidth(value) {
  const [int, type] = parse(value);
  if (int <= 100 && int >= 0 && type === '%') return true;
  if (int >= 0 && type === 'px') return true;
  return new Error(
    `Invalid prop itemWidth, 
      it must be 'center', 'left', 'right', 0-100%, or any px, 
      but received ${value}`
  );
}

function validateItemStickX(value) {
  if (value === 'center') return true;
  if (value === 'left') return true;
  if (value === 'right') return true;
  const [int, type] = parse(value);
  if (int <= 100 && int >= 0 && type === '%') return true;
  if (int >= 0 && type === 'px') return true;
  return new Error(
    `Invalid prop itemStickX, 
      it must be 'center', 'left', 'right', 0-100%, or any px, 
      but received ${value}`
  );
}

/*

{arLeft}
{arRight}
          </div>

 <DraggableCore
          disabled={false}
          enableUserSelectHack={false}
          onStart={this.handleDragStart}
          onDrag={this.handleDragging}
          onStop={this.handleDragEnd}>
</DraggableCore>

this.data.map((it, key)=> (
                <div data-elem="item" key={key} style={{backgroundColor: it}}/>))


<div data-elem="contentContainer">
  </div>
onStart={this.dragStartHandler}
                     onDrag={this.dragContinueHandler}
                     onStop={this.dragStopHandler}
                     disabled={ ( this.nativeControl ) }
 */
// {this.props.children}
function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

// this.supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;
// if(this.supportsTouch) this.setState({
//  nativeDrag : true
// });

/*
componentDidMount: function componentDidMount() {
			this.handleScroll(); // initialize state
			window.addEventListener('scroll', this.handleScroll);
			window.addEventListener("wheel", this.stopScrolling, false);
			window.addEventListener("touchstart", this.stopScrolling, false);
	},

	componentWillUnmount: function componentWillUnmount() {
			window.removeEventListener('scroll', this.handleScroll);
			window.removeEventListener("wheel", this.stopScrolling, false);
			window.removeEventListener("touchstart", this.stopScrolling, false);
	},
 */

/* startScrollProlongation() {
	let dragHistory = this.runtime.drag.history;

	if (dragHistory.length < 3) return;

	const now = Date.now();

	const historyDuration = 300;
	const afterInteraction = 50;
	const lastHistoryTimeStamp = dragHistory[dragHistory.length - 1].timestamp;
	if (now > lastHistoryTimeStamp + afterInteraction) return;

	dragHistory = dragHistory.filter(({ timestamp }) => timestamp >= now - historyDuration);
	dragHistory = dragHistory.filter(({ deltaX }) => deltaX !== 0);

	if (dragHistory.length < 2) return;

	const signs = dragHistory.map(({ deltaX })=> deltaX ? deltaX < 0 ? -1 : 1 : 0);
	const isSameSigns = !!signs.reduce((a, b) => (a === b) ? a : NaN);

	if (!isSameSigns) return;

	const dragStart = dragHistory[0];
	const dragEnd = dragHistory[dragHistory.length - 1];

	const dragSpeed = ((dragEnd.lastX - dragStart.lastX) / (dragEnd.timestamp - dragStart.timestamp)) / 2;

	this.runtime.prolongation = {
		startOffset: this.scrollContainerRef.scrollLeft,
		startTime: null,
		...Slider.scrollAnimation.prolongation,
		speed: dragSpeed,
	};
	this.runScrollProlongation();
	// this.runtime.rafId = window.requestAnimationFrame(this.runScrollProlongation);
}

runScrollProlongation() {
	if (!this.runtime.prolongation.startTime) this.runtime.prolongation.startTime = Date.now();
	const { startOffset, startTime, speed, duration, easing } = this.runtime.prolongation;

	const progress = (Date.now() - startTime) / duration;
	if (progress > 1) return;
	const deltaOffset = speed * duration * easing(progress);
	const finalOffset = startOffset - deltaOffset;
	if (finalOffset <= 0) return;
	if (finalOffset >= this.scrollContainerRef.scrollWidth) return;
	this.scrollContainerRef.scrollLeft = finalOffset;
	this.runtime.rafId = window.requestAnimationFrame(this.runScrollProlongation);
}

stopScrollProlongation() {
	this.stopAnyAnimations();
	this.runtime.prolongation = {};
}

stopAnyAnimations() {
	if (this.runtime.rafId) {
		window.cancelAnimationFrame(this.runtime.rafId);
		this.runtime.rafId = null;
	}
}

handleDragStart() {
	this.rootSelector.className += ' dragging';
}

handleDragEnd() {
	this.rootSelector.className =
		this.rootSelector.className
				.replace(/\bdragging\b/, '');
}

handleDragging(event, ui = {}) {
	this.scrollContainerRef.scrollLeft = this.scrollContainerRef.scrollLeft - ui.deltaX;
} */
