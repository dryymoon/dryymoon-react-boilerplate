/**
 * Created by dryymoon on 07.12.16.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'components/slider';
import ccn from 'ccn';
import bem from 'bem';
import sensor from 'components/sensor';

@ccn
@bem
@sensor
class MediaSlider extends Component {
  static propTypes = {
    children: PropTypes.node,
    desktop: PropTypes.number.isRequired, // eslint-disable-line
    tablet: PropTypes.number.isRequired, // eslint-disable-line
    phone: PropTypes.number.isRequired, // eslint-disable-line
  };

  constructor(props, context) {
    super(props, context);
    this.onMedia = this.onMedia.bind(this);
    const sensorRuntime = context.sensor.getRuntime();
    const itemWidth = this.onMedia(sensorRuntime, true);
    this.state = { itemWidth };
  }

  onMedia({ media }, init = false) {
    let itemWidth = '100%';
    ['desktop', 'tablet', 'phone'].forEach((type) => {
      if (media[type]) itemWidth = `${this.props[type]}%`;
    });
    if (init) return itemWidth;
    this.setState({ itemWidth });
  }

  render() {
    const { children } = this.props;
    const { itemWidth } = this.state;
    return (
      <Slider data-block="media-slider" {...{ children, itemWidth }} />
    );
  }
}

export default MediaSlider;
