import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import ccn from 'ccn';
import DropDown from 'components/drop-down';
import styles from './burger.scss';

@ccn
@bem
export default class Burger extends Component {
  static propTypes = {
    type: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    children: PropTypes.node.isRequired
  };

  render() {
    const { type = 1, children, ...rest } = this.props;

    const child = Children.only(children);

    return (
      <DropDown {...rest} data-block="burger" data-mods={[type]} data-bemstyles={styles}>
        <btn>
          <i data-elem="line" data-mods="1" />
          <i data-elem="line" data-mods="2" />
          <i data-elem="line" data-mods="3" />
        </btn>
        {child}
      </DropDown>
    );
  }
}
