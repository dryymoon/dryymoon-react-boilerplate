/**
 * Created by Vit on 19.07.2016.
 */

/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import ccn from 'ccn';

import styles from './flexbox.scss';

@ccn
@bem
class Flexbox extends Component {
  static propTypes = {
    children: PropTypes.node,
    row: PropTypes.bool,
    column: PropTypes.bool,
    wrap: PropTypes.bool,
    wrap_mobile: PropTypes.bool,
    spaceAroundContent: PropTypes.bool,
    spaceAroundContentTop: PropTypes.bool,
    spaceAroundContent_mobile: PropTypes.bool,
    spaceBetweenContent: PropTypes.bool,
    spaceBetweenContent_mobile: PropTypes.bool
  };

  // static cache = {};

  render() {
    const {
      row, column, children, wrap, wrap_mobile,
      spaceAroundContent, spaceAroundContentTop,
      spaceAroundContent_mobile,
      spaceBetweenContent, spaceBetweenContent_mobile,
      ...restProps
    } = this.props;
    const mods = {
      row,
      column,
      wrap,
      wrap_mobile,
      spaceAroundContent,
      spaceAroundContentTop,
      spaceAroundContent_mobile,
      spaceBetweenContent,
      spaceBetweenContent_mobile
    };
    return (
      <div
        {...restProps}
        data-block="flexbox"
        data-mods={mods}
        data-bemstyles={styles}
        children={children}
      />
    );
  }
}
export default Flexbox;
