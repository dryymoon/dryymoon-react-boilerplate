/**
 * Created by dryymoon on 21.05.17.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-webpack-loader-syntax
import variables from '!!sass-vars-to-js-loader!./../../theme/variables.scss';

let ckeditorLoader;

if (__CLIENT__) {
  // TODO Сделать проверку на правильность пути, есть ли слеш в пути или нету
  window.CKEDITOR_BASEPATH = `${__PUBLIC_PATH__}ckeditor/`;
  /* eslint-disable */
  // ?context=${__dirname}&outputPath=ckeditor/
  // require(`!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/config`);
  // require(`!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/styles`);
  // require(`!style-loader/url!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/contents.css`);

  require.context('!!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor', true);

  /* require.context
    '!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/plugins/',
    true
  );

  require.context(
    '!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/lang',
    true
  );

  require.context(
    '!file-loader?context=node_modules&name=[path][name].[ext]!ckeditor/skins/moono-lisa',
    true
  ); */

  require.context(
    `!file-loader?context=${__dirname}/raw_assets&outputPath=ckeditor/&name=[path][name].[ext]!./raw_assets`,
    true
  );

  ckeditorLoader = import(/* webpackChunkName: "ckeditor" */ 'ckeditor/ckeditor');
}

export default class CKEditor extends Component {
  static propTypes = {
    initialData: PropTypes.string,
    data: PropTypes.string,
    config: PropTypes.object, // eslint-disable-line
    onChange: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.data = props.data || props.initialData || '';
    this.state = {
      config: this.buildConfig(props.config),
    };
  }

  componentDidMount() {
    const that = this;

    ckeditorLoader.then(()=>{
      that.instance = window.CKEDITOR.appendTo(
        this.nodeRef,
        this.state.config,
        this.data
      );

      that.instance.on('change', () => {
        this.handleChange();
      });

    });
    /* if (!window.CKEDITOR) {
      console.error('CKEditor not found'); // eslint-disable-line
      return;
    } */
  }

  UNSAFE_componentWillReceiveProps(props) {
    // if (!this.instance) return;
    if (props.data !== undefined && this.getData() !== props.data) {
      // setData will move the cursor to the begining of the input
      this.setData(props.data);
    }

    /* if (props.config && this.state.config !== props.config) {
     if ('readOnly' in props.config) {
     this.instance.setReadOnly(props.config.readOnly);
     }
     } */

    /* this.setState({
     value: props.value,
     // config: props.config || {},
     }); */
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillUnmount() {
    if (!this.instance) return;
    this.instance.removeAllListeners();
    window.CKEDITOR.remove(this.instance);
  }

  getData() {
    if (!this.instance) return;
    return this.instance.getData();
  }

  setData(data) {
    this.data = data;
    if (!this.instance) return;
    return this.instance.setData(data);
  }

  buildConfig(forceConfig) {
    const { config: stateConfig } = this.state || {};
    const conf = forceConfig || stateConfig || {};

    return {
      // skin: 'minimalist', // 'office2013',
      ...conf,
      /* usage $fullscreen-z-index + 10000 */
      baseFloatZIndex: parseInt(variables.fullscreenZIndex, 10) + 10000,
      allowedContent: true,
      startupShowBorders: false,
      /* toolbarGroups: [{ name: 'pbckcode' }],
       extraPlugins: 'pbckcode',
       // ADVANCED CONTENT FILTER (ACF)
       // ACF protects your CKEditor instance of adding unofficial tags
       // however it strips out the pre tag of pbckcode plugin
       // add this rule to enable it, useful when you want to re edit a post
       // only needed on v1.1.x
       allowedContent: 'pre[*]{*}(*)', // add other rules here
       pbckcode: {
       // An optional class to your pre tag.
       cls: '',

       // The syntax highlighter you will use in the output view
       highlighter: 'PRETTIFY',

       // An array of the available modes for you plugin.
       // The key corresponds to the string shown in the select tag.
       // The value correspond to the loaded file for ACE Editor.
       modes: [['HTML', 'html'], ['CSS', 'css'], ['PHP', 'php'], ['JS', 'javascript']],

       // The theme of the ACE Editor of the plugin.
       theme: 'textmate',

       // Tab indentation (in spaces)
       tab_size: '4'
       } */
    };
  }

  handleChange() {
    const { onChange } = this.props;
    if (onChange) onChange(this.getData());
  }

  render() {
    return <div ref={node => this.nodeRef = node} />;
  }
}

/*
 <Helmet>
 <script
 src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"
 type="text/javascript"
 onLoad={() => this.ready()}
 />
 </Helmet>
 */
// export default CKEditorWrapper;
