/**
 * Created by dryymoon on 14.11.17.
 */
import Module, { shape as InfinityWrapperShape } from './wrapper';

import InfinityLoader from './loader';

export default Module;

export { InfinityLoader, InfinityWrapperShape };

