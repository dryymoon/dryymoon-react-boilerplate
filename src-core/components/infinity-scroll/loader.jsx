/**
 * Created by dryymoon on 14.11.17.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/loader';
import './infinity.scss';

export default class InfinityScrollLoader extends Component {
  static propTypes = { children: PropTypes.node, };

  static contextTypes = {
    registerInfinityLoaderComponentRef: PropTypes.func,
    unRegisterInfinityLoaderComponentRef: PropTypes.func,
  };

  constructor(props, ctx) {
    super(props, ctx);
    this.state = { shown: false };
  }

  UNSAFE_componentWillMount() {
    const { registerInfinityLoaderComponentRef } = this.context;
    registerInfinityLoaderComponentRef(this);
  }

  componentWillUnmount() {
    const { unRegisterInfinityLoaderComponentRef } = this.context;
    unRegisterInfinityLoaderComponentRef();
  }

  show() {
    if (this.state.shown) return;
    this.setState({ shown: true });
  }

  hide() {
    if (!this.state.shown) return;
    this.setState({ shown: false });
  }

  render() {
    const { children } = this.props;
    const { shown } = this.state;
    if (!shown) return null;
    if (children) return children;
    return (<Loader {...this.props} className="infinityLoader" active />);
  }
}
