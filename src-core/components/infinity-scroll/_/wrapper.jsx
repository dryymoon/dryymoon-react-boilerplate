/**
 * Created by dryymoon on 14.11.17.
 */
import { Component, cloneElement, Children } from 'react';
import PropTypes from 'prop-types';
import apiDecorator from 'api';
import uniqueId from 'uniqueId';

export const shape = {
  urlBuilder: PropTypes.func.isRequired,
  preloadedScreens: PropTypes.number,
  forceLoadPagesCount: PropTypes.number,
  updater: PropTypes.func.isRequired,
  intialPage: PropTypes.number,
  loadInitialPage: PropTypes.bool,
  infinityInstanceKey: PropTypes.string,
};

const infinityInstances = {};

@uniqueId
@apiDecorator
export default class InfinityScrollWrapper extends Component {
  static propTypes = {
    ...shape,
    children: PropTypes.node.isRequired,
  };

  static childContextTypes = {
    registerInfinityLoaderComponentRef: PropTypes.func,
    unRegisterInfinityLoaderComponentRef: PropTypes.func,
  };

  constructor(props, ctx) {
    super(props, ctx);
    let { intialPage = 0 } = props;
    const { loadInitialPage, forceLoadPagesCount = 0 } = props;
    if (loadInitialPage) intialPage -= 1;
    this.instanceState = {
      loadedPage: intialPage,
      hasNext: true,
    };
    // this.loadedPage = intialPage;
    // this.hasNext = true;
    this.forceLoadPagesCount = forceLoadPagesCount;
    this.handleScroll = this.handleScroll.bind(this);
    this.loadMore = this.loadMore.bind(this);
    this.maybeLoadMore = this.maybeLoadMore.bind(this);
    this.registerInfinityLoaderComponentRef = this.registerInfinityLoaderComponentRef.bind(this);
    this.unRegisterInfinityLoaderComponentRef =
      this.unRegisterInfinityLoaderComponentRef.bind(this);
  }

  getChildContext() {
    const { registerInfinityLoaderComponentRef, unRegisterInfinityLoaderComponentRef } = this;
    return { registerInfinityLoaderComponentRef, unRegisterInfinityLoaderComponentRef };
  }

  UNSAFE_componentWillMount() {
    const { infinityInstanceKey: instkey } = this.props;
    if (instkey) {
      infinityInstances[instkey] = infinityInstances[instkey] || this.instanceState;
      this.instanceState = infinityInstances[instkey];
      infinityInstances[instkey].instances = infinityInstances[instkey].instances || [];
      infinityInstances[instkey].instances.push(this.uid);
    }
  }

  componentDidMount() {
    this.containerRef.addEventListener('scroll', this.handleScroll, false);
    this.maybeLoadMore();
  }

  componentWillUnmount() {
    this.containerRef.removeEventListener('scroll', this.handleScroll);
    const { infinityInstanceKey: instkey } = this.props;
    if (instkey) {
      const index = infinityInstances[instkey].instances.indexOf(this.uid);
      infinityInstances[instkey].instances.splice(index, 1);
      if (infinityInstances[instkey].instances.length === 0) {
        infinityInstances[instkey] = undefined;
        delete infinityInstances[instkey];
      }
    }
  }

  handleScroll() {
    this.maybeLoadMore();
  }

  registerInfinityLoaderComponentRef(ref) {
    this.infinityLoaderRef = ref;
  }

  unRegisterInfinityLoaderComponentRef() {
    this.infinityLoaderRef = null;
  }

  maybeLoadMore() {
    const { loading, forceLoadPagesCount } = this;
    const {hasNext}  = this.instanceState
    if (!hasNext) return;
    if (loading) return;
    const { preloadedScreens = 10, loadInitialPage } = this.props;
    const { scrollTop, scrollHeight, clientHeight } = this.containerRef;

    if (forceLoadPagesCount > 0) {
      return this.loadMore();
    }

    // Initial Preload
    if ((scrollHeight <= clientHeight * (preloadedScreens + 1)) && (clientHeight > 0)) {
      // TODO Переписать hideLoader Логику
      return this.loadMore({ hideLoader: !loadInitialPage && (scrollHeight <= clientHeight) });
    }

    // Infiniti feature preload
    if (scrollTop + clientHeight > (scrollHeight - (clientHeight * preloadedScreens))) {
      return this.loadMore();
    }
  }

  loadMore({ hideLoader } = {}) {
    const { urlBuilder, updater } = this.props;

    const nextPage = this.instanceState.loadedPage + 1;
    this.loading = true;
    if (!hideLoader && this.infinityLoaderRef) this.infinityLoaderRef.show();
    this.api
      .get(urlBuilder(nextPage))
      .then(data => updater(data))
      .then((hasNext) => {
        this.loading = false;
        this.forceLoadPagesCount -= 1;
        if (this.infinityLoaderRef) this.infinityLoaderRef.hide();
        this.instanceState.loadedPage = nextPage;
        if (hasNext !== true && hasNext !== false) {
          throw new Error('updaterCb in infinity scroll must return in promise true or false');
        }
        this.instanceState.hasNext = hasNext;
        if (hasNext === true) this.maybeLoadMore();
      })
      .catch((error) => {
        if (__DEVELOPMENT__) {
          // eslint-disable-next-line
          console.error('Error downloading data for InfinityScrollWrapper: ', error);
        }
        return this.maybeLoadMore();
      });
    // TODO Implement Catch setTimeout
  }

  render() {
    const { children } = this.props;
    const child = Children.only(children);
    return cloneElement(child, {
      ref: node => this.containerRef = node,
      style: { willChange: 'transform', ...child.props.style }
    });
  }
}

// element.scrollTop - is the pixels hidden in top due to the scroll. With no scroll its value is 0.

// element.scrollHeight - is the pixels of the whole div.

// element.clientHeight - is the pixels that you see in your browser.
