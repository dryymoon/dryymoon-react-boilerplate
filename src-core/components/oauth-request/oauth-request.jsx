/**
 * Created by macbookpro on 02.12.16.
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import globalConfig from '../../../config';
import Link from '../../components/link/link';
import appMessage from '../../helpers/appMessage';

const configs = globalConfig.oauth || {};


class OAuthRequest extends Component {
  static propTypes = {
    socOauthName: PropTypes.oneOf(Object.keys(configs)).isRequired
  };

  constructor(props, context) {
    super(props, context);
    this.openOauthWindow = this.openOauthWindow.bind(this);
  }

  openOauthWindow() {
    const conf = configs[this.props.socOauthName];
    if (__APP__) {
      // event.preventDefault();
      if (conf.url === '/service/socialAuth/login/facebook') {
        appMessage({ goSdkLogin: 'FbSdkLogin' });
        return false;
      }
      if (conf.url === '/service/socialAuth/login/vkontakte') {
        appMessage({ goSdkLogin: 'VkSdkLogin' });
        return false;
      }
      if (conf.url === '/service/socialAuth/login/google') {
        appMessage({ goSdkLogin: 'GoogleSdkLogin' });
        return false;
        // TODO: define host
      } appMessage({ goApp: `http://example.com/${conf.url}` });
      return false;
    }
    const [width, height] = conf.window || [600, 400];
    const { availLeft = 0, availTop = 0, availWidth = 0, availHeight = 0 } = window.screen || {};
    const offsetLeft = (availLeft + (availWidth / 2)) - (width / 2);
    const offsetTop = (availTop + (availHeight / 2)) - (height / 2);
    const windowParam = `width= ${width}, height= ${height}, left= ${offsetLeft},top= ${offsetTop}`;
    window.open(conf.url, '_blank', windowParam);
    return false;
  }

  render() {
    const { socOauthName, ...restProps } = this.props;
    const conf = configs[socOauthName];
    if (!conf) return null;
    /* eslint-disable jsx-a11y/anchor-has-content */
    return (
      <Link
        to={conf.url}
        {...restProps}
        onClick={this.openOauthWindow}
      />
    );
  }
}
export default OAuthRequest;
