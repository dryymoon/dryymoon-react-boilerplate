/**
 * Created by KOTLIAR_A on 15.03.17.
 */
import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import { findDOMNode } from 'react-dom';
import sensor, { getScrollPositionSync } from 'components/sensor';
import * as redux from './redux';

@connect(() => ({}), { ...redux }) // state, {name}
@sensor
export default class ElementGeometry extends Component {
  static propTypes = {
    update: PropTypes.func,
    destroy: PropTypes.func,
    children: PropTypes.node.isRequired,
    name: PropTypes.string.isRequired
  };

  constructor(props, context) {
    super(props, context);
    this.calcElementGeometry = this.calcElementGeometry.bind(this);
    this.onResizeEnd = this.calcElementGeometry;
    // this.lastElementGeometry = null;
  }

  componentDidMount() {
    this.findDomNode();
    const docElem = document.documentElement;
    const body = document.body;
    this.clientTop = docElem.clientTop || body.clientTop || 0;
    this.clientLeft = docElem.clientLeft || body.clientLeft || 0;
    this.calcElementGeometry();
  }

  UNSAFE_componentWillReceiveProps({ name: nextName }) {
    if (this.props.name !== nextName) {
      this.props.destroy(this.props.name);
      // This update... with new name
    }
  }

  componentDidUpdate() {
    this.findDomNode();
    this.calcElementGeometry();
  }

  componentWillUnmount() {
    this.props.destroy(this.props.name);
  }

  findDomNode() {
    this.element = findDOMNode(this.nodeRef);
  }

  calcElementGeometry() {
    const { name } = this.props;
    if (!this.element) return;
    const clientRect = this.element.getBoundingClientRect();
    const { left: scrollLeft, top: scrollTop } = getScrollPositionSync();
    const top = ((clientRect.top + scrollTop) - this.clientTop);
    const left = ((clientRect.left + scrollLeft) - this.clientLeft);
    const { height, width } = clientRect;
    const elementGeometry = { height, width, left, top };
    if (isEqual(this.lastElementGeometry, elementGeometry)) return;
    if (width || height) {
      this.props.update({ name, coords: elementGeometry });
    } else if (this.lastElementGeometry) this.props.destroy(name);
    this.lastElementGeometry = elementGeometry;
  }

  render() {
    return React.cloneElement(
      Children.only(this.props.children),
      { ref: ref => this.nodeRef = ref }
    );
  }
}
