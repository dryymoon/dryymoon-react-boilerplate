/**
 * Created by KOTLIAR on 21.03.2017.
 */
import { connect } from 'react-redux';
import isArray from 'lodash/isArray';

export default function factory(...args) {
  if (isArray(args)) {
    return BaseComponent => connect(
      ((state) => {
        const elementsGeometries = {};
        args.forEach((item) => {
          elementsGeometries[item] = state.elementsGeometries[item];
        });
        return { elementsGeometries };
      })
    )(BaseComponent);
  }
  // eslint-disable-next-line
  console.warn('Args of @coordinator must be an Array');
}

