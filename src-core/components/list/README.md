`.list_inline {
  .list__item {
    vertical-align: top;
    white-space: normal;
    width: auto;
    text-align: left;
  }
}`

`.list_centeredVertical {
  .list__item {
    vertical-align: middle;
  }
}`

`.list_itemBlock {
  .list__item {
    display: block;
  }
}`

`.list_overflowHidden {
  .list__content {
    overflow: hidden;
  }
}`;
