/**
 * Created by Vit on 01.07.2016.
 */
/* eslint-disable react/jsx-closing-tag-location, react/no-array-index-key */
import React, { Children, Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const listCss = require('./list.scss');

@ccn
@bem
class List extends Component {
  static propTypes = {
    children: PropTypes.node,
    inline: PropTypes.bool,
    separate: PropTypes.bool,
    tile: PropTypes.bool,
    overflowHidden: PropTypes.bool,
    centeredVertical: PropTypes.bool,
    itemBlock: PropTypes.bool
  };

  // static cache = {};

  render() {
    const { children, inline, separate, tile,
      overflowHidden, centeredVertical, itemBlock } = this.props;
    const mods = { inline, tile, overflowHidden, centeredVertical, itemBlock };
    const childrenCount = Children.count(children);
    return (
      <div data-block="list" data-mods={mods} data-bemstyles={listCss}>
        <ul data-elem="content">
          {Children.map(children, (it, index) => {
            // if (index === 0 && it && it.type === 'header') return;
            const arr = [];
            arr.push(<li key={index} data-elem="item">{it}</li>);
            if (separate && (index < childrenCount - 1)) {
              arr.push(<li data-elem="itemSeparator" key={`separ_${index}`}>
                <span data-elem="separator" />
              </li>);
            }
            return arr;
          })}
        </ul>
      </div>
    );
  }
}
export default List;
