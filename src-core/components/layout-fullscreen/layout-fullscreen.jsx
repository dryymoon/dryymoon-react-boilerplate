/**
 * Created by PIV on 04.04.2017.
 */
/* eslint-disable jsx-a11y/html-has-lang */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import ccn from 'ccn';
import bem from 'bem';

import styles from './layout-fullscreen.scss';

@ccn
@bem
export default class LayoutFullScreen extends Component {
  static propTypes = { children: PropTypes.node };

  render() {
    return (
      <div
        data-block="layoutFullscreen"
        data-bemstyles={styles}
        // children={this.props.children}
      >
        <Helmet >
          <html data-spa-fullscreen="true" />
        </Helmet>
        <div data-elem="content" children={this.props.children} />
      </div>
    );
  }
}
