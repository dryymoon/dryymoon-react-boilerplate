/**
 * Created by PIV on 27.06.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
// import debounce from 'lodash/debounce';
import position from 'metal-position';
import ccn from '../../helpers/styles/CollectClasses';
/*
let detectZoom = {};
if (__CLIENT__) {
  detectZoom = require('detect-zoom');
} */

const imageCss = require('./image.scss');
const image2x = require('.././100_b.jpg');
// const image3x = require('../home/testImage/20_b.jpg');

@ccn
@bem
class Image extends Component {
  static propTypes = {
    thumbnailBase64: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    ratio: PropTypes.number.isRequired,
    alt: PropTypes.string,

    sources: PropTypes.node.isRequired
  };

  static contextTypes = {
    getWindowCords: PropTypes.func,
    getContainerCords: PropTypes.func,
    checkIntersectRegion: PropTypes.func
  };

  static cache = {};

  constructor(props, context) {
    super(props, context);
    this.state = {};
    /* this.handleWindowResize = debounce(
      this.handleWindowResize.bind(this), 1000, { trailing: true }
      ); */
    // this.handleWindowResize = this.handleWindowResize.bind(this);
    this.maybeLoadImage = this.maybeLoadImage.bind(this);
  }

  componentDidMount() {
    // this.regionWindow = (this.context.getRegionWindow)();
    this.maybeLoadImage();


    // window.addEventListener('resize', this.handleWindowResize, true);
    // window.addEventListener('resize', this.handleWindowResize);
  }

  componentWillUnmount() {
    // window.removeEventListener('resize', this.handleWindowResize);
  }

  maybeLoadImage() {
    // eslint-disable-next-line no-console
    console.log('---------------');

    const pictureRef = this.pictureRef;

    const windowCords = this.context.getWindowCords();
    // eslint-disable-next-line no-console
    console.log('windowCords CORDS', windowCords);

    const containerCords = this.context.getContainerCords(pictureRef);
    // eslint-disable-next-line no-console
    console.log('pictureRef CORDS', containerCords);

    const intersect = this.context.checkIntersectRegion(pictureRef);
    // eslint-disable-next-line no-console
    console.log('intersect IMG', intersect);

    const getRegionWindow = position.getRegion(window);
    // eslint-disable-next-line no-console
    console.log('getRegionWindow:', getRegionWindow);

    const getRegionPicture = position.getRegion(pictureRef);
    // eslint-disable-next-line no-console
    console.log('getRegionPicture:', getRegionPicture);

    const intersectRegion = position.intersectRegion(getRegionPicture, getRegionWindow);
    // eslint-disable-next-line no-console
    console.log('intersectRegion:', intersectRegion);

    const container = {
      width: position.getWidth(pictureRef),
      height: position.getHeight(pictureRef)
    };

    if (this.state.containerWidth > container.width
      && this.state.containerHeight > container.height) return;

    let newState = {};

    newState = {
      ...newState,
      containerWidth: container.width,
      containerHeight: container.height
    };

    // eslint-disable-next-line no-console
    // console.log('this.refs.pictureRef.WxH', `${container.width}px x ${container.height}px`);

    const { sources } = this.props;
    const examineVar = 'width';

    let imgSmaller;
    let imgBigger;

    /*
     * Find a little bigger and a little smaller image for future examine.
     * !!! Before change this see next Comment!!!
     */

    sources.forEach((it) => {
      if (!it || !it[examineVar]) return;
      const examineVal = parseFloat(it[examineVar]);
      const existingSmallerSize = imgSmaller ? imgSmaller[examineVar] : 0;
      const existingBiggerSize = imgBigger ? imgBigger[examineVar] : Infinity;
      if ((existingSmallerSize < examineVal)
        && (examineVal < container[examineVar])) {
        imgSmaller = it;
      }
      if ((examineVal > container[examineVar])
        && (existingBiggerSize > examineVal)) {
        imgBigger = it;
      }
    });
    // eslint-disable-next-line no-console
    // console.log('Finded Candidates: ', '\nSmaller:', imgSmaller, '\nBigger:', imgBigger);

    /*
     * Decide which of smaller or bigger image to use for this render case.
     * We can don`t check for existing of [examineVar] in object because
     * in previous rules if [examineVar] don`t exist, bigger or smaller object don`t created.
     */

    let imgForUse;

    if (!imgSmaller && !imgBigger) {
      // No image at sources...
    } else if (!imgSmaller || !imgBigger) {
      // We have only one candidate to use, and usage it.
      imgForUse = imgSmaller || imgBigger;
    } else if (imgSmaller && imgBigger) {
      // Decide which image to use:
      const diffRatioOfSmaller = container[examineVar] / imgSmaller[examineVar];
      const diffRatioOfBigger = imgBigger[examineVar] / container[examineVar];
      if (diffRatioOfSmaller < diffRatioOfBigger) {
        imgForUse = imgSmaller;
      } else {
        imgForUse = imgBigger;
      }
    } else {
      // This is unreachable code... In normal cases.
      throw new Error('Some fackup in code, in image decision which ' +
        'to show in render, in component "Image"');
    }

    if (this.state.image !== imgForUse) { // TODO CHECK THIS IF EXUAL VALID...
      // eslint-disable-next-line no-console
      // console.log('New image', imgForUse);
      newState = {
        ...newState,
        image: imgForUse
      };
    }
    if (Object.keys(newState).length > 0) {
      // eslint-disable-next-line no-console
      // console.log('Push ne image');
      this.setState(newState);
    }
  }

  /* handleWindowResize() {
    this.maybeLoadImage();
  } */

  render() {
    const { thumbnailBase64, ratio, alt } = this.props;
    const mods = {};
    const pictureStyle = {
      paddingBottom: `${(1 / ratio) * 100}%`,
      backgroundImage: `url(${thumbnailBase64})`,
      backgroundSize: '100% auto'
    };
    // eslint-disable-next-line
    console.log('thumbnailBase64', thumbnailBase64);
    return (
      <div data-block="image" data-mods={mods} data-bemstyles={imageCss}>
        <picture
          ref={(ref) => { this.pictureRef = ref; }}
          data-elem="imgThumbnail"
          style={pictureStyle}
        >
          {/* this.state.image && (
           <source srcSet={this.state.image.url} />
           ) */}
          {this.state.image && (
            <source
              srcSet={`${this.state.image.url} 1x, ${image2x} 2x`}
              media="only screen and (-webkit-min-device-pixel-ratio: 2),
                     only screen and (min--moz-device-pixel-ratio: 2),
                     only screen and (-o-min-device-pixel-ratio: 2/1),
                     only screen and (min-device-pixel-ratio: 2),
                     only screen and (min-resolution: 192dpi),
                     only screen and (min-resolution: 2dppx)"
            />
          )}
          {this.state.image && (
            <img data-elem="imgImage" src={this.state.image.url} alt={alt || ' '} />
          )}
        </picture>
      </div>
    );
  }
}
export default Image;
