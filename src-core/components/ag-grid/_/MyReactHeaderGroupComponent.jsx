/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/icon';

class MyReactHeaderGroupComponent extends Component {
  static propTypes = {
    data: PropTypes.object // eslint-disable-line
  };
  constructor(...args) {
    super(...args);
    this.state = {
      expanded: null
    };
  }

  expandOrCollapse()
  {
    this.props.setExpanded(!this.state.expanded);
    console.log('must hide-unhide', this.state.expanded);
    this.setState({
      expanded: this.props.columnGroup.getOriginalColumnGroup().isExpanded()
    });
  }

/*
  onExpandChanged()
  {
    this.setState({
      expanded: this.props.columnGroup.getOriginalColumnGroup().isExpanded()
    });
  }
*/

  render() {
    let arrowClassName = `customExpandButton ${this.state.expanded ? ' expanded' : ' collapsed'}`;

    return (<div>
      <div className="customHeaderLabel"> {this.props.displayName}</div>
     <div onClick={this.expandOrCollapse.bind(this)} className={arrowClassName}><Icon className="click-area rotate-270 font-size-10" type="carretThin" /></div>
    </div>);
  }

}


export default MyReactHeaderGroupComponent;
/*
import React from 'react';
import Icon from 'components/icon';

// Header component to be used as default for all the columns.
export default class MyReactHeaderGroupComponent extends React.Component {

  constructor(props) {
    super(props);
    this.props.columnGroup.getOriginalColumnGroup().addEventListener('expandedChanged', this.onExpandChanged.bind(this));
    this.state = {
      expanded: null
    };
    this.onExpandChanged();
  }

  render() {
    const arrowClassName = `customExpandButton ${this.state.expanded ? ' expanded' : ' collapsed'}`;

    return (<div>
      <div className="customHeaderLabel"> {this.props.displayName}</div>
      <div onClick={this.expandOrCollapse.bind(this)} className={arrowClassName}><Icon className="click-area rotate-270 font-size-10" type="carretThin" /></div>
    </div>);
  }

  expandOrCollapse() {
    this.props.setExpanded(!this.state.expanded);
    console.log('must hide-unhide');
  }

  onExpandChanged() {
    this.setState({
      expanded: this.props.columnGroup.getOriginalColumnGroup().isExpanded()
    });
  }
}

// the grid will always pass in one props called 'params',
// which is the grid passing you the params for the cellRenderer.
// this piece is optional. the grid will always pass the 'params'
// props, so little need for adding this validation meta-data.
MyReactHeaderGroupComponent.propTypes = {
  params: React.PropTypes.object
};

  */
