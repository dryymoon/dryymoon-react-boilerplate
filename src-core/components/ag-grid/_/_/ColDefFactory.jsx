/* eslint-disable */
/*import SkillsCellRenderer from './SkillsCellRenderer.jsx';
import NameCellEditor from './NameCellEditor.jsx';
import ProficiencyCellRenderer from './ProficiencyCellRenderer.jsx';
import RefData from './RefData';
import SkillsFilter from './SkillsFilter.jsx';
import ProficiencyFilter from './ProficiencyFilter.jsx';*/
import MyReactHeaderGroupComponent from './MyReactHeaderGroupComponent.jsx';

export default class ColDefFactory {

  createColDefs() {
    const columnDefs = [
      { headerName: '#',
        width: 30,
        checkboxSelection: true,
        suppressSorting: true,
        suppressMenu: true,
        pinned: true },
      { headerName: '№ ПП',
        field: 'id',
        width: 50,
        checkboxSelection: false,
        suppressSorting: true,
        suppressMenu: true,
        pinned: true },


      {
        headerName: 'Застройщик',
        headerGroupComponentFramework: MyReactHeaderGroupComponent,
        children: [
          {
            headerName: 'Название ЖК',
            field: 'name',
            enableRowGroup: true,
            enablePivot: true,
            width: 180,
            pinned: true,
            editable: true,
            // use a React cellEditor
            // cellEditorFramework: NameCellEditor
          },
          {
            headerName: 'Название',
            field: '$entity',
            width: 150,
            enableRowGroup: true,
            enablePivot: true,
            // an example of using a non-React cell renderer
            // cellRenderer: countryCellRenderer,
            pinned: true,
            columnGroupShow: 'open'
          },


        ]
      },
      {
        headerName: 'Информация',
        children: [
          { headerName: 'Фото', field: 'images', width: 150, filter: 'text' },
          { headerName: 'Ссылка на карточку', field: 'url_card', width: 225, filter: 'text' },
          { headerName: 'Цена', field: 'price', width: 150, filter: 'text' },
          { headerName: 'Сайт ЖК', field: 'url_site', width: 150, filter: 'text' },
          { headerName: 'Тел ОП', field: 'phone_sales', width: 150, filter: 'text' },
          { headerName: 'Тел ЖЕК', field: 'phone_zhk', width: 150, filter: 'text' },
          { headerName: 'ID темы на форуме', field: 'url_forum', width: 225, filter: 'text' },
          { headerName: 'Инфо', field: 'description', width: 150, filter: 'text' },
        ]
      },
    ];
    return columnDefs;
  }
}

// this is a simple cell renderer, putting together static html, no
// need to use React for it.
function countryCellRenderer(params) {
  if (params.value) {

  }
  return null;
}

// Utility function used to pad the date formatting.
function pad(num, totalStringSize) {
  let asString = `${num}`;
  while (asString.length < totalStringSize) asString = `0${asString}`;
  return asString;
}
