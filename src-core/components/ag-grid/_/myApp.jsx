/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { AgGridReact } from 'ag-grid-react';
// import 'ag-grid-enterprise';
/*import RowDataFactory from './RowDataFactory';*/
// import ColDefFactory from './ColDefFactory.jsx';
/*import MyReactDateComponent from './MyReactDateComponent.jsx';*/
import MyReactHeaderComponent from './MyReactHeaderComponent.jsx';

import './myApp.scss';
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/theme-blue.css';
// take this line out if you do not want to use ag-Grid-Enterprise

class MyApp extends Component {
  static propTypes = {
    data: PropTypes.node, // eslint-disable-line
    component: PropTypes.node, // eslint-disable-line
    columnsNames: PropTypes.node, // eslint-disable-line
    columnFilter: PropTypes.node // eslint-disable-line
  };

  constructor(...args) {
    super(...args);
    this.state = {
      quickFilterText: null,
      showGrid: true,
      showToolPanel: false,
      // columnDefs: new ColDefFactory().createColDefs(),
      columnDefs: this.props.columnsNames,
      // rowData: new RowDataFactory().createRowData(),
      rowData: this.props.data,
      componentOfGrid: this.props.component,
      // columnFilter: this.props.columnFilter,
      icons: {
        columnRemoveFromGroup: '<i class="fa fa-remove"/>',
        filter: '<i class="fa fa-filter"/>',
        sortAscending: '<i class="fa fa-long-arrow-down"/>',
        sortDescending: '<i class="fa fa-long-arrow-up"/>',
        groupExpanded: '<i class="fa fa-minus-square-o"/>',
        groupContracted: '<i class="fa fa-plus-square-o"/>',
        columnGroupOpened: '<i class="fa fa-minus-square-o"/>',
        columnGroupClosed: '<i class="fa fa-plus-square-o"/>'
      }
    };

    this.gridOptions = {

      // We register the react date component that ag-grid will use to render
      //dateComponentFramework: MyReactDateComponent,
      // this is how you listen for events using gridOptions
      onModelUpdated() {
        console.log('event onModelUpdated received');
      },
      defaultColDef: {
        headerComponentFramework: MyReactHeaderComponent,
        headerComponentParams: {
          menuIcon: 'fa-bars'
        }
      },
      // this is a simple property
      rowBuffer: 10 // no need to set this, the default is fine for almost all scenarios

    };
  }

  onShowGrid(show) {
    this.setState({
      showGrid: show
    });
  }

  onToggleToolPanel(event) {
    this.setState({ showToolPanel: event.target.checked });
  }

  onGridReady(params) {
    this.api = params.api;
    this.columnApi = params.columnApi;
  }

  selectAll() {
    this.api.selectAll();
  }

  deselectAll() {
    this.api.deselectAll();
  }

  setCountryVisible(visible) {
    this.columnApi.setColumnVisible('country', visible);
  }

  onQuickFilterText(event) {
    this.setState({ quickFilterText: event.target.value });
  }

  onCellClicked(event) {
    console.log(`onCellClicked: ${event.data.name}, col ${event.colIndex}`);
  }

  onRowSelected(event) {
    console.log(`onRowSelected: ${event.node.data.name}`);
  }

  onRefreshData() {
    const newRowData = new RowDataFactory().createRowData();
    this.setState({
      rowData: newRowData
    });
  }

  invokeSkillsFilterMethod() {
    const skillsFilter = this.api.getFilterInstance('skills');
    const componentInstance = skillsFilter.getFrameworkComponentInstance();
    componentInstance.helloFromSkillsFilter();
  }

  dobFilter() {
    const dateFilterComponent = this.gridOptions.api.getFilterInstance('dob');
    dateFilterComponent.setFilterType('equals');
    dateFilterComponent.setDateFrom('2000-01-01');
    this.gridOptions.api.onFilterChanged();
  }

  render() {
    /*let { data } = this.props;
     data = data || [];*/
    let gridTemplate;
    let colunmFilter;
    let bottomHeaderTemplate;
    let topHeaderTemplate;
    //const data = this.props;
    // console.log('rowData==',[this.state.rowData]);
    // console.log('rowData==from_home',data);
    if (this.props.columnFilter) {
      colunmFilter = (
        <div>
          <input type="checkbox" onChange={this.onToggleToolPanel.bind(this)}/>
          Показать фильтр полей
          {this.state.componentOfGrid}
        </div>
      );
    }
    topHeaderTemplate = (
      <div style={{ width: '100%', display: 'inline-block', boxSizing: 'border-box' }}>
        <div style={{ float: 'right' }}>
          <input
            type="text" onChange={this.onQuickFilterText.bind(this)}
            placeholder="Фильтр..."/>
          {/*<button
           id="btDestroyGrid" disabled={!this.state.showGrid}
           onClick={this.onShowGrid.bind(this, false)}>Destroy Grid
           </button>*/}
          {/*<button id="btCreateGrid" disabled={this.state.showGrid} onClick={this.onShowGrid.bind(this, true)}>
           Create Grid
           </button>*/}
        </div>
        <div style={{ padding: '4px' }}>
          <b></b> <span id="rowCount"/>
          <label>
            {colunmFilter}
          </label>
        </div>
      </div>
    );

    // showing the bottom header and grid is optional, so we put in a switch
    if (this.state.showGrid) {
      bottomHeaderTemplate = (
        <div>
          <div style={{ padding: 4 }} className={'toolbar'}>
            <span>
              Grid API:
              <button onClick={this.selectAll.bind(this)}>Select All</button>
              <button onClick={this.deselectAll.bind(this)}>Clear Selection</button>
            </span>
            <span style={{ marginLeft: 20 }}>
                            Column API:
                            <button onClick={this.setCountryVisible.bind(this, false)}>Hide Country Column</button>
              <button onClick={this.setCountryVisible.bind(this, true)}>Show Country Column</button>
            </span>
          </div>
          <div style={{ clear: 'both' }}/>
          <div style={{ padding: 4 }} className={'toolbar'}>
            <span>
              <label>
                <input type="checkbox" onChange={this.onToggleToolPanel.bind(this)}/>
                            Show Tool Panel
                        </label>
              <button onClick={this.onRefreshData.bind(this)}>Refresh Data</button>
            </span>
            <span style={{ marginLeft: 20 }}>
                            Filter API:
                            <button onClick={this.invokeSkillsFilterMethod.bind(this, false)}>Invoke Skills Filter Method</button>
              <button onClick={this.dobFilter.bind(this)}>DOB equals to 01/01/2000</button>
            </span>
          </div>
          <div style={{ clear: 'both' }}/>
        </div>
      );
      gridTemplate = (
        <div style={{ height: 400 }} className="ag-fresh">
          <AgGridReact
            // gridOptions is optional - it's possible to provide
            // all values as React props
            gridOptions={this.gridOptions}

            // listening for events
            onGridReady={this.onGridReady.bind(this)}
            onRowSelected={this.onRowSelected.bind(this)}
            onCellClicked={this.onCellClicked.bind(this)}

            // binding to simple properties
            showToolPanel={this.state.showToolPanel}
            quickFilterText={this.state.quickFilterText}

            // binding to an object property
            icons={this.state.icons}

            // binding to array properties
            columnDefs={this.state.columnDefs}
            rowData={this.state.rowData}


            // no binding, just providing hard coded strings for the properties
            suppressRowClickSelection="true"
            rowSelection="multiple"
            rowDeselection="true"
            enableColResize="true"
            suppressAutoSize="false"
            enableSorting="true"
            enableFilter="true"
            groupHeaders="true"
            rowHeight="22"
            debug="true"
          />
        </div>
      );
    }
    return (
      <div style={{ width: '100%' }}>
        <div style={{ padding: '4px' }}>
          {topHeaderTemplate}
          {/*{bottomHeaderTemplate}*/}
          {gridTemplate}
        </div>
      </div>
    );
  }
}

export default MyApp;
