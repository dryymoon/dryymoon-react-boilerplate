/**
 * Created by dryymoon on 11.09.17.
 */
import isBrowser from 'isBrowser';

export default function configurate(key) {
  return function decorate(Component) {
    return class LocalStorageSavedState extends Component {
      constructor(...args) {
        super(...args);
        if (!isBrowser) return;
        if (!window.localStorage) return;
        if (this.state) {
          try {
            const prevState = JSON.parse(localStorage.getItem(key));
            this.state = { ...this.state, ...prevState };
          } catch (e) {
            return undefined;
          }
        }
        if (this.getStateToSave) this.getStateToSave = this.getStateToSave.bind(this);
      }

      forceSaveState() {
        const { getStateToSave } = this;
        let { state } = this;
        if (state) {
          if (getStateToSave) state = getStateToSave(state);
          localStorage.setItem(key, JSON.stringify(state));
        }
      }

      componentWillUnmount() {
        this.forceSaveState();
        if (super.componentWillUnmount) super.componentWillUnmount();
      }
    };
  };
}
