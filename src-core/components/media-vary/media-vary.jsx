/**
 * Created by Vit on 11.07.2016.
 */
import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const mediaVaryCss = require('./media-vary.scss');

@ccn
@bem
class MediaVary extends Component {
  static propTypes = {
    children: PropTypes.node,
    column: PropTypes.bool
  };

  render() {
    const { column } = this.props;
    const mods = { column };
    return (
      <div data-block="mediaVary" data-mods={mods} data-bemstyles={mediaVaryCss} >
        {Children.map(this.props.children, (it, index) => {
          if (!it) return false;
          const props = {};
          props['data-elem'] = 'col';
          props.key = index;
          return cloneElement(it, props);
        })}
      </div>
    );
  }
}
export default MediaVary;
