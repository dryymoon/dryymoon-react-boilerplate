/**
 * Created by Vit on 03.11.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
// import List from 'components/list';
import ccn from '../../helpers/styles/CollectClasses';

import loaderCss from './loader.scss';

@ccn
@bem
class Loader extends Component {
  static propTypes = {
    gray: PropTypes.bool,
    color: PropTypes.bool,
    active: PropTypes.bool,
    children: PropTypes.node,
  };

  /* renderLoader() {
    return (
      <List data-elem="list" inline centeredVertical>
        <span data-elem="ball_1" />
        <span data-elem="ball_2" />
        <span data-elem="ball_3" />
      </List>
    );
  } */

  render() {
    const { gray, color, active, children } = this.props;
    const mods = { gray, color };
    const isWrapper = (children !== undefined);
    return (
      <div data-block="loader" data-mods={mods} data-bemstyles={loaderCss}>
        {isWrapper && <div data-elem="contentWrapper" data-mods={{ hidden: active }} children={children} />}
        {active && (
          <div data-elem="loaderContainer">
            <span data-elem="ball_1" />
            <span data-elem="ball_2" />
            <span data-elem="ball_3" />
          </div>
        )}
      </div>
    );
  }
}
export default Loader;

/*
 {!isWrapper && this.renderLoader()}
 {isWrapper && [
 <div data-elem="wrapper" data-mods={{ hidden: active }} children={children} />,
 active && this.renderLoader()
 ]}
 */
