/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';
import styles from './container.scss';

/**
 * Container
 * Обвертка для всего сайта, умеет держать ширину сайта и центрироваться.
 */
@ccn
@bem
class Container extends Component {
  static propTypes = {
    /**
     * Будет удерживать максимальную ширину сайта
     */
    body: PropTypes.bool,
    children: PropTypes.node,
    /**
     * Ручное задание стилей, deprecated
     */
    style: PropTypes.node
  };

  render() {
    const { body, children, style } = this.props;
    const mods = {
      body: !!body
    };
    return (
      <div
        data-block="container"
        data-mods={mods}
        data-bemstyles={styles}
        children={children}
        style={style}
      />
    );
  }
}
export default Container;
