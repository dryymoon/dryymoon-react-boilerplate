/**
 * Created by dryymoon on 26.06.17.
 */
/* eslint-disable no-console */
import Ajv from 'ajv';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';
// import { t } from 'c-3po';
// import uniq from 'lodash/uniq';
import keys from 'lodash/keys';
import set from 'lodash/set';
// import ajvError from 'ajv-errors';
// import pointer from 'json-pointer';
// import localize from 'ajv-i18n';
// var localize_ru = require('ajv-i18n/localize/ru');
import get from 'lodash/get';

export default class Invalidator {
  constructor(schema, isFull) {
    this.isFull = isFull;

    let ajvConfig = {};

    if (isFull) {
      ajvConfig = {
        ...ajvConfig,
        allErrors: true,
        jsonPointers: true,
        errorDataPath: 'property'
      };
    }

    const ajv = new Ajv(ajvConfig);

    // if (isFull) ajvError(ajv);

    this.validator = ajv.compile(schema);
  }

  validate(data) {
    this.isValid = this.validator(data);
    this.errors = {};

    if (this.isValid) return true;

    if (this.isFull) {
      // const uniqueCheckPaths = [];
      // console.log(localize.ru(this.validator.errors));
      const byKeyword = {};
      // console.log('this.validator.errors', this.validator.errors);
      this.validator.errors.forEach(({ dataPath, keyword, ...rest }) => {
        const path = dataPath.substring(1).replace(/\//g, '.');
        byKeyword[keyword] = byKeyword[keyword] || [];
        const value = get(data, path);
        byKeyword[keyword].push({ path, keyword, value, ...rest });
      });

      if (byKeyword.uniqueItems) {
        byKeyword.uniqueItems.forEach(({ path }) => {
          this.setKeyword(path, 'hasDuplicatedItems');
          const arr = get(data, path);
          for (let i1 = 0; i1 < arr.length; i1 += 1) {
            for (let i2 = i1 + 1; i2 < arr.length; i2 += 1) {
              if (isEqual(arr[i1], arr[i2])) {
                // this.setKeyword(`${path}.${i1}`, 'firstDuplicatedItem');
                this.setKeyword(`${path}.${i1}`, 'duplicatedItem');
                this.setKeyword(`${path}.${i2}`, 'duplicatedItem');
              }
            }
          }
        });
        delete byKeyword.uniqueItems;
      }

      if (byKeyword.required) {
        byKeyword.required.forEach(({ path }) => {
          this.setKeyword(path, 'requiredItemIsEmpty');
        });
        delete byKeyword.required;
      }

      if (byKeyword.minLength) {
        byKeyword.minLength.forEach(({ path, params: { limit } }) => {
          if (limit === 1) this.setKeyword(path, 'requiredItemIsEmpty');
          if (limit > 1) this.setKeyword(path, 'requiredMinLength', { limit });
        });
        delete byKeyword.minLength;
      }

      if (byKeyword.minItems) {
        // eslint-disable-next-line
        byKeyword.minItems.forEach(({ path, ...rest }) => {
          // console.log('minItem', rest);
          this.setKeyword(path, 'requiredMinItems', rest);
        });
        delete byKeyword.minItems;
      }

      if (!isEmpty(byKeyword)) {
        // if (__DEVELOPMENT__) console.warn('Unprocessed keywords', byKeyword);
        keys(byKeyword).forEach(kw =>
          byKeyword[kw].forEach(({ path, ...rest }) => this.setKeyword(path, `$${kw}`, rest)));
      }
      // console.error('Errors', this.errors);

      keys(this.errors).forEach((key) => {
        if (key) set(this.errors, key.split('.'), this.errors[key]);
      });

      return false;
    }

    this.errors = this.validator.errors;
    return false;
  }

  setKeyword(path, keyword, params = {}) {
    this.errors[path] = this.errors[path] || {};
    // if (!this.errors[path]) this.errors[path] = { path, keywords: [] };
    // if (!this.errors[path][keyword]) this.errors[path].keywords.push(keyword);
    this.errors[path][keyword] = params; // this.errors[path][keyword] || 0;
    // this.errors[path][keyword] += 1;
  }

  /* unsetKeyword(path, keyword) {
   if (!this.errors[path]) return;
   if (this.errors[path][keyword]=== undefined) return;
   if (this.errors[path][keyword]=== 0)
   } */

  invalidate(data) {
    return !this.validate(data);
  }

  getErrors() {
    return this.errors;
  }

  isInvalidByPath(path) {
    if (this.isValid === undefined) {
      throw new Error('Before call testFn you need call validate(data)');
    }
    if (!this.isFull) {
      throw new Error('You cannot call testFn when isFullValidate disabled');
    }
    if (!path) throw new Error('isInvalidByPath must receive path');
    return get(this.errors, path, false);
  }
}
