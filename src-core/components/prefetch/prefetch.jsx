/**
 * Created by dryymoon on 01.02.2018.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import castArray from 'lodash/castArray';
import Promise from '../../helpers/cancelable-promise';

let Observer;
if (__CLIENT__) require('intersection-observer');
if (__CLIENT__) Observer = require('@researchgate/react-intersection-observer').default;

export default class DataPrefetch extends Component {
  static propTypes = {
    children: PropTypes.node,
    pageUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    apiUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
    wait: PropTypes.number,
    root: PropTypes.string,
  };

  static contextTypes = {
    prefetchPage: PropTypes.func,
    prefetchApi: PropTypes.func,
  };

  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.runPrefetch = this.runPrefetch.bind(this);
    this.stopPrefetching = this.stopPrefetching.bind(this);
    this.prefetching = false;
    this.prefetched = false;
  }

  componentDidMount() {
    const { children } = this.props;
    if (!children) this.startAwait();
  }

  componentWillUnmount() {
    this.cancelAwait();
    this.stopPrefetching();
  }

  handleChange({ isIntersecting }) {
    // const { pageUrl, /* wait = 1 */ } = this.props;

    // if (!pageUrl) return;
    /* console.log(
     pageUrl,
     `handleChange ${isIntersecting}`,
     `this.prefetching ${this.prefetching}`,
     `this.prefetched ${this.prefetched}`
     ); */

    if (this.prefetched) return;

    if (!this.prefetching) {
      if (isIntersecting) this.startAwait();
      if (!isIntersecting) this.cancelAwait();

      return;
    }

    if (this.prefetching) {
      /* if (!isIntersecting && !this.stopPrefetchingRef) {
       console.log('Start kill timeout');
       this.stopPrefetchingRef = setTimeout(() => {
       console.log('Run kill');
       this.stopPrefetching();
       this.stopPrefetchingRef = null;
       }, wait * 1000);
       }

       if (isIntersecting && this.stopPrefetchingRef) {
       console.log('Destroy kill timeout');
       clearTimeout(this.stopPrefetchingRef);
       this.stopPrefetchingRef = null;
       } */

      if (!isIntersecting) this.stopPrefetching();
    }
  }

  runPrefetch() {
    const { pageUrl, apiUrl } = this.props;
    const { prefetchPage, prefetchApi } = this.context;
    this.prefetching = true;
    const that = this;
    const promises = [];

    if (pageUrl) {
      castArray(pageUrl).forEach(url => promises.push(prefetchPage(url)));
    }

    if (apiUrl) {
      castArray(apiUrl).forEach(url => promises.push(prefetchApi(url)));
    }

    this.prefetchPromise = Promise.all(promises, () => promises.forEach(p => p.cancel()));
    this.prefetchPromise.then(() => {
      that.prefetching = false;
      that.prefetched = true;
      that.prefetchPromise = null;
    }).catch(() => {
      that.prefetching = false;
      that.prefetched = false;
      that.prefetchPromise = null;
    });
  }

  stopPrefetching() {
    if (this.prefetchPromise) this.prefetchPromise.cancel();
    this.prefetching = false;
    this.prefetchPromise = null;
  }

  startAwait() {
    if (this.awaitRef) return;
    const { wait = 1 } = this.props;
    const that = this;
    this.awaitRef = setTimeout(() => {
      that.awaitRef = null;
      that.runPrefetch();
    }, wait * 1000);
  }

  cancelAwait() {
    if (!this.awaitRef) return;
    clearTimeout(this.awaitRef);
    this.awaitRef = null;
  }

  render() {
    const { children = null, root } = this.props;

    if (__CLIENT__ && children) {
      return (
        <Observer
          root={root}
          threshold={1}
          disabled={this.prefetched}
          onChange={this.handleChange}
          children={children}
        />
      );
    }

    return children;
  }
}
