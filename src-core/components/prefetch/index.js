/**
 * Created by dryymoon on 01.02.2018.
 */
import Module from './prefetch';
import Provider from './prefetch-provider';

export default Module;

export { Provider };
