/* eslint-disable max-len */
/**
 * Created by dryymoon on 20.01.18.
 */
import { Component } from 'react';
import PropTypes from 'prop-types';
import { match } from 'react-router';
import flatMap from 'lodash/flatMap';
import values from 'lodash/values';
import api from 'api';
import { buildFnArgs, dataIncubator } from 'preload';
import Promise from '../../helpers/cancelable-promise';

const pathRegXP = /^#/;

@api
export default class DataPrefetchProvider extends Component {
  static propTypes = {
    // api: PropTypes.object, // eslint-disable-line
    routes: PropTypes.object, // eslint-disable-line
    history: PropTypes.object, // eslint-disable-line
    children: PropTypes.node
  };

  static childContextTypes = {
    prefetchPage: PropTypes.func,
    prefetchApi: PropTypes.func,
  };

  constructor() {
    super();
    this.prefetchPageAsync = this.prefetchPageAsync.bind(this);
    this.prefetchApiAsync = this.prefetchApiAsync.bind(this);
    this.apiGet = this.apiGet.bind(this);
  }

  getChildContext() {
    return {
      prefetchPage: this.prefetchPageAsync,
      prefetchApi: this.prefetchApiAsync,
    };
  }

  apiGet(url, config) {
    return this.api.get(url, { ...config, config: { $source: 'preload' } });
  }

  prefetchPageAsync(relativeUrl) {
    if (__SERVER__) return;
    const { routes, history } = this.props;

    let execPromise;
    let canceled = false;

    const adaptedRelativeUrl = relativeUrl.replace(pathRegXP, '');

    return new Promise((resolve, reject) => {
      match({ routes, history, location: adaptedRelativeUrl }, (err, redirect, renderProps) => {
        if (err || redirect) return resolve();
        if (canceled) return reject();
        // execPromise = preloadExecutor({ renderProps, apiInstance: this.api });

        // const preloadArr = flatMap(renderProps.components, ({ dataPreload = [] } = {}) => dataPreload);
        const dataPreloadConfigs = flatMap(renderProps.components, ({ dataPreload = [] } = {}) => dataPreload);

        const requestArrOfObjs = dataPreloadConfigs.map(conf => dataIncubator(buildFnArgs(renderProps), {
          apiGet: this.apiGet,
          dataPreloadConfig: conf,
          initial: true,
        }));

        // const unresolvedInjectAllReqs = flatMap(requestArrOfObjs, (({ unresolvedInjectReqs }) => unresolvedInjectReqs));

        /* if (__DEVELOPMENT__ && unresolvedInjectAllReqs.length) {
          // eslint-disable-next-line
          console.warn(`Prefetch of url: ${adaptedRelativeUrl} degraded due to ${unresolvedInjectAllReqs.length} reqs unCachenable`, unresolvedInjectAllReqs);
        } */

        const requestArr = flatMap(requestArrOfObjs, it => values(it));

        execPromise = Promise.all(requestArr /* , () => requestArr.forEach(it => it.cancel()) */);

        resolve(execPromise);
      });
    }, () => {
      if (execPromise) execPromise.cancel();
      canceled = true;
    });
  }

  prefetchApiAsync(apiUrl) {
    if (__SERVER__) return;
    const { get: apiGet } = this.api;
    return apiGet(apiUrl);
  }

  render() {
    return this.props.children;
  }
}

/* const { routes: currRoutes = [] } = renderProps;

 // Preload async components
 const getComponents = currRoutes
 .map(({ getComponent } = {}) => getComponent)
 .filter(it => it);
 getComponents.map(getComponent => getComponent(null, () => null)); */

// Preload data
/* execPromise.then(() =>
 execPromise = preloadExecutor({ renderProps, apiInstance: this.api }));
 resolve(execPromise); */
