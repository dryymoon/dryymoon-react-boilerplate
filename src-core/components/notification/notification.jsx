// https://github.com/gor181/react-notification-system-redux

import React, { Component } from 'react';
import { connect } from 'react-redux';
import bem from 'bem';
import ccn from 'ccn';
import Container, { reducer } from 'react-notification-system-redux';
import ReducersRegistry from '../../helpers/redux/reducers-registry';
import styles from './notification.scss';

ReducersRegistry.register({ notifications: reducer });

/* eslint-disable */

const style = {
  Containers: {
    DefaultStyle: {
      left: 0,
      right: 0,
      padding: 0,
      height: 0
    },
    tl: {
      top: 0,
      bottom: 'auto'
    },
    tc: {
      top: 0,
      bottom: 'auto'
    },
    tr: {
      top: 0,
      bottom: 'auto'
    },
    bl: {
      top: 'auto',
      bottom: 0
    },
    bc: {
      top: 'auto',
      bottom: 0
    },
    br: {
      top: 'auto',
      bottom: 0
    }
  },
  NotificationItem: { // Override the notification item
    DefaultStyle: { // Applied to every notification, regardless of the notification level
      width: 320,
      zIndex: false,
      position: 'relative',
    },
    custom: {
      width: 'auto',
      borderRadius: 0,
      padding: 0,
      margin: 0,
      border: 0,
      background: 0,
      boxShadow: 'none',
      fontSize: '14px'
    }
    // success: { // Applied only to the success notification item
    //   color: 'red'
    // }
  }
};

@connect(({ notifications }, { channel = 'default' }) => ({ notifications }))
@ccn
@bem
export default class Notification extends Component {
  static bemStylesOverride = styles;

  render() {
    const { notifications } = this.props;
    return (
      <Container
        data-block="notification"
        notifications={notifications}
        style={style} />
    );
  }
}

/*
* Position of the notification.
* Available:
* tr (top right),
* tl (top left),
* tc (top center),
* br (bottom right),
* bl (bottom left),
* bc (bottom center)
* */
