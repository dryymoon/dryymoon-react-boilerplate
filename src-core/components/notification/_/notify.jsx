// import Notify from 'react-redux-notify';
// https://www.npmjs.com/package/react-redux-notify
import { connect } from 'react-redux';
import notifyReducer, {
  createNotification,
  removeNotification,
  removeAllNotifications,
  NOTIFICATION_TYPE_SUCCESS,
  NOTIFICATION_TYPE_WARNING,
  NOTIFICATION_TYPE_INFO,
  NOTIFICATION_TYPE_ERROR,
  /* NOTIFICATIONS_POS_TOP_RIGHT,
   NOTIFICATIONS_POS_BOT_RIGHT,
   NOTIFICATIONS_POS_BOT_LEFT,
   NOTIFICATIONS_POS_TOP_LEFT,
   ADD_NOTIFICATION,
   REMOVE_NOTIFICATION,
   REMOVE_ALL_NOTIFICATIONS */
} from 'react-redux-notify';
import NotifyContainer from 'react-redux-notify/lib/components/Notify/index';
import 'react-redux-notify/dist/ReactReduxNotify.css';
import ReducersRegistry from '../../helpers/redux/reducers-registry';

ReducersRegistry.register({ notifications: notifyReducer });

export default connect((state, { channel = 'default' }) =>
    ({ notifications: state.notifications.filter(({ channel: ch }) => ch === channel) }),
  dispatch => ({
    remove: id => dispatch(removeNotification(id)),
    removeAll: force => dispatch(removeAllNotifications(force)),
  })
)(NotifyContainer);

export const types = {
  success: NOTIFICATION_TYPE_SUCCESS,
  error: NOTIFICATION_TYPE_ERROR,
  info: NOTIFICATION_TYPE_INFO,
  warning: NOTIFICATION_TYPE_WARNING
};

export const actions = {
  createNotification,
  removeNotification,
  removeAllNotifications
};
