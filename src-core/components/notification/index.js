/**
 * Created by dryymoon on 25.05.17.
 */
import {
  show, success, error, warning, info, hide, removeAll
} from 'react-notification-system-redux';
import Module from './notification';

function customShow(opts) {
  return show(opts, 'custom');
}

export default Module;
export { customShow, show, success, error, warning, info, hide, removeAll };

