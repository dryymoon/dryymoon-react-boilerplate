/**
 * Created by Vit on 01.07.2016.
 */
import React, { Component } from 'react';
import Helmet from 'react-helmet';
// import isBrowser from '../../helpers/isBrowser';
import serialize from 'serialize-javascript';

const liveFn = serialize(
  (eventType, elementQuerySelector, cb) => {
    document.addEventListener(eventType, (event) => {
      const qs = document.querySelectorAll(elementQuerySelector);

      if (qs) {
        let el = event.target;
        let index = Array.prototype.indexOf.call(qs, el);
        while (el && (index === -1)) {
          el = el.parentElement;
          index = Array.prototype.indexOf.call(qs, el);
        }

        if (index > -1) {
          cb.call(el, event);
        }
      }
    });
  }
);

class Live extends Component {
  render() {
    return (<Helmet script={[{ type: 'text/javascript', innerHTML: `window.live = ${liveFn}` }]} />);
  }
}
export default Live;
