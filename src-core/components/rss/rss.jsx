/**
 * Created by Vit on 01.07.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';
import Link from '../link/link';

const rssCss = require('./rss.scss');

@ccn
@bem
class Rss extends Component {
  static propTypes = {
    href: PropTypes.string.isRequired,
    title: PropTypes.string,
    children: PropTypes.node
  };

  static cache = {};

  render() {
    const { children, href, title } = this.props;
    return (
      <div data-block="rss" data-bemstyles={rssCss}>
        <Link data-elem="link" href={href} title={title} >
          {/* <span data-elem="icon" /> */}
          {children
            ? <span data-elem="text">{children}</span>
            : <span data-elem="text">RSS</span>
          }
        </Link>
      </div>
    );
  }
}
export default Rss;
