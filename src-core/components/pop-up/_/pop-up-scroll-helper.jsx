import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

@connect(({ modals: { modals = [] } }) => ({ modalsCount: modals.length }))
export default class PopUpScrollHelper extends Component {
  static propTypes = {
    modalsCount: PropTypes.number.isRequired
  };
  /* eslint-disable jsx-a11y/html-has-lang */
  render() {
    const { modalsCount } = this.props;
    return (
      <Helmet>
        {modalsCount > 0 && <html data-prevent-scroll-due-popup="true" />}
      </Helmet>
    );
  }
}
