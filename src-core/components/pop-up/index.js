/**
 * Created by PIV on 22.06.2016.
 */
import ReduxModal from './react-redux-modal';
import Module, { shape, clear } from './pop-up';

export { shape, clear };
export const Provider = ReduxModal;
export default Module;
