import PropTypes from 'prop-types';
import { modal } from './react-redux-modal';
// import variables from '!!sass-to-js-var!../../theme/variables.scss';

// import 'react-redux-modal/lib/css/react-redux-modal.css';
import './pop-up.scss';
// import ReducersRegistry from '../../helpers/redux/reducers-registry';

// ReducersRegistry.register({ modals: reducer });

const popup = (Template, {
  size = 'large',
  closeOnOutsideClick = true,
  hideTitleBar = true,
  hideCloseButton = true,
  ...rest
} = {}) => modal.add(Template, {
  // index,
  size,
  closeOnOutsideClick,
  hideTitleBar,
  hideCloseButton,
  ...rest
});

const { clear } = modal;

popup.clear = clear;

export { clear };

export const shape = { removeModal: PropTypes.func };

export default popup;
