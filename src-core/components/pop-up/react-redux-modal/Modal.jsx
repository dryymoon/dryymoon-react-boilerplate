import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

/* eslint-disable jsx-a11y/no-static-element-interactions */

export default class Modal extends Component {
  static propTypes = {
    id: PropTypes.string,
    index: PropTypes.number,
    removeModal: PropTypes.func.isRequired,
    onRemoveModal: PropTypes.func,
    isCurrent: PropTypes.bool,
    options: PropTypes.shape({
      size: PropTypes.string,
      title: PropTypes.string,
      hideCloseButton: PropTypes.bool,
      hideTitleBar: PropTypes.bool,
      closeOnOutsideClick: PropTypes.bool
    }).isRequired,
    component: PropTypes.any.isRequired, // eslint-disable-line
  };

  constructor() {
    super();
    this.handleOnOutsideClick = this.handleOnOutsideClick.bind(this);
    this.removeModal = this.removeModal.bind(this);
  }

  handleOnOutsideClick(e) {
    const { options: { closeOnOutsideClick } = {} } = this.props;
    if (closeOnOutsideClick && !this.isChildOf(e.target, this.modalContent)) {
      this.removeModal();
    }
  }

  removeModal() {
    const { id, removeModal, options: { onRemoveModal } = {} } = this.props;
    if (onRemoveModal) onRemoveModal();
    removeModal(id);
  }

  isChildOf(child, parent) {
    if (child.parentNode === parent) {
      return true;
    } else if (child.parentNode === null) {
      return false;
    }
    return this.isChildOf(child.parentNode, parent);
  }

  render() {
    const {
      index, isCurrent, options: { size, title, hideCloseButton, hideTitleBar } = {},
      component: Cmp
    } = this.props;
    const extraScrollAttrs = {};
    if (!isCurrent) extraScrollAttrs['data-no-scroll-due-popup'] = true;
    return (
      <div
        className="rrm-holder"
        style={{ zIndex: `999${index}` }}>
        <div
          className="scroll"
          {...extraScrollAttrs}
          onClick={this.handleOnOutsideClick}>
          <div
            ref={node => this.modalContent = node}
            className={classnames('rrm-content', `m-${size}` || 'm-medium')}>
            {hideTitleBar ? null : <div className="rrm-title">
              <h2>{title}</h2>
              <div className="rr-title-actions">
                {hideCloseButton ? null : <button
                  type="button"
                  className="rr-close rrm-icon-cancel"
                  onClick={this.removeModal}>X</button>
                }
              </div>
            </div>
            }

            <div className="rrm-body">
              <Cmp {...this.props.options} removeModal={this.removeModal} />
            </div>
          </div>

        </div>

        <div className="rrm-shadow" />
      </div>
    );
  }
}
