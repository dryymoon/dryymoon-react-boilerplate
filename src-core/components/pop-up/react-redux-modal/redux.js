

import uiid from 'uuid';
import ReducersRegistry from 'core/helpers/redux/reducers-registry';
import isBrowser from 'isBrowser';
// import createReducer from './utils';

export const key = 'modals';
export const ADD_MODAL = `${key}/ADD_MODAL`;
export const REMOVE_MODAL = `${key}/REMOVE_MODAL`;
export const CLEAR_ALL = `${key}/CLEAR_ALL`;

/* const initialSate = {
  modals: []
}; */

export default function reducer(state = [], action = {}) {
  const { type } = action;

  switch (type) {
    case ADD_MODAL: {
      return [...state, { id: uiid.v1(), ...action.payload }];
    }
    case REMOVE_MODAL: {
      return state.filter(({ id }) => id !== action.id);
    }
    case CLEAR_ALL: {
      return [];
    }
    default: {
      return state;
    }
  }
}

export function addModal(payload) {
  return {
    type: ADD_MODAL,
    payload
  };
}

export function removeModal(id) {
  return {
    type: REMOVE_MODAL,
    id
  };
}

export function clearAll() {
  return { type: CLEAR_ALL };
}

if (isBrowser) ReducersRegistry.register({ [key]: reducer });
