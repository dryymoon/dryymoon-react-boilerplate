import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { EE } from './emitter';
import * as redux from './redux';
import Modal from './Modal';

@connect(({ [redux.key]: modals }) => ({ modals }), redux)
export default class ReduxModal extends Component {
  static propTypes = {
    modals: PropTypes.array, // eslint-disable-line
    addModal: PropTypes.func,
    clearAll: PropTypes.func,
    removeModal: PropTypes.func,
  };

  static displayName = 'ReduxModal';

  componentDidMount() {
    const { addModal, clearAll } = this.props;
    EE.on('add/modal', obj => addModal(obj));
    EE.on('clear/all', clearAll);
  }

  /* UNSAFE_componentWillReceiveProps({ modals: nextModals }) {
    const { modals: currModals } = this.props;

    if ((currModals.length === 0) && (nextModals.length > 0)) {
      const doc = document.documentElement;
      // const left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
      this.windowScrollTop = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      const body = document.querySelector('body');
      const style = {
        position: 'fixed',
        top: `-${this.windowScrollTop}px`,
        left: 0,
        right: 0,
      };
      assign(body.style, style);
    }

    if ((currModals.length > 0) && (nextModals.length === 0)) {
      const body = document.querySelector('body');
      const style = {
        position: '',
        top: '',
        left: '',
        right: ''
      };
      assign(body.style, style);
      window.scrollTo(0, this.windowScrollTop);
    }
  } */

  componentWillUnmount() {
    EE.off('add/modal');
    EE.off('clear/all');
  }

  render() {
    const { removeModal, modals = [] } = this.props;

    if (modals.length === 0) return null;

    return (
      <div className="react-redux-modal">
        <Helmet>
          {modals.length > 0 && <body data-no-scroll-due-popup="true" />}
          <style type="text/css">{`
            [data-no-scroll-due-popup] {
                overflow-y: hidden;
                overflow-x: hidden;
            }
          `}
          </style>
        </Helmet>
        <div className="rr-modals">
          {modals.map((modal, i) => (
            <Modal
              index={i}
              key={modal.id}
              removeModal={removeModal}
              isCurrent={i === (modals.length - 1)}
              {...modal} />
          ))}
        </div>
      </div>
    );
  }
}
