/**
 * Created by Vit on 14.07.2016.
 */
import React, { Component } from 'react'; // PropTypes
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';
import styles from './button.scss';

/**
 * Button
 * Абстракция кнопки, поправлены стили.
 * Все пропсы копирует в button tag
 * Пример:
 * ```html
 * <Button onClick="() => alert('Clicked!')"> Click me!</Button>
 * ```
 */
@ccn
@bem
class Button extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    // 'data-test': PropTypes.string,
  };

  constructor() {
    super();
    this.onClick = this.onClick.bind(this);
  }

  onClick(event) {
    const { onClick, disabled } = this.props;
    if (onClick && !disabled) {
      event.target.blur();
      onClick(event);
    }
  }

  render() {
    const { disabled, ...rest } = this.props;
    // const dataTestAttr = dataTest ? { 'data-test': dataTest} : {};
    // const extend = {};
    // if (disabled) extend.disabled = true;
    return (
      <button
        type="button"
        {...rest}
        // {...extend}
        onClick={this.onClick}
        data-block="button"
        data-mods={{ disabled }}
        data-bemstyles={styles}
      />
    );
  }
}
export default Button;
