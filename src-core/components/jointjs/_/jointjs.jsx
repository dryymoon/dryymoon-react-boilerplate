/**
 * Created by dryymoon on 01.03.17.
 */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import sensor from 'components/sensor';
import bem from 'bem';
import ccn from 'ccn';
import 'ag-grid/dist/styles/ag-grid.css';
import 'ag-grid/dist/styles/theme-blue.css';

import styles from './ag-grid-styles.scss';

let AgGridReact = null;
if (__CLIENT__) {
  AgGridReact = require('ag-grid-react').AgGridReact;
}

/**
 * Ag-grid Tables
 * Документация по модулю [https://www.ag-grid.com/](https://www.ag-grid.com/)
 * Кроме одного параметра принимает еще много параметров, см. документацию.
 */

@sensor
@bem
@ccn
class AgGrid extends Component {
  static propTypes = {
    /**
     * Указывает нужно ли автоматом вписывать размер таблици в размер блока
     */
    fit: PropTypes.bool, // eslint-disable-line
  };

  static getDefaultProps = {
    fit: false
  };

  constructor(props, context) {
    super(props, context);
    this.onGridReady = this.onGridReady.bind(this);
    this.onGridSizeChanged = this.onGridSizeChanged.bind(this);
  }

  onResizeEnd() {
    this.fit();
  }

  onGridReady(...args) {
    const [{ api }] = args;
    this.api = api;
    this.fit();
    if (this.props.onGridReady) this.props.onGridReady(...args);
  }

  onGridSizeChanged(...args) {
    this.fit();
    if (this.props.onGridSizeChanged) this.props.onGridSizeChanged(...args);
  }

  getGridOptions() {
    const { gridOptions: { localeText } = {} } = this.props;
    return { ...this.props.gridOptions, localeText: { ...defaultGridlocaleText, ...localeText } };
  }

  fit() { // event
    if (this.props.fit && this.api) this.api.sizeColumnsToFit();
  }

  renderGrid() {
    if (!AgGridReact) return null;
    const { fit, ...rest } = this.props; //eslint-disable-line
    return (
      <AgGridReact
        suppressClickEdit="true"
        rowSelection="multiple"
        enableColResize="true"
        enableSorting="true"
        animateRows="true"
        {...rest}
        gridOptions={this.getGridOptions()}
        onGridReady={this.onGridReady}
        onGridSizeChanged={this.onGridSizeChanged}
      />
    );
  }

  render() {
    return (
      <div
        data-block="agGridAdaptedComponent"
        className="ag-blue"
        data-bemstyles={styles}
      >
        {this.renderGrid()}
      </div>
    );
  }
}

const defaultGridlocaleText = {
  localeText: {
    // for filter panel
    page: 'daPage',
    more: 'daMore',
    to: 'daTo',
    of: 'daOf',
    next: 'daNexten',
    last: 'daLasten',
    first: 'daFirsten',
    previous: 'daPreviousen',
    loading: 'Загрузка данных...',
    loadingOoo: 'daLoading...',
    // for set filter
    selectAll: 'daSelect Allen',
    searchOoo: 'daSearch...',
    blanks: 'daBlanc',
    // for number filter and text filter
    filterOoo: 'Фильтр...',
    applyFilter: 'daApplyFilter...',
    // for number filter
    equals: 'Равно',
    notEquals: 'Не равно ',
    lessThan: 'daLessThan',
    greaterThan: 'daGreaterThan',
    // for text filter
    contains: 'Содержит',
    notContains: 'Не содержит',
    startsWith: 'Начинается с',
    endsWith: 'Заканчивается на',
    // the header of the default group column
    group: 'laGroup',
    // tool panel
    columns: 'laColumns',
    rowGroupColumns: 'laPivot Cols',
    rowGroupColumnsEmptyMessage: 'la please drag cols to group',
    valueColumns: 'laValue Cols',
    pivotMode: 'laPivot-Mode',
    groups: 'laGroups',
    values: 'laValues',
    pivots: 'laPivots',
    valueColumnsEmptyMessage: 'la drag cols to aggregate',
    pivotColumnsEmptyMessage: 'la drag here to pivot',
    // other
    noRowsToShow: 'la no rows',
    // enterprise menu
    pinColumn: 'laPin Column',
    valueAggregation: 'laValue Agg',
    autosizeThiscolumn: 'laAutosize Diz',
    autosizeAllColumns: 'laAutsoie em All',
    groupBy: 'laGroup by',
    ungroupBy: 'laUnGroup by',
    resetColumns: 'laReset Those Cols',
    expandAll: 'laOpen-em-up',
    collapseAll: 'laClose-em-up',
    toolPanel: 'laTool Panelo',
    // enterprise menu pinning
    pinLeft: 'laPin <<',
    pinRight: 'laPin >>',
    noPin: 'laDontPin <>',
    // enterprise menu aggregation and status panel
    sum: 'laSum',
    min: 'laMin',
    max: 'laMax',
    // first: 'laFirst',
    // last: 'laLast',
    none: 'laNone',
    count: 'laCount',
    average: 'laAverage',
    // standard menu
    copy: 'laCopy',
    ctrlC: 'ctrl n C',
    paste: 'laPaste',
    ctrlV: 'ctrl n C'
  }
};

export default AgGrid;
