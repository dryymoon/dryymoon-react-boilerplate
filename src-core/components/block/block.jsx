/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component, cloneElement, createElement } from 'react';
import PropTypes from 'prop-types';
import objectPath from 'object-path';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const blockCss = require('./block.scss');

/**
 * Block
 * Основной компонент сайта, у него есть шапка и подвал, контент внутри.
 * Использется для однообразия показа контента.
 * На мобилах шапка прилипает.
 * Пример:
 * ```html
 * <Block>
 *   <div header>
 *      Этого элемента может и не быть,
 *      шапку схватывает компонент именно по пропсу header
 *   </div>
 *   <div>Контент1</div>
 *   ...
 *   <div>Контент ...</div>
 *   <div footer>
 *      Этого элемента может и не быть,
 *      шапку схватывает компонент именно по пропсу footer
 *   </div>
 * </Block>
 * ```
 */

@ccn
@bem
class Block extends Component {
  static propTypes = {
    /**
     * Тут и шапка и футер и контент Шапка и футер имеют нужные пропсы: header, footer.
     * Контент без этих пропсов
     */
    children: PropTypes.node,
    /**
     * Используется для заглушения блока,
     * т.е. он вроде есть но им нельзя воспользоваться и ничего не кликабельно.
     */
    disabled: PropTypes.bool,
    /**
     * Скрывает избыток информации, тянется по родителю или жестко задан
     */
    overflowHidden: PropTypes.bool,
  };

  static getDefaultProps = {
    disabled: false,
    overflowHidden: false,
  };

  //
  // static cache = {};

  render() {
    const { children, disabled, overflowHidden } = this.props;
    let headerEl;
    let footerEl;
    let contentEl = children;
    if (Array.isArray(contentEl)) {
      if (objectPath.get(children, '0.props.header')) {
        headerEl = children[0];
        // eslint-disable-next-line no-unused-vars
        const { header, ...restHeadProps } = headerEl.props;
        headerEl = createElement(headerEl.type, { ...restHeadProps, 'data-elem': 'header' });
        contentEl = children[1];
      } else {
        contentEl = children[0];
      }
      if (objectPath.get(children, [children.length - 1, 'props', 'footer'])) {
        footerEl = children[children.length - 1];
        // eslint-disable-next-line no-unused-vars
        const { footer, ...restFootProps } = footerEl.props;
        footerEl = createElement(footerEl.type, { ...restFootProps, 'data-elem': 'footer' });
      }
    }
    const mods = { disabled, overflowHidden };
    // {header && cloneElement(header, { elem: 'header', header: null })}
    return (
      <div data-block="block" data-mods={mods} data-bemstyles={blockCss}>
        {headerEl}
        {contentEl && cloneElement(contentEl, { 'data-elem': 'content' })}
        {footerEl}
        {disabled && (<div data-elem="disabled" />)}
      </div>
    );
  }
}
export default Block;
