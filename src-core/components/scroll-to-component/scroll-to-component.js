import { findDOMNode } from 'react-dom';
import TweenFunctions from 'tween-functions';
import detectPassiveEvents from 'detect-passive-events';
import isBrowser from 'isBrowser';
import { getViewport, getScrollPositionSync } from 'components/sensor/sensor';
/*
 easing ==> ONE OF
 'linear', 'easeInQuad', 'easeOutQuad', 'easeInOutQuad', 'easeInCubic',
 'easeOutCubic', 'easeInOutCubic', 'easeInQuart', 'easeOutQuart', 'easeInOutQuart', 'easeInQuint',
 'easeOutQuint', 'easeInOutQuint', 'easeInSine', 'easeOutSine',
 'easeInOutSine', 'easeInExpo', 'easeOutExpo',
 'easeInOutExpo', 'easeInCirc', 'easeOutCirc', 'easeInOutCirc', 'easeInElastic', 'easeOutElastic',
 'easeInOutElastic', 'easeInBack', 'easeOutBack', 'easeInOutBack', 'easeInBounce', 'easeOutBounce',
 'easeInOutBounce'
 */

const hasNativeScrollTo = isBrowser && document.documentElement.scrollIntoView;

function calculateScrollOffset(element, offset = 0, alignment = 'top') {
  const body = document.body;
  const html = document.documentElement;
  const elementRect = element.getBoundingClientRect();
  const { height: clientHeight } = getViewport(); // html.clientHeight;
  const documentHeight = Math.max(
    body.scrollHeight, body.offsetHeight,
    clientHeight, html.scrollHeight, html.offsetHeight
  );
  let scrollPosition;
  switch (alignment) {
    case 'top':
      scrollPosition = elementRect.top;
      break;
    case 'middle':
      scrollPosition = elementRect.bottom - (clientHeight / 2) - (elementRect.height / 2);
      break;
    case 'bottom':
      scrollPosition = elementRect.bottom - clientHeight;
      break;
    default:
      scrollPosition = elementRect.bottom - (clientHeight / 2) - (elementRect.height / 2);
      break; // defaul to middle
  }

  const { top: pageYOffset } = getScrollPositionSync();

  const maxScrollPosition = documentHeight - clientHeight;

  return Math.min(scrollPosition + offset + pageYOffset, maxScrollPosition);
}

const data = {};

function stopScrolling() {
  window.cancelAnimationFrame(data.rafId);
  window.removeEventListener('wheel', stopScrolling, false);
  window.removeEventListener('touchstart', stopScrolling, false);
}

export default function scrollTo(ref, {
  offset = 0, align = 'top',
  easing = 'easeOutCubic', duration = 250
} = {}) {
  const element = findDOMNode(ref); // eslint-disable-line

  stopScrolling();

  if (element === null) return;

  if (hasNativeScrollTo && element.scrollIntoView
    && offset === 0 && ['top', 'bottom'].indexOf(align) > -1) {
    const block = align === 'top' ? 'start' : 'end';
    const behavior = duration > 0 ? 'smooth' : 'auto';
    return element.scrollIntoView({ behavior, block });
  }

  const finalPosition = calculateScrollOffset(element, offset, align);
  const { top: pageYOffset, left: pageXOffset } = getScrollPositionSync();
  let scrollDirection;
  if (finalPosition > pageYOffset) scrollDirection = 'down';
  if (finalPosition < pageYOffset) scrollDirection = 'up';

  function scrollStep(timestamp) {
    if (!data.startTime) {
      data.startTime = timestamp;
    }

    data.currentTime = timestamp - data.startTime;

    const position = TweenFunctions[easing](
      data.currentTime,
      data.startValue,
      finalPosition,
      duration
    );

    if (position === finalPosition) return stopScrolling();
    if (scrollDirection === 'down' && position > finalPosition) {
      window.scrollTo(pageXOffset, finalPosition);
      return stopScrolling();
    }
    if (scrollDirection === 'up' && position < finalPosition) {
      window.scrollTo(pageXOffset, finalPosition);
      return stopScrolling();
    }
    window.scrollTo(pageXOffset, position);
    data.rafId = window.requestAnimationFrame(scrollStep);
  }

  data.startValue = pageYOffset;
  data.currentTime = 0;
  data.startTime = null;
  data.rafId = window.requestAnimationFrame(scrollStep);

  window.addEventListener('wheel', stopScrolling, detectPassiveEvents.hasSupport ? { passive: true } : false);
  window.addEventListener('touchstart', stopScrolling, detectPassiveEvents.hasSupport ? { passive: true } : false);
}
