/**
 * Created by dryymoon on 11.05.17.
 */
import getDisplayName from 'react-display-name';
import { routerShape } from 'react-router/lib/PropTypes';
import { formatPattern } from 'react-router/lib/PatternUtils';
import omit from 'lodash/omit';
import pick from 'lodash/pick';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import urlLib from 'url';
import urlJoin from 'url-join';
import { navigateHelperShape } from '../../helpers/router/nav-helper';

export default function linkDecorator(Component) {
  return class ComponentWithLink extends Component {
    static displayName = `link(${getDisplayName(Component)})`;

    static contextTypes = {
      ...Component.contextTypes,
      router: routerShape,
      ...navigateHelperShape
    };

    /* constructor(props, context) {
     super(props, context);
     } */

    createLocationDescriptor({ to, query, hash = '', state, params: paramsInLink = {} } = {}) {
      const { location, namedRoutes = {} } = this.context;
      const { params: routeParams } = location;

      if (!to && !query && !hash && !state && isEmpty(paramsInLink)) return;

      let parsedTo = {};

      if (to) {
        try {
          parsedTo = urlLib.parse(to, true, true);
          parsedTo = omit(parsedTo, ['search', 'href', 'path']);
        } catch (err) {
          if (!__DEVELOPMENT__) {
            Raven.captureException(`Catch broken link param to: ${to}, can't parse 'to'`, {
              level: 'error',
              extra: {
                link: { to, query, hash, state, params: paramsInLink },
                err
              }
            });
          } else {
            // eslint-disable-next-line
            console.error(`Catch broken link param to: ${to}`);
          }
          return;
        }
      }

      if (!to) { // && (query || hash)
        parsedTo = pick(location, ['query', 'hash']);
        parsedTo.pathname = '$self';
      }

      parsedTo.pathname = parsedTo.pathname || '';

      const isNamedUrl = parsedTo.pathname[0] === '$';

      if (isNamedUrl) {
        parsedTo.pathname = parsedTo.pathname.substring(1); // remove $;
        const [namedUrlKey, ...restUrlPart] = parsedTo.pathname.split('/');
        const namedRouteUrl = namedRoutes[namedUrlKey];

        if (__DEVELOPMENT__ && !namedRouteUrl) {
          const error = new Error(`Link: cant find named url by key:${JSON.stringify(namedUrlKey)}, 
        given to:${JSON.stringify(to)}`);
          throw error;
        }
        if (!namedRouteUrl) return;
        // console.log('namedRouteUrl', namedRouteUrl);
        // getParams(namedRouteUrl, );
        // TODO Может быть надо парсить параметры и сообщать когда не хватает параметров для Урла
        // const routeUrl = formatPattern(namedRouteUrl, { ...routeParams, ...params });
        // parsedTo.pathname = urlJoin(routeUrl, ...restUrlPart);

        /*
        Use for absolute named urls
        const parsedNamedRouteUrl = urlLib.parse(namedRouteUrl, false, true);

        const isAbsoluteNamedUrl = parsedNamedRouteUrl.host
          && (parsedNamedRouteUrl.protocol === 'http:'
            || parsedNamedRouteUrl.protocol === 'https:'
            || parsedNamedRouteUrl.slashes);

        if (isAbsoluteNamedUrl) {
          parsedTo.host = parsedNamedRouteUrl.host;
          parsedTo.protocol = parsedNamedRouteUrl.protocol;
          parsedTo.slashes = parsedNamedRouteUrl.slashes;
        }
        parsedTo.pathname = urlJoin(parsedNamedRouteUrl.pathname, ...restUrlPart);
        */
        parsedTo.pathname = urlJoin(namedRouteUrl, ...restUrlPart);
      }

      // const isSameOrigin = sameOrigin(window.location.href, );
      const isAbsoluteUrl = parsedTo.host
        && (parsedTo.protocol === 'http:' || parsedTo.protocol === 'https:' || parsedTo.slashes);
      const isRelativeUrl = !parsedTo.host;
      const isSomeProtocol = !isAbsoluteUrl && !isRelativeUrl && parsedTo.protocol && parsedTo.host;

      // Add first slash if needed to relative url
      if (isRelativeUrl && parsedTo.pathname && parsedTo.pathname[0] !== '/') {
        parsedTo.pathname = `/${parsedTo.pathname}`;
      }
      // merge if needed
      if (isRelativeUrl) {
        /* isRelativeUrl - to Prevent user hack for link
        "https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute"
        this link cause front down
        */
        parsedTo.pathname = formatPattern(parsedTo.pathname, { ...routeParams, ...paramsInLink });
      }
      if (query) parsedTo.query = { ...parsedTo.query, ...query };
      if (hash) parsedTo.hash = hash;

      return { ...parsedTo, state, isAbsoluteUrl, isRelativeUrl, isSomeProtocol };
    }

    createHrefFromLocationDescriptor({ isAbsoluteUrl, ...location } = {}) {
      if (isAbsoluteUrl) return urlLib.format(location);
      const { router: { createHref } } = this.context;
      return createHref(location);
    }

    createHref(objOrString) {
      let obj = objOrString;
      if (isString(objOrString)) obj = { to: objOrString };
      const location = this.createLocationDescriptor(obj);
      return this.createHrefFromLocationDescriptor(location);
    }
  };
}
