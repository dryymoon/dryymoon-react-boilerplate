/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import { routerShape } from 'react-router/lib/PropTypes';
// import { formatPattern } from 'react-router/lib/PatternUtils';
import unique from 'lodash/uniq';
import isFunction from 'lodash/isFunction';
import Prefetch from 'component/prefetch';
import omit from 'lodash/omit';
import keys from 'lodash/keys';
// import urlLib from 'url';
// import urlJoin from 'url-join';
import ccn from '../../helpers/styles/CollectClasses';
// import { navigateHelperShape } from '../../helpers/router/nav-helper';
import appMessage from '../../helpers/appMessage';
import linkDecorator from './link-decorator';
import styles from './link.scss';

const propShape = {
  to: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
  query: PropTypes.object, // eslint-disable-line
  hash: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
  state: PropTypes.object, // eslint-disable-line
  // eslint-disable-next-line react/no-unused-prop-types
  params: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
  replace: PropTypes.bool,
  // href: PropTypes.string.isRequired,
  button: PropTypes.bool,
  rel: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  inlineBlock: PropTypes.bool,
  noFollow: PropTypes.bool,
  target: PropTypes.string,
  newWindow: PropTypes.bool,
  blank: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  prefetch: PropTypes.bool,
  // className: PropTypes.string,
  isActive: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  isActiveStrict: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
};

const propShapeKeys = keys(propShape);

@ccn
@bem
@linkDecorator
class Link extends Component {
  static propTypes = propShape;

  static contextTypes = {
    router: routerShape,
    // ...navigateHelperShape
  };

  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
    this.router = context.router;
    // if (__CLIENT__) window.router = this.router;
  }

  isNewWindow() {
    const { target, newWindow, blank } = this.props;
    return newWindow || blank || target === '_blank';
  }

  calcRel({ newWindow }) {
    const { rel, noFollow } = this.props;
    const r = typeof rel === 'string' ? rel.split(' ') : [];
    if (noFollow) r.push('nofollow');
    // https://tproger.ru/articles/target-blank-the-most-underestimated-vulnerability-ever
    if (newWindow) r.push('noopener noreferrer');
    return unique(r).join(' ');
  }

  handleClick(event) {
    if (isModifiedEvent(event) || !isLeftClickEvent(event)) return;

    const { onClick, replace } = this.props;

    if (onClick && (onClick(event) === false)) {
      event.preventDefault();
      return;
    }
    if (event.defaultPrevented) return;

    const { router } = this.context;

    // const location = this.props.to;
    const location = this.createLocationDescriptor(this.props);

    if (!location) return event.preventDefault();

    const { isAbsoluteUrl, isRelativeUrl, isSomeProtocol, host } = location;

    if (isAbsoluteUrl && __APP__) {
      event.preventDefault();
      return appMessage({ goUrl: router.createHref(location) });
    }

    if (isAbsoluteUrl) {
      // TODO: Detect domain
      if (this.isNewWindow()) return null;
      // is not same origin // TODO TEST IT
      if (host && (host !== window.location.host)) return null;
      event.preventDefault();
      return router.push(location);
    }

    /*
     * Internal Links
     */
    if (isRelativeUrl) {
      if (this.isNewWindow()) return null;

      event.preventDefault();
      // TODO: SEO Хрень - ссылка которая ссылается на жту же страницу - кликабельная или нет?...
      // if (!router.isActive(location, /* onlyActiveOnIndex */ true)) {
      if (replace) return router.replace(location);
      return router.push(location);
    }

    if (isSomeProtocol && __APP__) {
      event.preventDefault();
      return appMessage({ goApp: router.createHref(location) });
    }

    if (isSomeProtocol) return null;

    event.preventDefault();
  }

  /* createLocationDescriptor() {
   const { to, query, hash = '', state, params = {} } = this.props;
   if (!to) return null;

   let parsedTo = urlLib.parse(to, true);
   parsedTo = omit(parsedTo, ['search', 'href', 'path']);
   const isNamedUrl = parsedTo.pathname[0] === '$';

   if (isNamedUrl) {
   parsedTo.pathname = parsedTo.pathname.substring(1); // remove $;
   const [namedUrlKey, ...restUrlPart] = parsedTo.pathname.split('/');
   const { namedRoutes = {}, location: { params: routeParams } } = this.context;
   const namedRouteUrl = namedRoutes[namedUrlKey];

   if (__DEVELOPMENT__ && !namedRouteUrl) {
   const error = new Error(`Link: cant find named url by key:${JSON.stringify(namedUrlKey)},
   given to:${JSON.stringify(to)}`);
   throw error;
   }
   if (!namedRouteUrl) return null;
   // TODO Может быть надо парсить параметры и сообщать когда не хватает параметров для Урла
   const routeUrl = formatPattern(namedRouteUrl, { ...routeParams, ...params });
   parsedTo.pathname = urlJoin(routeUrl, ...restUrlPart);
   }

   const isAbsoluteUrl = parsedTo.host && (parsedTo.protocol ===
   'http:' || parsedTo.protocol === 'https:');
   const isRelativeUrl = !parsedTo.host;
   const isSomeProtocol = !isAbsoluteUrl && !isRelativeUrl && parsedTo.protocol && parsedTo.host;

   // Add first slash if needed to relative url
   if (isRelativeUrl && parsedTo.pathname && parsedTo.pathname[0] !== '/') {
   parsedTo.pathname = `/${parsedTo.pathname}`;
   }

   // merge if needed
   if (query) parsedTo.query = { ...parsedTo.query, ...query };
   if (hash) parsedTo.hash = hash;

   return { ...parsedTo, state, isAbsoluteUrl, isRelativeUrl, isSomeProtocol };
   } */

  render() {
    const { router } = this.context;
    const {
      inlineBlock, disabled, button, prefetch,
      children, title,
      isActive, isActiveStrict
    } = this.props;

    const props = {
      ...omit(this.props, propShapeKeys),
      title
    };

    // assign(props, pickBy(this.props, (v, k) => startsWith(k, 'data-')));

    // if (dataTest) props['data-test'] = dataTest;

    if (disabled) props.disabled = 'disabled';

    const isNewWindow = this.isNewWindow();
    if (isNewWindow) props.target = '_blank';

    if (!__AMP__) props.rel = this.calcRel({ isNewWindow });

    let location;
    if (!disabled) location = this.createLocationDescriptor(this.props);

    let activeStrict = false;
    let active = false;

    if (location) {
      const { isAbsoluteUrl, isRelativeUrl } = location;
      const href = this.createHrefFromLocationDescriptor(location);

      // Вадос попросил чтоб каждая внешняя ссылка открывалась в новом окне...
      if (isAbsoluteUrl) props.target = '_blank';

      if (isActive && isFunction(isActive)) {
        active = isActive(location, router.location);
      } else if (isActive === false || isActive) {
        active = !!isActive;
      } else {
        active = isRelativeUrl && router.isActive(location, /* onlyActiveOnIndex */ false);
      }

      if (isActiveStrict && isFunction(isActiveStrict)) {
        active = isActive(location, router.location);
      } else if (isActiveStrict === false || isActiveStrict) {
        activeStrict = !!isActiveStrict;
      } else {
        activeStrict = isRelativeUrl && router.isActive(location, /* onlyActiveOnIndex */ true);
      }

      if (!__APP__ && !activeStrict && !disabled) props.href = href;
    }

    if (!disabled) props.onClick = this.handleClick;

    const link = (
      <a
        {...props}
        data-block="link"
        data-mods={{ inlineBlock, activeStrict, active, disabled, button }}
        data-bemstyles={styles}
        children={children}
      />
    );

    const { href } = props;
    const isRelativeUrl = location && location.isRelativeUrl;

    if (prefetch && isRelativeUrl && href) {
      return (<Prefetch pageUrl={href} data-block="link" children={link} />);
    }

    return link;
  }
}

export default Link;

function isLeftClickEvent(event) {
  return event.button === 0;
}

function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}
