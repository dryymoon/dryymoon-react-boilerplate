/**
 * Created by PIV on 22.06.2016.
 */
import Module from './link';
import linkDecorator from './link-decorator';

export { linkDecorator };
export default Module;
