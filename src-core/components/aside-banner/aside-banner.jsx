/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';
import Banner from '../banner/banner';
import Pic from '../pic/pic';
import Link from '../link/link';

const asideBannerCss = require('./aside-banner.scss');


/**
 * AsideBanner
 * Используется для прилипания банеров слева контента и справа
 * Пример:
 * ```html
 * <AsideBanner left />
 * ```
 */

@ccn
@bem
class AsideBanner extends Component {
  static propTypes = {
    /**
     * Прилепить слева банер
     */
    left: PropTypes.bool,
    /**
     * Прилепить ссправа банер
     */
    right: PropTypes.bool
  };

  render() {
    const { left, right } = this.props;
    return (
      <div
        data-block="asideBanner"
        data-mods={{ left, right }}
        data-bemstyles={asideBannerCss}>
        <Banner data-elem="banner">
          <Link
            inlineBlock
            rel="nofollow"
            blank
            href="/data/go/avtograf.kiev.ua" >
            <Pic
              data-elem="bannerImg"
              // src={bannerImg}
              width={260}
              height={900}
            />
          </Link>
        </Banner>
      </div>
    );
  }
}
export default AsideBanner;
