/**
 * Created by Vit on 25.01.2017.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import ccn from 'ccn';

import styles from './viewport.scss';

@ccn
@bem
class Viewport extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  render() {
    const { children } = this.props;
    return (
      <div
        data-block="viewport"
        data-bemstyles={styles}
        children={children}
      />
    );
  }
}
export default Viewport;
