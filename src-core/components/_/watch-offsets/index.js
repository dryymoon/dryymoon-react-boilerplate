/**
 * Created by PIV on 22.06.2016.
 */
// import module from './sensor';
import module from './watch-offsets';
import redux from './redux';

export default module;
export { redux };
