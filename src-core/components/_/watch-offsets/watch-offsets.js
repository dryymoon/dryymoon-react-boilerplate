/**
 * Created by DryyMoon on 27.10.2016.
 */
/* eslint-disable */
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import keys from 'lodash/keys';
import omit from 'lodash/omit';
import getDisplayName from 'react-display-name';
import isFunction from 'lodash/isFunction';
import extend from 'extend';
import { actions } from './redux';

export default function factory(watchObj = {}) {
  const needleKeys = keys(watchObj);
  return function (BaseComponent) {
    @connect(({ offsets = {} }) => {
      const filtered = omit(offsets, needleKeys);
      return { ...watchObj, ...filtered };
    }, actions)
    class ComponentWithWatchOffsets extends BaseComponent {
      /* componentDidMount(...args) {
        if(super.componentDidMount) super.componentDidMount(...args);
        console.log('componentDidMount', this.props);
      }

      componentDidUpdate(...args) {
        if(super.componentDidUpdate) super.componentDidUpdate(...args);
      }

      componentWillUnmount(...args) {
        if(super.componentWillUnmount) super.componentWillUnmount(...args);

      } */
    }
    return ComponentWithWatchOffsets;
  };
}
/* return (BaseComponent) => {
 // Check BaseComponent
 if (!isFunction(BaseComponent)) {
 throw new Error('data-preload factory must decorate only React Component');
 }
 }; */
// return class ComponentWitWatchOffsets extends Component {

/*    static displayName = `Sensor(${getDisplayName(Component)})`;

 static contextTypes = extend(Component.contextTypes || {}, {
 sensor: PropTypes.instanceOf(SensorLib)
 });

 constructor(props, context) {
 super(props, context);
 this.sensor = context.sensor;
 }

 componentDidMount() {
 allEvents.forEach((e) => {
 if (this[e] && isFunction(this[e])) {
 this[e] = this[e].bind(this);
 this.context.sensor[e](this[e]);
 }
 });
 if (super.componentDidMount) super.componentDidMount();
 }

 componentWillUnmount() {
 allEvents.forEach((e) => {
 if (this[e] && isFunction(this[e])) {
 this.context.sensor[e.replace(/^on/, 'off')](this[e]);
 }
 });
 if (super.componentWillUnmount) super.componentWillUnmount();
 }
 }; */


/* @connect((state) => ({
 initialized: state.forms.initialized,
 form: state.forms[props.name] || redux.formInitialState
 }), { ...redux }) */
