/**
 * Created by dryymoon on 13.03.17.
 */
import omit from 'lodash/omit';
import keys from 'lodash/keys';
import ReducersRegistry from '../../helpers/redux/reducers-registry';

export const UPDATE = 'offsets/UPDATE';
export const DESTROY = 'offsets/DESTROY';

export default function reducer(state = {}, { type, offsets } = {}) {
  switch (type) {
    case UPDATE:
      return { ...state, ...offsets };
    case DESTROY:
      return omit(state, keys(offsets));
    default:
      return state;
  }
}

function actionUpdate(offsets) {
  return { type: UPDATE, offsets };
}

function actionDestroy(offsets) {
  return { type: DESTROY, offsets };
}

export const actions = { actionUpdate, actionDestroy };

ReducersRegistry.register({ offsets: reducer });
