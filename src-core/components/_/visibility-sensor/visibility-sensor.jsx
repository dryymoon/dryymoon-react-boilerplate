/**
 * Created by DryyMoon on 24.10.2016.
 */
import { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { findDOMNode } from 'react-dom';
import uuid from 'uuid/v4';
import ScrollListenerLib from 'react-scroll-listener';
import onResizeLib from 'onresize';
import position from 'metal-position';

const isBrowser = typeof window !== 'undefined';
let scrollListener;

if (isBrowser) {
  scrollListener = new ScrollListenerLib();
  onResizeLib.on();
}

class VisibilitySensor extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };

  constructor(props, context) {
    super(props, context);
    // this.calc = this.calc.bind(this);
    this.onScroll = this.onScroll.bind(this);
    this.onResize = this.onResize.bind(this);
    this.state = {};
  }

  componentDidMount() {
    /* Called only in Browser */
    this.el = findDOMNode(this);
    this.uuid = uuid.v1();
    scrollListener.addScrollStartHandler(this.uuid, this.onScroll);
    onResizeLib.on(this.onResize);
    this.reCalc();
  }

  componentDidUpdate() {
    this.el = findDOMNode(this);
    this.reCalc();
  }

  componentWillUnmount() {
    /* Called only in Browser */
    scrollListener.removeScrollStartHandler(this.uuid);
    onResizeLib.off(this.onResize);
  }

  onResize() {
    this.reCalc();
  }

  onScroll() {
    this.reCalc();
  }

  reCalc() {
    if (!this.el) return;
    this.rect = this.el.getBoundingClientRect();
    // this.rect = this.el.getClientRects();

    console.log('this.rect', this.rect, scrollListener.scrollTop);
  }

  render() {
    return Children.only(this.props.children);
  }
}

export default VisibilitySensor;

