/**
 * Created by dryymoon on 02.08.17.
 */
/* Rename "main.default.js" to "main.js" and edit it if you need configure
 * elFInder options or any things. And use that in elfinder.html.
 * e.g. `<script data-main="./main.js" src="./require.js"></script>`
 * */

// import require from 'requirejs/require';
/* eslint-disable */

import requirejs from '!exports?requirejs!requirejs/require';

// const requirejs = require('!exports?requirejs!requirejs/require');

console.log('INIT initElFinder');

(function initElFinder() {
  // jQuery and jQueryUI version
  const jqver = '3.2.1';
  const uiver = '1.12.1';

  // Detect language (optional)
  const lang = (() => {
    const locq = window.location.search;
    const locm = locq.match(/lang=([a-zA-Z_-]+)/);
    let fullLang;
    let parsedLang;
    if (locq && locm) {
      // detection by url query (?lang=xx)
      fullLang = locm[1];
    } else {
      // detection by browser language
      fullLang = (navigator.browserLanguage || navigator.language || navigator.userLanguage);
    }
    parsedLang = fullLang.substr(0, 2);
    if (parsedLang === 'ja') parsedLang = 'jp';
    else if (parsedLang === 'pt') parsedLang = 'pt_BR';
    else if (parsedLang === 'ug') parsedLang = 'ug_CN';
    else if (parsedLang === 'zh') parsedLang = (fullLang.substr(0, 5).toLowerCase() === 'zh-tw') ? 'zh_TW' : 'zh_CN';
    return parsedLang;
  })();

  // elFinder options (REQUIRED)
  // Documentation for client options:
  // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
  const opts = {
    getFileCallback(file, fm) {
      window.opener.CKEDITOR.tools.callFunction((() => {
        const reParam = new RegExp('(?:[\?&]|&amp;)CKEditorFuncNum=([^&]+)', 'i');
        const match = window.location.search.match(reParam);
        return (match && match.length > 1) ? match[1] : '';
      })(), file.url);
      fm.destroy();
      window.close();
    },
    url: 'php/connector.minimal.php', // connector URL (REQUIRED)
    lang                         // auto detected language (optional)
  };

  // Start elFinder (REQUIRED)
  function start(elFinder) {
    // load jQueryUI CSS
    elFinder.prototype.loadCss(`//cdnjs.cloudflare.com/ajax/libs/jqueryui/${uiver}/themes/smoothness/jquery-ui.css`);

    $(() => {
      // Optional for Japanese decoder "extras/encoding-japanese.min"
      if (window.Encoding && Encoding.convert) {
        elFinder.prototype._options.rawStringDecoder = function (s) {
          return window.Encoding.convert(s, { to: 'UNICODE', type: 'string' });
        };
      }
      // Make elFinder (REQUIRED)
      $('#elfinder').elfinder(opts);
    });
  }

  // JavaScript loader (REQUIRED)
  function load() {
    requirejs(
      [
        'elfinder', (lang !== 'en') ? 'elfinder.lang' : null    // load detected language
        // 	, 'extras/quicklook.googledocs'
        // optional preview for GoogleApps contents on the GoogleDrive volume
        // 	, (lang === 'jp')? 'extras/encoding-japanese.min' : null
        // optional Japanese decoder for archive preview
      ],
      start,
      (error) => {
        alert(error.message);
      }
    );
  }

  const elFinderUrl = '//cdnjs.cloudflare.com/ajax/libs/elfinder/2.1.26/js';

  // config of RequireJS (REQUIRED)
  // eslint-disable-next-line
  requirejs.config({
    baseUrl: 'js',
    paths: {
      jquery: `//cdnjs.cloudflare.com/ajax/libs/jquery/${jqver}/jquery.min`,
      'jquery-ui': `//cdnjs.cloudflare.com/ajax/libs/jqueryui/${uiver}/jquery-ui.min`,
      elfinder: `${elFinderUrl}/elfinder.min`,
      'elfinder.lang': [
        `${elFinderUrl}/i18n/elfinder.${lang}`,
        `${elFinderUrl}/i18n/elfinder.fallback`
      ]
    },
    waitSeconds: 10 // optional
  });

  // load JavaScripts (REQUIRED)
  load();
}());
