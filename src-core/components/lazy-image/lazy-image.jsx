/**
 * Created by dryymoon on 03.02.2018.
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import ccn from 'ccn';
// import Helmet from 'Helmet';
// import 'intersection-observer'; // optional polyfill
// import Observer from '@researchgate/react-intersection-observer';
import styles from './lazy-image.scss';

@ccn
@bem
class LazyImg extends PureComponent {
  static bemStylesOverride = styles;

  static defaultProps = {
    alt: ''
  };

  static propTypes = {
    src: PropTypes.string.isRequired,
    onImageLoad: PropTypes.func,
    alt: PropTypes.string,
    // className: PropTypes.string,
    // loadByVisibility: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      // src: 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==',
      loaded: false
    };

    this.handleLoad = this.handleLoad.bind(this);
    // this.handleVisibleChange = this.handleVisibleChange.bind(this);
  }

  componentDidMount() {
    if (this.img && this.img.complete && !this.state.loaded) {
      this.handleLoad();
    }
  }

  handleLoad() {
    const { onImageLoad } = this.props;
    this.setState({ loaded: true });

    if (onImageLoad) onImageLoad();
  }

  /* handleVisibleChange({ isIntersecting }) {
    if (!isIntersecting) return;
    if (this.state.wasVisible) return;
    const { src } = this.props;
    this.setState({ wasVisible: true, src });
  } */

  render() {
    const { src, alt } = this.props; // loadByVisibility
    const { loaded } = this.state;

    return (
      <img
        data-block="lazyImage"
        src={src}
        alt={alt}
        onLoad={this.handleLoad}
        data-mods={{ loaded }}
        ref={node => this.img = node}
      />
    );

    /* if (loadByVisibility) {
      return (
        <Observer
          onlyOnce
          onChange={this.handleVisibleChange}
          data-block="lazyImage"
          children={img}/>
      );
    } */

    // return img;
  }
}

export default LazyImg;
