/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const bannerCss = require('./banner.scss');

/**
 * Banner
 * Основной компонент показа банеров, пока в разработке в будущем измениться
 * Пример:
 * ```html
 * <Banner>
 *   I`m a banner
 * </Banner>
 * ```
 */
@ccn
@bem
class Banner extends Component {
  static propTypes = {
    /**
     * Сюда прилетает баннер, покачто, это измениться в будущем
     */
    children: PropTypes.node
  };

  render() {
    return (
      <div data-block="banner" data-bemstyles={bannerCss} children={this.props.children} />
    );
  }
}
export default Banner;
