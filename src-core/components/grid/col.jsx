import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';

// TODO: перевірка вхідних значень

import * as styles from './col.scss';

const varies = ['xs', 'sm', 'md', 'lg'];

@ccn
@bem
export default class Col extends Component {
  /* eslint-disable react/no-unused-prop-types */
  static propTypes = {
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    children: PropTypes.node
  };

  render() {
    const { props } = this;
    const { children } = props;

    const mods = [];
    varies.forEach((it) => {
      if (props[it] >= 0 && props[it] <= 12) mods.push(`${it}-${props[it]}`);
    });

    return (
      <div
        data-block="grid"
        data-mods={mods}
        data-bemstyles={styles}
        children={children} />
    );
  }
}
