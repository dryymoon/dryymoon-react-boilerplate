/**
 * Created by Vit on 20.09.2016.
 */
import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import { diff } from 'deep-object-diff';
import capitalize from 'lodash/capitalize';
import onClickOutside from 'react-onclickoutside';
import ccn from 'ccn';
import bem from 'bem';
import Button from 'components/button';
import Loader from 'components/loader';
import UniqueId from '../../helpers/react/ReactUniqueIds';
import styles from './drop-down.scss';
// import * as redux from './redux';

// import Live from 'components/live';
// import Helmet from 'react-helmet';
// const JS = 'live(\'click\',
// \'button[data-drop-down-button]\', function(event) { console.log(event); }); ';

export const shape = {
  'data-test': PropTypes.string,
  position: PropTypes.oneOf(['up', 'down']),
  float: PropTypes.oneOf(['left', 'right']),
  defaultOpened: PropTypes.bool,
  /**
   * Не закрывать при клике вне контейнера
   */
  dontCloseOnOutsideClick: PropTypes.bool,
  /**
   * Каллбек при открытии
   */
  onOpen: PropTypes.func,
  /**
   * Каллбек при закрытии
   */
  onClose: PropTypes.func,

  normalFlow: PropTypes.bool,
  setRef: PropTypes.func,
  beforeOpen: PropTypes.func,
  beforeClose: PropTypes.func,
  afterOpen: PropTypes.func,
  afterClose: PropTypes.func,
};

export const propTypes = {
  ...shape,
  children: PropTypes.node,
  /**
   * Зарезервировано на будущее
   */
  group: PropTypes.string,
  /**
   * Internal from Redux
   */
  /* whichOpened: PropTypes.number,
   actionOpen: PropTypes.func,
   actionClose: PropTypes.func,
   actionDestroyed: PropTypes.func,
   enableOnClickOutside: PropTypes.func, */
  /**
   * Internal from react-onclickoutside
   */
  disableOnClickOutside: PropTypes.func,
};

/**
 * DropDown
 * Выпадалка при клике
 */

/*  @connect(state => ({
 whichOpened: state.dropDown.whichOpened
 }), { ...redux }, null, { withRef: true }) */
@onClickOutside
@ccn
@bem
@UniqueId
class DropDown extends Component {
  static propTypes = propTypes;

  constructor(props, context) {
    super(props, context);
    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.state = { opened: props.defaultOpened };
    this.runtime = {
      isMounted: false,
      id: context.getUid(),
      group: props.group
    };
  }

  componentDidMount() {
    const { setRef } = this.props;
    this.runtime.isMounted = true;
    if (setRef) setRef(this);
    this.nodeRef.removeAttribute('data-dropdown-loading');
    if (!this.isThisOpened()) {
      this.props.disableOnClickOutside();
    }
  }

  /* shouldComponentUpdate(nextProps, nextState) {
    console.log(
      'UPDATED DD', this.props,
      diff(this.props, nextProps), diff(this.state, nextState)
    );
    return true;
  } */

  componentWillUnmount() {
    this.runtime.isMounted = false;
    /* if (this.whichOpened === this.runtime.id) {
     this.props.actionDestroyed(this.runtime);
     } */
  }

  // TODO: Maybe Need Shod component update

  isThisOpened() {
    return this.state.opened;
    // if (props) return props.whichOpened === this.runtime.id;
    // return this.props.whichOpened === this.runtime.id;
  }

  handleButtonClick() {
    if (this.isThisOpened()) return this.close();
    return this.open();
  }

  open() {
    const { beforeOpen, afterOpen } = this.props;
    if (this.isThisOpened()) return; // This is already opened
    if (beforeOpen && beforeOpen() === false) return;
    this.setState({ opened: true }, () => afterOpen && afterOpen());
    if (this.props.onOpen) this.props.onOpen();
    if (!this.props.dontCloseOnOutsideClick) this.props.enableOnClickOutside();
  }

  close() {
    const { beforeClose, afterClose } = this.props;
    if (!this.isThisOpened()) return; // This is already closed
    if (beforeClose && beforeClose() === false) return;
    this.setState({ opened: false }, () => afterClose && afterClose());
    // this.props.actionClose(this.runtime);
    if (this.props.onClose) this.props.onClose();
    this.props.disableOnClickOutside();
  }

  handleClickOutside() {
    if (this.props.dontCloseOnOutsideClick) return;
    this.close();
  }

  render() {
    const children = Children.toArray(this.props.children);
    const { position = 'down', float = 'left', 'data-test': dataTest, normalFlow, extraProps } = this.props;

    const absoluteFlow = !normalFlow;

    const modFlow = normalFlow ? 'normalFlow' : 'absoluteFlow';
    const mods = [`${modFlow}Position${capitalize(position)}`, `${modFlow}Float${capitalize(float)}`,
      { normalFlow, absoluteFlow }];
    const [btn = {}, content = {}] = children;
    const attrs = { ...extraProps };
    if (!this.runtime.isMounted) attrs['data-dropdown-loading'] = true;
    const opened = this.isThisOpened();
    const dataTestAttr = dataTest ? { 'data-test': `${dataTest}_openDropDownBtn` } : {};

    return (
      <div
        {...attrs}
        data-block="dropdown"
        data-bemstyles={styles}
        data-mods={[{ opened }, ...mods]}
        ref={node => this.nodeRef = node}>
        <Button
          onClick={this.handleButtonClick}
          {...btn.props}
          {...dataTestAttr}
          data-elem="btn"
          type="button"
        />
        <div {...content.props} data-elem="content" data-mods={mods}/>
        <div data-elem="loading">
          <Loader data-elem="loader" gray/>
        </div>
      </div>
    );
  }
}

export default DropDown;

export function close(instance) {
  if (!(instance instanceof DropDown)) {
    if (__DEVELOPMENT__) throw new Error('DropDown.close called without instance aggument.');
    return false;
  }
  // return instance.getWrappedInstance().getInstance().close();
  return instance.getInstance().close();
}

export function open(instance) {
  if (!(instance instanceof DropDown)) {
    if (__DEVELOPMENT__) throw new Error('DropDown.close called without instance aggument.');
    return false;
  }
  // return instance.getWrappedInstance().getInstance().close();
  return instance.getInstance().open();
}

/*
 TODO: Improve on server loading
 <Live />
 <Helmet script={[{ type: 'text/javascript', innerHTML: JS }]} />
 */
