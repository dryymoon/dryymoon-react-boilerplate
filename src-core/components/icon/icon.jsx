/**
 * Created by Vit on 20.09.2016.
 */
/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'bem';
import ccn from 'ccn';

import styles from './icon.scss';

const fontMaps = {
  search: '\ue900',
  home: '\ue901',
  envelope: '\ue902',
  map: '\ue903',
  user2: '\ue904',
  gamburger: '\ue905',
  arrowSlider: '\ue906',
  settings: '\ue907',
  cancel2: '\ue908',
  carret: '\ue909',
  arrow: '\ue90a',
  cancel: '\ue90b',
  copy: '\ue90c',
  youtube:
    '<span class="p_0">\ue90d</span>' +
    '<span class="p_1">\ue90e</span>',
  dollar: '\ue90f',
  user: '\ue910',
  cart: '\ue911',
  dropdown: '\ue913',
  exit: '\ue920',
  menu: '\ue921',
  favorites: '\ue922',
  login: '\ue923',
  person: '\ue924',
  plusCircle: '\ue925',
  menuClose: '\ue926',
  notification: '\ue927',
  carretThin: '\ue928',
  checkbox: '\ue929',
  checkboxChecked:
    '<span class="p_0">\ue930</span>' +
    '<span class="p_1">\ue931</span>' +
    '<span class="p_2">\ue932</span>',
  radio: '\ue933',
  radioChecked:
    '<span class="p_0">\ue934</span>' +
    '<span class="p_1">\ue935</span>' +
    '<span class="p_2">\ue936</span>',
  threePoints: '\ue937',
  mapPoint: '\ue938',
  mapMetro: '\ue939',
  mapRegion: '\ue940',
  mapArea: '\ue941',
  mapStreet: '\ue942',
  pdf: '\ue943',
  metro: '\ue944',
  starCircle: '\ue945',
  arrowTopCircle: '\ue946',
  brushCircle: '\ue947',
  purse: '\ue948',
  plus: '\ue949',
  viewBig: '\ue950',
  viewSmall: '\ue951',
  print: '\ue952',
  commentsCount: '\ue953',
  maximize: '\ue954',
  placeholder: '\ue955',
  groups: '\ue956',
  // : 'ue957 - ue959'
  expand: '\ue960',
  collapse: '\ue961',
  minimize: '\ue962',
  phoneCalls: '\ue963',
  complainCircle: '\ue964',
  minus: '\ue965',
  undo: '\ue966',
  favoriteList: '\ue967',
  compare: '\ue968'
};

export const types = Object.keys(fontMaps).sort();

@ccn
@bem
class Icon extends Component {
  static propTypes = {
    type: PropTypes.oneOf(Object.keys(fontMaps)),
    title: PropTypes.string
  };

  static cache = {};

  render() {
    const { type, title: unsafeTitle } = this.props;
    const title = (typeof unsafeTitle === 'string') ? unsafeTitle : null;

    return (
      <span
        data-block="icon"
        data-bemstyles={styles}
        data-mods={type}
        dangerouslySetInnerHTML={{ __html: fontMaps[type] }}
        title={title}
      />
    );
  }
}
export default Icon;
