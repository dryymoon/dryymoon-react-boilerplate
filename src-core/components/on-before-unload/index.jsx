/**
 * Created by dryymoon on 26.07.17.
 */

import PropTypes from 'prop-types';
import React from 'react';

class Beforeunload extends React.Component {
  static propTypes = {
    children: PropTypes.any, // eslint-disable-line
    text: PropTypes.string.isRequired,
  };

  constructor(...args) {
    super(...args);
    this.handleBeforeunload = this.handleBeforeunload.bind(this);
  }

  componentDidMount() {
    window.addEventListener('beforeunload', this.handleBeforeunload);
  }

  componentWillUnmount() {
    window.removeEventListener('beforeunload', this.handleBeforeunload);
  }

  handleBeforeunload(e) {
    const { text = ' ' } = this.props;
    const event = e || window.event;
    // For IE and Firefox prior to version 4
    if (event) {
      event.returnValue = text; // eslint-disable-line
    }
    // For Chrome, Safari and Opera 12+
    return text;
  }

  render() {
    return this.props.children || null;
  }
}

export default Beforeunload;
