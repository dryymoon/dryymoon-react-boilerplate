/**
 * Created by PIV on 28.06.2016.
 */
/* eslint-disable jsx-a11y/label-has-for, jsx-a11y/click-events-have-key-events */
import React, { Component, cloneElement } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import uuid from 'uuid/v4';
import Helmet from 'react-helmet';
import get from 'lodash/get';
import castArray from 'lodash/castArray';
// import omit from 'lodash/omit';
import ccn from '../../helpers/styles/CollectClasses';
import styles from './react-nojs-tabs.scss';
import getUid from '../../helpers/react/ReactUniqueIds';
/*
 USAGE:
 <Tabs>
 __<anyTag>
 ____...SOME AVAILABLE SCHEMA, see down for schemas
 __</anyTag>
 __<anyTag>
 ____...SOME AVAILABLE SCHEMA, see down for schemas
 __</anyTag>
 </Tabs>
 */

@ccn
@bem
@getUid
class ReactNoJsTabs extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    equalWidth: PropTypes.bool,
    fullWidth: PropTypes.bool,
    onTabChange: PropTypes.func
  };

  static isFullStyle(item) { // TODO Broken untested
    /*
     <$anyTag>
     __<active>Some Tab 1</active>
     __<passive>Some Tab 1</passive>
     </$anyTag>
     <$anyTag>
     __Some Content
     </$anyTag>
     ------------- OR ----------------
     <$anyTag>
     __<$anyTag active>Some Tab 1</$anyTag>
     __<$anyTag passive>Some Tab 1</$anyTag>
     </$anyTag>
     <$anyTag>
     __Some Content
     </$anyTag>
     */
    if (!Array.isArray(item)) return false;
    if (item.length !== 2) return false;
    // TODO Add wrapper
    const active =
      (get(item[0], 'props.children.0.type') === 'active'
      || get(item[0], 'props.children.0.props.active'))
      && item[0].props.children[0];
    if (!active) return false;

    const passive =
      (get(item[0], 'props.children.1.type') === 'passive'
      || get(item[0], 'props.children.1.props.passive'))
      && item[0].props.children[1];
    if (!passive) return false;

    const content = item[1];
    if (!content) return false;
    return { active, passive, content };
  }

  static isFullStyle2(item) { // TODO Broken untested
    /*
     <$anyTag>
     __<any>Some Tab 1</any>
     </$anyTag>
     <$anyTag>
     __Some Content
     </$anyTag>
     ------------- OR ----------------
     <$anyTag>
     __<$anyTag any>Some Tab 1</$anyTag>
     </$anyTag>
     <$anyTag>
     __Some Content
     </$anyTag>
     */
    // TODO Add wrapper
    if (!Array.isArray(item)) return false;
    if (item.length !== 2) return false;
    const any =
      (get(item[0], 'props.children.0.type') === 'any'
      || get(item[0], 'props.children.0.props.any'))
      && item[0].props.children[0];
    if (!any) return false;
    const content = item[1];
    if (!content) return false;
    return { active: any, passive: any, content };
  }

  static isFastStyle(tabData) {
    /*
     <active>Some Tab 1</active>
     <passive>Some Tab 1</passive>
     <$anyTag>
     __Some Content
     </$anyTag>
     */

    if (!Array.isArray(tabData)) return;
    if (tabData.length !== 3) return;
    const [active, passive, content] = tabData;
    if (get(active, 'type') !== 'active') return;
    if (get(passive, 'type') !== 'passive') return;
    if (!content) return;
    // const onActivateVal = omit(item.props, ['children']);
    return { active, passive, content };
  }

  static isFastStyle2(tabData) {
    /*
     <any>Some Tab 1</any>
     <$anyTag>
     __Some Content
     </$anyTag>
     */
    if (!Array.isArray(tabData)) return;
    if (tabData.length !== 2) return;
    const [any, content] = tabData;
    if (get(any, 'type') !== 'any') return;
    if (!content) return;
    // const onActivateVal = omit(item.props, ['children']);
    return { active: any, passive: any, content };
  }

  constructor(props, context) {
    super(props, context);
    this._instanceId = (context.getUid || uuid)();
    this.reserverIds = [];
    this.onTabChange = this.onTabChange.bind(this);
  }

  onTabChange(value, index) {
    const { onTabChange } = this.props;
    if (onTabChange) onTabChange(value, index);
  }

  /* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
  renderTab({
    wrapper: { props: tabWrapperProps } = {},
    tabId, controlId, index, onActivateVal,
    active: tabActiveView, passive: tabPassiveView,
  }) {
    return (
      <li {...tabWrapperProps} id={tabId} key={tabId} data-elem="tab">
        <label
          {...tabActiveView.props}
          // children={tabActiveView.props.children}
          id={`${tabId}_active`}
          data-elem="tabElement"
          data-mods="active"
          className={`active ${tabActiveView.props.className || ''}`}
          htmlFor={controlId} />
        <label
          {...tabPassiveView.props}
          // children={tabPassiveView.props.children}
          id={`${tabId}_passive`}
          data-elem="tabElement"
          data-mods="passive"
          className={`passive ${tabPassiveView.props.className || ''}`}
          htmlFor={controlId}
          onClick={() => this.onTabChange(onActivateVal, index)}
        />
      </li>
    );
  }

  render() {
    const instanceId = this._instanceId;
    const controlsArr = [];
    const tabsArr = [];
    const contentsArr = [];
    const stylesArr = [];

    const filteredChildren = castArray(this.props.children).filter(it => it);

    while (filteredChildren.length - this.reserverIds.length > 0) {
      this.reserverIds.push((this.getUid || uuid)());
    }

    let selectedIndex = 0;

    filteredChildren.forEach(({ props: { selected } }, index) => {
      if (selected) selectedIndex = index;
    });

    filteredChildren.forEach((it, index) => {
      const { children, manageByVisibility } = it.props;
      const tabData =
        // ReactNoJsTabs.isFullStyle(children)
        // || ReactNoJsTabs.isFullStyle2(children)
        ReactNoJsTabs.isFastStyle(children)
        || ReactNoJsTabs.isFastStyle2(children);

      if (!tabData) return;

      const id = `_${this.reserverIds[index]}`;
      const controlId = `clid_${id}`;
      const tabId = `tbid_${id}`;
      const contentId = `ctid_${id}`;

      controlsArr.push(<input
        key={controlId}
        data-elem="control"
        id={controlId}
        type="radio"
        // value={index}
        // onChange={({ target: { value } }) => this.onTabChange(value, index)}
        name={`reactNoJsTabs_${instanceId}`}
        defaultChecked={index === selectedIndex} />);

      tabsArr.push(this.renderTab({
        ...tabData,
        tabId,
        controlId,
        index,
        onActivateVal: it.props
      }));

      stylesArr.push(`#${controlId}:checked ~ ul #${tabId} #${tabId}_active {display: block;}`);
      stylesArr.push(`#${controlId}:checked ~ ul #${tabId} #${tabId}_passive {display: none;}`);

      contentsArr.push(cloneElement(tabData.content, { key: index, 'data-elem': 'content', id: contentId }));

      stylesArr.push(`#${controlId}:checked ~ .reactNoJsTabs__tabsContent #${contentId} {display: block; visibility: visible; position: relative; z-index: inherit;}`);

      if (manageByVisibility) {
        stylesArr.push(`#${controlId} ~ .reactNoJsTabs__tabsContent #${contentId} {display: block; position: absolute;}`);
      }
    });

    const combinedStyle = stylesArr.join(' ');

    // TODO Проверить, чтоб стили убирались, когда компонент демонтируется...
    const { fullWidth, equalWidth } = this.props;

    if (__AMP__) {
      return null;
    }

    return (
      <div data-block="reactNoJsTabs" data-bemstyles={styles}>
        <Helmet style={[{ cssText: combinedStyle }]} />
        {controlsArr}
        <ul data-elem="tabs" data-mods={{ fullWidth, equalWidth }}>
          {tabsArr}
        </ul>
        <div data-elem="tabsContent">
          {contentsArr}
        </div>
      </div>
    );
  }
}
export default ReactNoJsTabs;
