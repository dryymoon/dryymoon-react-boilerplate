/**
 * Created by DryyMoon on 22.11.2016.
 */
/* eslint-disable no-console */
import { Component, Children } from 'react';
import PropTypes from 'prop-types';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
import intersection from 'lodash/intersection';
import keys from 'lodash/keys';
import { match as cssMediaQueryMatch } from 'css-mediaquery';
import reCssMediaQuery from 'regex-css-media-query'; // eslint-disable-line
import SensorLib from './sensor';
import cookie from '../../helpers/react/ReactCookiesProvider';

@cookie
class SensorProvider extends Component {
  static propTypes = {
    provideMedia: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
    defaultMedia: PropTypes.objectOf(PropTypes.oneOfType([PropTypes.string])),
    children: PropTypes.node
  };

  static childContextTypes = { sensor: PropTypes.instanceOf(SensorLib) };

  constructor(props, context) {
    super(props, context);
    this.sensor = new SensorLib();

    if (!props.provideMedia) return;

    const providedMedia = pickBy(props.provideMedia, v => reCssMediaQuery().test(v));

    if (__CLIENT__) {
      this.sensor.addMediaQueries(providedMedia);
    }
    if (__SERVER__) {
      const defaultMedia = props.defaultMedia || { type: 'screen', width: '1280px' };

      let clientMatchedMedia = mapValues(providedMedia, (v) => {
        try {
          return cssMediaQueryMatch(v, defaultMedia);
        } catch (e) {
          if (__DEVELOPMENT__) console.error('MediaMatch fail die to', e);
          return false;
        }
      });

      try {
        const clientMatchMediaRaw = context.cookies.get('__matchMedia');
        if (clientMatchMediaRaw) {
          let realClientMediaMatch = JSON.parse(decodeURIComponent(clientMatchMediaRaw));
          realClientMediaMatch = intersection(realClientMediaMatch, keys(providedMedia));
          if (realClientMediaMatch.length > 0) {
            clientMatchedMedia = mapValues(providedMedia, () => false);
            realClientMediaMatch.forEach(v => clientMatchedMedia[v] = true);
          }
        }
      } catch (e) {
        if (__DEVELOPMENT__) console.error('errorParseMedia', e);
      }

      this.sensor.addMediaQueries(clientMatchedMedia);
    }

    this.storeMediaInCookies = this.storeMediaInCookies.bind(this);
  }

  getChildContext() {
    return { sensor: this.sensor };
  }

  componentDidMount() {
    this.storeMediaInCookies();
    this.sensor.onMediaEnd(this.storeMediaInCookies);
  }

  storeMediaInCookies() {
    const { media } = this.sensor.getRuntime();
    const date = new Date();
    date.setDate(date.getDate() + (365 * 3));
    const string = JSON.stringify(keys(pickBy(media, it => it)));
    this.cookies.set('__matchMedia', string, { expires: date.toUTCString(), httpOnly: false });
  }

  render() {
    return Children.only(this.props.children);
  }
}

export default SensorProvider;
