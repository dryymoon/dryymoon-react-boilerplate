/**
 * Created by DryyMoon on 25.10.2016.
 */
import isFunction from 'lodash/isFunction';
import upperFirst from 'lodash/upperFirst';
import extend from 'lodash/extend';
import defer from 'lodash/defer';
import isString from 'lodash/isString';
import isNumber from 'lodash/isNumber';
import isObject from 'lodash/isObject';
import mapValues from 'lodash/mapValues';
import isBoolean from 'lodash/isBoolean';
import reCssMediaQuery from 'regex-css-media-query'; // eslint-disable-line
import isBrowser from '../../helpers/isBrowser';

const eventTypes = ['media', 'resize', 'zoom', 'scroll'];

const IMMEDIATE_EVENTS = eventTypes.map(e => `on${upperFirst(e)}`);
const SYNC_EVENTS = eventTypes.map(e => `on${upperFirst(e)}Sync`);
const START_EVENTS = eventTypes.map(e => `on${upperFirst(e)}Start`);
const END_EVENTS = eventTypes.map(e => `on${upperFirst(e)}End`);
const ALL_EVENTS = [
  ...SYNC_EVENTS,
  ...IMMEDIATE_EVENTS,
  ...START_EVENTS,
  ...END_EVENTS];

class Sensor {
  constructor() {
    this.handleScroll = this.handleScroll.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.handleMedia = this.handleMedia.bind(this);
    this.addListener = this.addListener.bind(this);
    this.removeListener = this.removeListener.bind(this);
    this.emit = this.emit.bind(this);
    this.emitRunner = this.emitRunner.bind(this);
    this.events = {};

    ALL_EVENTS.forEach((e) => {
      this.events[e] = {};
      this.events[e].listeners = [];
      this.events[e].fired = 0;

      this[e] = fn => this.addListener(e, fn);
      this[e.replace(/^on/, 'off')] = fn => this.removeListener(e, fn);
    });
    const that = this;

    SYNC_EVENTS.forEach((e) => {
      this.events[e].fn = () => {
        // that.events[e].raf = null;
        that.emitRunner(e);
      };
    });

    IMMEDIATE_EVENTS.forEach((eventName) => {
      const event = this.events[eventName];
      const startEvent = this.events[`${eventName}Start`];
      const endEvent = this.events[`${eventName}End`];

      startEvent.try = () => {
        if (event.awaitRafs) return;
        // console.warn(`${eventName}  Start`);
        startEvent.fired += 1;
      };

      event.try = () => {
        event.fired += 1;
        event.awaitRafs = 5;
      };

      let tickRunning = false;

      endEvent.try = () => {
        if (tickRunning) return;
        tickRunning = true;
        endEvent.tick();
      };

      endEvent.tick = () => {
        event.awaitRafs -= 1;
        if (event.awaitRafs) {
          return window.requestAnimationFrame(() => defer(endEvent.tick));
        }
        // console.warn(`${eventName}  End`);
        endEvent.fired += 1;
        tickRunning = false;
        this.emit();
      };

      endEvent.tick = endEvent.tick.bind(this);
    });

    this.runtime = { media: {} };
    const init = true;
    this.handleScroll({ init });
    this.handleResize({ init });

    if (isBrowser) {
      window.addEventListener('scroll', this.handleScroll, false);
      window.addEventListener('resize', this.handleResize, false);
    }
  }

  getRuntime() {
    return { ...this.runtime };
  }

  addMediaQueries(Obj) {
    if (!isObject(Obj)) return;

    this.events.onMedia.store = this.events.onMedia.store || {};

    if (__SERVER__) {
      mapValues(Obj, (serverValue, key) => {
        if (!isBoolean(serverValue)) return;
        this.runtime.media[key] = serverValue;
      });
    }

    if (__CLIENT__) {
      mapValues(Obj, (mediaQueryString, key) => {
        if (!reCssMediaQuery().test(mediaQueryString)) return;

        this.events.onMedia.store[key] = this.events.onMedia.store[key] || {};

        const evStore = this.events.onMedia.store[key];

        // maybe event exist and it is a same
        if (evStore && evStore.mediaQueryString === mediaQueryString) return;

        extend(evStore, { key, mediaQueryString });

        // maybe listener setted, remove it.
        if (evStore.matchMedia) {
          evStore.matchMedia.removeListener(evStore.handler);
          evStore.matchMedia = undefined;
        }

        const handler = () => this.handleMedia({ key });

        if (isString(mediaQueryString) || isNumber(mediaQueryString)) {
          const matchMedia = window.matchMedia(mediaQueryString);
          extend(evStore, { key, mediaQueryString, matchMedia, handler });
          matchMedia.addListener(handler);
        }

        // TODO Можут тут надо и дергать вызо всплывающего события
        this.handleMedia({ key, init: true });

        // handler();
      });
    }
  }

  addListener(type, fn) {
    if (!isFunction(fn)) return false;
    if (ALL_EVENTS.indexOf(type) === -1) return false;
    if (this.events[type].listeners.indexOf(fn) > 0) return false;
    this.events[type].listeners.push(fn);
  }

  removeListener(type, fn) {
    if (ALL_EVENTS.indexOf(type) === -1) return false;
    const index = this.events[type].listeners.indexOf(fn);
    if (index === -1) return true;
    this.events[type].listeners.splice(index, 1);
    this.removeListener(type, fn);
  }

  handleScroll({ init }) {
    const ev = 'onScroll';
    const { top, left } = getScrollPositionSync();
    this.runtime.scrollTop = top;
    this.runtime.scrollLeft = left;

    if (!init) this.handleEvent(ev);
  }

  handleResize({ init }) {
    const ev = 'onResize';

    this.runtime.scrollBarWidth = getScrollBarWidth();
    const { height, width } = getViewport();
    this.runtime.viewPortHeight = height;
    this.runtime.viewPortWidth = width;

    if (!init) this.handleEvent(ev);
  }

  handleMedia({ key, init }) {
    const ev = 'onMedia';
    this.runtime.media[key] = this.events[ev].store[key].matchMedia.matches;
    if (!init) this.handleEvent(ev);
  }

  emit() {
    if (this.emitting) return;
    this.emitting = true;
    ALL_EVENTS
      .filter((e) => {
        const isNotEmitting = !this.events[e].emitting;
        const isFired = this.events[e].fired > 0;
        const hasListeners = this.events[e].listeners.length > 0;
        return isFired && isNotEmitting && hasListeners;
      })
      .forEach((e) => {
        this.events[e].emitting = true;
        const that = this;
        /*
         * No Improvments with requestAnimationFrame;
         * setTimeout(() => requestAnimationFrame()(() => that.emitRunner(e)));
         */
        // setTimeout(() => requestAnimationFrame()(() => that.emitRunner(e)), 0);
        // setTimeout(() => that.emitRunner(e), 0);
        // window.requestAnimationFrame(() => defer(that.emitRunner, e));
        defer(that.emitRunner, e);
      });
    this.emitting = false;
  }

  emitRunner(e) {
    this.events[e].fired = 0;
    this.events[e].listeners.forEach(fn => fn({ ...this.runtime, event: e }));
    this.events[e].emitting = false;
  }

  handleEvent(ev) {
    // Sync Event
    const syncEvName = `${ev}Sync`;
    this.events[syncEvName].fn();

    // End Event
    this.events[`${ev}Start`].try();
    this.events[ev].try();
    this.events[`${ev}End`].try();
    this.emit();
  }
}

export default Sensor;

const doc = isBrowser ? document.documentElement : {};

function isTouchSupported() { // eslint-disable-line
  if (!isBrowser) return false;
  return 'ontouchstart' in window || navigator.msMaxTouchPoints;
}

export function getScrollPositionSync() {
  if (!isBrowser) return { left: 0, top: 0 };
  const left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
  const top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
  return { left, top };
}

export function getViewport() {
  if (!isBrowser) return { width: 0, height: 0 };
  // the more standards compliant browsers (mozilla/netscape/opera/IE7)
  // use window.innerWidth and window.innerHeight
  /* if (typeof window.innerWidth !== 'undefined') {
   return {
   width: window.innerWidth,
   height: window.innerHeight
   };
   } */

  // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
  if (typeof document.documentElement !== 'undefined'
    && typeof document.documentElement.clientWidth !== 'undefined'
    && document.documentElement.clientWidth !== 0) {
    return {
      width: document.documentElement.clientWidth,
      height: document.documentElement.clientHeight
    };
  }

  // older versions of IE
  return {
    width: document.getElementsByTagName('body')[0].clientWidth,
    height: document.getElementsByTagName('body')[0].clientHeight
  };
}

const widthContainers = {};

function getScrollBarWidth() {
  if (!isBrowser) return 0;
  widthContainers.inner = widthContainers.inner || document.getElementById('innerContainerOfGetScrollBarWidth');
  widthContainers.outer = widthContainers.outer || document.getElementById('outerContainerOfGetScrollBarWidth');

  let { inner, outer } = widthContainers;

  if (!inner) {
    inner = document.createElement('p');
    inner.style.width = '100%';
    inner.style.height = '200px';
    inner.id = 'innerContainerOfGetScrollBarWidth';
    extend(widthContainers, { inner });
  }
  if (!outer) {
    outer = document.createElement('div');
    outer.style.position = 'absolute';
    outer.style.top = '-300px';
    outer.style.left = '-300px';
    outer.style.visibility = 'hidden';
    outer.style.width = '200px';
    outer.style.height = '150px';
    outer.style.overflow = 'hidden';
    outer.style.zIndex = '-9999';
    outer.appendChild(inner);
    outer.id = 'outerContainerOfGetScrollBarWidth';
    document.body.appendChild(outer);
    extend(widthContainers, { outer });
  }
  const w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  let w2 = inner.offsetWidth;
  if (w1 === w2) w2 = outer.clientWidth;

  return (w1 - w2);
}

export const allEvents = ALL_EVENTS;


if (__CLIENT__) {
  (function requestAnimationFrameShim() {
    // shim layer with setTimeout fallback
    if (window.requestAnimationFrame) return;
    window.requestAnimationFrame =
      window.webkitRequestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      function requestAnimationFrameLegacy(callback) {
        window.setTimeout(callback, 1000 / 60);
      };
  }());
}
