/**
 * Created by PIV on 22.06.2016.
 */
import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import bem from 'react-bem-mod';
import ccn from '../../helpers/styles/CollectClasses';

const colsCss = require('./cols.scss');

/**
 * Cols
 * Компонент правильно создает колонки, перебирает чилдов
 */
@ccn
@bem
class Cols extends Component {
  static propTypes = {
    /**
     * Каждый элемент станет колонкой
     */
    children: PropTypes.node
  };

  render() {
    return (
      <div data-block="cols" data-bemstyles={colsCss}>
        <div data-elem="row">
          {Children.map(this.props.children, (it, key) => {
            if (!it) return null;
            return cloneElement(it, { key, 'data-elem': 'col' });
          })}
        </div>
      </div>
    );
  }
}
export default Cols;
