/**
 * Created by DryyMoon on 28.10.2016.
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';
import Container from 'components/container';

import styles from './head-menu.scss';

@ccn
@bem
class HeadMenu extends Component {
  static propTypes = {
    height: PropTypes.integer,
    body: PropTypes.bool,
    children: PropTypes.node,
    semiTransparent: PropTypes.bool
  };

  render() {
    const { semiTransparent, children, body, height } = this.props;
    return (
      <Container
        data-block="headMenu"
        data-bemstyles={styles}
        data-mods={{ semiTransparent }}
        style={{ height }}
      >
        <Container data-elem="container" {...{ body, children }} style={{ height }} />
        <div data-elem="scrollGetter" />
      </Container>
    );
  }
}

export default HeadMenu;

