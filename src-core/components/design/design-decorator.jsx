import React, { Component } from 'react';
import bem from 'bem';
import ccn from 'ccn';
import sensor from 'components/sensor';
import Button from 'components/button';
import FlexBox from 'components/flexbox';
import Helmet from 'react-helmet';

import styles from './design-decorator.scss';

// let browserImageSize = null;
// if (__CLIENT__) browserImageSize = require('browser-image-size');

@sensor
@ccn
@bem
export default function designConfigurer(designs) {
  return function designDecorator(BaseComponent) {
    if (!__DEV_CLI__) return BaseComponent;
    return class ComponentWithDesign extends Component {
  constructor(...args) {
    super(...args);
    this.state = {};
    this.setActive = this.setActive.bind(this);
    this.setPassive = this.setPassive.bind(this);
  }

  componentDidMount() {
    const maybeDesignKey = window.sessionStorage.getItem('__designImageKey');
    if (maybeDesignKey && designs[maybeDesignKey]) {
      // eslint-disable-next-line
          this.setDesignKey(maybeDesignKey);
    }
    this.dom = document.getElementById('designImgContainer');
    document.addEventListener('keydown', this.setActive);
    document.addEventListener('keyup', this.setPassive);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.setActive);
    document.removeEventListener('keyup', this.setPassive);
  }

  onResizeEnd() {
    this.forceUpdate();
  }

  setActive(e) {
    const code = e.keyCode ? e.keyCode : e.which;
    if (code !== 17) return;
    this.dom.classList.add('active');
  }

  setPassive(e) {
    const code = e.keyCode ? e.keyCode : e.which;
    if (code !== 17) return;
    this.dom.classList.remove('active');
  }

  setDesignKey(key) {
    // const existImg = key && designs[key];
    this.setState({ designImageKey: key });
    window.sessionStorage.setItem('__designImageKey', key);
    /* if (existImg) browserImageSize(designs[key])
         .then(({ width }) => this.setState({ imgWidth: width }))
         .catch(() => null); */
  }

  render() {
    const { viewPortHeight, viewPortWidth } = this.sensor.getRuntime();
    const { designImageKey } = this.state;

    return (
      <div
        data-block="designHelper"
        data-bemstyles={styles}
        id="designImgContainer"
      >
        <Helmet >
          {designImageKey && <style type="text/css">{`
            body {
              width: ${designImageKey}px;
              margin: 0 auto;
              overflow-x: visible;
              overflow-y: visible;
            }

            #${__ROOT_DOM_POINT__} {
              width: ${designImageKey}px;
            }
          `}
          </style>}
        </Helmet>
        {designImageKey &&
        <FlexBox data-elem="imgContainer" style={{ width: `${designImageKey}px` }}>
          <div data-elem="imgWrapper" style={{ width: `${designImageKey}px` }}>
            <img src={designs[designImageKey]} alt="" data-elem="img" />
          </div>
        </FlexBox>
        }
        <div data-elem="switchContainer">
          <span>{viewPortWidth} X {viewPortHeight}</span>
          {Object.keys(designs).map(key => (
            <Button
              data-elem="switchContainerBtn"
              data-mods={{ active: designImageKey === key }}
              onClick={() => this.setDesignKey(key)}
              key={key}>
              <span >{key}</span>
            </Button>
          ))}
          <Button onClick={() => this.setDesignKey()}>
            <span>X</span>
          </Button>
        </div>
        <BaseComponent {...this.props} />
      </div>
    );
  }
};
  };
}
