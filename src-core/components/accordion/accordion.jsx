/**
 * Created by SLK on 21.03.2017.
 */
import React, { Component, cloneElement, createElement } from 'react';
import PropTypes from 'prop-types';
import objectPath from 'object-path';
import bem from 'bem';
import ccn from 'ccn';
import Button from 'components/button';

import styles from './accordion.scss';

@ccn
@bem
class Accordion extends Component {
  static propTypes = {
    // panelTopic: PropTypes.string.isRequired,
    children: PropTypes.node,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      active: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      active: !this.state.active
    });
  }

  render() {
    const { children } = this.props;

    let headerEl;
    let contentEl;

    if (Array.isArray(children)) {
      if (objectPath.get(children, '0.props.panel')) {
        headerEl = children[0];
        // eslint-disable-next-line no-unused-vars
        const { panel, ...restHeadProps } = headerEl.props;
        headerEl = createElement(headerEl.type, { ...restHeadProps, 'data-elem': 'panel' });
        contentEl = children[1];
      } else {
        contentEl = children[0];
      }
    } else {
      contentEl = children;
    }

    return (
      <div data-block="accordion" data-bemstyles={styles}>
        <Button data-elem="panelContainer" onClick={this.toggle}>
          {headerEl}
        </Button>
        <div data-elem="contentContainer" data-mods={this.state.active ? 'active' : 'passive'}>
          {cloneElement(contentEl, { 'data-elem': 'content' })}
        </div>
      </div>
    );
  }
}

export default Accordion;
