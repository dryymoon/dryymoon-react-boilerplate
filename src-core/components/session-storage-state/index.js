/**
 * Created by dryymoon on 11.09.17.
 */
import isBrowser from 'isBrowser';

export default function configurate(key) {
  return function decorate(Component) {
    return class SessionSavedState extends Component {
      constructor(...args) {
        super(...args);
        if (!isBrowser) return;
        if (!window.sessionStorage) return;
        if (this.state) {
          try {
            const prevState = JSON.parse(sessionStorage.getItem(key));
            this.state = { ...this.state, ...prevState };
          } catch (e) {
            return undefined;
          }
        }
      }

      componentWillUnmount() {
        const { getStateToSave } = this;
        let { state } = this;
        if (state) {
          if (getStateToSave) state = getStateToSave(state);
          sessionStorage.setItem(key, JSON.stringify(state));
        }
        if (super.componentWillUnmount) super.componentWillUnmount();
      }
    };
  };
}
