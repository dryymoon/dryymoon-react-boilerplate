/**
 * Created by Vit on 11.07.2016.
 */
import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import ccn from 'ccn';
import bem from 'bem';

const mediaColsCss = require('./media-cols.scss');

@ccn
@bem
class MediaCols extends Component {
  static propTypes = {
    children: PropTypes.node,
    column: PropTypes.bool
  };
  render() {
    const { column } = this.props;
    const mods = { column };
    const child =
      Children.map(this.props.children, (it, index) => {
        if (!it) return false;
        return cloneElement(it, { 'data-elem': 'col', key: index });
      });
    return (
      <div data-block="mediaCols" data-mods={mods} data-bemstyles={mediaColsCss} >
        {child}
      </div>
    );
  }
}
export default MediaCols;
