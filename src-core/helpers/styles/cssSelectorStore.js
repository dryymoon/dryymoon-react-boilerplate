/**
 * Created by dryymoon on 22.04.2018.
 */
import mapKeys from 'lodash/mapKeys';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';
import isNumber from 'lodash/isNumber';

export default class CssSelectorStore {
  constructor() {
    this.store = {};
    this.push = this.push.bind(this);
  }

  updateSelector(selector, value = 0) {
    this.store[selector] = this.store[selector] || 0;
    this.store[selector] += value;
  }

  push(some) {
    if (typeof some === 'string') return this.updateSelector(some, 1);
    if (isArray(some)) return some.forEach(this.push);
    if (isObject(some)) {
      mapKeys(some, (val, key) => isNumber(val) && (val > 0) && this.updateSelector(key, val));
    }
  }

  get() {
    return this.store;
  }
}
