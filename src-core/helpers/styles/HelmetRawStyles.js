/**
 * Created by PIV on 07.10.2016.
 */

export default function (helmetHeadData) {
  const styles = helmetHeadData.style.toComponent();

  const res = styles.map((it) => {
    const rawStyle = it.props.dangerouslySetInnerHTML.__html;
    return rawStyle;
  });
  return res.join('\n');
}
