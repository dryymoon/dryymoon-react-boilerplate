/**
 * Created by PIV on 22.06.2016.
 */
// import cx from 'classnames';
import { Component } from 'react'; // cloneElement,
import PropTypes from 'prop-types';
import getDisplayName from 'react-display-name';
import extend from 'lodash/extend';
import isBrowser from './../isBrowser';
/* const isBrowser =
 !!(typeof window !== 'undefined' && window.document && window.document.createElement); */

function pushClass(s, fn) {
  // eslint-disable-next-line no-useless-escape
  const selector = s.test('^.') ? s : `.${s}`;
  return s && fn && push(selector, fn);
}
function pushId(s, fn) {
  const selector = s.test('^#') ? s : `#${s}`;
  return s && fn && push(selector, fn);
}
function pushTag(tag, fn) {
  return tag && fn && fn(tag);
}

function push(selector, fn) {
  return selector && fn && fn(selector);
}

function factory() { // { propName = 'classNames' }
  return (InnerComponent) => {
    if (isBrowser) return InnerComponent;

    const convertToClassName = (element, pushFn) => {
      if (Array.isArray(element)) {
        return element.map(it => (it ? convertToClassName(it, pushFn) : it));
      }
      if (element && element.props) {
        /* const props = {};
         let className = '';
         if (element.props[propName]) {
         className = cx(element.props[propName]);
         }

         if (element.props.className) {
         className = `${element.props.className} ${className}`;
         }

         if (className) {
         props.className = className.trim();
         }
         */
        if (!isBrowser && element.props.className) {
          element.props.className.split(/\s+/)
            .map(cl => `.${cl}`)
            .forEach(cl => push(cl, pushFn));
        }

        if (!isBrowser && element.props.id) push(`#${element.props.id}`, pushFn);

        if (!isBrowser && typeof element.type === 'string') push(element.type, pushFn);

        /* if (element.props.children) {
         props.children = Array.isArray(element.props.children) ?
         element.props.children.map(child => convertToClassName(child, pushFn)) :
         convertToClassName(element.props.children, pushFn);
         } */

        if (element.props.children && Array.isArray(element.props.children)) {
          element.props.children.forEach(child => convertToClassName(child, pushFn));
        } else if (element.props.children) {
          convertToClassName(element.props.children, pushFn);
        }

        return element;
        // return cloneElement(element, props);
      }

      return element;
    };

    return class WrappedComponentWithSelectorCollector extends InnerComponent {
      // static displayName = `SelectorCollector(${getDisplayName(InnerComponent)})`;
      static displayName = getDisplayName(InnerComponent);

      static contextTypes = extend(
        InnerComponent.contextTypes || {},
        { pushRuntimeCssSelector: PropTypes.func }
      );

      pushRuntimeCssSelector(name) {
        if (this.context.pushRuntimeCssSelector) {
          this.context.pushRuntimeCssSelector(name);
        }
        if (!this.usedStyleSelectors[name]) this.usedStyleSelectors[name] = 0;
        return this.usedStyleSelectors[name] += 1;
      }

      constructor(props, context) {
        super(props, context);
        // const fn = context.pushRuntimeCssSelector;
        // this.externalPushRuntimeCssSelector = context.pushRuntimeCssSelector;
        // this.collectSelectorsFn = this.collectSelectorsFn.bind(this);
        this.pushRuntimeCssSelector = this.pushRuntimeCssSelector.bind(this);
        this.usedStyleSelectors = {};
        const fn = this.pushRuntimeCssSelector;
        if (InnerComponent.selectors && Array.isArray(InnerComponent.selectors) && fn) {
          InnerComponent.selectors.forEach(sel => push(sel, fn));
        }
        if (InnerComponent.selectors && (typeof InnerComponent.selectors === 'object')) {
          (InnerComponent.selectors.classes || []).forEach(it => pushClass(it, fn));
          (InnerComponent.selectors.classes || []).forEach(it => pushId(it, fn));
          (InnerComponent.selectors.tags || []).forEach(it => pushTag(it, fn));
        }
        if (InnerComponent.selectors && (typeof InnerComponent.selectors === 'string')) {
          push(InnerComponent.selectors, fn);
        }
      }

      /* collectSelectorsFn(selector) {
       const extFn = this.context.pushRuntimeCssSelector;
       if (extFn) {
       this.collectedStyleTags.push(selector);
       extFn(selector);
       }
       } */

      render(...args) {
        const self = this;
        const rendered = convertToClassName(super.render(...args), self.pushRuntimeCssSelector);
        /* if(this.context.pushRuntimeCssSelector){
         this.context.pushRuntimeCssSelector(name);
         } */
        return rendered;
      }
    };
  };
}

class RuntimeCssSelectors extends Component {
  static propTypes = {
    children: PropTypes.node,
    // store: PropTypes.objectOf(PropTypes.number),
    pushSelector: PropTypes.func.isRequired,
    // PropTypes.object.isRequired // eslint-disable-line
  };

  static childContextTypes = { pushRuntimeCssSelector: PropTypes.func };

  constructor(props, context) {
    super(props, context);
    props.pushSelector(['html', 'body']);
    // this.pushRuntimeCssSelector(['html', 'body']);
  }

  getChildContext() {
    // this.pushRuntimeCssSelector = this.pushRuntimeCssSelector.bind(this);
    return { pushRuntimeCssSelector: this.props.pushSelector };
  }

  /* pushRuntimeCssSelector(nameOrObj) {
    if (typeof nameOrObj === 'string') {
      this.props.store[nameOrObj] = this.props.store[nameOrObj] || 0;
      this.props.store[nameOrObj] += 1;
    } else if (Array.isArray(nameOrObj)) {
      nameOrObj.forEach(it => this.pushRuntimeCssSelector(it));
    } else if (typeof nameOrObj === 'object') {
      Object
        .keys(nameOrObj)
        .forEach((key) => {
          this.props.store[key] = this.props.store[key] || 0;
          this.props.store[key] += nameOrObj[key];
        });
    }
  } */

  render() {
    return this.props.children;
  }
}

export default factory({});
export { factory, RuntimeCssSelectors };

