/**
 * Created by PIV on 23.06.2016.
 */
import cacheLib from 'lru-cache';
import fs from 'fs';
import postcss from 'postcss';
import selectorTokenizer from 'css-selector-tokenizer';
import assign from 'lodash/assign';
import values from 'lodash/values';
import isEmpty from 'lodash/isEmpty';
import keys from 'lodash/keys';
import pick from 'lodash/pick';
import sha512 from 'sha512';

const ast = postcss.root();
const parsedFiles = [];
// TODO Improve cache manege to prevent memory leaks
const compiledCache = cacheLib({ size: 1000, maxElements: 10000, maxAge: 60 * 60 * 1000 });

export function setMultipleCss(urlsObj = {}) {
  Object.keys(urlsObj)
    .map(key => setCss(urlsObj[key]));
  //  return Promise.all(PromisesArr);
}

export function setCss(cssFilePath) {
  // TODO maybe slowly in Prod soso
  try {
    if (parsedFiles.indexOf(cssFilePath) > -1) return;
    parsedFiles.push(cssFilePath);
    const stylesheet = fs
      .readFileSync(cssFilePath, 'utf-8')
      .replace(/\/\*#\s+sourceMappingURL=.+\*\//, '');
    const parsed = postcss.parse(stylesheet, { map: { inline: false, prev: false, sourcesContent: false, annotation: false } });
    ast.append(parsed);
    parseSelectors();
  } catch (e) {
    console.error('CSS PARSE FAIL: ', cssFilePath, e);
  }
}

let fullMapper = {};

function parseSelectors() {
  fullMapper = {};
  let index = 0;
  ast.walkRules((rule) => {
    const { selectors } = rule;
    selectors.forEach((it) => {
      try {
        const { nodes: [{ nodes: tokens }] } = selectorTokenizer.parse(it);
        tokens.forEach(({ type, name }) => {
          // if (type === 'element') parsedSelectors.push(name);
          // if (type === 'id') parsedSelectors.push(`#${name}`);
          // if (type === 'class') parsedSelectors.push(`.${name}`);

          let selector;
          if (type === 'element') selector = name; // set(fullMapper, [name, index], true);
          if (type === 'id') selector = `#${name}`;
          if (type === 'class') selector = `.${name}`;

          if (selector) {
            fullMapper[selector] = fullMapper[selector] || {};
            fullMapper[selector][index] = true;
          }
        });
      } catch (e) {
        console.log('CANT PARSE selector:', it, e);
      }
    });
    index += 1;
  });
}

/*
 $selectorTokens = [
 { type: "element", name: "a" },
 { type: "id", name: "content" },
 { type: "class", name: "active" },
 { type: "operator", operator: ">", before: " ", after: " " },
 { type: "element", name: "div" },
 { type: "pseudo-element", name: "first-line" },
 { type: "spacing", value: " " },
 { type: "attribute", con1tent: "data-content" },
 ]
 */
let debug = false;
if (__DEVELOPMENT__) debug = true;

export function getInlineStyles(usedSelectorsNotAdapted = {}) { // {div: 7, .class:8, #id:3}

  const usedSelectors = { html: 1, body: 1, svg: 1, ...usedSelectorsNotAdapted };
  if (debug) console.time('css_hash');
  const hash = sha512(JSON.stringify(Object.keys(usedSelectors).sort())).toString('hex');
  if (debug) console.timeEnd('css_hash');
  const cachedStyle = compiledCache.get(hash);

  if (cachedStyle) return cachedStyle;
  if (debug) console.time('css_clone');
  const clonedAst = ast.clone(); // cloneDeep(ast);
  if (debug) console.timeEnd('css_clone');
  if (debug) console.time('css_pick');
  const mapper = pick(fullMapper, keys(usedSelectors)); // {div: {1: true, 3: true}, }
  if (debug) console.timeEnd('css_pick');
  if (debug) console.time('css_assign');
  const mapping = assign({}, ...values(mapper));
  if (debug) console.timeEnd('css_assign');
  let index = 0;
  if (debug) console.time('css_walkRules');
  clonedAst.walkRules((rule) => {
    if (!mapping[index]) rule.parent.removeChild(rule);
    index += 1;
  });
  if (debug) console.timeEnd('css_walkRules');
  if (debug) console.time('css_walkAtRules');
  clonedAst.walkAtRules((rule) => {
    if (['charset', 'import', 'keyframes'].indexOf(rule.name) > -1) return;

    if (!isEmpty(rule.nodes)) return;
    // const removeByDefault = ['font-face', 'charset', 'import', 'keyframes'].indexOf(rule.name) >= 0;

    rule.parent.removeChild(rule);
  });
  if (debug) console.timeEnd('css_walkAtRules');

  if (debug) console.time('css_toString');
  const css = clonedAst.toString().replace(/\n/g, '');
  // let css = '';
  // postcss.stringify(clonedAst, part => (css += part));
  if (debug) console.timeEnd('css_toString');

  compiledCache.set(hash, css);

  return css;
}

export default function (urls = [], selectors = {}) {
  setMultipleCss(urls);
  return getInlineStyles(selectors);
}

// TODO: Collect used fonts, and add it to css
// https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/webfont-optimization
