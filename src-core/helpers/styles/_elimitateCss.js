/**
 * Created by PIV on 23.06.2016.
 */
import cacheLib from 'lru-cache';
import fs from 'fs';
import slick from 'slick';
import css from 'css';
import reduce from 'lodash/reduce';
import result from 'lodash/result';
import isFunction from 'lodash/isFunction';
import isRegExp from 'lodash/isRegExp';
import size from 'lodash/size';
import reject from 'lodash/reject';
// import sha1 from 'string-hash';
import sha512 from 'sha512';

const extend = require('util')._extend;

const astCache = {};
// TODO Improve cache manege to prevent memory leaks
const compiledCache = cacheLib({ size: 1000, maxElements: 10000, maxAge: 60 * 60 * 1000 });

export function setMultipleCss(urlsObj = {}) {
  Object.keys(urlsObj)
    .map(key => setCss(urlsObj[key]));
  //  return Promise.all(PromisesArr);
}

export function setCss(cssFilePath) {
  // TODO maybe slowly in Prod soso
  try {
    if (astCache[cssFilePath]) return;
    // const assetPath = path.join(projectConfig.assetsPath,
    // url.slice(projectConfig.assetsUrl.length));
    const stylesheet = fs.readFileSync(cssFilePath, 'utf-8');
    astCache[cssFilePath] = css.parse(stylesheet);
  } catch (e) {
    console.log('CSS PARSE FAIL: ', cssFilePath, e);
  }
}

function reduceRules(ignoreFn) {
  const matcher = (identifier, node) => element => !!ignoreFn(identifier, element, node || element);

  return function reducer(rules, rule) {
    // check if whole type is ignored
    if (matcher('type', rule)(`@${rule.type}`)) {
      return rules;
    }

    // check for ignores in media queries
    if (rule.type === 'media') {
      const childRules = reduce(rule.rules || [], reducer, []);
      if (!size(childRules)) return;
      const newRule = { ...rule, rules: childRules };
      if (size(childRules)) return [...rules, newRule];
    }

    if (rule.type === 'rule') {
      // check selector
      // eslint-disable-next-line no-param-reassign
      const filteredSelectors = reject(rule.selectors || [], matcher('selector', rule));
      const newRule = { ...rule, selectors: filteredSelectors };
      return [...rules, newRule]; // TODO Надо чтот с этим сделать, тут таког оне было
    }

    return [...rules, rule];
  };
}

function cssRuleItemFilterFn(usedSelectors) {
  const REMOVE = true;
  const PASS = false;
  return function SelectorMatcher(context, value) { // , node
    // Remove comments
    // console.log('\n\nparsedSelectors', context, value);

    if (context === 'type' && value === '@comment') return REMOVE;

    // Parse only selectors
    if (context !== 'selector') return PASS;
    if (usedSelectors[value]) return PASS;
    const parsedSelectors = slick.parse(value)[0];
    const deepMatchedSelector = Object
      .keys(parsedSelectors)
      .some((key) => {
        const selector = parsedSelectors[key];
        if (selector.tag === '*' && !selector.id && !selector.classList) return true;
        if (selector.id && usedSelectors[`#${selector.id}`]) return true;
        if (selector.tag && usedSelectors[selector.tag]) return true;
        if (selector.classList && selector.classList.some(cl => !!usedSelectors[`.${cl}`])) return true;
        return false;
      });
    if (deepMatchedSelector) return PASS;
    return REMOVE;
  };
}

export function getInlineStyles(usedSelectors = {}) { // {div: 7, .class:8, #id:3}
  const hash = sha512(JSON.stringify(Object.keys(usedSelectors).sort())).toString('hex');

  const cachedStyle = compiledCache.get(hash);

  // console.log('Is Exist cahce', hash,' => ', !!cachedStyle);

  if (cachedStyle) return cachedStyle;

  const ignoreFn = cssRuleItemFilterFn(usedSelectors);
  const style = Object
    .keys(astCache)
    .map((key) => {
      const sheet = extend({}, astCache[key]);
      // TODO: Них. не работает при втором рендере и дальше,
      // портится astCache, нужно переписать фильтрация, чтоб в кеш не лазила....
      sheet.stylesheet.rules = reduce(sheet.stylesheet.rules, reduceRules(ignoreFn), []);
      /* const newRules = sheet.stylesheet.rules
        .map(rule => checkRule({ ignoreFn, rule }))
        .filter(it => it); */
      return css.stringify(sheet, { compress: false, sourcemap: false, inputSourcemaps: false });
    })
    .join(' ');
  compiledCache.set(hash, style);
  return style;
}

export default function (urls = [], selectors = {}) {
  setMultipleCss(urls);
  return getInlineStyles(selectors);
}

// TODO: Collect used fonts, and add it to css
// https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/webfont-optimization
