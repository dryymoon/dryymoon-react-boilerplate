/**
 * Created by dryymoon on 18.04.2018.
 */
import isRegExp from 'lodash/isRegExp';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import flattenDeep from 'lodash/flattenDeep';
import values from 'lodash/values';
import find from 'lodash/find';
import mapValues from 'lodash/mapValues';
import sortBy from 'lodash/sortBy';
import url from 'url';
import * as vhostRaw from '../../src-sites/*/vhost.js';

function sitePreProcessor({ vhost, folder }) {
  if (isRegExp(vhost)) return { $vhostMatch: { regexp: new RegExp(vhost) }, vhost, folder };
  if (isArray(vhost)) {
    return vhost.map(it => sitePreProcessor({ vhost: it, folder }));
  }
  if (isString(vhost)) {
    const maybeProtoHost = vhost.split('//');
    let protocol;
    let host;
    if (maybeProtoHost[2]) throw new Error('Some fackup in vhosts due to multiple "://"');
    if (maybeProtoHost[1]) {
      protocol = maybeProtoHost[0].replace(':', '');
      host = maybeProtoHost[1]; // eslint-disable-line prefer-destructuring
    } else {
      host = maybeProtoHost[0].replace('/', '');
    }
    host = host.replace('/', '');
    if (protocol && host) {
      const origin = url.format({ protocol, host });
      return { $vhostMatch: { origin }, vhost, folder };
    }
    if (host) return { $vhostMatch: { host }, vhost, folder };
  }
}
// let sites = pickBy(vhostRaw, isObject);
let sitesCombine = mapValues(vhostRaw, (vhost, folder) => ({ vhost, folder }));

sitesCombine = values(sitesCombine);

sitesCombine = sitesCombine.map(sitePreProcessor);
sitesCombine = flattenDeep(sitesCombine);
sitesCombine = sortBy(sitesCombine, ({ $vhostMatch: { host } }) => host === '*'); // mode to last

const sites = sitesCombine.filter(it => it);

export default function ({ req: serverReq } = {}) {
  let origin;
  let host;
  if (__CLIENT__) {
    origin = window.location.origin; // eslint-disable-line prefer-destructuring
    host = window.location.host; // eslint-disable-line prefer-destructuring
  }
  if (__SERVER__) {
    // let protocol = 'http';
    // if (serverReq.connection.encrypted) protocol = 'https';
    // TODO req.headers['x-forwarded-proto']
    origin = `${serverReq.protocol}://${serverReq.get('host')}`;
    host = serverReq.get('host');
  }

  return find(sites, ({ $vhostMatch }) => {
    if ($vhostMatch.regexp) return $vhostMatch.regexp.test(origin);
    if ($vhostMatch.origin) return $vhostMatch.origin === origin;
    if ($vhostMatch.host === '*') return true;
    if ($vhostMatch.host) return $vhostMatch.host === host;
  });
}
