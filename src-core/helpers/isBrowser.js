/**
 * Created by DryyMoon on 31.10.2016.
 */
const isBrowser =
  !!(typeof window !== 'undefined' && window.document && window.document.createElement);

export default isBrowser;
