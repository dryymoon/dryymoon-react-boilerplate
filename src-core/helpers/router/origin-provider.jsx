/**
 * Created by dryymoon on 11.04.17.
 */
import { Component } from 'react';
import PropTypes from 'prop-types';
import isBrowser from '../isBrowser';

export const originShape = {
  hostname: PropTypes.string,
  port: PropTypes.number,
  protocol: PropTypes.string,
};

export default class OriginProvider extends Component {
  static propTypes = {
    children: PropTypes.node,
    serverReq: PropTypes.object,// eslint-disable-line
  };

  static childContextTypes = originShape;

  constructor(props) {
    super(props);

    if (__CLIENT__ && isBrowser) {
      this.hostname = window.location.hostname;
      this.port = +window.location.port;
      this.protocol = window.location.protocol;
    }

    if (__SERVER__) {
      const req = props.serverReq;
      const proto = req.protocol;
      let port = req.get('Host').split(':')[1];
      if (!port && proto === 'http') port = 80;
      if (!port && proto === 'https') port = 443;
      this.hostname = req.hostname;
      this.port = +port;
      this.protocol = `${proto}:`;
    }
  }

  getChildContext() {
    const { hostname, port, protocol } = this;
    return { hostname, port, protocol };
  }

  render() {
    return this.props.children;
  }
}
