/**
 * Created by dryymoon on 11.04.17.
 */
/* eslint-disable camelcase */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import urlLib from 'url';
import urlJoinLib from 'url-join';
import startsWith from 'lodash/startsWith';
import { TooltipContainer } from 'components/tooltip';
import { Provider as PopUpProvider } from 'components/pop-up'; //  clear as clearPopups
import Notification from 'components/notification'; // , { removeAll as clearNotifications }

export const outsideDefaultRoute = {
  path: '*',
  // We added "callBack", because we need to stot to render content of * route
  // (link from spa to mololit)
  // eslint-disable-next-line
  onEnter({ location: { pathname, query, hash } }, replace, callBack) {
    if (__SERVER__) throw new Error('Outside routing you cant use on server');

    const key = '__from-spa-redirect';
    if (query[key]) {
      // eslint-disable-next-line
      return console.error('Need 404 redirect, die to spa dont know anithing about this link');
    }

    const url = urlLib.format({ pathname, query: { ...query, [key]: true }, hash });

    // browserHistory.replace(url);
    console.info('GO FROM SPA TO MONOLITH', url); // eslint-disable-line
    return document.location.replace(url);
    // return document.location.assign(url);
    // return document.location.reload(true);
  }
};

export const navigateHelperShape = {
  // routeParams: PropTypes.objectOf(PropTypes.string),
  namedRoutes: PropTypes.objectOf(PropTypes.string),
  location: PropTypes.object, // eslint-disable-line
  setDefaultLocationParams: PropTypes.func,
};

// @connect(null, { clearNotifications })
@withRouter
export default class NavigateHelper extends Component {
  static propTypes = {
    children: PropTypes.node,
    params: PropTypes.objectOf(PropTypes.string),
    location: PropTypes.object, // eslint-disable-line
    routes: PropTypes.array,  // eslint-disable-line
    route: PropTypes.object,  // eslint-disable-line
    // history: PropTypes.object.isRequired, //eslint-disable-line
    // clearNotifications: PropTypes.func,
  };

  static childContextTypes = navigateHelperShape;

  constructor(props, context) {
    super(props, context);
    this.buildRoutes = this.buildRoutes.bind(this);
    this.namedRoutes = {};
    this.defaultLocationParams = {};
    this.buildRoutes(props.route);
    this.buildSelfRote(props);
    this.setDefaultLocationParams = this.setDefaultLocationParams.bind(this);
    /* this.lastLocation = null;
     const that = this;
     context.router.history.listen(({ pathname }) => {
     if (that.lastLocation && (that.lastLocation !== pathname)) {
     clearPopups();
     props.clearNotifications();
     }
     that.lastLocation = pathname;
     }); */
  }

  getChildContext() {
    return {
      // routeParams: this.props.params,
      namedRoutes: this.namedRoutes,
      location: {
        ...this.props.location,
        params: this.props.params,
      },
    };
  }

  // TODO Maybe need shoud component update check
  UNSAFE_componentWillMount() {
    this.buildSelfRote();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.buildSelfRote(nextProps);
  }

  setDefaultLocationParams(defaultLocationParams) {
    // if (isMatch(this.defaultLocationParams, defaultLocationParams)) return;
    // console.log('setDefaultLocationParams');
    this.defaultLocationParams = { ...this.defaultLocationParams, ...defaultLocationParams };
    // this.forceUpdate();
  }

  buildSelfRote({ routes } = this.props) {
    const currPaths = routes
      .filter(it => it)
      .map(({ path }) => path)
      .filter(it => it);
    this.namedRoutes.self = this.buildUrl(currPaths);
    // .replace('//', '/');
  }

  buildRoutes({ childRoutes = [], path: currPath, virtualPath, name } = {}, parentPaths = []) {
    const paths = [...parentPaths];
    if (currPath) paths.push(currPath);
    if (virtualPath) paths.push(virtualPath);
    if (name) {
      const pathString = this.buildUrl(paths);
      // urlJoin(path).replace('//', '/');
      if (__DEVELOPMENT__ && this.namedRoutes[name]) {
        // eslint-disable-next-line
        console.error('DUPLICATED named LINK, existed:', this.namedRoutes[name], '; NEW:', pathString);
      }
      this.namedRoutes[name] = pathString;
    }
    childRoutes.map(it => this.buildRoutes(it, paths));
  }

  buildUrl(paths) {
    return paths.reduce((acc, it) => {
      if (it[0] === '/') return it;
      if (startsWith(it, 'http://') || startsWith(it, 'https://')) return it;
      return urlJoinLib(acc, it);
    });
  }

  render() {
    return (
      <Fragment>
        <TooltipContainer />
        <PopUpProvider />
        <Notification />
        {this.props.children}
      </Fragment>
    );
  }
}
