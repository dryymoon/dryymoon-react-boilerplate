const SESSIONSTORAGE_PREFIX = 'BackForwardHistory_';

class BackForwardHistory {
  constructor() {
    this.loadStateFromSession();
  }

  listenTo(browserHistory) {
    this.browserHistory = browserHistory;
    this.onHistoryEvent(browserHistory.getCurrentLocation());
    browserHistory.listen(this.onHistoryEvent.bind(this));
  }

  get firstLocationKey() {
    return this.historyStack[0].key;
  }

  get currentLocationKey() {
    return this.historyStack[this.currentIndex].key;
  }

  canGoBack(n = 1) {
    return this.currentIndex >= n;
  }

  canGoForward(n = 1) {
    return this.currentIndex < (this.historyStack.length - n);
  }

  get currentlyOnFirstPage() {
    return this.currentLocationKey === this.firstLocationKey;
  }

  get currentlyOnLastPage() {
    return !this.canGoForward();
  }

  loadStateFromSession() {
    this.historyStack = JSON.parse(sessionStorage.getItem(`${SESSIONSTORAGE_PREFIX}historyStack`)) || [];
    this.currentIndex = sessionStorage.getItem(`${SESSIONSTORAGE_PREFIX}currentIndex`) || 0;
    this.tags = JSON.parse(sessionStorage.getItem(`${SESSIONSTORAGE_PREFIX}tags`)) || {};
  }

  saveStateToSession() {
    sessionStorage.setItem(`${SESSIONSTORAGE_PREFIX}historyStack`, JSON.stringify(this.historyStack));
    sessionStorage.setItem(`${SESSIONSTORAGE_PREFIX}currentIndex`, this.currentIndex);
    sessionStorage.setItem(`${SESSIONSTORAGE_PREFIX}tags`, JSON.stringify(this.tags));
  }

  resetState() {
    this.historyStack = [];
    this.currentIndex = 0;
    this.tags = {};
    this.saveStateToSession();
  }

  onHistoryEvent({ pathname, key, action }) {
    if (this.historyStack.length === 0) {
      this.historyStack.push({ key, pathname });
      return;
    }

    if (action === 'PUSH') {
      if (!this.currentlyOnLastPage) {
        this.historyStack = this.historyStack.splice(0, this.currentIndex + 1);
      }

      this.historyStack.push({ key, pathname });
      this.currentIndex += 1;
    }

    if (action === 'POP') {
      const potentialIndex = this.getIndexForKey(key);
      if (potentialIndex === -1) {
        // User hit go on url bar. Reset history stack.
        this.resetState();
        this.historyStack = [{ key, pathname }];
      }

      this.currentIndex = this.getIndexForKey(key);
    }

    this.saveStateToSession();
  }

  getIndexForKey(key) {
    return this.historyStack.findIndex(i => i.key === key);
  }

  getIndexForTag(tag) {
    return this.getIndexForKey(this.tags[tag]);
  }

  getKeyForTag(tag) {
    return this.tags[tag];
  }

  getCurrentPathname(n = 0) {
    return this.historyStack[this.currentIndex + n].pathname;
  }

  getCurrentLocationTitle(n = 0) {
    return this.historyStack[this.currentIndex + n].title;
  }

  getTitleForKey(key) {
    return this.historyStack[this.getIndexForKey(key)].title;
  }

  setTitleForKey(key, title) {
    this.historyStack[this.getIndexForKey(key)].title = title;
    this.saveStateToSession();
  }

  setTitleForCurrentLocation(title, n = 0) {
    this.historyStack[this.currentIndex + n].title = title;
    this.saveStateToSession();
  }

  setTagForKey(key, tag) {
    this.tags[tag] = key;
    this.saveStateToSession();
  }

  deleteTag(tag) {
    delete this.tags[tag];
    this.saveStateToSession();
  }

  isCurrentlyOnTag(tag) {
    return this.currentLocationKey === this.tags[tag];
  }

  goToLocationWithTag(tag, n = 0) {
    const indexOfTag = this.getIndexForTag(tag);
    if (indexOfTag === -1) {
      throw new Error(`No index found for tag: ${tag}`);
    }

    this.browserHistory.go(indexOfTag - (this.currentIndex + n));
  }
}

export default new BackForwardHistory();
