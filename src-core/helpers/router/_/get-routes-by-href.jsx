/**
 * Created by dryymoon on 11.04.17.
 */
import { match } from 'react-router';
import urlLib from 'url';
import sameOrigin from 'same-origin';
import isBrowser from '../isBrowser';

import projectConfig from '../../../config';

/* let listenerRef = null;

function listenBrowserHistory() {
  if (listenerRef) return;
  // eslint-disable-next-line
  listenerRef = browserHistory.listen((e) => {
    // e = pathname: "/ru/v5/subscribe-form/list", search: "", hash: "", state: null,
    // action: "PUSH"…};

    // TODO Сдесь нужно придумать костыль чтоб при нескольких кликах в СПА и
    // при переходе в монолит и при возврате в СПА, приложуха рендерилась,
    // а она поднимается из кеша и не рендериться, сука...
    // Т.е. СПА при пуше в историю браузера линков использует вирутальные пуши,
    // которые не приводят к перерендеру страници
    // console.log('History change %s:', e.action, ); // eslint-disable-line
  });
} */

// window.browserHistory = browserHistory;

export default function ({ history, routes }) {
  if (!isBrowser || __DEV_CLI__) return;
  const { $ } = window;
  if (!$) throw new Error('jQuery not existed'); // Test exist JQuery
  // listenBrowserHistory();
  const appMountId = __ROOT_DOM_POINT__;
  $(document).on('click', `a:not(#${appMountId} a)`, function handleClick(event) {
    // Don't handle preventer links
    if (event.isDefaultPrevented()) return;

    const href = $(this).prop('href');
    if (!href) return;


    // Support normal WorkFlow when app not running
    if (!projectConfig.spaRunningGlobalIdentifier) {
      throw new Error('projectConfig.spaRunningGlobalIdentifier must be defined');
    }
    if (!window[projectConfig.spaRunningGlobalIdentifier]) return;

    const isSameOrigin = sameOrigin(window.location.href, href);

    if (__DEVELOPMENT__) {
      // eslint-disable-next-line
      console.info('Outside React link click detected, href: "%s" isSameOrigin: %s',
        href,
        isSameOrigin
      );
    }

    if (!isSameOrigin) return;
    const { pathname, query, hash } = urlLib.parse(href, true, true);
    /*
     Pass true as the second argument to also parse the query string
     using the querystring module. Defaults to false.

     Pass true as the third argument to treat //foo/bar as { host: 'foo', pathname: '/bar' }
     rather than { pathname: '//foo/bar' }. Defaults to false.
     */

    // TODO refactor this

    const formattedRelativeUrl = urlLib.format({ pathname, query, hash });

    match({ routes, location: formattedRelativeUrl }, (err, redirect, renderProps) => {
      if (redirect) {
        // TODO Implement client redirect
        // Результат консультации с вадосом.
        // С сервера когда такой запрос приходит - 301 редирект,
        // На клиенте тупо меняем урл и подменяем а не пушим историю, и дергаем гуглоаналитику.
        throw new Error('CLIENT REDIRECT NOT IMPLEMENTED');
      }

      if (renderProps) {
        return history.push(formattedRelativeUrl);
      }

      const key = '__from-spa-redirect';
      if (query[key]) {
        // eslint-disable-next-line
        return console.error('Need 404 redirect, die to spa dont know anithing about this link');
      }

      const url = urlLib.format({ pathname, query: { ...query, [key]: true }, hash });

      return document.location.assign(url);
    });

    event.preventDefault();
  });
}
