/**
 * Created by dryymoon on 08.06.17.
 */


export function errorLoading(error) {
  if (!__DEVELOPMENT__) Raven.captureException(error);
  else console.error('Dynamic page loading failed', error); // eslint-disable-line
}

export function loadRoute(cb) {
  // console.log('Load Chunked Route Module', module); // eslint-disable-line
  return module => cb(null, module.default);
}

export default loadRoute;
