function errorLoading(err) {
  console.error('Dynamic page loading failed', err);
}
function loadRoute(cb) {
  return module => cb(null, module.default);
}

export default function getComponent(componentPath) {
  return (location, cb) => {
    System.import(componentPath)
      .then(loadRoute(cb))
      .catch(errorLoading);
  };
}
