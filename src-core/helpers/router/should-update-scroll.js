/**
 * Created by dryymoon on 06.10.17.
 */

import isEqual from 'lodash/isEqual';
import get from 'lodash/get';

export default function shouldUpdateScroll(prevRouterProps, currRoutesProps) {
  const prevRoutes = get(prevRouterProps, 'routes', []);
  const currRoutes = get(currRoutesProps, 'routes', []);

  if (currRoutes.some((route, index) => route && route.ignoreScrollBehaviorOnSelf
    && isEqual(currRoutes[index], prevRoutes[index]))) {
    return false;
  }

  if (currRoutes.some(route => route && route.ignoreScrollBehavior)) {
    return false;
  }

  if (currRoutes.some(route => route && route.scrollToTop)) {
    return [0, 0];
  }

  return true;
}
