/**
 * Created by dryymoon on 17.10.17.
 */
const crypto = require('crypto');

export default function uniqueString(len = 32) {
  if (!Number.isFinite(len)) {
    throw new TypeError('Expected a finite number');
  }

  return crypto.randomBytes(Math.ceil(len / 2)).toString('hex').slice(0, len);
}
