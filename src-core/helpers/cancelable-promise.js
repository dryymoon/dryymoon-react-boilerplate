/**
 * Created by dryymoon on 24.01.2018.
 */
import isObject from 'lodash/isObject';
import isFunction from 'lodash/isFunction';

export default class CancelablePromise {
  static all(iterable, onCancel) {
    return new CancelablePromise((y, n) => {
      Promise.all(iterable).then(y, n);
    }, () => {
      if (onCancel) onCancel();
      iterable.forEach(it => isObject(it) && isFunction(it.cancel) && !it._canceled && it.cancel());
    });
  }

  static race(iterable, onCancel) {
    return new CancelablePromise((y, n) => {
      Promise.race(iterable).then(y, n);
    }, onCancel);
  }

  static reject(value, onCancel) {
    return new CancelablePromise((y, n) => {
      Promise.reject(value).then(y, n);
    }, onCancel);
  }

  static resolve(value, onCancel) {
    return new CancelablePromise((y, n) => {
      Promise.resolve(value).then(y, n);
    }, onCancel);
  }

  constructor(executor, onCancel) {
    this._promise = new Promise(executor);
    this._canceled = false;
    this._onCancel = onCancel;
    this.cancel = this.cancel.bind(this);
  }

  then(successUserHandler, failUserHandler) {
    const p = new CancelablePromise((resolve, reject) => {
      this._promise
        .then((data) => {
          if (this._canceled) p._canceled = true;
          if (successUserHandler && !this._canceled) {
            try {
              resolve(successUserHandler(data));
            } catch (e) {
              reject(e);
            }
          } else {
            resolve(data);
          }
        })
        .catch((data) => {
          if (this._canceled) p._canceled = true;
          if (failUserHandler && !this._canceled) {
            try {
              resolve(failUserHandler(data));
            } catch (e) {
              reject(e);
            }
          } else {
            reject(data);
          }
        });
    }, this._onCancel);
    return p;
  }

  catch(error) {
    return this.then(undefined, error);
  }

  cancel() {
    this._canceled = true;
    if (this._onCancel) this._onCancel();
    return this;
  }
}
