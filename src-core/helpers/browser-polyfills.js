/**
 * Created by DryyMoon on 24.11.2016.
 */
/* eslint-disable import/no-extraneous-dependencies */
// require('smoothscroll-polyfill').polyfill();
// import 'babel-polyfill'; TODO не работает в ИЕ

/* Фикс проеба IE11 Axios, там нема Промисов в природе... */
import 'es6-promise/auto';
import objectFitImages from 'object-fit-images';
import initReactFastclick from 'react-fastclick';
import 'classlist-polyfill';

objectFitImages(null, { watchMQ: true });

const supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;

if (supportsTouch) {
  document.documentElement.className += 'touch';
  initReactFastclick();
} else {
  document.documentElement.className += 'no-touch';
}
