/**
 * Created by PIV on 29.06.2016.
 */
import { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import position from 'metal-position';

// const detectZoom = require('detect-zoom');

class ReactViewWatcher extends Component {
  static propTypes = {
    children: PropTypes.node
  };

  static childContextTypes = {
    getViewWidth: PropTypes.func,
    getViewHeight: PropTypes.func,
    getDocumentWidth: PropTypes.func,
    getDocumentHeight: PropTypes.func,

    getWindowCords: PropTypes.func,
    getContainerCords: PropTypes.func,
    checkIntersectRegion: PropTypes.func
  };

  static getViewWidth() {
    return position.getClientWidth(window);
  }

  static getViewHeight() {
    return position.getClientHeight(window);
  }

  static getDocumentWidth() {
    return position.getClientWidth(document.body);
  }

  static getDocumentHeight() {
    return position.getClientHeight(document.body);
  }

  static getWindowCords() {
    const Cords = position.getRegion(window);
    return {
      x: Cords.top,
      y: Cords.left,
      x1: Cords.bottom,
      y1: Cords.right
    };
  }

  static getContainerCords(DomRef) {
    const Cords = position.getRegion(DomRef);
    return {
      x: Cords.top,
      y: Cords.left,
      x1: Cords.bottom,
      y1: Cords.right
    };
  }

  static checkIntersectRegion(DomRef) {
    return position.intersectRegion(
      position.getRegion(DomRef),
      position.getRegion(window)
    );
  }

  constructor(props, context) {
    super(props, context);
    this.handleWindowResize = debounce(
      this.handleWindowResize.bind(this), 1000, { trailing: true }
    );
  }

  getChildContext() {
    return {
      getViewWidth: this.getViewWidth,
      getViewHeight: this.getViewHeight,
      getDocumentWidth: this.getDocumentWidth,
      getDocumentHeight: this.getDocumentHeight,

      getWindowCords: this.getWindowCords,
      getContainerCords: this.getContainerCords,
      checkIntersectRegion: this.checkIntersectRegion
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleWindowResize, true);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowResize);
  }


  handleWindowResize() {
    // const zoom = detectZoom.zoom();
    // const device = detectZoom.device();

    // eslint-disable-next-line no-console
    // console.log('detectZoom:', zoom, device);

    this.getViewWidth();
    // eslint-disable-next-line no-console
    console.log('getViewWidth:', position.getClientWidth(window));
    this.getViewHeight();
    // eslint-disable-next-line no-console
    console.log('getViewHeight:', position.getClientHeight(window));
    this.getDocumentWidth();
    // eslint-disable-next-line no-console
    console.log('getDocumentWidth:', position.getClientWidth(document.body));
    this.getDocumentHeight();
    // eslint-disable-next-line no-console
    console.log('getDocumentHeight:', position.getClientHeight(document.body));
    // this.checkIntersectRegion();
    // eslint-disable-next-line no-console
    // console.log('getWindowCords:', this.checkIntersectRegion());
  }

  render() {
    return this.props.children;
  }
}
export default ReactViewWatcher;
