/**
 * Created by dryymoon on 30.08.16.
 */
import { Component, cloneElement } from 'react';
import PropTypes from 'prop-types';
import sha1 from 'string-hash';

// TODO: RangeError: Maximum call stack size exceeded

class Cache extends Component {
  static propTypes = {
    children: PropTypes.node,
    deep: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
    // eslint-disable-next-line react/forbid-prop-types
    cacheKey: PropTypes.oneOfType([PropTypes.string, PropTypes.func])
  }
  static displayName = 'Cache';
  static cache = {
    keyGenerator: (props = {}) => {
      if (props.cacheKey) {
        return sha1(JSON.stringify(props.cacheKey));
      }
      return null;
    }
  }

  render() {
    // eslint-disable-next-line no-unused-vars
    const { children, ...restArgs } = this.props; // deep, cacheKey,
    return cloneElement(children, { ...restArgs });
  }
}

export default Cache;
