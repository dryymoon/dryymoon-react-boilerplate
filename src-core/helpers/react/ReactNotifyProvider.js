/* eslint-disable import/prefer-default-export */
/**
 * Created by PIV on 29.06.2016.
 */
import { Component } from 'react';
import PropTypes from 'prop-types';
// import getDisplayName from 'react-display-name';
import { connect } from 'react-redux';
import { error, info, show, hide, success, warning, } from 'components/notification';

// import extend from 'lodash/extend';

@connect(null, { show, hide, success, error, warning, info })
class Provider extends Component {
  static propTypes = {
    show: PropTypes.func,
    hide: PropTypes.func,
    success: PropTypes.func,
    error: PropTypes.func,
    warning: PropTypes.func,
    info: PropTypes.func,
    children: PropTypes.node
  };

  static childContextTypes = {
    notification: PropTypes.shape({
      show: PropTypes.func,
      hide: PropTypes.func,
      success: PropTypes.func,
      error: PropTypes.func,
      warning: PropTypes.func,
      info: PropTypes.func
    })
  };

  getChildContext() {
    return {
      notification: {
        show: this.props.show,
        hide: this.props.hide,
        success: this.props.success,
        error: this.props.error,
        warning: this.props.warning,
        info: this.props.info
      }
    };
  }

  render() {
    return this.props.children;
  }
}

export { Provider };

/* function factory() {
  return function wrapComponent(BaseComponent) {
    return class ComponentWithUniqId extends BaseComponent {
      static displayName = `uniqId(${getDisplayName(BaseComponent)})`;

      static contextTypes = extend(BaseComponent.contextTypes || {}, { getUid: PropTypes.func });

      constructor(props, context) {
        super(props, context);
        this.getUid = context.getUid;
        this.uid = context.getUid();
      }
    };
  };
}

export default factory();
export { factory }; */
