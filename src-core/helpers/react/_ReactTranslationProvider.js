/**
 * Created by akot on 08.08.17.
 */

import { Component, Children } from 'react';
import PropTypes from 'prop-types';
// import uniq from 'lodash/uniq';
import flatten from 'lodash/flatten';
import values from 'lodash/values';
import keys from 'lodash/keys';
import intersection from 'lodash/intersection';
import { addLocale, useLocale as useLocaleLib } from 'c-3po';
import getDisplayName from 'react-display-name';
import extend from 'extend';
import cookie from './ReactCookiesProvider';
import * as localeLoaders from '../../../translations'; // eslint-disable-line

let acceptLanguageParser;
// const loadedTranslations = {};
const availableTranslations = keys(localeLoaders);
if (__SERVER__) {
  acceptLanguageParser = require('accept-language-parser');
  // values(localeLoaders).forEach(loadLocale => loadLocale((data = {}) => {
  values(localeLoaders).forEach((data) => {
    const { headers: { language } } = data;
    if (!language) return;
    // loadedTranslations[language] = data;
    // availableTranslations.push(language);
    addLocale(language, data);
  });
}
let clientPreferLang = null;
if (__CLIENT__) {
  (() => {
    // keys(localeLoaders).forEach(it => availableTranslations.push(it));
    const cookieLib = require('cookie-cutter');
    const cookieLocale = cookieLib.get('locale');
    const clientLangsUnparsed = window.navigator.languages || ['en'];
    const proposedLangs = flatten(clientLangsUnparsed.map(it => it.split('-')))
      .filter(it => it)
      .map(it => it.toLowerCase());
    const preferLang = determinePreferLang({ proposedLangs, cookieLocale, cookieLib });
    console.log('preferLang', preferLang, { proposedLangs, cookieLocale, cookieLib });
    // addLocale(preferLang, localeLoaders[preferLang]);
    // useLocaleLib(preferLang);
    clientPreferLang = preferLang;
  })();
}

export const contextShape = {
  locale: PropTypes.string,
  useLocale: PropTypes.func,
  // locales: PropTypes.arrayOf(PropTypes.string),
};

const maxAge = 10 * 365 * 12 * 30 * 24 * 60 * 60 * 1000; // 10 years

@cookie
class ReactTranslationProvider extends Component {
  static propTypes = {
    request: PropTypes.object, // eslint-disable-line
    // translations: PropTypes.object, // eslint-disable-line
    children: PropTypes.node
  };

  static childContextTypes = contextShape;

  constructor(props, context) {
    super(props, context);
    let proposedLangs = [];

    if (__SERVER__) {
      const cookieLocale = context.cookies.get('locale');
      const { request } = props;
      const clientReqLangsUnparsed = request.headers['accept-language'] || 'en';
      const clientReqLangsParsed = acceptLanguageParser.parse(clientReqLangsUnparsed);
      proposedLangs = clientReqLangsParsed.map(({ code }) => code);
      const preferLang = determinePreferLang({ cookieLib: context.cookies, proposedLangs, cookieLocale });
      useLocaleLib(preferLang);
      this.locale = preferLang;
    }

    if (__CLIENT__) {
      this.locale = clientPreferLang;
    }

    this.useLocale = this.useLocale.bind(this);
  }

  getChildContext() {
    const { locale, useLocale } = this;
    return { locale, useLocale };
  }

  useLocale(locale) {
    console.log('USE LOCALE', locale);
    // const { cookies, forceUpdate } = this;
    const that = this;
    // return new Promise((resolve, reject) => {
    if (!localeLoaders[locale]) return;
    // localeLoaders[locale]((data) => {

    that.cookies.set('locale', locale, { maxAge, httpOnly: false });
    addLocale(locale, localeLoaders[locale]);
    useLocaleLib(locale);
    that.forceUpdate();
    // resolve();
    // });
    // });
  }

  render() {
    return Children.only(this.props.children);
  }
}

export { ReactTranslationProvider };

export default function decorator(BaseComponent) {
  return class ComponentWithLocales extends BaseComponent {
    static displayName = `withLocale(${getDisplayName(BaseComponent)})`;

    static contextTypes = extend(BaseComponent.contextTypes || {}, contextShape);

    constructor(props, context) {
      super(props, context);
      this.locale = context.locale;
      this.useLocale = context.useLocale;
    }
  };
}

function determinePreferLang({ cookieLocale, proposedLangs, cookieLib }) {
  const proposedLangsInt = [...proposedLangs];
  if (cookieLocale) proposedLangsInt.unshift(cookieLocale);
  const preferLang = intersection(proposedLangsInt, availableTranslations)[0] || 'en';
  if (cookieLocale !== preferLang) {
    cookieLib.set('locale', preferLang, { maxAge, httpOnly: false });
  }
  return preferLang;
}

