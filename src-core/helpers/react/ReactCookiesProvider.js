/**
 * Created by dryymoon on 14.12.16.
 */

import { Children, Component } from 'react';
import PropTypes from 'prop-types';
import getDisplayName from 'react-display-name';
import extend from 'lodash/extend';

/* let CookiesLib = () => ({
  get: () => null,
  set: () => null
});
*/

const contextShape = PropTypes.shape({
  set: PropTypes.func,
  get: PropTypes.func
});

export { contextShape };

class ReactCookiesProvider extends Component {
  static propTypes = {
    cookies: contextShape.isRequired,
    children: PropTypes.node
  };

  static childContextTypes = { cookies: contextShape };

  getChildContext() {
    const { cookies } = this.props;
    return { cookies };
  }

  render() {
    return Children.only(this.props.children);
  }
}

export { ReactCookiesProvider };

function factory() {
  return function wrapComponent(BaseComponent) {
    return class ComponentWithCookies extends BaseComponent {
      static displayName = `withCookies(${getDisplayName(BaseComponent)})`;

      static contextTypes = extend(BaseComponent.contextTypes || {}, { cookies: contextShape });

      constructor(props, context) {
        super(props, context);
        this.cookies = context.cookies;
      }
    };
  };
}

export default factory();
export { factory };

