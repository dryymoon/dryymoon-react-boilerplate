/**
 * Created by akot on 08.08.17.
 */

import { Component, Children } from 'react';
import PropTypes from 'prop-types';
import flatten from 'lodash/flatten';
import mapValues from 'lodash/mapValues';
import keys from 'lodash/keys';
import intersection from 'lodash/intersection';
import { addLocale, useLocale as useLocaleLib } from 'c-3po';
import getDisplayName from 'react-display-name';
import extend from 'extend';
import cookie from './ReactCookiesProvider';
import * as localeLoaders from '../../../translations'; // eslint-disable-line

// const localeLoaders = import('../../../translations/');

let acceptLanguageParser;
const loadedTranslationsOnServer = {};
if (__SERVER__) {
  acceptLanguageParser = require('accept-language-parser');
  mapValues(localeLoaders, ((loadLocale, language) =>
    loadLocale((data = {}) => {
      // const { headers: { language } } = data;
      // if (!language) return;
      loadedTranslationsOnServer[language] = new Buffer(JSON.stringify({ language, data }), 'utf8').toString('base64');
      addLocale(language, data);
    })));
}

export const contextShape = {
  locale: PropTypes.string,
  useLocale: PropTypes.func,
};

const maxAge = 10 * 365 * 12 * 30 * 24 * 60 * 60 * 1000; // 10 years

@cookie
class ReactTranslationProvider extends Component {
  static propTypes = {
    request: PropTypes.object, // eslint-disable-line
    upSetTranslationData: PropTypes.func,
    children: PropTypes.node
  };

  static childContextTypes = contextShape;

  constructor(props, context) {
    super(props, context);
    let proposedLangs = [];
    const cookieLocale = context.cookies.get('locale');

    if (__SERVER__) {
      const { request } = props;
      const clientReqLangsUnparsed = request.headers['accept-language'] || 'en';
      const clientReqLangsParsed = acceptLanguageParser.parse(clientReqLangsUnparsed);
      proposedLangs = clientReqLangsParsed.map(({ code }) => code);
    }

    if (__CLIENT__) {
      const clientLangsUnparsed = window.navigator.languages || ['en'];
      proposedLangs = flatten(clientLangsUnparsed.map(it => it.split('-')))
        .filter(it => it)
        .map(it => it.toLowerCase());
    }

    if (cookieLocale) proposedLangs.unshift(cookieLocale);
    const preferLang = intersection(proposedLangs, keys(localeLoaders))[0] || 'en';

    if (cookieLocale !== preferLang) {
      context.cookies.set('locale', preferLang, { maxAge, httpOnly: false });
    }

    if (__SERVER__) {
      const { upSetTranslationData } = props;
      upSetTranslationData(loadedTranslationsOnServer[preferLang]);
      useLocaleLib(preferLang);
      this.localeReady = true;
    }

    if (__CLIENT__) {
      if (window.__translationData) {
        const { language, data } = JSON.parse(new Buffer(window.__translationData, 'base64').toString('utf8'));
        const element = document.getElementById('__translationData');
        if (element) element.outerHTML = '';
        if (preferLang === language) {
          addLocale(language, data);
          useLocaleLib(language);
          this.localeReady = true;
        }
      }
      if (!this.localeReady) {
        const that = this;
        localeLoaders[preferLang]((data) => {
          addLocale(preferLang, data);
          useLocaleLib(preferLang);
          that.localeReady = true;
          if (that.componentMounted) that.forceUpdate();
        });
      }
    }

    this.locale = preferLang;
    this.useLocale = this.useLocale.bind(this);
  }

  getChildContext() {
    const { locale, useLocale } = this;
    return { locale, useLocale };
  }

  componentDidMount() {
    this.componentMounted = true;
  }

  useLocale(locale) {
    const that = this;
    return new Promise((resolve, reject) => {
      if (!localeLoaders[locale]) return reject();
      if (locale === that.locale) return resolve();
      localeLoaders[locale]((data) => {
        that.cookies.set('locale', locale, { maxAge, httpOnly: false });
        that.locale = locale;
        addLocale(locale, data);
        useLocaleLib(locale);
        that.forceUpdate();
        resolve();
      });
    });
  }

  render() {
    if (!this.localeReady) return null;
    return Children.only(this.props.children);
  }
}

export { ReactTranslationProvider };

export default function decorator(BaseComponent) {
  return class ComponentWithLocales extends BaseComponent {
    static displayName = `withLocale(${getDisplayName(BaseComponent)})`;

    static contextTypes = extend(BaseComponent.contextTypes || {}, contextShape);
  };
}
