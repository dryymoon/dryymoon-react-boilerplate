import EventEmitter from 'events';
import processEnv from 'process-env';
import lruCacheLib from 'lru-cache';
// import sha1 from 'string-hash';
import sha512 from 'sha512';
import getDisplayName from 'react-display-name';
import htmlAstParser from 'fast-html-parser';
import dot from 'dot';

const Module = require('module');

const require_ = Module.prototype.require;
const origInstantiateReactComponent = require('react/lib/instantiateReactComponent');

const DEFAULT_MINUTES_TO_CACHE = 60;

const DEFAULT_LRU_CONFIG = {
  maxElements: 10000, // The maximum size of the cache
  maxAge: DEFAULT_MINUTES_TO_CACHE * 60 * 1000,
  size: 1000
};

const REACT_IDS_REGEXP = new RegExp('<!--\\sreact-\\w+:\\s(\\d+)\\s-->|data-reactid="(\\d+)"', 'g');

class InstantiateReactComponentOptimizer {
  constructor() {
    let isDev = true;
    if (processEnv.get('NODE_ENV') === 'production') {
      isDev = false;
    }
    if (isDev) {
      console.info( // eslint-disable-line no-console
        'Caching is disabled in non-production environments.'
      );
      return;
    }

    /*
     * Determine if WebPack DefinePlugin setted NODE_ENV same as global NODE_ENV in system,
     * die to ReactInstantieElement in dev mode
     * has active check for patching inner functions.
     */

    console.info( // eslint-disable-line no-console
      'React Caching Enabled :)'
    );
    /* this.config = config;
     this.componentsToCache = config.components ? Object.keys(config.components) : [];
     this.componentsToCache.forEach((cmpName) => {
     let cacheConfig = config.components[cmpName];
     if (cacheConfig instanceof Function) {
     cacheConfig = config.components[cmpName] = {
     cacheKeyGen: cacheConfig
     };
     }
     if (isObject(cacheConfig) && !cacheConfig.cacheKeyGen) {
     cacheConfig.cacheKeyGen = cacheConfig.cacheAttrs && cacheConfig.cacheAttrs.length
     ? genAttrBasedKeyFunction(cacheConfig.cacheAttrs)
     : defaultCacheKeyFunction;
     }
     }, this); */
    /* eslint-disable no-nested-ternary */
    /* this.lruCache = (config.cacheImpl) ? config.cacheImpl :
     (config.lruCacheSettings) ? cache(config.lruCacheSettings) : cache(DEFAULT_LRU_CONFIG); */
    this.lruCache = lruCacheLib(DEFAULT_LRU_CONFIG);
    this.enabled = true; // !(config.disabled === true);
    this.eventEmitter = new EventEmitter();
    this.patched = false;
    this.shouldCollectLoadTimeStats = true;
    this.componentsToCache = {};
    this.wrapInstantiateReactComponent();
  }

  add(elem, config) {
    this.componentsToCache[elem] = config;
  }

  wrapInstantiateReactComponent() {
    const self = this;

    const WrappedInstantiateReactComponent = (...wircArgs) => {
      /*
       * Wrap original instance for future cache
       */
      const component = origInstantiateReactComponent.apply(
        origInstantiateReactComponent, wircArgs);

      if (component._instantiateReactComponent
        && (!component._instantiateReactComponent.__wrapped)) {
        component._instantiateReactComponent = WrappedInstantiateReactComponent;
      }

      /*
       * Get Default component properties
       */
      const curEl = component._currentElement;
      const cmpName = safeGetDisplayName(curEl);

      /*
       * Adding Component staticaly to caching factory
       * Check if Exist "static cache" in component
       */
      if (!self.componentsToCache[cmpName] && curEl && curEl.type && curEl.type.cache) {
        self.componentsToCache[cmpName] = curEl.type.cache;
      }

      /*
       * Decide if component needs to cache, templatize it, and return cached or normal version
       */

      if (!self.enabled) return component;
      // const existingCacheKey = curEl && curEl.props && curEl.props.cacheKey;
      // if (existingCacheKey)console.log('existingCacheKey', existingCacheKey);
      // if (existingCacheKey) delete curEl.props.cacheKey;
      // const componentCacheConfig = self.componentsToCache[cmpName];
      if (!self.componentsToCache[cmpName]) return component;
      // if (!componentCacheConfig || !existingCacheKey) return component;

      const origMountComponent = component.mountComponent;

      component.mountComponent = (...mcArgs) => {
        // TODO Change this ...
        let generatedKeyFn = defaultCacheKeyFunction;
        if (self.componentsToCache[cmpName].keyGenerator) {
          generatedKeyFn = self.componentsToCache[cmpName].keyGenerator;
        }

        const generatedKey = curEl && generatedKeyFn(curEl.props);

        if (generatedKey === null) {
          return origMountComponent.apply(component, mcArgs);
        }

        const cacheKey = `${cmpName}:${generatedKey}`;

        // const [rootId,, reactRenderRuntime, context] = mcArgs; FOR OLD REACT

        const cachedObj = self.lruCache.get(cacheKey);
        if (cachedObj) {
          const [,, reactRenderRuntime, context] = mcArgs;
          self.eventEmitter.emit('cache-hit', { type: 'cache', event: 'hit', cmpName });

          if (context && context.pushRuntimeCssSelector && cachedObj.usedSelectors) {
            context.pushRuntimeCssSelector(cachedObj.usedSelectors);
          }
          // process.nextTick(() => { console.log('CACHE HIT', cacheKey); } );
          try {
            return cachedObj.dotTemplate({
              reactIdFn: () => {
                const val = reactRenderRuntime._idCounter;
                reactRenderRuntime._idCounter += 1;
                return val;
              }
            });
          } catch (e) {
            console.error('ReactCache, cant run template:', cacheKey, ' Because:', e);
            return origMountComponent.apply(component, mcArgs);
          }

          /* This is for older react, which has data-reactid=1.1.1.1.2.2
           * return cacheMarkup.replace(
           * new RegExp(`data-reactid='${cachedObj.rootId}`, 'g'),
           * `data-reactid='${rootId}`);
           */
        }

        const markup = origMountComponent.apply(component, mcArgs);

        const usedSelectors = inspectCssSelectors(markup);

        const dotTemplate = dot.template(templatizeReactIds(markup));

        // const compiledMarkup = templateAttrs.length ? template(markup) : null;

        self.eventEmitter.emit('cache-miss', {
          type: 'cache',
          event: 'miss',
          cmpName
        });

        self.lruCache.set(cacheKey, {
          dotTemplate,
          usedSelectors
        });

        return markup;
      };

      return component;
    };

    WrappedInstantiateReactComponent.__wrapped = true;

    /*
     * Patch node require for future react patching,
     * very important is usage of ES5 syntax in this construction
     */
    Module.prototype.require = function (...args) { // eslint-disable-line func-names
      const [path] = args;

      const mod = require_.apply(this, args);

      if (path === './instantiateReactComponent') {
        console.info('React succesful Patched for Caching'); // eslint-disable-line no-console
        self.patched = true;
        self.eventEmitter.emit('react-patched', true);
        return WrappedInstantiateReactComponent;
      }

      return mod;
    };
  }

  enable(enableFlag) {
    this.enabled = enableFlag;
  }

  cacheDump() {
    return this.lruCache.dump();
  }

  cacheLength() {
    return this.lruCache.length;
  }

  cacheReset() {
    return this.lruCache.reset();
  }
}

const instance = new InstantiateReactComponentOptimizer(); // eslint-disable-line no-new

export default instance;

function defaultCacheKeyFunction(props) {
  const str = JSON.stringify(props, (key, val) => {
    if (key[0] === '_') return;
    if (key[0] === '$' && key[1] === '$') return;
    return val;
  });
  return sha512(str)
    .toString('hex');
}

const safeGetDisplayName = el => el && el.type && getDisplayName(el.type);

function templatizeReactIds(html) {
  return html.replace(REACT_IDS_REGEXP,
    (match, reactIdInText1, reactIdInText2) => {
      const reactIdInText = reactIdInText1 || reactIdInText2;
      return match.replace(reactIdInText, '{{=it.reactIdFn()}}');
    });
}

function inspectCssSelectors(html) {
  // const collector = {};
  const ast = htmlAstParser.parse(html);
  return grabSelectors(ast);
  // return collector;
}

function grabSelectors(parsedHtml = {}) {
  const collector = {};
  const { tagName, classNames, childNodes } = parsedHtml;

  if (tagName) {
    // eslint-disable-next-line no-param-reassign
    collector[tagName] = collector[tagName] ? (collector[tagName] + 1) : 1;
  }
  if (Array.isArray(classNames) && classNames.length > 0) {
    classNames.forEach((cl) => {
      const clSelector = `.${cl}`;
      // eslint-disable-next-line no-param-reassign
      collector[clSelector] = collector[clSelector] ? (collector[clSelector] + 1) : 1;
    });
  }
  if (Array.isArray(childNodes) && childNodes.length > 0) {
    childNodes.forEach((cn) => {
      const ChildCollector = grabSelectors(cn);
      Object.keys(ChildCollector).forEach((key) => {
        collector[key] = collector[key] || 0;
        collector[key] += ChildCollector[key];
      });
    });
  }
  return collector;
}

/*

 import get from 'lodash/get';
 import set from 'lodash/set';
 import isObject from 'lodash/isObject';


 function genAttrBasedKeyFunction(attrs) {
 return ((props) => {
 let key = '';
 attrs.forEach((attr) => {
 const val = get(props, attr);
 key += isObject(val) ? JSON.stringify(val) : val;
 });
 return key;
 });
 }


 function restorePropsAndProcessTemplate(compiled, templateAttrs, templateAttrValues, curEl) {
 templateAttrs.forEach((attrKey) => {
 const _attrKey = attrKey.replace('.', '__');
 set(curEl.props, attrKey, templateAttrValues[_attrKey]);
 });
 return compiled(templateAttrValues);
 }

 const templatizeProps = (attrKey, templateAttrValues, curEl, cmpName) => {
 const _attrKey = attrKey.replace('.', '__');
 templateAttrValues[_attrKey] = get(curEl.props, attrKey);
 if (isObject(templateAttrValues[_attrKey])) {
 throw new Error(
 `Cannot templatize Object at ${attrKey} for component ${cmpName}`
 );
 }
 set(curEl.props, attrKey, '${' + _attrKey + '}');
 };
 */
