/**
 * Created by dryymoon on 05.05.17.
 */

import Finger from 'fingerprintjs2';

if (__SERVER__) throw new Error('uuid is not compatible with serverside as for now, rewrite IT');

new Finger().get((result, components) => {
  console.log(result); // a hash, representing your device fingerprint
  console.log(components); // an array of FP components
});

export default function uuid() {

}


// 550e8400-e29b-41d4-a716-446655440000
// 8f5636b7-a8cc-5a5c-1a47-1e47daa24eca
