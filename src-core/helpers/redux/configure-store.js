/**
 * Created by PIV on 15.08.2016.
 */
/* eslint-disable import/no-extraneous-dependencies */
import { applyMiddleware, compose, createStore } from 'redux'; //
import thunkMiddleware from 'redux-thunk';
import catchMiddleware from 'redux-catch';
// import defer from 'lodash/defer';
// import debounce from 'lodash/debounce';
import keys from 'lodash/keys';
import difference from 'lodash/difference';
import { routerMiddleware, routerReducer } from 'react-router-redux';
import { createDynamicMiddlewares } from 'redux-dynamic-middlewares';
// import { batchedSubscribe } from 'redux-batched-subscribe';
import ReducersRegistryLib from './reducers-registry';
import apiMiddleware from './api-middleware';
// import { createStateSyncMiddleware, initStateWithPrevTab } from 'redux-state-sync';
// import ReducersRegistry from './reducers-registry';
// import analytics from 'redux-analytics'; // https://github.com/markdalgleish/redux-analytics

export default function configureStore(history, api, preLoadedState) {
  /*
   * Configure Redux MiddleWares
   */

  const middlewares = [];

  middlewares.push(thunkMiddleware);
  middlewares.push(routerMiddleware(history));
  middlewares.push(apiMiddleware(api));
  // middlewares.push(createStateSyncMiddleware());

  if (__DEVELOPMENT__) middlewares.push(require('redux-immutable-state-invariant').default());
  if (__DEVELOPMENT__) middlewares.push(require('redux-unhandled-action').default());
  if (__DEVELOPMENT__ && __CLIENT__) {
    //  middlewares.push(require('redux-logger').createLogger({ collapsed: true }));
  }

  const dynamicMiddlewaresInstance = createDynamicMiddlewares();

  middlewares.push(dynamicMiddlewaresInstance.enhancer);

  function errorHandler(error, getState, lastAction, /* dispatch */) {
    if (__DEVELOPMENT__) {
      console.error('Reducers catch error', error); // eslint-disable-line
      console.debug('current state', getState()); // eslint-disable-line
      console.debug('last action was', lastAction); // eslint-disable-line
      // optionally dispatch an action due to the error using the dispatch parameter
    }

    Raven.captureException('Reducers catch error', {
      level: 'error',
      extra: { error, action: lastAction, state: getState() }
    });
  }

  middlewares.push(catchMiddleware(errorHandler));
  /*
   * Configure Reducers Registry
   */
  const reducersRegistry = new ReducersRegistryLib();
  reducersRegistry.register({ routing: routerReducer });

  /*
   * Configure Redux Store
   */

  const storeEnchancers = [applyMiddleware(...middlewares)];

  if (__DEVELOPMENT__ && __CLIENT__ && typeof window === 'object'
    && typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined') {
    // storeModifiers = compose(storeModifiers, window.__REDUX_DEVTOOLS_EXTENSION__());
    storeEnchancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }

  // https://www.npmjs.com/package/redux-batched-subscribe
  if (__CLIENT__) {
    // TODO Improve this
    // const batchDebounce = debounce(notify => notify(), 300);

    // storeEnchancers.push(batchedSubscribe(batchDebounce));
  }

  const storeModifiers = compose(...storeEnchancers);

  const featuredReducers = difference(
    keys(preLoadedState),
    keys(reducersRegistry._reducers)
  );

  featuredReducers.forEach(reducerName => reducersRegistry.register({
    [reducerName]: (state = {}) => ({ ...state })
  }));

  const store = createStore(
    reducersRegistry.rootReducer,
    preLoadedState, storeModifiers
  );

  store.addMiddleware = dynamicMiddlewaresInstance.addMiddleware;
  // store.removeMiddleware = dynamicMiddlewaresInstance.removeMiddleware;
  // store.resetMiddlewares = dynamicMiddlewaresInstance.resetMiddlewares;

  // const initStateWithPrevTab(store);

  reducersRegistry.setChangeListener(() => store.replaceReducer(reducersRegistry.rootReducer));

  return store;
}

//  if (__DEVELOPMENT__ && module.hot) {
/* module.hot.accept('./ducks/core', () => {
 var nextCoreReducers = require('./ducks/core')
 reducerRegistry.register(nextCoreReducers)
 }) */
// Enable Webpack hot module replacement for reducers
/* module.hot.accept('../reducers', () => {
 store.replaceReducer(reducers);
 }); */
//  }

// middlewares.push(analytics(({ type, payload }, state) => null));

/* analitics
 *const action = {
 *  type: 'MY_ACTION',
 *  meta: {
 *    analytics: {
 *      type: 'my-analytics-event',
 *      payload: {
 *        some: 'data',
 *        more: 'stuff'
 *      }
 *    }
 *  }
 *};
 */
