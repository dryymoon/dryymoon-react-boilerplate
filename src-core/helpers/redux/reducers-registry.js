/**
 * Created by PIV on 17.08.2016.
 */
import intersection from 'lodash/intersection';
import keys from 'lodash/keys';
import assign from 'lodash/assign';
import isObject from 'lodash/isObject';
import isFunction from 'lodash/isFunction';

const $partialStateReducers = Symbol('$partialStateReducers');
const $globalStateReducers = Symbol('$globalStateReducers');
const $changeReducersCallback = Symbol('$changeReducersCallback');
const staticPartialReducers = {};
const staticGlobalReducers = [];

let InstanceOnClient = null;

class ReducersRegistry {
  static register(ObjOrFn) { // newReducers
    if (isObject(ObjOrFn)
      && isObjectsHasSameKeys(staticPartialReducers, ObjOrFn)
      && __DEVELOPMENT__) {
      // eslint-disable-next-line
      console.warn('Duplicated listener registered in ReducerRegistry.');
      // throw new Error('Can only once register reducer in static reducer Registry');
    }
    if (__CLIENT__ && InstanceOnClient) {
      return InstanceOnClient.register(ObjOrFn);
    }

    if (isFunction(ObjOrFn)) return staticGlobalReducers.push(ObjOrFn);
    if (isObject(ObjOrFn)) return assign(staticPartialReducers, ObjOrFn);
  }

  constructor() {
    this[$partialStateReducers] = { ...staticPartialReducers };
    this[$globalStateReducers] = [...staticGlobalReducers];
    if (__CLIENT__) InstanceOnClient = this;
    this.rootReducer = this.rootReducer.bind(this);
  }

  register(newReducersObjOrFn) {
    if (isFunction(newReducersObjOrFn)) this[$globalStateReducers].push(newReducersObjOrFn);

    if (isObject(newReducersObjOrFn)) assign(this[$partialStateReducers], newReducersObjOrFn);

    if (this[$changeReducersCallback]) this[$changeReducersCallback]();
  }

  rootReducer(oldState = {}, action) {
    let newState = oldState;

    Object.keys(this[$partialStateReducers]).forEach((partialStateKey) => {
      const reducer = this[$partialStateReducers][partialStateKey];
      const newPartialState = reducer(newState[partialStateKey], action);
      if (newPartialState) newState = { ...newState, [partialStateKey]: newPartialState };
    });

    this[$globalStateReducers].forEach((globalReducer) => {
      newState = globalReducer(newState, action) || newState;
    });

    return newState;
  }

  setChangeListener(listener) {
    if (this[$changeReducersCallback] && __DEVELOPMENT__) {
      // eslint-disable-next-line
      console.warn('Duplicated listener registered in ReducerRegistry.');
    }
    this[$changeReducersCallback] = listener;
  }
}

export default ReducersRegistry;

function isObjectsHasSameKeys(Obj1, Obj2) {
  const sameKeys = intersection(keys(Obj1), keys(Obj2));
  return sameKeys.length > 0;
}
