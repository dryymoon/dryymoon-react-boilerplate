/**
 * Created by sem on 12.10.16.
 */

// const STATUS_SUCCESS = 'success';

export default function apiMiddleware(apiClient) {
  return (/* {dispatch, getState } c */) => next => (action) => {
    if (!action.apiRequest) {
      return next(action);
    }

    const [REQUEST, SUCCESS, FAILURE] = action.types || [];
    const { method = 'get', url, ...requestOpts } = action.apiRequest;

    if (REQUEST) next({ ...action, type: REQUEST });
    // const queryString = method === 'get' ? request.params : undefined;
    // const postData = ['post', 'put', 'delete'].indexOf(request.method) >= 0
    // ? request.params : undefined;

    return apiClient[method](url, requestOpts)
      .then((payload) => {
        if (SUCCESS) next({ ...action, type: SUCCESS, payload });
        return { ...action, payload };
      })
      .catch((error) => {
        if (FAILURE) next({ ...action, type: FAILURE, payload: error });
        // eslint-disable-next-line no-console
        // console.error('API request error', error, error.payload, error.response);
        // eslint-disable-next-line prefer-promise-reject-errors
        return Promise.reject({ ...action, payload: error });
      });
  };
}

/*
 apiClient[request.method](request.url, { queryString, postData })
 .then((response) => {
 if (response.status !== STATUS_SUCCESS) {
 next({ ...action, type: FAILURE, payload: response });
 } else {
 next({ ...action, type: SUCCESS, payload: response.payload });
 }
 }) */

