Components
----------

**src-core/components/ag-grid/ag-grid.jsx**

### 1. Ag-grid Tables

Документация по модулю [https://www.ag-grid.com/](https://www.ag-grid.com/)   
Кроме одного параметра принимает еще много параметров, см. документацию.   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
fit|bool|no|false|Указывает нужно ли автоматом вписывать размер таблици в размер блока
-----
**src-core/components/aside-banner/aside-banner.jsx**

### 1. AsideBanner

Используется для прилипания банеров слева контента и справа   
Пример:   
```html   
<AsideBanner left />   
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
left|bool|no||Прилепить слева банер
right|bool|no||Прилепить ссправа банер
-----
**src-core/components/banner/banner.jsx**

### 1. Banner

Основной компонент показа банеров, пока в разработке в будущем измениться   
Пример:   
```html   
<Banner>   
  I`m a banner   
</Banner>   
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||Сюда прилетает баннер, покачто, это измениться в будущем
-----
**src-core/components/block/block.jsx**

### 1. Block

Основной компонент сайта, у него есть шапка и подвал, контент внутри.   
Использется для однообразия показа контента.   
На мобилах шапка прилипает.   
Пример:   
```html   
<Block>   
  <div header>   
     Этого элемента может и не быть,   
     шапку схватывает компонент именно по пропсу header   
  </div>   
  <div>Контент1</div>   
  ...   
  <div>Контент ...</div>   
  <div footer>   
     Этого элемента может и не быть,   
     шапку схватывает компонент именно по пропсу footer   
  </div>   
</Block>   
```   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||Тут и шапка и футер и контент Шапка и футер имеют нужные пропсы: header, footer. Контент без этих пропсов
bgGray|bool|no||Задает цвет подкладки
gray|bool|no||Задает цвет
noTabs|bool|no||Веталь знает что это
olive|bool|no||Задает цвет
disabled|bool|no|false|Используется для заглушения блока, т.е. он вроде есть но им нельзя воспользоваться и ничего не кликабельно.
overflowHidden|bool|no|false|Скрывает избыток информации, тянется по родителю или жестко задан
-----
**src-core/components/button/button.jsx**

### 1. Button

Абстракция кнопки, поправлены стили.   
Все пропсы копирует в button tag   
Пример:   
```html   
<Button onClick="() => alert('Clicked!')"> Click me!</Button>   
```   




-----
**src-core/components/cols/cols.jsx**

### 1. Cols

Компонент правильно создает колонки, перебирает чилдов   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||Каждый элемент станет колонкой
-----
**src-core/components/container/container.jsx**

### 1. Container

Обвертка для всего сайта, умеет держать ширину сайта и центрироваться.   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
body|bool|no||Будет удерживать максимальную ширину сайта
children|node|no||
style|node|no||Ручное задание стилей, deprecated
-----
**src-core/components/drop-down/drop-down.jsx**

### 1. DropDown

Выпадалка при клике   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
dontCloseOnOutsideClick|bool|no|false|Не закрывать при клике вне контейнера
group|string|no|&lt;See the source code&gt;|Зарезервировано на будущее
onOpen|func|no||Каллбек при открытии
onClose|func|no||Каллбек при закрытии
whichOpened|number|no||Internal from Redux
actionOpen|func|no||Internal from Redux
actionClose|func|no||Internal from Redux
actionDestroyed|func|no||Internal from Redux
enableOnClickOutside|func|no||Internal from react-onclickoutside
disableOnClickOutside|func|no||Internal from react-onclickoutside
-----
**src-core/components/element-geometry/element-geometry.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
update|func|no||
destroy|func|no||
children|node|no||
name|string|no||
-----
**src-core/components/flexbox/flexbox.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
row|bool|no||
column|bool|no||
wrap|bool|no||
wrap_mobile|bool|no||
spaceAroundContent|bool|no||
spaceAroundContent_mobile|bool|no||
spaceBetweenContent|bool|no||
spaceBetweenContent_mobile|bool|no||
-----
**src-core/components/head-menu/head-menu.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
height|custom|no||
body|bool|no||
children|node|no||
semiTransparent|bool|no||
-----
**src-core/components/head-menu-sticked/head-menu-sticked.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
height|custom|no||
body|bool|no||
children|node|no||
semiTransparent|bool|no||
-----
**src-core/components/icon/icon.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
type|enum|no||
title|string|no||
-----
**src-core/components/image/image.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
thumbnailBase64|string|yes||
url|string|yes||
ratio|number|yes||
alt|string|no||
sources|node|yes||
-----
**src-core/components/link/link.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
to|union|no||
query|object|no||
hash|string|no||
state|object|no||
rel|string|no||
title|string|no||
children|node|yes||
inlineBlock|bool|no||
noFollow|bool|no||
target|string|no||
newWindow|bool|no||
blank|bool|no||
onClick|func|no||
-----
**src-core/components/list/list.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
inline|bool|no||
separate|bool|no||
tile|bool|no||
overflowHidden|bool|no||
centeredVertical|bool|no||
itemBlock|bool|no||
-----
**src-core/components/live/live.jsx**

### 1. 




-----
**src-core/components/loader/loader.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
gray|bool|no||
color|bool|no||
-----
**src-core/components/media-cols/media-cols.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
column|bool|no||
-----
**src-core/components/media-vary/media-vary.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
column|bool|no||
-----
**src-core/components/oauth-request/oauth-request.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
socOauthName|enum|yes||
-----
**src-core/components/pic/pic.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
width|number|yes||
height|number|yes||
src|string|yes||
layout|string|no||
alt|string|no||
inlineBlock|string|no||
-----
**src-core/components/react-nojs-tabs/react-nojs-tabs.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|yes||
equalWidth|bool|no||
fullWidth|bool|no||
-----
**src-core/components/rss/rss.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
href|string|yes||
title|string|no||
children|node|no||
-----
**src-core/components/sensor/provider.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
provideMedia|objectOf|no||
children|node|no||
-----
**src-core/components/seo/seo.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
-----
**src-core/components/shade/shade.jsx**

### 1. 




-----
**src-core/components/slider/media-slider.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
children|node|no||
desktop|number|yes||
tablet|number|yes||
phone|number|yes||
-----
**src-core/components/slider/slider.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
itemWidth|custom|no|&lt;See the source code&gt;|
itemStickX|custom|no|&lt;See the source code&gt;|
children|node|no||
-----
**src-core/components/smart-app-banner/smart-app-banner.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
fixedBottom|bool|no||
userAgent|string|no|&lt;See the source code&gt;|
defaultLanguage|string|no|&lt;See the source code&gt;|
daysHidden|number|no|15|
daysReminder|number|no|90|
appName|string|yes||
appDeveloper|string|yes||
appIdIOS|number|no||
appIdAndroid|number|no||
priceText|string|no|&lt;See the source code&gt;|
price|string|no|&lt;See the source code&gt;|
installButtonText|string|no|&lt;See the source code&gt;|
installButtonTextIOS|string|no|&lt;See the source code&gt;|
installButtonTextAndroid|string|no|&lt;See the source code&gt;|
force|string|no|&lt;See the source code&gt;|
children|node|no||
disabled|bool|no||
-----
**src-core/components/tooltip/tooltip.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
container|any|no|&lt;See the source code&gt;|
children|node|yes||
title|string|yes||
position|enum|no|&lt;See the source code&gt;|
fixed|bool|no|true|
space|union|no|5|
-----
**src-core/containers/data-provider/data-provider.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
initialized|custom|no||
name|string|yes||
url|string|yes||
method|enum|yes|&lt;See the source code&gt;|
provider|custom|no||
actionInit|func|no||
actionLoad|func|no||
children|element|no||
-----
**src-core/containers/dict/dict.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
initialized|bool|no||
dict|any|no||
url|string|yes||
actionInit|func|no||
actionLoad|func|no||
injectPropName|string|no||
children|element|no||
-----
**src-core/containers/form/form.jsx**

### 1. Basic Form Component

   
For full data binding (receive data from loader and send to server) -   
use dataBind="name" parameters in the form elements.   
"name" for data binding MUST be unique.   
If component with "dataBinding" prop found in form child element -   
such properties are injecting to this element:   




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
initialized|bool|no||
name|string|yes||
controller|string|no||
primaryKey|string|no||
form|custom|no||
actionInit|func|yes||
actionFieldChange|func|yes||
actionLoadSchema|func|yes||
actionLoadData|func|yes||
actionInsert|func|yes||
actionSubmit|func|yes||
children|node|no||
onChange|func|no||
-----
**src-core/containers/form/input-checkbox.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
id|string|no||
name|string|no||
children|node|no||
-----
**src-core/containers/form/input-image.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
defaultImage|string|no|&lt;See the source code&gt;|
value|object|no||
style|object|no||
onEdit|func|no||
onDelete|func|no||
onRepair|func|no||
_arrayKey|number|no||
_arrayElementProps|shape|no||
### 2. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
_form|object|no||
emptyArrayBlock|element|no|&lt;See the source code&gt;|
-----
**src-core/containers/form/input-inputarray.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
addEmptyElementWhenNull|bool|no|true|
safeDelete|bool|no|true|
deletedElementStyle|object|no|&lt;See the source code&gt;|
-----
**src-core/containers/form/input-map.jsx**

### 1. 




-----
**src-core/containers/form/input-masked.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
mask|string|no||
placeholder|string|no||
### 2. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
alwaysShowMask||no|false|
maskChar||no|&lt;See the source code&gt;|
-----
**src-core/containers/form/input-phone.jsx**

### 1. 




-----
**src-core/containers/form/input-simple-combo.jsx**

### 1. 




-----
**src-core/containers/form/input.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
type|string|no||
id|string|no||
name|string|no||
placeholder|string|no||
-----
**src-core/containers/maps/layers.jsx**

### 1. 




### 2. 




-----
**src-core/containers/maps/maps.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
withBaseLayers|bool|no||
children|node|no||
width|string|no|&lt;See the source code&gt;|
height|string|no|&lt;See the source code&gt;|
-----
**src-core/containers/not-found/not-found.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
location|custom|no||
router|custom|no||
history|node|no||
-----
**src-core/containers/page-mini-site/content/about-me.jsx**

### 1. 




-----
**src-core/containers/page-mini-site/content/portfolio.jsx**

### 1. 




-----
**src-core/containers/page-mini-site/content/real-estate.jsx**

### 1. 




-----
**src-core/containers/page-mini-site/content/slider-main.jsx**

### 1. 




-----
**src-core/containers/page-mini-site/menu/menu-mini-site.jsx**

### 1. 




-----
**src-core/containers/page-mini-site/page-mini-site.jsx**

### 1. 




-----
**src-core/containers/page-not-found_v1/not-found.jsx**

### 1. 




Property | Type | Required | Default value | Description
:--- | :--- | :--- | :--- | :---
location|custom|no||
router|custom|no||
history|node|no||
-----
**src-core/containers/sandbox/sandbox1.jsx**

### 1. 




-----

<sub>This document was generated by the <a href="https://github.com/marborkowski/react-doc-generator" target="_blank">**React DOC Generator v1.2.5**</a>.</sub>
