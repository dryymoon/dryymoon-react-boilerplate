/**
 * Created by dryymoon on 19.04.2018.
 */
import React from 'react';
import { SensorProvider } from 'components/sensor';
import { Provider as ReduxProvider } from 'react-redux';
import { RouterContext } from 'react-router'; // Router
import { Provider as ReactUniqueIdProvider } from '../helpers/react/ReactUniqueIds';
import { Provider as ReactNotifyProvider } from '../helpers/react/ReactNotifyProvider';
import ReactApiProvider from '../helpers/react/ReactApiProvider';
import { RuntimeCssSelectors } from '../helpers/styles/CollectClasses';
import { ReactCookiesProvider } from '../helpers/react/ReactCookiesProvider';
import OriginProvider from '../helpers/router/origin-provider';

// eslint-disable-next-line
export default function ({ renderProps, api, store, cookies, serverSideRequest, serverSideResponse, media, defaultMedia, pushSelector }) {
  return (
    <ReactCookiesProvider cookies={cookies}>
      <SensorProvider provideMedia={media} defaultMedia={defaultMedia}>
        <OriginProvider serverReq={serverSideRequest}>
          <ReactUniqueIdProvider>
            <ReactApiProvider api={api}>
              <RuntimeCssSelectors pushSelector={pushSelector}>
                <ReduxProvider store={store}>
                  <ReactNotifyProvider>
                    <RouterContext {...renderProps} />
                  </ReactNotifyProvider>
                </ReduxProvider>
              </RuntimeCssSelectors>
            </ReactApiProvider>
          </ReactUniqueIdProvider>
        </OriginProvider>
      </SensorProvider>
    </ReactCookiesProvider>
  );
}
