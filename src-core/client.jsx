/* eslint-disable import/first */
import 'babel-polyfill'; // eslint-disable-line import/no-extraneous-dependencies
import './helpers/browser-polyfills'; // eslint-disable-line
import ReactDOM from 'react-dom';
import { browserHistory, createMemoryHistory, hashHistory, match } from 'react-router'; // Router
import urlLib from 'url';
import CookiesLib from 'cookie-cutter';
import HistoryTracker from 'back-forward-history';
import { syncHistoryWithStore } from 'react-router-redux';
import { clear as clearPopups } from 'components/pop-up'; //  Provider as PopUpProvider,
import { removeAll as clearNotifications } from 'components/notification'; // Notification,
import { preloadExecutor } from './api/data-preload';
import reactRoot from './boot-client/client-react-root';
// import './helpers/browser-polyfills';
import configureStore from './helpers/redux/configure-store';
import ApiClient from './api/api-client';
import ravenInitializer from './boot-client/raven';
import reactSsrChecker from './boot-client/react-ssr-checker';
import './theme/theme.default.scss';
import getRoutes from './routes';
import { apiConfig, media, routes as siteRoutes, } from '../src-sites/__SRC_SITE_FOLDER__/boot-client';

const siteConfig = require('../src-sites/__SRC_SITE_FOLDER__/boot-client');

if (siteConfig.sentryConfig) ravenInitializer(siteConfig.sentryConfig);

const api = new ApiClient(apiConfig.url, apiConfig.middlewares || []);

if (window.__apiPreload) {
  const preloadedData = JSON.parse(Buffer.from(window.__apiPreload, 'base64').toString('utf8'));
  api.fillCache(preloadedData);
  deleteElById('__apiPreload');
}

let historyLib = browserHistory;
if (siteConfig.useHashHistory) historyLib = hashHistory;
if (siteConfig.useMemoryHistory) historyLib = createMemoryHistory(window.location);
if (__APP__) historyLib = createMemoryHistory(window.location);

let reduxStoreFromServer;
if (window.__reduxStore) {
  reduxStoreFromServer = JSON.parse(Buffer.from(window.__reduxStore, 'base64').toString('utf8'));
  deleteElById('__reduxStore');
}
const store = configureStore(historyLib, api, reduxStoreFromServer);

const history = syncHistoryWithStore(historyLib, store);

let lastLocation = history.getCurrentLocation().pathname;
history.listen(({ pathname }) => {
  if (lastLocation && (lastLocation !== pathname)) {
    clearPopups();
    store.dispatch(clearNotifications());
  }
  lastLocation = pathname;
});

HistoryTracker.listenTo(history);
HistoryTracker.onHistoryEvent(history.getCurrentLocation());

const dest = document.getElementById(__ROOT_DOM_POINT__);

const cookies = new CookiesLib(document);

const routes = getRoutes(siteRoutes, { api, store, history, cookies });

(function runRouteMatching() { // self executed
  match({ history, routes }, (err, redirect, renderProps) => {
    if (err) throw new Error(`CLIENT FIRST RENDER GOT AN ERROR: ${JSON.stringify(err)}`);

    if (redirect) {
      // TODO Implement client redirect
      // С сервера когда такой запрос приходит - 301 редирект,
      // На клиенте тупо меняем урл и подменяем а не пушим историю, и дергаем гуглоаналитику.
      let action;
      if (redirect.action === 'REPLACE') action = history.replace;
      if (redirect.action === 'PUSH') action = history.push;
      if (action) {
        action(urlLib.format(redirect));
        return runRouteMatching();
      }

      throw new Error('CLIENT REDIRECT NOT IMPLEMENTED');
    }

    if (renderProps) {
      return api.awaitInit()
        .then(() => preloadExecutor({ renderProps, apiInstance: api }))
        .catch(() => null)
        .then(() => {
          const component = reactRoot({ renderProps, api, routes, store, media, history, cookies });
          ReactDOM.render(component, dest);
          api.enableExpireCheck();
          reactSsrChecker(dest);
        })
        .catch((renderError) => {
          // eslint-disable-next-line
          console.error('Init application failed.', renderError);

          Raven.captureException('Init application failed.', {
            level: 'error',
            extra: { detail: renderError }
          });

          // eslint-disable-next-line
          console.error(renderError.stack);
          // eslint-disable-next-line
          // window.alert(`Init application failed. \nDetails: \n${JSON.stringify(renderError)} `);
          // setTimeout(() => window.location.reload(), 10000);
        });
    }

    if (!__DEVELOPMENT__) {
      Raven.captureException('Client normal first render: in most cases unreachable code', {
        level: 'error',
        extra: { err, redirect, renderProps }
      });
    } else {
      // eslint-disable-next-line
      console.error('Client normal first render: in most cases unreachable code: ',
        { err, redirect, renderProps }
      );
    }
  });
}());

// watchExternalLinks({ history, routes });

function deleteElById(id) {
  const element = document.getElementById(id);
  if (element) element.outerHTML = '';
}
